/****************************************************************************
** Form implementation generated from reading ui file 'FecDialog.ui'
**
** Created by: The User Interface Compiler ($Id: qt/main.cpp   3.3.6   edited Aug 31 2005 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include ".ui/FecDialog.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qtabwidget.h>
#include <qwidget.h>
#include <qcombobox.h>
#include <qcheckbox.h>
#include <qradiobutton.h>
#include <qlistbox.h>
#include <qbuttongroup.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a FecDialog as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
FecDialog::FecDialog( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
	setName( "FecDialog" );

    QWidget* privateLayoutWidget = new QWidget( this, "layout77" );
    privateLayoutWidget->setGeometry( QRect( 980, 30, 222, 840 ) );
    layout77 = new QVBoxLayout( privateLayoutWidget, 11, 6, "layout77"); 

    Command = new QGroupBox( privateLayoutWidget, "Command" );
    QFont Command_font(  Command->font() );
    Command->setFont( Command_font ); 

    QWidget* privateLayoutWidget_2 = new QWidget( Command, "layout76" );
    privateLayoutWidget_2->setGeometry( QRect( 10, 20, 200, 550 ) );
    layout76 = new QVBoxLayout( privateLayoutWidget_2, 11, 6, "layout76"); 

    fecResetPlx = new QPushButton( privateLayoutWidget_2, "fecResetPlx" );
    layout76->addWidget( fecResetPlx );

    vmeCrateReset = new QPushButton( privateLayoutWidget_2, "vmeCrateReset" );
    vmeCrateReset->setEnabled( FALSE );
    layout76->addWidget( vmeCrateReset );

    fecResetFec = new QPushButton( privateLayoutWidget_2, "fecResetFec" );
    layout76->addWidget( fecResetFec );

    fecResetFSM = new QPushButton( privateLayoutWidget_2, "fecResetFSM" );
    layout76->addWidget( fecResetFSM );

    fecDisableReceive = new QPushButton( privateLayoutWidget_2, "fecDisableReceive" );
    layout76->addWidget( fecDisableReceive );

    fecRelease = new QPushButton( privateLayoutWidget_2, "fecRelease" );
    layout76->addWidget( fecRelease );

    fecClearErrors = new QPushButton( privateLayoutWidget_2, "fecClearErrors" );
    layout76->addWidget( fecClearErrors );

    scanAll = new QPushButton( privateLayoutWidget_2, "scanAll" );
    layout76->addWidget( scanAll );

    fecDisableIRQ = new QPushButton( privateLayoutWidget_2, "fecDisableIRQ" );
    layout76->addWidget( fecDisableIRQ );

    layout205 = new QHBoxLayout( 0, 0, 6, "layout205"); 

    TextLabel23 = new QLabel( privateLayoutWidget_2, "TextLabel23" );
    layout205->addWidget( TextLabel23 );

    fecReadCR0Bis = new QPushButton( privateLayoutWidget_2, "fecReadCR0Bis" );
    fecReadCR0Bis->setMinimumSize( QSize( 20, 32 ) );
    fecReadCR0Bis->setMaximumSize( QSize( 20, 32 ) );
    layout205->addWidget( fecReadCR0Bis );

    fecWriteCR0Bis = new QPushButton( privateLayoutWidget_2, "fecWriteCR0Bis" );
    fecWriteCR0Bis->setMinimumSize( QSize( 20, 32 ) );
    fecWriteCR0Bis->setMaximumSize( QSize( 20, 32 ) );
    layout205->addWidget( fecWriteCR0Bis );

    fecCR0Bis = new QLineEdit( privateLayoutWidget_2, "fecCR0Bis" );
    fecCR0Bis->setAlignment( int( QLineEdit::AlignHCenter ) );
    layout205->addWidget( fecCR0Bis );
    layout76->addLayout( layout205 );

    layout290 = new QGridLayout( 0, 1, 1, 0, 6, "layout290"); 

    fecReadSR0Bis = new QPushButton( privateLayoutWidget_2, "fecReadSR0Bis" );

    layout290->addWidget( fecReadSR0Bis, 0, 0 );

    fecSR1Bis = new QLineEdit( privateLayoutWidget_2, "fecSR1Bis" );
    fecSR1Bis->setAlignment( int( QLineEdit::AlignHCenter ) );

    layout290->addWidget( fecSR1Bis, 1, 1 );

    fecReadSR1Bis = new QPushButton( privateLayoutWidget_2, "fecReadSR1Bis" );

    layout290->addWidget( fecReadSR1Bis, 1, 0 );

    fecSR0Bis = new QLineEdit( privateLayoutWidget_2, "fecSR0Bis" );
    fecSR0Bis->setAlignment( int( QLineEdit::AlignHCenter ) );

    layout290->addWidget( fecSR0Bis, 0, 1 );
    layout76->addLayout( layout290 );

    layout292 = new QGridLayout( 0, 1, 1, 0, 6, "layout292"); 

    fecReadFifoRetWordButton = new QPushButton( privateLayoutWidget_2, "fecReadFifoRetWordButton" );
    fecReadFifoRetWordButton->setMinimumSize( QSize( 20, 32 ) );
    fecReadFifoRetWordButton->setMaximumSize( QSize( 20, 32 ) );

    layout292->addWidget( fecReadFifoRetWordButton, 2, 1 );

    fecClearFifoTraButton = new QPushButton( privateLayoutWidget_2, "fecClearFifoTraButton" );
    fecClearFifoTraButton->setEnabled( TRUE );
    fecClearFifoTraButton->setMinimumSize( QSize( 20, 32 ) );
    fecClearFifoTraButton->setMaximumSize( QSize( 20, 32 ) );

    layout292->addWidget( fecClearFifoTraButton, 1, 3 );

    fecWriteFifoRetWordButton = new QPushButton( privateLayoutWidget_2, "fecWriteFifoRetWordButton" );
    fecWriteFifoRetWordButton->setEnabled( TRUE );
    fecWriteFifoRetWordButton->setMinimumSize( QSize( 20, 32 ) );
    fecWriteFifoRetWordButton->setMaximumSize( QSize( 20, 32 ) );

    layout292->addWidget( fecWriteFifoRetWordButton, 2, 2 );

    fecClearFifoRetButton = new QPushButton( privateLayoutWidget_2, "fecClearFifoRetButton" );
    fecClearFifoRetButton->setEnabled( TRUE );
    fecClearFifoRetButton->setMinimumSize( QSize( 20, 32 ) );
    fecClearFifoRetButton->setMaximumSize( QSize( 20, 32 ) );

    layout292->addWidget( fecClearFifoRetButton, 2, 3 );

    TextLabel23_2 = new QLabel( privateLayoutWidget_2, "TextLabel23_2" );

    layout292->addWidget( TextLabel23_2, 0, 0 );

    fecWriteFifoTraWordButton = new QPushButton( privateLayoutWidget_2, "fecWriteFifoTraWordButton" );
    fecWriteFifoTraWordButton->setEnabled( TRUE );
    fecWriteFifoTraWordButton->setMinimumSize( QSize( 20, 32 ) );
    fecWriteFifoTraWordButton->setMaximumSize( QSize( 20, 32 ) );

    layout292->addWidget( fecWriteFifoTraWordButton, 1, 2 );

    fecReadFifoRecWordButton = new QPushButton( privateLayoutWidget_2, "fecReadFifoRecWordButton" );
    fecReadFifoRecWordButton->setMinimumSize( QSize( 20, 32 ) );
    fecReadFifoRecWordButton->setMaximumSize( QSize( 20, 32 ) );

    layout292->addWidget( fecReadFifoRecWordButton, 0, 1 );

    fecReadFifoTraWordButton = new QPushButton( privateLayoutWidget_2, "fecReadFifoTraWordButton" );
    fecReadFifoTraWordButton->setMinimumSize( QSize( 20, 32 ) );
    fecReadFifoTraWordButton->setMaximumSize( QSize( 20, 32 ) );

    layout292->addWidget( fecReadFifoTraWordButton, 1, 1 );

    fecFifoRecWord = new QLineEdit( privateLayoutWidget_2, "fecFifoRecWord" );
    fecFifoRecWord->setAlignment( int( QLineEdit::AlignHCenter ) );

    layout292->addWidget( fecFifoRecWord, 0, 4 );

    TextLabel23_2_2 = new QLabel( privateLayoutWidget_2, "TextLabel23_2_2" );

    layout292->addWidget( TextLabel23_2_2, 1, 0 );

    fecFifoRetWord = new QLineEdit( privateLayoutWidget_2, "fecFifoRetWord" );
    fecFifoRetWord->setAlignment( int( QLineEdit::AlignHCenter ) );

    layout292->addWidget( fecFifoRetWord, 2, 4 );

    fecClearFifoRecButton = new QPushButton( privateLayoutWidget_2, "fecClearFifoRecButton" );
    fecClearFifoRecButton->setMinimumSize( QSize( 20, 32 ) );
    fecClearFifoRecButton->setMaximumSize( QSize( 20, 32 ) );

    layout292->addWidget( fecClearFifoRecButton, 0, 3 );

    fecFifoTraWord = new QLineEdit( privateLayoutWidget_2, "fecFifoTraWord" );
    fecFifoTraWord->setAlignment( int( QLineEdit::AlignHCenter ) );

    layout292->addWidget( fecFifoTraWord, 1, 4 );

    TextLabel23_2_2_2 = new QLabel( privateLayoutWidget_2, "TextLabel23_2_2_2" );

    layout292->addWidget( TextLabel23_2_2_2, 2, 0 );

    fecWriteFifoRecWordButton = new QPushButton( privateLayoutWidget_2, "fecWriteFifoRecWordButton" );
    fecWriteFifoRecWordButton->setEnabled( TRUE );
    fecWriteFifoRecWordButton->setMinimumSize( QSize( 20, 32 ) );
    fecWriteFifoRecWordButton->setMaximumSize( QSize( 20, 32 ) );

    layout292->addWidget( fecWriteFifoRecWordButton, 0, 2 );
    layout76->addLayout( layout292 );
    layout77->addWidget( Command );
    spacer92_2_2 = new QSpacerItem( 220, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout77->addItem( spacer92_2_2 );

    groupBox175 = new QGroupBox( privateLayoutWidget, "groupBox175" );
    groupBox175->setColumnLayout(0, Qt::Vertical );
    groupBox175->layout()->setSpacing( 6 );
    groupBox175->layout()->setMargin( 11 );
    groupBox175Layout = new QGridLayout( groupBox175->layout() );
    groupBox175Layout->setAlignment( Qt::AlignTop );

    textLabel2 = new QLabel( groupBox175, "textLabel2" );

    groupBox175Layout->addWidget( textLabel2, 0, 0 );

    fecSR0Thread = new QLineEdit( groupBox175, "fecSR0Thread" );
    fecSR0Thread->setAlignment( int( QLineEdit::AlignHCenter ) );

    groupBox175Layout->addMultiCellWidget( fecSR0Thread, 0, 0, 1, 3 );

    StartThreadButton = new QPushButton( groupBox175, "StartThreadButton" );

    groupBox175Layout->addMultiCellWidget( StartThreadButton, 2, 2, 0, 3 );

    LinkInitThreadLinkLed2 = new QLabel( groupBox175, "LinkInitThreadLinkLed2" );
    LinkInitThreadLinkLed2->setMinimumSize( QSize( 40, 40 ) );
    LinkInitThreadLinkLed2->setScaledContents( TRUE );

    groupBox175Layout->addMultiCellWidget( LinkInitThreadLinkLed2, 1, 1, 0, 1 );

    textLabel3 = new QLabel( groupBox175, "textLabel3" );

    groupBox175Layout->addWidget( textLabel3, 1, 2 );
    spacer34 = new QSpacerItem( 41, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    groupBox175Layout->addItem( spacer34, 1, 3 );
    layout77->addWidget( groupBox175 );
    spacer92_2 = new QSpacerItem( 220, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout77->addItem( spacer92_2 );

    layout206 = new QVBoxLayout( 0, 0, 6, "layout206"); 

    about = new QPushButton( privateLayoutWidget, "about" );
    QFont about_font(  about->font() );
    about->setFont( about_font ); 
    layout206->addWidget( about );

    quitB = new QPushButton( privateLayoutWidget, "quitB" );
    QFont quitB_font(  quitB->font() );
    quitB->setFont( quitB_font ); 
    layout206->addWidget( quitB );
    layout77->addLayout( layout206 );

    FEC = new QTabWidget( this, "FEC" );
    FEC->setGeometry( QRect( 11, 11, 965, 856 ) );
    FEC->setMaximumSize( QSize( 1000, 32767 ) );
    QFont FEC_font(  FEC->font() );
    FEC->setFont( FEC_font ); 

    tab = new QWidget( FEC, "tab" );

    QWidget* privateLayoutWidget_3 = new QWidget( tab, "layout157" );
    privateLayoutWidget_3->setGeometry( QRect( 12, 17, 829, 708 ) );
    layout157 = new QGridLayout( privateLayoutWidget_3, 1, 1, 11, 6, "layout157"); 

    layout153 = new QHBoxLayout( 0, 0, 6, "layout153"); 

    fecScanForFECs = new QPushButton( privateLayoutWidget_3, "fecScanForFECs" );
    layout153->addWidget( fecScanForFECs );

    TextLabel2 = new QLabel( privateLayoutWidget_3, "TextLabel2" );
    TextLabel2->setAlignment( int( QLabel::AlignCenter ) );
    layout153->addWidget( TextLabel2 );

    fecSlots = new QComboBox( FALSE, privateLayoutWidget_3, "fecSlots" );
    layout153->addWidget( fecSlots );
    spacer40 = new QSpacerItem( 281, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout153->addItem( spacer40 );

    fecReadAll = new QPushButton( privateLayoutWidget_3, "fecReadAll" );
    layout153->addWidget( fecReadAll );

    layout157->addMultiCellLayout( layout153, 0, 0, 0, 2 );

    DeviceDriver = new QGroupBox( privateLayoutWidget_3, "DeviceDriver" );
    DeviceDriver->setColumnLayout(0, Qt::Vertical );
    DeviceDriver->layout()->setSpacing( 6 );
    DeviceDriver->layout()->setMargin( 11 );
    DeviceDriverLayout = new QVBoxLayout( DeviceDriver->layout() );
    DeviceDriverLayout->setAlignment( Qt::AlignTop );

    Layout66 = new QHBoxLayout( 0, 0, 6, "Layout66"); 

    fecEnableErrorCounter = new QCheckBox( DeviceDriver, "fecEnableErrorCounter" );
    fecEnableErrorCounter->setChecked( FALSE );
    Layout66->addWidget( fecEnableErrorCounter );

    fecResetCounter = new QPushButton( DeviceDriver, "fecResetCounter" );
    Layout66->addWidget( fecResetCounter );
    DeviceDriverLayout->addLayout( Layout66 );

    fecIRQEnableButton = new QCheckBox( DeviceDriver, "fecIRQEnableButton" );
    DeviceDriverLayout->addWidget( fecIRQEnableButton );

    Layout63 = new QGridLayout( 0, 1, 1, 0, 6, "Layout63"); 

    fecWarningDevice = new QLineEdit( DeviceDriver, "fecWarningDevice" );
    fecWarningDevice->setAlignment( int( QLineEdit::AlignHCenter ) );
    fecWarningDevice->setReadOnly( TRUE );

    Layout63->addWidget( fecWarningDevice, 4, 1 );

    fecNbPlxReset = new QLineEdit( DeviceDriver, "fecNbPlxReset" );
    fecNbPlxReset->setAlignment( int( QLineEdit::AlignHCenter ) );
    fecNbPlxReset->setReadOnly( TRUE );

    Layout63->addWidget( fecNbPlxReset, 5, 1 );

    fecBadTransaction = new QLineEdit( DeviceDriver, "fecBadTransaction" );
    fecBadTransaction->setAlignment( int( QLineEdit::AlignHCenter ) );
    fecBadTransaction->setReadOnly( TRUE );

    Layout63->addWidget( fecBadTransaction, 3, 1 );

    TextLabel16 = new QLabel( DeviceDriver, "TextLabel16" );

    Layout63->addWidget( TextLabel16, 6, 0 );

    TextLabel12 = new QLabel( DeviceDriver, "TextLabel12" );

    Layout63->addWidget( TextLabel12, 2, 0 );

    fecDDStatus = new QLineEdit( DeviceDriver, "fecDDStatus" );
    fecDDStatus->setAlignment( int( QLineEdit::AlignHCenter ) );
    fecDDStatus->setReadOnly( TRUE );

    Layout63->addWidget( fecDDStatus, 0, 1 );

    TextLabel15 = new QLabel( DeviceDriver, "TextLabel15" );

    Layout63->addWidget( TextLabel15, 5, 0 );

    TextLabel13 = new QLabel( DeviceDriver, "TextLabel13" );

    Layout63->addWidget( TextLabel13, 3, 0 );

    TextLabel11 = new QLabel( DeviceDriver, "TextLabel11" );

    Layout63->addWidget( TextLabel11, 1, 0 );

    TextLabel14 = new QLabel( DeviceDriver, "TextLabel14" );

    Layout63->addWidget( TextLabel14, 4, 0 );

    fecLongFrame = new QLineEdit( DeviceDriver, "fecLongFrame" );
    fecLongFrame->setAlignment( int( QLineEdit::AlignHCenter ) );
    fecLongFrame->setReadOnly( TRUE );

    Layout63->addWidget( fecLongFrame, 1, 1 );

    TextLabel10 = new QLabel( DeviceDriver, "TextLabel10" );

    Layout63->addWidget( TextLabel10, 0, 0 );

    fecNbFecReset = new QLineEdit( DeviceDriver, "fecNbFecReset" );
    fecNbFecReset->setAlignment( int( QLineEdit::AlignHCenter ) );
    fecNbFecReset->setReadOnly( TRUE );

    Layout63->addWidget( fecNbFecReset, 6, 1 );

    fecShortFrame = new QLineEdit( DeviceDriver, "fecShortFrame" );
    fecShortFrame->setAlignment( int( QLineEdit::AlignHCenter ) );
    fecShortFrame->setReadOnly( TRUE );

    Layout63->addWidget( fecShortFrame, 2, 1 );
    DeviceDriverLayout->addLayout( Layout63 );
    spacer61 = new QSpacerItem( 20, 16, QSizePolicy::Minimum, QSizePolicy::Expanding );
    DeviceDriverLayout->addItem( spacer61 );

    fecReadDeviceDriver = new QPushButton( DeviceDriver, "fecReadDeviceDriver" );
    DeviceDriverLayout->addWidget( fecReadDeviceDriver );

    layout157->addWidget( DeviceDriver, 2, 2 );

    GroupBox2_3_2 = new QGroupBox( privateLayoutWidget_3, "GroupBox2_3_2" );
    GroupBox2_3_2->setColumnLayout(0, Qt::Vertical );
    GroupBox2_3_2->layout()->setSpacing( 6 );
    GroupBox2_3_2->layout()->setMargin( 11 );
    GroupBox2_3_2Layout = new QVBoxLayout( GroupBox2_3_2->layout() );
    GroupBox2_3_2Layout->setAlignment( Qt::AlignTop );

    fecSR1InputLine = new QLineEdit( GroupBox2_3_2, "fecSR1InputLine" );
    fecSR1InputLine->setAlignment( int( QLineEdit::AlignHCenter ) );
    fecSR1InputLine->setReadOnly( TRUE );
    GroupBox2_3_2Layout->addWidget( fecSR1InputLine );

    fecSR1IllData = new QCheckBox( GroupBox2_3_2, "fecSR1IllData" );
    GroupBox2_3_2Layout->addWidget( fecSR1IllData );

    fecCR1IllSeq = new QCheckBox( GroupBox2_3_2, "fecCR1IllSeq" );
    GroupBox2_3_2Layout->addWidget( fecCR1IllSeq );

    fecSR1CRCError = new QCheckBox( GroupBox2_3_2, "fecSR1CRCError" );
    GroupBox2_3_2Layout->addWidget( fecSR1CRCError );

    fecSR1DataCopied = new QCheckBox( GroupBox2_3_2, "fecSR1DataCopied" );
    GroupBox2_3_2Layout->addWidget( fecSR1DataCopied );

    fecSR1AddrSeen = new QCheckBox( GroupBox2_3_2, "fecSR1AddrSeen" );
    GroupBox2_3_2Layout->addWidget( fecSR1AddrSeen );

    fecSR1Error = new QCheckBox( GroupBox2_3_2, "fecSR1Error" );
    GroupBox2_3_2Layout->addWidget( fecSR1Error );

    fecSR1Timeout = new QCheckBox( GroupBox2_3_2, "fecSR1Timeout" );
    GroupBox2_3_2Layout->addWidget( fecSR1Timeout );

    fecSR1ClockError = new QCheckBox( GroupBox2_3_2, "fecSR1ClockError" );
    GroupBox2_3_2Layout->addWidget( fecSR1ClockError );
    spacer41 = new QSpacerItem( 20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
    GroupBox2_3_2Layout->addItem( spacer41 );

    fecSR1Read = new QPushButton( GroupBox2_3_2, "fecSR1Read" );
    GroupBox2_3_2Layout->addWidget( fecSR1Read );

    layout157->addWidget( GroupBox2_3_2, 2, 1 );

    fecGroupCR1 = new QGroupBox( privateLayoutWidget_3, "fecGroupCR1" );
    fecGroupCR1->setColumnLayout(0, Qt::Vertical );
    fecGroupCR1->layout()->setSpacing( 6 );
    fecGroupCR1->layout()->setMargin( 11 );
    fecGroupCR1Layout = new QGridLayout( fecGroupCR1->layout() );
    fecGroupCR1Layout->setAlignment( Qt::AlignTop );

    Layout25 = new QGridLayout( 0, 1, 1, 0, 6, "Layout25"); 

    fecCR1ClearErrors = new QCheckBox( fecGroupCR1, "fecCR1ClearErrors" );

    Layout25->addMultiCellWidget( fecCR1ClearErrors, 2, 2, 0, 1 );

    fecCR1InputLine = new QLineEdit( fecGroupCR1, "fecCR1InputLine" );
    fecCR1InputLine->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout25->addWidget( fecCR1InputLine, 0, 0 );

    fecCR1Release = new QCheckBox( fecGroupCR1, "fecCR1Release" );

    Layout25->addMultiCellWidget( fecCR1Release, 3, 3, 0, 1 );

    fecCR1ClearIrq = new QCheckBox( fecGroupCR1, "fecCR1ClearIrq" );

    Layout25->addMultiCellWidget( fecCR1ClearIrq, 1, 1, 0, 1 );

    fecCR1WriteCR1 = new QPushButton( fecGroupCR1, "fecCR1WriteCR1" );

    Layout25->addWidget( fecCR1WriteCR1, 0, 1 );

    fecGroupCR1Layout->addMultiCellLayout( Layout25, 0, 0, 0, 1 );

    fecCR1Read = new QPushButton( fecGroupCR1, "fecCR1Read" );

    fecGroupCR1Layout->addWidget( fecCR1Read, 2, 0 );

    fecCR1WriteBit = new QPushButton( fecGroupCR1, "fecCR1WriteBit" );

    fecGroupCR1Layout->addWidget( fecCR1WriteBit, 2, 1 );
    spacer42 = new QSpacerItem( 20, 130, QSizePolicy::Minimum, QSizePolicy::Expanding );
    fecGroupCR1Layout->addItem( spacer42, 1, 0 );

    layout157->addWidget( fecGroupCR1, 1, 1 );

    fecGroupCR0 = new QGroupBox( privateLayoutWidget_3, "fecGroupCR0" );

    QWidget* privateLayoutWidget_4 = new QWidget( fecGroupCR0, "layout75" );
    privateLayoutWidget_4->setGeometry( QRect( 11, 22, 242, 284 ) );
    layout75 = new QVBoxLayout( privateLayoutWidget_4, 11, 6, "layout75"); 

    layout76_2 = new QHBoxLayout( 0, 0, 6, "layout76_2"); 

    fecCR0InputLine = new QLineEdit( privateLayoutWidget_4, "fecCR0InputLine" );
    fecCR0InputLine->setAlignment( int( QLineEdit::AlignHCenter ) );
    layout76_2->addWidget( fecCR0InputLine );

    fecCR0writeCR0 = new QPushButton( privateLayoutWidget_4, "fecCR0writeCR0" );
    layout76_2->addWidget( fecCR0writeCR0 );
    layout75->addLayout( layout76_2 );
    spacer29 = new QSpacerItem( 20, 16, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout75->addItem( spacer29 );

    layout74 = new QGridLayout( 0, 1, 1, 0, 6, "layout74"); 

    fecCR0InvertPolarity = new QCheckBox( privateLayoutWidget_4, "fecCR0InvertPolarity" );
    QFont fecCR0InvertPolarity_font(  fecCR0InvertPolarity->font() );
    fecCR0InvertPolarity->setFont( fecCR0InvertPolarity_font ); 

    layout74->addWidget( fecCR0InvertPolarity, 3, 1 );

    fecCR0ResetTTCRx = new QCheckBox( privateLayoutWidget_4, "fecCR0ResetTTCRx" );
    QFont fecCR0ResetTTCRx_font(  fecCR0ResetTTCRx->font() );
    fecCR0ResetTTCRx->setFont( fecCR0ResetTTCRx_font ); 

    layout74->addWidget( fecCR0ResetTTCRx, 3, 0 );

    fecCR0SelSerIn = new QCheckBox( privateLayoutWidget_4, "fecCR0SelSerIn" );
    QFont fecCR0SelSerIn_font(  fecCR0SelSerIn->font() );
    fecCR0SelSerIn->setFont( fecCR0SelSerIn_font ); 

    layout74->addWidget( fecCR0SelSerIn, 2, 1 );

    fecCR0InternalClock = new QCheckBox( privateLayoutWidget_4, "fecCR0InternalClock" );
    QFont fecCR0InternalClock_font(  fecCR0InternalClock->font() );
    fecCR0InternalClock->setFont( fecCR0InternalClock_font ); 

    layout74->addWidget( fecCR0InternalClock, 1, 0 );

    fecCR0enableFEC = new QCheckBox( privateLayoutWidget_4, "fecCR0enableFEC" );
    QFont fecCR0enableFEC_font(  fecCR0enableFEC->font() );
    fecCR0enableFEC->setFont( fecCR0enableFEC_font ); 

    layout74->addWidget( fecCR0enableFEC, 0, 0 );

    fecCR0ResetFSM = new QCheckBox( privateLayoutWidget_4, "fecCR0ResetFSM" );
    QFont fecCR0ResetFSM_font(  fecCR0ResetFSM->font() );
    fecCR0ResetFSM->setFont( fecCR0ResetFSM_font ); 

    layout74->addWidget( fecCR0ResetFSM, 4, 1 );

    fecCR0Send = new QCheckBox( privateLayoutWidget_4, "fecCR0Send" );
    QFont fecCR0Send_font(  fecCR0Send->font() );
    fecCR0Send->setFont( fecCR0Send_font ); 

    layout74->addWidget( fecCR0Send, 0, 1 );

    fecCR0DisableRec = new QCheckBox( privateLayoutWidget_4, "fecCR0DisableRec" );
    QFont fecCR0DisableRec_font(  fecCR0DisableRec->font() );
    fecCR0DisableRec->setFont( fecCR0DisableRec_font ); 

    layout74->addWidget( fecCR0DisableRec, 4, 0 );

    fecCR0SelSerOut = new QCheckBox( privateLayoutWidget_4, "fecCR0SelSerOut" );
    QFont fecCR0SelSerOut_font(  fecCR0SelSerOut->font() );
    fecCR0SelSerOut->setFont( fecCR0SelSerOut_font ); 

    layout74->addWidget( fecCR0SelSerOut, 2, 0 );

    fecCR0ResetB = new QCheckBox( privateLayoutWidget_4, "fecCR0ResetB" );

    layout74->addWidget( fecCR0ResetB, 5, 0 );

    fecCR0ResetOut = new QCheckBox( privateLayoutWidget_4, "fecCR0ResetOut" );

    layout74->addWidget( fecCR0ResetOut, 5, 1 );
    layout75->addLayout( layout74 );
    spacer29_2 = new QSpacerItem( 20, 16, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout75->addItem( spacer29_2 );

    layout77_2 = new QHBoxLayout( 0, 0, 6, "layout77_2"); 

    fecCR0Read = new QPushButton( privateLayoutWidget_4, "fecCR0Read" );
    layout77_2->addWidget( fecCR0Read );

    fecCR0WriteBit = new QPushButton( privateLayoutWidget_4, "fecCR0WriteBit" );
    layout77_2->addWidget( fecCR0WriteBit );
    layout75->addLayout( layout77_2 );

    layout157->addWidget( fecGroupCR0, 1, 0 );

    GroupBox2_3 = new QGroupBox( privateLayoutWidget_3, "GroupBox2_3" );
    GroupBox2_3->setColumnLayout(0, Qt::Vertical );
    GroupBox2_3->layout()->setSpacing( 6 );
    GroupBox2_3->layout()->setMargin( 11 );
    GroupBox2_3Layout = new QGridLayout( GroupBox2_3->layout() );
    GroupBox2_3Layout->setAlignment( Qt::AlignTop );

    fecSR0Read = new QPushButton( GroupBox2_3, "fecSR0Read" );

    GroupBox2_3Layout->addMultiCellWidget( fecSR0Read, 8, 8, 0, 2 );
    spacer43 = new QSpacerItem( 20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
    GroupBox2_3Layout->addItem( spacer43, 7, 1 );

    fecSR0InputLine = new QLineEdit( GroupBox2_3, "fecSR0InputLine" );
    fecSR0InputLine->setAlignment( int( QLineEdit::AlignHCenter ) );
    fecSR0InputLine->setReadOnly( TRUE );

    GroupBox2_3Layout->addMultiCellWidget( fecSR0InputLine, 0, 0, 0, 2 );

    fecSR0TraRunning = new QCheckBox( GroupBox2_3, "fecSR0TraRunning" );

    GroupBox2_3Layout->addMultiCellWidget( fecSR0TraRunning, 1, 1, 0, 1 );

    fecSR0TraFull = new QCheckBox( GroupBox2_3, "fecSR0TraFull" );

    GroupBox2_3Layout->addWidget( fecSR0TraFull, 2, 0 );

    fecSR0RecFull = new QCheckBox( GroupBox2_3, "fecSR0RecFull" );

    GroupBox2_3Layout->addMultiCellWidget( fecSR0RecFull, 3, 3, 0, 1 );

    fecSR0RetFull = new QCheckBox( GroupBox2_3, "fecSR0RetFull" );

    GroupBox2_3Layout->addMultiCellWidget( fecSR0RetFull, 4, 4, 0, 1 );

    fecSR0LinkInit = new QCheckBox( GroupBox2_3, "fecSR0LinkInit" );

    GroupBox2_3Layout->addWidget( fecSR0LinkInit, 5, 0 );

    fecSR0PendingIrq = new QCheckBox( GroupBox2_3, "fecSR0PendingIrq" );

    GroupBox2_3Layout->addWidget( fecSR0PendingIrq, 6, 0 );

    fecSR0RetEmpty = new QCheckBox( GroupBox2_3, "fecSR0RetEmpty" );
    fecSR0RetEmpty->setEnabled( TRUE );
    fecSR0RetEmpty->setChecked( FALSE );
    fecSR0RetEmpty->setTristate( FALSE );

    GroupBox2_3Layout->addWidget( fecSR0RetEmpty, 4, 2 );

    fecSR0RecEmpty = new QCheckBox( GroupBox2_3, "fecSR0RecEmpty" );

    GroupBox2_3Layout->addWidget( fecSR0RecEmpty, 3, 2 );

    fecSR0TraEmpty = new QCheckBox( GroupBox2_3, "fecSR0TraEmpty" );

    GroupBox2_3Layout->addWidget( fecSR0TraEmpty, 2, 2 );

    fecSR0RecRunning = new QCheckBox( GroupBox2_3, "fecSR0RecRunning" );

    GroupBox2_3Layout->addWidget( fecSR0RecRunning, 1, 2 );

    fecSR0TTCRx = new QCheckBox( GroupBox2_3, "fecSR0TTCRx" );

    GroupBox2_3Layout->addWidget( fecSR0TTCRx, 5, 2 );

    fecSR0DataToFec = new QCheckBox( GroupBox2_3, "fecSR0DataToFec" );

    GroupBox2_3Layout->addWidget( fecSR0DataToFec, 6, 2 );

    layout157->addWidget( GroupBox2_3, 2, 0 );
    FEC->insertTab( tab, QString::fromLatin1("") );

    tab_2 = new QWidget( FEC, "tab_2" );

    QWidget* privateLayoutWidget_5 = new QWidget( tab_2, "layout169" );
    privateLayoutWidget_5->setGeometry( QRect( 20, 20, 910, 776 ) );
    layout169 = new QVBoxLayout( privateLayoutWidget_5, 11, 6, "layout169"); 

    layout159 = new QHBoxLayout( 0, 0, 6, "layout159"); 

    TextLabel2_3 = new QLabel( privateLayoutWidget_5, "TextLabel2_3" );
    TextLabel2_3->setAlignment( int( QLabel::AlignCenter ) );
    layout159->addWidget( TextLabel2_3 );

    ccuFecSlots = new QComboBox( FALSE, privateLayoutWidget_5, "ccuFecSlots" );
    layout159->addWidget( ccuFecSlots );

    ccuScanForCcus = new QPushButton( privateLayoutWidget_5, "ccuScanForCcus" );
    layout159->addWidget( ccuScanForCcus );

    TextLabel2_5 = new QLabel( privateLayoutWidget_5, "TextLabel2_5" );
    TextLabel2_5->setAlignment( int( QLabel::AlignCenter ) );
    layout159->addWidget( TextLabel2_5 );

    ccuAddresses = new QComboBox( FALSE, privateLayoutWidget_5, "ccuAddresses" );
    layout159->addWidget( ccuAddresses );

    ccuIsACcu25 = new QCheckBox( privateLayoutWidget_5, "ccuIsACcu25" );
    layout159->addWidget( ccuIsACcu25 );

    ccuAllRead = new QPushButton( privateLayoutWidget_5, "ccuAllRead" );
    layout159->addWidget( ccuAllRead );
    layout169->addLayout( layout159 );

    layout162 = new QGridLayout( 0, 1, 1, 0, 6, "layout162"); 

    CCUCRB = new QGroupBox( privateLayoutWidget_5, "CCUCRB" );
    CCUCRB->setColumnLayout(0, Qt::Vertical );
    CCUCRB->layout()->setSpacing( 6 );
    CCUCRB->layout()->setMargin( 11 );
    CCUCRBLayout = new QGridLayout( CCUCRB->layout() );
    CCUCRBLayout->setAlignment( Qt::AlignTop );

    ccuCRB = new QLineEdit( CCUCRB, "ccuCRB" );
    ccuCRB->setAlignment( int( QLineEdit::AlignHCenter ) );

    CCUCRBLayout->addWidget( ccuCRB, 0, 0 );

    ccuCRBWrite = new QPushButton( CCUCRB, "ccuCRBWrite" );

    CCUCRBLayout->addWidget( ccuCRBWrite, 0, 1 );

    Layout43 = new QHBoxLayout( 0, 0, 6, "Layout43"); 

    ccuCRBRead = new QPushButton( CCUCRB, "ccuCRBRead" );
    Layout43->addWidget( ccuCRBRead );

    ccuCRBWriteBit = new QPushButton( CCUCRB, "ccuCRBWriteBit" );
    Layout43->addWidget( ccuCRBWriteBit );

    CCUCRBLayout->addMultiCellLayout( Layout43, 2, 2, 0, 1 );

    Layout162 = new QVBoxLayout( 0, 0, 6, "Layout162"); 

    ccuEnableAlarm1 = new QCheckBox( CCUCRB, "ccuEnableAlarm1" );
    Layout162->addWidget( ccuEnableAlarm1 );

    ccuEnableAlarm2 = new QCheckBox( CCUCRB, "ccuEnableAlarm2" );
    Layout162->addWidget( ccuEnableAlarm2 );

    ccuEnableAlarm3 = new QCheckBox( CCUCRB, "ccuEnableAlarm3" );
    Layout162->addWidget( ccuEnableAlarm3 );

    ccuEnableAlarm4 = new QCheckBox( CCUCRB, "ccuEnableAlarm4" );
    Layout162->addWidget( ccuEnableAlarm4 );

    ccuNoRety = new QCheckBox( CCUCRB, "ccuNoRety" );
    Layout162->addWidget( ccuNoRety );

    ccuRetryOnce = new QCheckBox( CCUCRB, "ccuRetryOnce" );
    ccuRetryOnce->setEnabled( TRUE );
    Layout162->addWidget( ccuRetryOnce );

    ccuRetryTwice = new QCheckBox( CCUCRB, "ccuRetryTwice" );
    Layout162->addWidget( ccuRetryTwice );

    ccuRetryFourth = new QCheckBox( CCUCRB, "ccuRetryFourth" );
    Layout162->addWidget( ccuRetryFourth );

    CCUCRBLayout->addMultiCellLayout( Layout162, 1, 1, 0, 1 );

    layout162->addMultiCellWidget( CCUCRB, 3, 4, 0, 0 );

    CCUSR = new QGroupBox( privateLayoutWidget_5, "CCUSR" );
    CCUSR->setColumnLayout(0, Qt::Vertical );
    CCUSR->layout()->setSpacing( 6 );
    CCUSR->layout()->setMargin( 11 );
    CCUSRLayout = new QGridLayout( CCUSR->layout() );
    CCUSRLayout->setAlignment( Qt::AlignTop );

    TextLabel17 = new QLabel( CCUSR, "TextLabel17" );

    CCUSRLayout->addWidget( TextLabel17, 0, 0 );

    TextLabel18 = new QLabel( CCUSR, "TextLabel18" );

    CCUSRLayout->addMultiCellWidget( TextLabel18, 6, 7, 2, 3 );

    TextLabel17_3 = new QLabel( CCUSR, "TextLabel17_3" );

    CCUSRLayout->addWidget( TextLabel17_3, 3, 0 );

    TextLabel20 = new QLabel( CCUSR, "TextLabel20" );

    CCUSRLayout->addMultiCellWidget( TextLabel20, 4, 4, 2, 3 );

    TextLabel17_5 = new QLabel( CCUSR, "TextLabel17_5" );

    CCUSRLayout->addWidget( TextLabel17_5, 5, 0 );

    Layout180 = new QHBoxLayout( 0, 0, 6, "Layout180"); 

    TextLabel24 = new QLabel( CCUSR, "TextLabel24" );
    Layout180->addWidget( TextLabel24 );
    Spacer19 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout180->addItem( Spacer19 );

    ccuSRCInputA = new QRadioButton( CCUSR, "ccuSRCInputA" );
    Layout180->addWidget( ccuSRCInputA );

    ccuSRCInputB = new QRadioButton( CCUSR, "ccuSRCInputB" );
    Layout180->addWidget( ccuSRCInputB );

    CCUSRLayout->addMultiCellLayout( Layout180, 2, 2, 0, 3 );

    TextLabel22 = new QLabel( CCUSR, "TextLabel22" );

    CCUSRLayout->addMultiCellWidget( TextLabel22, 1, 1, 2, 3 );

    ccuSRB = new QLineEdit( CCUSR, "ccuSRB" );
    ccuSRB->setAlignment( int( QLineEdit::AlignHCenter ) );

    CCUSRLayout->addMultiCellWidget( ccuSRB, 0, 0, 2, 3 );

    ccuSRD = new QLineEdit( CCUSR, "ccuSRD" );
    ccuSRD->setAlignment( int( QLineEdit::AlignHCenter ) );

    CCUSRLayout->addMultiCellWidget( ccuSRD, 3, 3, 2, 3 );

    ccuSRF = new QLineEdit( CCUSR, "ccuSRF" );
    ccuSRF->setAlignment( int( QLineEdit::AlignHCenter ) );

    CCUSRLayout->addMultiCellWidget( ccuSRF, 5, 5, 2, 3 );
    Spacer10 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    CCUSRLayout->addItem( Spacer10, 0, 1 );
    Spacer11 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    CCUSRLayout->addItem( Spacer11, 3, 1 );
    Spacer9 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    CCUSRLayout->addItem( Spacer9, 5, 1 );

    ccuSRRead = new QPushButton( CCUSR, "ccuSRRead" );

    CCUSRLayout->addMultiCellWidget( ccuSRRead, 8, 8, 2, 3 );
    spacer47 = new QSpacerItem( 20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
    CCUSRLayout->addItem( spacer47, 7, 3 );

    layout162->addMultiCellWidget( CCUSR, 2, 3, 2, 2 );

    CCUSRE = new QGroupBox( privateLayoutWidget_5, "CCUSRE" );
    CCUSRE->setColumnLayout(0, Qt::Vertical );
    CCUSRE->layout()->setSpacing( 6 );
    CCUSRE->layout()->setMargin( 11 );
    CCUSRELayout = new QGridLayout( CCUSRE->layout() );
    CCUSRELayout->setAlignment( Qt::AlignTop );

    ccuSRERead = new QPushButton( CCUSRE, "ccuSRERead" );

    CCUSRELayout->addWidget( ccuSRERead, 2, 1 );

    Layout161 = new QGridLayout( 0, 1, 1, 0, 6, "Layout161"); 

    ccuSREI2C16 = new QCheckBox( CCUSRE, "ccuSREI2C16" );

    Layout161->addWidget( ccuSREI2C16, 3, 3 );

    ccuSREI2C3 = new QCheckBox( CCUSRE, "ccuSREI2C3" );

    Layout161->addWidget( ccuSREI2C3, 2, 0 );

    ccuSREI2C6 = new QCheckBox( CCUSRE, "ccuSREI2C6" );

    Layout161->addWidget( ccuSREI2C6, 1, 1 );

    ccuSREI2C7 = new QCheckBox( CCUSRE, "ccuSREI2C7" );

    Layout161->addWidget( ccuSREI2C7, 2, 1 );

    ccuSREI2C11 = new QCheckBox( CCUSRE, "ccuSREI2C11" );

    Layout161->addWidget( ccuSREI2C11, 2, 2 );

    ccuSREI2C1 = new QCheckBox( CCUSRE, "ccuSREI2C1" );

    Layout161->addWidget( ccuSREI2C1, 0, 0 );

    ccuSREI2C4 = new QCheckBox( CCUSRE, "ccuSREI2C4" );

    Layout161->addWidget( ccuSREI2C4, 3, 0 );

    ccuSREI2C13 = new QCheckBox( CCUSRE, "ccuSREI2C13" );

    Layout161->addWidget( ccuSREI2C13, 0, 3 );

    ccuSREI2C15 = new QCheckBox( CCUSRE, "ccuSREI2C15" );

    Layout161->addWidget( ccuSREI2C15, 2, 3 );

    ccuSREI2C10 = new QCheckBox( CCUSRE, "ccuSREI2C10" );

    Layout161->addWidget( ccuSREI2C10, 1, 2 );

    ccuSREI2C12 = new QCheckBox( CCUSRE, "ccuSREI2C12" );

    Layout161->addWidget( ccuSREI2C12, 3, 2 );

    ccuSREI2C2 = new QCheckBox( CCUSRE, "ccuSREI2C2" );

    Layout161->addWidget( ccuSREI2C2, 1, 0 );

    ccuSREI2C5 = new QCheckBox( CCUSRE, "ccuSREI2C5" );

    Layout161->addWidget( ccuSREI2C5, 0, 1 );

    ccuSREI2C9 = new QCheckBox( CCUSRE, "ccuSREI2C9" );

    Layout161->addWidget( ccuSREI2C9, 0, 2 );

    ccuSREI2C8 = new QCheckBox( CCUSRE, "ccuSREI2C8" );

    Layout161->addWidget( ccuSREI2C8, 3, 1 );

    ccuSREI2C14 = new QCheckBox( CCUSRE, "ccuSREI2C14" );

    Layout161->addWidget( ccuSREI2C14, 1, 3 );

    CCUSRELayout->addMultiCellLayout( Layout161, 1, 1, 0, 1 );

    ccuSRE = new QLineEdit( CCUSRE, "ccuSRE" );
    ccuSRE->setAlignment( int( QLineEdit::AlignHCenter ) );

    CCUSRELayout->addWidget( ccuSRE, 0, 0 );

    layout162->addWidget( CCUSRE, 4, 2 );

    CCUCRC = new QGroupBox( privateLayoutWidget_5, "CCUCRC" );
    CCUCRC->setColumnLayout(0, Qt::Vertical );
    CCUCRC->layout()->setSpacing( 6 );
    CCUCRC->layout()->setMargin( 11 );
    CCUCRCLayout = new QGridLayout( CCUCRC->layout() );
    CCUCRCLayout->setAlignment( Qt::AlignTop );

    ccuCRC = new QLineEdit( CCUCRC, "ccuCRC" );
    ccuCRC->setAlignment( int( QLineEdit::AlignHCenter ) );

    CCUCRCLayout->addWidget( ccuCRC, 0, 0 );

    Layout45 = new QHBoxLayout( 0, 0, 6, "Layout45"); 

    ccuCRCRead = new QPushButton( CCUCRC, "ccuCRCRead" );
    Layout45->addWidget( ccuCRCRead );

    ccuCRCWriteBit = new QPushButton( CCUCRC, "ccuCRCWriteBit" );
    Layout45->addWidget( ccuCRCWriteBit );

    CCUCRCLayout->addMultiCellLayout( Layout45, 3, 3, 0, 1 );

    ccuCRCWrite = new QPushButton( CCUCRC, "ccuCRCWrite" );

    CCUCRCLayout->addWidget( ccuCRCWrite, 0, 1 );

    ccuSelectInputB = new QCheckBox( CCUCRC, "ccuSelectInputB" );

    CCUCRCLayout->addWidget( ccuSelectInputB, 1, 0 );

    ccuSelectOuputB = new QCheckBox( CCUCRC, "ccuSelectOuputB" );

    CCUCRCLayout->addWidget( ccuSelectOuputB, 2, 0 );

    layout162->addWidget( CCUCRC, 0, 1 );

    CCUCRD = new QGroupBox( privateLayoutWidget_5, "CCUCRD" );
    CCUCRD->setColumnLayout(0, Qt::Vertical );
    CCUCRD->layout()->setSpacing( 6 );
    CCUCRD->layout()->setMargin( 11 );
    CCUCRDLayout = new QGridLayout( CCUCRD->layout() );
    CCUCRDLayout->setAlignment( Qt::AlignTop );

    ccuCRDWrite = new QPushButton( CCUCRD, "ccuCRDWrite" );

    CCUCRDLayout->addWidget( ccuCRDWrite, 0, 1 );

    ccuCRD = new QLineEdit( CCUCRD, "ccuCRD" );
    ccuCRD->setAlignment( int( QLineEdit::AlignHCenter ) );

    CCUCRDLayout->addWidget( ccuCRD, 0, 0 );

    ccuCRDRead = new QPushButton( CCUCRD, "ccuCRDRead" );

    CCUCRDLayout->addWidget( ccuCRDRead, 2, 1 );

    layout162->addMultiCellWidget( CCUCRD, 1, 2, 1, 1 );

    CCUCRA = new QGroupBox( privateLayoutWidget_5, "CCUCRA" );
    CCUCRA->setColumnLayout(0, Qt::Vertical );
    CCUCRA->layout()->setSpacing( 6 );
    CCUCRA->layout()->setMargin( 11 );
    CCUCRALayout = new QGridLayout( CCUCRA->layout() );
    CCUCRALayout->setAlignment( Qt::AlignTop );

    ccuCRAWrite = new QPushButton( CCUCRA, "ccuCRAWrite" );

    CCUCRALayout->addWidget( ccuCRAWrite, 0, 1 );

    ccuExternalReset = new QCheckBox( CCUCRA, "ccuExternalReset" );

    CCUCRALayout->addWidget( ccuExternalReset, 1, 0 );

    ccuClearError = new QCheckBox( CCUCRA, "ccuClearError" );

    CCUCRALayout->addWidget( ccuClearError, 2, 0 );

    ccuCRA = new QLineEdit( CCUCRA, "ccuCRA" );
    ccuCRA->setAlignment( int( QLineEdit::AlignHCenter ) );

    CCUCRALayout->addWidget( ccuCRA, 0, 0 );

    Layout44 = new QHBoxLayout( 0, 0, 6, "Layout44"); 

    ccuCRARead = new QPushButton( CCUCRA, "ccuCRARead" );
    Layout44->addWidget( ccuCRARead );

    ccuCRAWriteBit = new QPushButton( CCUCRA, "ccuCRAWriteBit" );
    Layout44->addWidget( ccuCRAWriteBit );

    CCUCRALayout->addMultiCellLayout( Layout44, 6, 6, 0, 1 );

    ccuResetAllChannels = new QCheckBox( CCUCRA, "ccuResetAllChannels" );

    CCUCRALayout->addWidget( ccuResetAllChannels, 4, 0 );
    spacer45 = new QSpacerItem( 20, 150, QSizePolicy::Minimum, QSizePolicy::Expanding );
    CCUCRALayout->addItem( spacer45, 5, 0 );

    layout162->addMultiCellWidget( CCUCRA, 0, 2, 0, 0 );

    CCUSRA = new QGroupBox( privateLayoutWidget_5, "CCUSRA" );
    CCUSRA->setColumnLayout(0, Qt::Vertical );
    CCUSRA->layout()->setSpacing( 6 );
    CCUSRA->layout()->setMargin( 11 );
    CCUSRALayout = new QGridLayout( CCUSRA->layout() );
    CCUSRALayout->setAlignment( Qt::AlignTop );

    ccuSRA = new QLineEdit( CCUSRA, "ccuSRA" );
    ccuSRA->setAlignment( int( QLineEdit::AlignHCenter ) );

    CCUSRALayout->addWidget( ccuSRA, 0, 0 );

    ccuSRARead = new QPushButton( CCUSRA, "ccuSRARead" );

    CCUSRALayout->addWidget( ccuSRARead, 2, 1 );

    Layout165 = new QGridLayout( 0, 1, 1, 0, 6, "Layout165"); 

    ccuSRAGlobalError = new QCheckBox( CCUSRA, "ccuSRAGlobalError" );

    Layout165->addWidget( ccuSRAGlobalError, 3, 1 );

    ccuSRAIllegalSequence = new QCheckBox( CCUSRA, "ccuSRAIllegalSequence" );

    Layout165->addWidget( ccuSRAIllegalSequence, 2, 1 );

    ccuSRAAlarmInputActive = new QCheckBox( CCUSRA, "ccuSRAAlarmInputActive" );

    Layout165->addWidget( ccuSRAAlarmInputActive, 1, 0 );

    ccuSRACRCError = new QCheckBox( CCUSRA, "ccuSRACRCError" );

    Layout165->addWidget( ccuSRACRCError, 0, 0 );

    ccuSRACCUParityError = new QCheckBox( CCUSRA, "ccuSRACCUParityError" );

    Layout165->addWidget( ccuSRACCUParityError, 1, 1 );

    ccuSRAChannelParityError = new QCheckBox( CCUSRA, "ccuSRAChannelParityError" );

    Layout165->addWidget( ccuSRAChannelParityError, 2, 0 );

    ccuSRAInternalError = new QCheckBox( CCUSRA, "ccuSRAInternalError" );

    Layout165->addWidget( ccuSRAInternalError, 0, 1 );

    ccuSRAInvalidCommand = new QCheckBox( CCUSRA, "ccuSRAInvalidCommand" );

    Layout165->addWidget( ccuSRAInvalidCommand, 3, 0 );

    CCUSRALayout->addMultiCellLayout( Layout165, 1, 1, 0, 1 );

    layout162->addMultiCellWidget( CCUSRA, 0, 1, 2, 2 );

    CCUCRE = new QGroupBox( privateLayoutWidget_5, "CCUCRE" );
    CCUCRE->setColumnLayout(0, Qt::Vertical );
    CCUCRE->layout()->setSpacing( 6 );
    CCUCRE->layout()->setMargin( 11 );
    CCUCRELayout = new QGridLayout( CCUCRE->layout() );
    CCUCRELayout->setAlignment( Qt::AlignTop );

    Layout21 = new QGridLayout( 0, 1, 1, 0, 6, "Layout21"); 

    ccuCREMemory = new QCheckBox( CCUCRE, "ccuCREMemory" );

    Layout21->addMultiCellWidget( ccuCREMemory, 9, 10, 4, 5 );

    ccuCREI2C5 = new QCheckBox( CCUCRE, "ccuCREI2C5" );

    Layout21->addMultiCellWidget( ccuCREI2C5, 3, 3, 1, 3 );

    ccuCREPIA1 = new QCheckBox( CCUCRE, "ccuCREPIA1" );

    Layout21->addWidget( ccuCREPIA1, 8, 0 );

    ccuCREPIA2 = new QCheckBox( CCUCRE, "ccuCREPIA2" );

    Layout21->addMultiCellWidget( ccuCREPIA2, 8, 8, 1, 3 );

    ccuCREI2C11 = new QCheckBox( CCUCRE, "ccuCREI2C11" );

    Layout21->addMultiCellWidget( ccuCREI2C11, 5, 5, 1, 3 );

    ccuCREI2C9 = new QCheckBox( CCUCRE, "ccuCREI2C9" );

    Layout21->addMultiCellWidget( ccuCREI2C9, 4, 4, 4, 5 );

    ccuCREI2C14 = new QCheckBox( CCUCRE, "ccuCREI2C14" );

    Layout21->addMultiCellWidget( ccuCREI2C14, 6, 6, 1, 3 );

    ccuCRERead = new QPushButton( CCUCRE, "ccuCRERead" );

    Layout21->addMultiCellWidget( ccuCRERead, 11, 11, 1, 4 );

    ccuCREWrite = new QPushButton( CCUCRE, "ccuCREWrite" );

    Layout21->addMultiCellWidget( ccuCREWrite, 0, 0, 3, 5 );

    ccuCREI2C15 = new QCheckBox( CCUCRE, "ccuCREI2C15" );

    Layout21->addMultiCellWidget( ccuCREI2C15, 6, 6, 4, 5 );

    ccuCREJTAG = new QCheckBox( CCUCRE, "ccuCREJTAG" );

    Layout21->addMultiCellWidget( ccuCREJTAG, 9, 10, 1, 3 );

    ccuCRE = new QLineEdit( CCUCRE, "ccuCRE" );
    ccuCRE->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout21->addMultiCellWidget( ccuCRE, 0, 0, 0, 2 );

    ccuAllEnable = new QPushButton( CCUCRE, "ccuAllEnable" );

    Layout21->addWidget( ccuAllEnable, 11, 0 );

    ccuCREI2C10 = new QCheckBox( CCUCRE, "ccuCREI2C10" );

    Layout21->addWidget( ccuCREI2C10, 5, 0 );

    ccuCREWriteBit = new QPushButton( CCUCRE, "ccuCREWriteBit" );

    Layout21->addWidget( ccuCREWriteBit, 11, 5 );

    ccuCREI2C3 = new QCheckBox( CCUCRE, "ccuCREI2C3" );

    Layout21->addMultiCellWidget( ccuCREI2C3, 2, 2, 4, 5 );

    ccuCREI2C12 = new QCheckBox( CCUCRE, "ccuCREI2C12" );

    Layout21->addMultiCellWidget( ccuCREI2C12, 5, 5, 4, 5 );

    ccuCREI2C6 = new QCheckBox( CCUCRE, "ccuCREI2C6" );

    Layout21->addMultiCellWidget( ccuCREI2C6, 3, 3, 4, 5 );

    ccuCREI2C8 = new QCheckBox( CCUCRE, "ccuCREI2C8" );

    Layout21->addMultiCellWidget( ccuCREI2C8, 4, 4, 1, 3 );

    ccuCREI2C7 = new QCheckBox( CCUCRE, "ccuCREI2C7" );

    Layout21->addWidget( ccuCREI2C7, 4, 0 );

    ccuCREPIA3 = new QCheckBox( CCUCRE, "ccuCREPIA3" );

    Layout21->addMultiCellWidget( ccuCREPIA3, 8, 8, 4, 5 );

    ccuCREI2C13 = new QCheckBox( CCUCRE, "ccuCREI2C13" );

    Layout21->addWidget( ccuCREI2C13, 6, 0 );

    ccuCRETrigger = new QCheckBox( CCUCRE, "ccuCRETrigger" );

    Layout21->addMultiCellWidget( ccuCRETrigger, 9, 10, 0, 0 );

    ccuCREI2C2 = new QCheckBox( CCUCRE, "ccuCREI2C2" );

    Layout21->addMultiCellWidget( ccuCREI2C2, 2, 2, 1, 3 );

    ccuCREI2C4 = new QCheckBox( CCUCRE, "ccuCREI2C4" );

    Layout21->addWidget( ccuCREI2C4, 3, 0 );

    ccuCREI2C1 = new QCheckBox( CCUCRE, "ccuCREI2C1" );

    Layout21->addWidget( ccuCREI2C1, 2, 0 );

    ccuCREI2C16 = new QCheckBox( CCUCRE, "ccuCREI2C16" );

    Layout21->addWidget( ccuCREI2C16, 7, 0 );

    CCUCRELayout->addLayout( Layout21, 0, 0 );

    layout162->addMultiCellWidget( CCUCRE, 3, 4, 1, 1 );
    layout169->addLayout( layout162 );

    TextLabel1_3_2 = new QLabel( privateLayoutWidget_5, "TextLabel1_3_2" );
    TextLabel1_3_2->setPaletteForegroundColor( QColor( 255, 0, 0 ) );
    TextLabel1_3_2->setPaletteBackgroundColor( QColor( 230, 230, 230 ) );
    layout169->addWidget( TextLabel1_3_2 );
    FEC->insertTab( tab_2, QString::fromLatin1("") );

    tab_3 = new QWidget( FEC, "tab_3" );

    QWidget* privateLayoutWidget_6 = new QWidget( tab_3, "layout168" );
    privateLayoutWidget_6->setGeometry( QRect( 9, 20, 910, 715 ) );
    layout168 = new QVBoxLayout( privateLayoutWidget_6, 11, 6, "layout168"); 

    layout167 = new QHBoxLayout( 0, 0, 6, "layout167"); 

    TextLabel2_3_2 = new QLabel( privateLayoutWidget_6, "TextLabel2_3_2" );
    TextLabel2_3_2->setAlignment( int( QLabel::AlignCenter ) );
    layout167->addWidget( TextLabel2_3_2 );

    i2cFecSlots = new QComboBox( FALSE, privateLayoutWidget_6, "i2cFecSlots" );
    layout167->addWidget( i2cFecSlots );

    TextLabel2_5_2 = new QLabel( privateLayoutWidget_6, "TextLabel2_5_2" );
    TextLabel2_5_2->setAlignment( int( QLabel::AlignCenter ) );
    layout167->addWidget( TextLabel2_5_2 );

    i2cCcuAddresses = new QComboBox( FALSE, privateLayoutWidget_6, "i2cCcuAddresses" );
    layout167->addWidget( i2cCcuAddresses );

    i2cCcuIsACcu25 = new QCheckBox( privateLayoutWidget_6, "i2cCcuIsACcu25" );
    layout167->addWidget( i2cCcuIsACcu25 );

    TextLabel26 = new QLabel( privateLayoutWidget_6, "TextLabel26" );
    layout167->addWidget( TextLabel26 );

    i2cChannels = new QComboBox( FALSE, privateLayoutWidget_6, "i2cChannels" );
    layout167->addWidget( i2cChannels );

    i2cAllRead = new QPushButton( privateLayoutWidget_6, "i2cAllRead" );
    layout167->addWidget( i2cAllRead );
    layout168->addLayout( layout167 );

    layout166 = new QGridLayout( 0, 1, 1, 0, 6, "layout166"); 

    CCUCRA_2_3 = new QGroupBox( privateLayoutWidget_6, "CCUCRA_2_3" );
    CCUCRA_2_3->setColumnLayout(0, Qt::Vertical );
    CCUCRA_2_3->layout()->setSpacing( 6 );
    CCUCRA_2_3->layout()->setMargin( 11 );
    CCUCRA_2_3Layout = new QGridLayout( CCUCRA_2_3->layout() );
    CCUCRA_2_3Layout->setAlignment( Qt::AlignTop );

    i2cAutoDectection = new QPushButton( CCUCRA_2_3, "i2cAutoDectection" );

    CCUCRA_2_3Layout->addWidget( i2cAutoDectection, 1, 0 );

    i2cListDeviceDetected = new QListBox( CCUCRA_2_3, "i2cListDeviceDetected" );
    i2cListDeviceDetected->setVScrollBarMode( QListBox::AlwaysOn );

    CCUCRA_2_3Layout->addWidget( i2cListDeviceDetected, 0, 0 );

    layout166->addMultiCellWidget( CCUCRA_2_3, 0, 3, 2, 2 );
    spacer48 = new QSpacerItem( 20, 41, QSizePolicy::Minimum, QSizePolicy::Expanding );
    layout166->addItem( spacer48, 2, 1 );

    CCUCRA_3 = new QGroupBox( privateLayoutWidget_6, "CCUCRA_3" );
    CCUCRA_3->setColumnLayout(0, Qt::Vertical );
    CCUCRA_3->layout()->setSpacing( 6 );
    CCUCRA_3->layout()->setMargin( 11 );
    CCUCRA_3Layout = new QGridLayout( CCUCRA_3->layout() );
    CCUCRA_3Layout->setAlignment( Qt::AlignTop );

    i2cSRA = new QLineEdit( CCUCRA_3, "i2cSRA" );
    i2cSRA->setAlignment( int( QLineEdit::AlignHCenter ) );

    CCUCRA_3Layout->addWidget( i2cSRA, 0, 0 );

    i2cSRARead = new QPushButton( CCUCRA_3, "i2cSRARead" );

    CCUCRA_3Layout->addWidget( i2cSRARead, 2, 1 );

    Layout184 = new QVBoxLayout( 0, 0, 6, "Layout184"); 

    i2cLastTrSuccessfull = new QCheckBox( CCUCRA_3, "i2cLastTrSuccessfull" );
    Layout184->addWidget( i2cLastTrSuccessfull );

    i2cLow = new QCheckBox( CCUCRA_3, "i2cLow" );
    Layout184->addWidget( i2cLow );

    i2cInvalidCommand = new QCheckBox( CCUCRA_3, "i2cInvalidCommand" );
    Layout184->addWidget( i2cInvalidCommand );

    i2cLastOperationNotAck = new QCheckBox( CCUCRA_3, "i2cLastOperationNotAck" );
    Layout184->addWidget( i2cLastOperationNotAck );

    i2cGlobalError = new QCheckBox( CCUCRA_3, "i2cGlobalError" );
    Layout184->addWidget( i2cGlobalError );

    CCUCRA_3Layout->addMultiCellLayout( Layout184, 1, 1, 0, 1 );

    layout166->addMultiCellWidget( CCUCRA_3, 0, 1, 1, 1 );

    CCUSR_2 = new QGroupBox( privateLayoutWidget_6, "CCUSR_2" );
    CCUSR_2->setColumnLayout(0, Qt::Vertical );
    CCUSR_2->layout()->setSpacing( 6 );
    CCUSR_2->layout()->setMargin( 11 );
    CCUSR_2Layout = new QGridLayout( CCUSR_2->layout() );
    CCUSR_2Layout->setAlignment( Qt::AlignTop );

    TextLabel17_2 = new QLabel( CCUSR_2, "TextLabel17_2" );

    CCUSR_2Layout->addWidget( TextLabel17_2, 0, 0 );

    TextLabel17_3_2 = new QLabel( CCUSR_2, "TextLabel17_3_2" );

    CCUSR_2Layout->addWidget( TextLabel17_3_2, 2, 0 );

    TextLabel17_5_2 = new QLabel( CCUSR_2, "TextLabel17_5_2" );

    CCUSR_2Layout->addWidget( TextLabel17_5_2, 4, 0 );

    i2cSRC = new QLineEdit( CCUSR_2, "i2cSRC" );
    i2cSRC->setAlignment( int( QLineEdit::AlignHCenter ) );

    CCUSR_2Layout->addWidget( i2cSRC, 2, 1 );

    i2cSRD = new QLineEdit( CCUSR_2, "i2cSRD" );
    i2cSRD->setAlignment( int( QLineEdit::AlignHCenter ) );

    CCUSR_2Layout->addWidget( i2cSRD, 4, 1 );

    i2cSRRead = new QPushButton( CCUSR_2, "i2cSRRead" );

    CCUSR_2Layout->addWidget( i2cSRRead, 6, 2 );

    i2cSRB = new QLineEdit( CCUSR_2, "i2cSRB" );
    i2cSRB->setAlignment( int( QLineEdit::AlignHCenter ) );

    CCUSR_2Layout->addMultiCellWidget( i2cSRB, 0, 0, 1, 2 );

    TextLabel18_2 = new QLabel( CCUSR_2, "TextLabel18_2" );

    CCUSR_2Layout->addMultiCellWidget( TextLabel18_2, 5, 5, 0, 2 );

    TextLabel20_2 = new QLabel( CCUSR_2, "TextLabel20_2" );

    CCUSR_2Layout->addMultiCellWidget( TextLabel20_2, 3, 3, 0, 2 );

    TextLabel22_2 = new QLabel( CCUSR_2, "TextLabel22_2" );

    CCUSR_2Layout->addMultiCellWidget( TextLabel22_2, 1, 1, 0, 2 );

    layout166->addWidget( CCUSR_2, 4, 0 );

    CCUCRA_2 = new QGroupBox( privateLayoutWidget_6, "CCUCRA_2" );
    CCUCRA_2->setColumnLayout(0, Qt::Vertical );
    CCUCRA_2->layout()->setSpacing( 6 );
    CCUCRA_2->layout()->setMargin( 11 );
    CCUCRA_2Layout = new QGridLayout( CCUCRA_2->layout() );
    CCUCRA_2Layout->setAlignment( Qt::AlignTop );

    i2cCRAWrite = new QPushButton( CCUCRA_2, "i2cCRAWrite" );

    CCUCRA_2Layout->addMultiCellWidget( i2cCRAWrite, 0, 0, 1, 2 );

    i2cCRA = new QLineEdit( CCUCRA_2, "i2cCRA" );
    i2cCRA->setAlignment( int( QLineEdit::AlignHCenter ) );

    CCUCRA_2Layout->addWidget( i2cCRA, 0, 0 );

    i2cCRARead = new QPushButton( CCUCRA_2, "i2cCRARead" );

    CCUCRA_2Layout->addMultiCellWidget( i2cCRARead, 2, 2, 0, 1 );

    i2cCRAWriteBit = new QPushButton( CCUCRA_2, "i2cCRAWriteBit" );

    CCUCRA_2Layout->addWidget( i2cCRAWriteBit, 2, 2 );

    Layout175 = new QGridLayout( 0, 1, 1, 0, 6, "Layout175"); 
    Spacer13_2 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout175->addItem( Spacer13_2, 2, 0 );
    Spacer13 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout175->addItem( Spacer13, 1, 0 );

    i2cSpeed400 = new QRadioButton( CCUCRA_2, "i2cSpeed400" );

    Layout175->addWidget( i2cSpeed400, 2, 1 );

    i2cSpeed200 = new QRadioButton( CCUCRA_2, "i2cSpeed200" );

    Layout175->addWidget( i2cSpeed200, 1, 2 );

    TextLabel25 = new QLabel( CCUCRA_2, "TextLabel25" );

    Layout175->addMultiCellWidget( TextLabel25, 0, 0, 0, 1 );

    i2cSpeed100 = new QRadioButton( CCUCRA_2, "i2cSpeed100" );
    i2cSpeed100->setChecked( FALSE );

    Layout175->addWidget( i2cSpeed100, 1, 1 );

    i2cSpeed1000 = new QRadioButton( CCUCRA_2, "i2cSpeed1000" );

    Layout175->addWidget( i2cSpeed1000, 2, 2 );

    i2cForceAcknowledge = new QCheckBox( CCUCRA_2, "i2cForceAcknowledge" );

    Layout175->addMultiCellWidget( i2cForceAcknowledge, 4, 4, 0, 2 );

    i2cEnableBroadCast = new QCheckBox( CCUCRA_2, "i2cEnableBroadCast" );

    Layout175->addMultiCellWidget( i2cEnableBroadCast, 3, 3, 0, 2 );

    CCUCRA_2Layout->addMultiCellLayout( Layout175, 1, 1, 0, 2 );

    layout166->addMultiCellWidget( CCUCRA_2, 1, 3, 0, 0 );

    GroupBox85 = new QGroupBox( privateLayoutWidget_6, "GroupBox85" );
    GroupBox85->setColumnLayout(0, Qt::Vertical );
    GroupBox85->layout()->setSpacing( 6 );
    GroupBox85->layout()->setMargin( 11 );
    GroupBox85Layout = new QGridLayout( GroupBox85->layout() );
    GroupBox85Layout->setAlignment( Qt::AlignTop );

    i2cChannelReset = new QPushButton( GroupBox85, "i2cChannelReset" );

    GroupBox85Layout->addWidget( i2cChannelReset, 0, 1 );

    i2cEnable = new QCheckBox( GroupBox85, "i2cEnable" );

    GroupBox85Layout->addWidget( i2cEnable, 0, 0 );

    layout166->addWidget( GroupBox85, 0, 0 );

    GroupBox63 = new QGroupBox( privateLayoutWidget_6, "GroupBox63" );
    GroupBox63->setColumnLayout(0, Qt::Vertical );
    GroupBox63->layout()->setSpacing( 6 );
    GroupBox63->layout()->setMargin( 11 );
    GroupBox63Layout = new QGridLayout( GroupBox63->layout() );
    GroupBox63Layout->setAlignment( Qt::AlignTop );

    i2cRalMode = new QRadioButton( GroupBox63, "i2cRalMode" );

    GroupBox63Layout->addWidget( i2cRalMode, 2, 0 );

    i2cExtendedMode = new QRadioButton( GroupBox63, "i2cExtendedMode" );

    GroupBox63Layout->addWidget( i2cExtendedMode, 1, 0 );

    i2cNormalMode = new QRadioButton( GroupBox63, "i2cNormalMode" );
    i2cNormalMode->setChecked( TRUE );

    GroupBox63Layout->addWidget( i2cNormalMode, 0, 0 );

    Layout19 = new QHBoxLayout( 0, 0, 6, "Layout19"); 

    i2cReadModifyWriteB = new QPushButton( GroupBox63, "i2cReadModifyWriteB" );
    i2cReadModifyWriteB->setEnabled( FALSE );
    Layout19->addWidget( i2cReadModifyWriteB );

    i2cReadB = new QPushButton( GroupBox63, "i2cReadB" );
    Layout19->addWidget( i2cReadB );

    i2cWriteB = new QPushButton( GroupBox63, "i2cWriteB" );
    Layout19->addWidget( i2cWriteB );

    GroupBox63Layout->addMultiCellLayout( Layout19, 4, 4, 0, 1 );

    Layout50 = new QGridLayout( 0, 1, 1, 0, 6, "Layout50"); 

    TextLabel27_3_2 = new QLabel( GroupBox63, "TextLabel27_3_2" );

    Layout50->addWidget( TextLabel27_3_2, 3, 0 );

    i2cShiftAccess = new QLineEdit( GroupBox63, "i2cShiftAccess" );
    i2cShiftAccess->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout50->addWidget( i2cShiftAccess, 3, 1 );

    TextLabel2_2 = new QLabel( GroupBox63, "TextLabel2_2" );
    TextLabel2_2->setEnabled( FALSE );

    Layout50->addWidget( TextLabel2_2, 4, 0 );

    i2cLogMaskRegister = new QLineEdit( GroupBox63, "i2cLogMaskRegister" );
    i2cLogMaskRegister->setEnabled( FALSE );
    i2cLogMaskRegister->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout50->addWidget( i2cLogMaskRegister, 4, 1 );

    TextLabel27_3 = new QLabel( GroupBox63, "TextLabel27_3" );

    Layout50->addWidget( TextLabel27_3, 2, 0 );

    TextLabel27_2 = new QLabel( GroupBox63, "TextLabel27_2" );

    Layout50->addWidget( TextLabel27_2, 1, 0 );

    i2cDataAccess = new QLineEdit( GroupBox63, "i2cDataAccess" );
    i2cDataAccess->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout50->addWidget( i2cDataAccess, 1, 1 );

    TextLabel27 = new QLabel( GroupBox63, "TextLabel27" );

    Layout50->addWidget( TextLabel27, 0, 0 );

    i2cOffsetAccess = new QLineEdit( GroupBox63, "i2cOffsetAccess" );
    i2cOffsetAccess->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout50->addWidget( i2cOffsetAccess, 2, 1 );

    i2cAddressAccess = new QLineEdit( GroupBox63, "i2cAddressAccess" );
    i2cAddressAccess->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout50->addWidget( i2cAddressAccess, 0, 1 );

    GroupBox63Layout->addMultiCellLayout( Layout50, 3, 3, 0, 1 );

    i2cAndOperation = new QRadioButton( GroupBox63, "i2cAndOperation" );
    i2cAndOperation->setEnabled( FALSE );
    i2cAndOperation->setChecked( FALSE );

    GroupBox63Layout->addWidget( i2cAndOperation, 0, 1 );

    i2cOrOperation = new QRadioButton( GroupBox63, "i2cOrOperation" );
    i2cOrOperation->setEnabled( FALSE );

    GroupBox63Layout->addWidget( i2cOrOperation, 1, 1 );

    i2cXorOperation = new QRadioButton( GroupBox63, "i2cXorOperation" );
    i2cXorOperation->setEnabled( FALSE );

    GroupBox63Layout->addWidget( i2cXorOperation, 2, 1 );

    layout166->addMultiCellWidget( GroupBox63, 3, 4, 1, 1 );
    layout168->addLayout( layout166 );

    TextLabel1_3 = new QLabel( privateLayoutWidget_6, "TextLabel1_3" );
    TextLabel1_3->setPaletteForegroundColor( QColor( 255, 0, 0 ) );
    TextLabel1_3->setPaletteBackgroundColor( QColor( 220, 220, 220 ) );
    layout168->addWidget( TextLabel1_3 );
    FEC->insertTab( tab_3, QString::fromLatin1("") );

    tab_4 = new QWidget( FEC, "tab_4" );

    QWidget* privateLayoutWidget_7 = new QWidget( tab_4, "layout179" );
    privateLayoutWidget_7->setGeometry( QRect( 13, 20, 910, 760 ) );
    layout179 = new QVBoxLayout( privateLayoutWidget_7, 11, 6, "layout179"); 

    layout177 = new QVBoxLayout( 0, 0, 6, "layout177"); 

    layout175 = new QHBoxLayout( 0, 0, 6, "layout175"); 

    TextLabel2_3_2_2 = new QLabel( privateLayoutWidget_7, "TextLabel2_3_2_2" );
    TextLabel2_3_2_2->setAlignment( int( QLabel::AlignCenter ) );
    layout175->addWidget( TextLabel2_3_2_2 );

    memoryFecSlots = new QComboBox( FALSE, privateLayoutWidget_7, "memoryFecSlots" );
    layout175->addWidget( memoryFecSlots );

    TextLabel2_5_2_2 = new QLabel( privateLayoutWidget_7, "TextLabel2_5_2_2" );
    TextLabel2_5_2_2->setAlignment( int( QLabel::AlignCenter ) );
    layout175->addWidget( TextLabel2_5_2_2 );

    memoryCcuAddresses = new QComboBox( FALSE, privateLayoutWidget_7, "memoryCcuAddresses" );
    layout175->addWidget( memoryCcuAddresses );

    memoryCcuIsACcu25 = new QCheckBox( privateLayoutWidget_7, "memoryCcuIsACcu25" );
    layout175->addWidget( memoryCcuIsACcu25 );

    TextLabel1 = new QLabel( privateLayoutWidget_7, "TextLabel1" );
    TextLabel1->setPaletteForegroundColor( QColor( 0, 0, 0 ) );
    layout175->addWidget( TextLabel1 );

    memoryAllRead = new QPushButton( privateLayoutWidget_7, "memoryAllRead" );
    layout175->addWidget( memoryAllRead );
    layout177->addLayout( layout175 );

    layout174 = new QGridLayout( 0, 1, 1, 0, 6, "layout174"); 

    GroupBox57 = new QGroupBox( privateLayoutWidget_7, "GroupBox57" );
    GroupBox57->setColumnLayout(0, Qt::Vertical );
    GroupBox57->layout()->setSpacing( 6 );
    GroupBox57->layout()->setMargin( 11 );
    GroupBox57Layout = new QGridLayout( GroupBox57->layout() );
    GroupBox57Layout->setAlignment( Qt::AlignTop );

    TextLabel27_3_2_3_2_2 = new QLabel( GroupBox57, "TextLabel27_3_2_3_2_2" );

    GroupBox57Layout->addWidget( TextLabel27_3_2_3_2_2, 0, 0 );

    TextLabel27_2_3_2_3 = new QLabel( GroupBox57, "TextLabel27_2_3_2_3" );

    GroupBox57Layout->addMultiCellWidget( TextLabel27_2_3_2_3, 5, 5, 0, 1 );

    TextLabel27_5_2_3 = new QLabel( GroupBox57, "TextLabel27_5_2_3" );

    GroupBox57Layout->addMultiCellWidget( TextLabel27_5_2_3, 4, 4, 0, 1 );

    memorySize = new QLineEdit( GroupBox57, "memorySize" );
    memorySize->setAlignment( int( QLineEdit::AlignHCenter ) );

    GroupBox57Layout->addMultiCellWidget( memorySize, 0, 0, 1, 3 );

    memoryBandwith = new QLineEdit( GroupBox57, "memoryBandwith" );
    memoryBandwith->setAlignment( int( QLineEdit::AlignHCenter ) );

    GroupBox57Layout->addMultiCellWidget( memoryBandwith, 5, 5, 2, 3 );

    memoryTime = new QLineEdit( GroupBox57, "memoryTime" );
    memoryTime->setAlignment( int( QLineEdit::AlignHCenter ) );

    GroupBox57Layout->addMultiCellWidget( memoryTime, 4, 4, 2, 3 );

    memoryFillSameData = new QCheckBox( GroupBox57, "memoryFillSameData" );

    GroupBox57Layout->addMultiCellWidget( memoryFillSameData, 1, 1, 0, 2 );

    memoryDisplayOnList = new QCheckBox( GroupBox57, "memoryDisplayOnList" );

    GroupBox57Layout->addWidget( memoryDisplayOnList, 1, 3 );

    memoryDisplayList = new QListBox( GroupBox57, "memoryDisplayList" );
    memoryDisplayList->setVScrollBarMode( QListBox::AlwaysOn );
    memoryDisplayList->setHScrollBarMode( QListBox::Auto );

    GroupBox57Layout->addMultiCellWidget( memoryDisplayList, 3, 3, 0, 3 );

    Layout34 = new QHBoxLayout( 0, 0, 6, "Layout34"); 

    memoryReadBMB = new QPushButton( GroupBox57, "memoryReadBMB" );
    Layout34->addWidget( memoryReadBMB );

    memoryWriteBMB = new QPushButton( GroupBox57, "memoryWriteBMB" );
    Layout34->addWidget( memoryWriteBMB );

    memoryWriteBMBCompare = new QPushButton( GroupBox57, "memoryWriteBMBCompare" );
    Layout34->addWidget( memoryWriteBMBCompare );

    GroupBox57Layout->addMultiCellLayout( Layout34, 2, 2, 0, 3 );

    layout174->addWidget( GroupBox57, 3, 2 );

    CCUCRA_3_2 = new QGroupBox( privateLayoutWidget_7, "CCUCRA_3_2" );
    CCUCRA_3_2->setColumnLayout(0, Qt::Vertical );
    CCUCRA_3_2->layout()->setSpacing( 6 );
    CCUCRA_3_2->layout()->setMargin( 11 );
    CCUCRA_3_2Layout = new QGridLayout( CCUCRA_3_2->layout() );
    CCUCRA_3_2Layout->setAlignment( Qt::AlignTop );

    Layout184_2 = new QVBoxLayout( 0, 0, 6, "Layout184_2"); 

    memoryInvalidCommand = new QCheckBox( CCUCRA_3_2, "memoryInvalidCommand" );
    Layout184_2->addWidget( memoryInvalidCommand );

    memoryInvalidAddress = new QCheckBox( CCUCRA_3_2, "memoryInvalidAddress" );
    Layout184_2->addWidget( memoryInvalidAddress );

    memoryGlobalError = new QCheckBox( CCUCRA_3_2, "memoryGlobalError" );
    Layout184_2->addWidget( memoryGlobalError );

    CCUCRA_3_2Layout->addMultiCellLayout( Layout184_2, 2, 2, 0, 1 );

    memorySR = new QLineEdit( CCUCRA_3_2, "memorySR" );
    memorySR->setAlignment( int( QLineEdit::AlignHCenter ) );

    CCUCRA_3_2Layout->addWidget( memorySR, 0, 0 );

    memorySRRead = new QPushButton( CCUCRA_3_2, "memorySRRead" );

    CCUCRA_3_2Layout->addWidget( memorySRRead, 4, 1 );
    spacer52 = new QSpacerItem( 20, 21, QSizePolicy::Minimum, QSizePolicy::Expanding );
    CCUCRA_3_2Layout->addItem( spacer52, 1, 0 );
    spacer52_2 = new QSpacerItem( 20, 21, QSizePolicy::Minimum, QSizePolicy::Expanding );
    CCUCRA_3_2Layout->addItem( spacer52_2, 3, 1 );

    layout174->addWidget( CCUCRA_3_2, 3, 1 );

    GroupBox56 = new QGroupBox( privateLayoutWidget_7, "GroupBox56" );
    GroupBox56->setColumnLayout(0, Qt::Vertical );
    GroupBox56->layout()->setSpacing( 6 );
    GroupBox56->layout()->setMargin( 11 );
    GroupBox56Layout = new QGridLayout( GroupBox56->layout() );
    GroupBox56Layout->setAlignment( Qt::AlignTop );

    TextLabel27_3_2_3_2 = new QLabel( GroupBox56, "TextLabel27_3_2_3_2" );

    GroupBox56Layout->addWidget( TextLabel27_3_2_3_2, 0, 0 );

    Layout65 = new QHBoxLayout( 0, 0, 6, "Layout65"); 

    memoryAndOperation = new QRadioButton( GroupBox56, "memoryAndOperation" );
    memoryAndOperation->setChecked( TRUE );
    Layout65->addWidget( memoryAndOperation );

    memoryOrOperation = new QRadioButton( GroupBox56, "memoryOrOperation" );
    Layout65->addWidget( memoryOrOperation );

    memoryXorOperation = new QRadioButton( GroupBox56, "memoryXorOperation" );
    Layout65->addWidget( memoryXorOperation );

    GroupBox56Layout->addMultiCellLayout( Layout65, 1, 1, 0, 1 );

    Layout64 = new QHBoxLayout( 0, 0, 6, "Layout64"); 

    memoryMaskRegRead = new QPushButton( GroupBox56, "memoryMaskRegRead" );
    Layout64->addWidget( memoryMaskRegRead );

    memoryReadModifyWriteB = new QPushButton( GroupBox56, "memoryReadModifyWriteB" );
    Layout64->addWidget( memoryReadModifyWriteB );

    GroupBox56Layout->addMultiCellLayout( Layout64, 2, 2, 0, 1 );

    memoryMask = new QLineEdit( GroupBox56, "memoryMask" );
    memoryMask->setAlignment( int( QLineEdit::AlignHCenter ) );

    GroupBox56Layout->addWidget( memoryMask, 0, 1 );

    layout174->addWidget( GroupBox56, 2, 2 );

    CCUCRA_2_4 = new QGroupBox( privateLayoutWidget_7, "CCUCRA_2_4" );
    CCUCRA_2_4->setColumnLayout(0, Qt::Vertical );
    CCUCRA_2_4->layout()->setSpacing( 6 );
    CCUCRA_2_4->layout()->setMargin( 11 );
    CCUCRA_2_4Layout = new QGridLayout( CCUCRA_2_4->layout() );
    CCUCRA_2_4Layout->setAlignment( Qt::AlignTop );

    layout172 = new QHBoxLayout( 0, 0, 6, "layout172"); 

    memoryCRARead = new QPushButton( CCUCRA_2_4, "memoryCRARead" );
    layout172->addWidget( memoryCRARead );

    memoryCRAWriteBit = new QPushButton( CCUCRA_2_4, "memoryCRAWriteBit" );
    layout172->addWidget( memoryCRAWriteBit );

    CCUCRA_2_4Layout->addLayout( layout172, 4, 0 );

    Layout175_2 = new QGridLayout( 0, 1, 1, 0, 6, "Layout175_2"); 
    Spacer13_2_2 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout175_2->addItem( Spacer13_2_2, 2, 0 );
    Spacer13_3 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout175_2->addItem( Spacer13_3, 1, 0 );

    memorySpeed10 = new QRadioButton( CCUCRA_2_4, "memorySpeed10" );

    Layout175_2->addWidget( memorySpeed10, 2, 1 );

    memorySpeed4 = new QRadioButton( CCUCRA_2_4, "memorySpeed4" );

    Layout175_2->addWidget( memorySpeed4, 1, 2 );

    TextLabel25_2 = new QLabel( CCUCRA_2_4, "TextLabel25_2" );

    Layout175_2->addMultiCellWidget( TextLabel25_2, 0, 0, 0, 1 );

    memorySpeed1 = new QRadioButton( CCUCRA_2_4, "memorySpeed1" );
    memorySpeed1->setChecked( FALSE );

    Layout175_2->addWidget( memorySpeed1, 1, 1 );

    memorySpeed20 = new QRadioButton( CCUCRA_2_4, "memorySpeed20" );

    Layout175_2->addWidget( memorySpeed20, 2, 2 );

    memoryWin2Enable = new QCheckBox( CCUCRA_2_4, "memoryWin2Enable" );

    Layout175_2->addMultiCellWidget( memoryWin2Enable, 4, 4, 0, 2 );

    memoryWin1Enable = new QCheckBox( CCUCRA_2_4, "memoryWin1Enable" );

    Layout175_2->addMultiCellWidget( memoryWin1Enable, 3, 3, 0, 2 );

    CCUCRA_2_4Layout->addLayout( Layout175_2, 2, 0 );

    layout173 = new QHBoxLayout( 0, 0, 6, "layout173"); 

    memoryCRA = new QLineEdit( CCUCRA_2_4, "memoryCRA" );
    memoryCRA->setAlignment( int( QLineEdit::AlignHCenter ) );
    layout173->addWidget( memoryCRA );

    memoryCRAWrite = new QPushButton( CCUCRA_2_4, "memoryCRAWrite" );
    layout173->addWidget( memoryCRAWrite );

    CCUCRA_2_4Layout->addLayout( layout173, 0, 0 );

    layout174->addMultiCellWidget( CCUCRA_2_4, 1, 3, 0, 0 );

    GroupBox63_3_2 = new QGroupBox( privateLayoutWidget_7, "GroupBox63_3_2" );
    GroupBox63_3_2->setColumnLayout(0, Qt::Vertical );
    GroupBox63_3_2->layout()->setSpacing( 6 );
    GroupBox63_3_2->layout()->setMargin( 11 );
    GroupBox63_3_2Layout = new QGridLayout( GroupBox63_3_2->layout() );
    GroupBox63_3_2Layout->setAlignment( Qt::AlignTop );

    memoryReadBSB = new QPushButton( GroupBox63_3_2, "memoryReadBSB" );

    GroupBox63_3_2Layout->addWidget( memoryReadBSB, 1, 0 );

    memoryWriteBSB = new QPushButton( GroupBox63_3_2, "memoryWriteBSB" );

    GroupBox63_3_2Layout->addWidget( memoryWriteBSB, 1, 1 );

    Layout61 = new QGridLayout( 0, 1, 1, 0, 6, "Layout61"); 

    TextLabel27_5_2 = new QLabel( GroupBox63_3_2, "TextLabel27_5_2" );

    Layout61->addWidget( TextLabel27_5_2, 0, 0 );

    memoryData = new QLineEdit( GroupBox63_3_2, "memoryData" );
    memoryData->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout61->addWidget( memoryData, 2, 1 );

    TextLabel27_2_3_2 = new QLabel( GroupBox63_3_2, "TextLabel27_2_3_2" );

    Layout61->addWidget( TextLabel27_2_3_2, 1, 0 );

    memoryAH = new QLineEdit( GroupBox63_3_2, "memoryAH" );
    memoryAH->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout61->addWidget( memoryAH, 1, 1 );

    memoryAL = new QLineEdit( GroupBox63_3_2, "memoryAL" );
    memoryAL->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout61->addWidget( memoryAL, 0, 1 );

    TextLabel27_3_4_2 = new QLabel( GroupBox63_3_2, "TextLabel27_3_4_2" );

    Layout61->addWidget( TextLabel27_3_4_2, 2, 0 );

    GroupBox63_3_2Layout->addMultiCellLayout( Layout61, 0, 0, 0, 1 );

    layout174->addMultiCellWidget( GroupBox63_3_2, 0, 1, 2, 2 );

    GroupBox63_3 = new QGroupBox( privateLayoutWidget_7, "GroupBox63_3" );
    GroupBox63_3->setColumnLayout(0, Qt::Vertical );
    GroupBox63_3->layout()->setSpacing( 6 );
    GroupBox63_3->layout()->setMargin( 11 );
    GroupBox63_3Layout = new QGridLayout( GroupBox63_3->layout() );
    GroupBox63_3Layout->setAlignment( Qt::AlignTop );

    Layout19_3 = new QHBoxLayout( 0, 0, 6, "Layout19_3"); 

    memoryWinRead = new QPushButton( GroupBox63_3, "memoryWinRead" );
    Layout19_3->addWidget( memoryWinRead );

    memoryWinWrite = new QPushButton( GroupBox63_3, "memoryWinWrite" );
    Layout19_3->addWidget( memoryWinWrite );

    GroupBox63_3Layout->addLayout( Layout19_3, 2, 0 );

    Layout48 = new QGridLayout( 0, 1, 1, 0, 6, "Layout48"); 

    memoryWin1H = new QLineEdit( GroupBox63_3, "memoryWin1H" );
    memoryWin1H->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout48->addWidget( memoryWin1H, 1, 1 );

    TextLabel27_3_2_3 = new QLabel( GroupBox63_3, "TextLabel27_3_2_3" );

    Layout48->addWidget( TextLabel27_3_2_3, 3, 0 );

    memoryWin2L = new QLineEdit( GroupBox63_3, "memoryWin2L" );
    memoryWin2L->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout48->addWidget( memoryWin2L, 2, 1 );

    TextLabel27_5 = new QLabel( GroupBox63_3, "TextLabel27_5" );

    Layout48->addWidget( TextLabel27_5, 0, 0 );

    TextLabel27_3_4 = new QLabel( GroupBox63_3, "TextLabel27_3_4" );

    Layout48->addWidget( TextLabel27_3_4, 2, 0 );

    TextLabel27_2_3 = new QLabel( GroupBox63_3, "TextLabel27_2_3" );

    Layout48->addWidget( TextLabel27_2_3, 1, 0 );

    memoryWin2H = new QLineEdit( GroupBox63_3, "memoryWin2H" );
    memoryWin2H->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout48->addWidget( memoryWin2H, 3, 1 );

    memoryWin1L = new QLineEdit( GroupBox63_3, "memoryWin1L" );
    memoryWin1L->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout48->addWidget( memoryWin1L, 0, 1 );

    GroupBox63_3Layout->addLayout( Layout48, 0, 0 );
    spacer51 = new QSpacerItem( 20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
    GroupBox63_3Layout->addItem( spacer51, 1, 0 );

    layout174->addMultiCellWidget( GroupBox63_3, 0, 2, 1, 1 );

    GroupBox85_2 = new QGroupBox( privateLayoutWidget_7, "GroupBox85_2" );
    GroupBox85_2->setColumnLayout(0, Qt::Vertical );
    GroupBox85_2->layout()->setSpacing( 6 );
    GroupBox85_2->layout()->setMargin( 11 );
    GroupBox85_2Layout = new QGridLayout( GroupBox85_2->layout() );
    GroupBox85_2Layout->setAlignment( Qt::AlignTop );

    memoryChannelReset = new QPushButton( GroupBox85_2, "memoryChannelReset" );

    GroupBox85_2Layout->addWidget( memoryChannelReset, 0, 1 );

    memoryEnable = new QCheckBox( GroupBox85_2, "memoryEnable" );

    GroupBox85_2Layout->addWidget( memoryEnable, 0, 0 );

    layout174->addWidget( GroupBox85_2, 0, 0 );
    layout177->addLayout( layout174 );
    layout179->addLayout( layout177 );

    TextLabel1_3_3 = new QLabel( privateLayoutWidget_7, "TextLabel1_3_3" );
    TextLabel1_3_3->setPaletteForegroundColor( QColor( 255, 0, 0 ) );
    TextLabel1_3_3->setPaletteBackgroundColor( QColor( 220, 220, 220 ) );
    layout179->addWidget( TextLabel1_3_3 );

    TextLabel1_3_4 = new QLabel( privateLayoutWidget_7, "TextLabel1_3_4" );
    TextLabel1_3_4->setPaletteForegroundColor( QColor( 255, 0, 0 ) );
    TextLabel1_3_4->setPaletteBackgroundColor( QColor( 220, 220, 220 ) );
    layout179->addWidget( TextLabel1_3_4 );
    FEC->insertTab( tab_4, QString::fromLatin1("") );

    tab_5 = new QWidget( FEC, "tab_5" );

    QWidget* privateLayoutWidget_8 = new QWidget( tab_5, "layout183" );
    privateLayoutWidget_8->setGeometry( QRect( 40, 60, 900, 475 ) );
    layout183 = new QGridLayout( privateLayoutWidget_8, 1, 1, 11, 6, "layout183"); 

    layout182 = new QHBoxLayout( 0, 0, 6, "layout182"); 

    TextLabel2_3_2_3 = new QLabel( privateLayoutWidget_8, "TextLabel2_3_2_3" );
    TextLabel2_3_2_3->setAlignment( int( QLabel::AlignCenter ) );
    layout182->addWidget( TextLabel2_3_2_3 );

    piaFecSlots = new QComboBox( FALSE, privateLayoutWidget_8, "piaFecSlots" );
    layout182->addWidget( piaFecSlots );

    TextLabel2_5_2_3 = new QLabel( privateLayoutWidget_8, "TextLabel2_5_2_3" );
    TextLabel2_5_2_3->setAlignment( int( QLabel::AlignCenter ) );
    layout182->addWidget( TextLabel2_5_2_3 );

    piaCcuAddresses = new QComboBox( FALSE, privateLayoutWidget_8, "piaCcuAddresses" );
    layout182->addWidget( piaCcuAddresses );

    piaCcuIsACcu25 = new QCheckBox( privateLayoutWidget_8, "piaCcuIsACcu25" );
    layout182->addWidget( piaCcuIsACcu25 );

    TextLabel26_2 = new QLabel( privateLayoutWidget_8, "TextLabel26_2" );
    layout182->addWidget( TextLabel26_2 );

    piaChannels = new QComboBox( FALSE, privateLayoutWidget_8, "piaChannels" );
    layout182->addWidget( piaChannels );

    piaAllRead = new QPushButton( privateLayoutWidget_8, "piaAllRead" );
    layout182->addWidget( piaAllRead );

    layout183->addLayout( layout182, 0, 0 );

    layout181 = new QGridLayout( 0, 1, 1, 0, 6, "layout181"); 

    PIASTATUSREGISTER = new QGroupBox( privateLayoutWidget_8, "PIASTATUSREGISTER" );
    PIASTATUSREGISTER->setColumnLayout(0, Qt::Vertical );
    PIASTATUSREGISTER->layout()->setSpacing( 6 );
    PIASTATUSREGISTER->layout()->setMargin( 11 );
    PIASTATUSREGISTERLayout = new QGridLayout( PIASTATUSREGISTER->layout() );
    PIASTATUSREGISTERLayout->setAlignment( Qt::AlignTop );

    Layout184_2_2 = new QVBoxLayout( 0, 0, 6, "Layout184_2_2"); 

    piaInt = new QCheckBox( PIASTATUSREGISTER, "piaInt" );
    Layout184_2_2->addWidget( piaInt );

    piaInvcom = new QCheckBox( PIASTATUSREGISTER, "piaInvcom" );
    Layout184_2_2->addWidget( piaInvcom );

    piaGlobalError = new QCheckBox( PIASTATUSREGISTER, "piaGlobalError" );
    Layout184_2_2->addWidget( piaGlobalError );

    PIASTATUSREGISTERLayout->addMultiCellLayout( Layout184_2_2, 1, 1, 0, 1 );

    piaSR = new QLineEdit( PIASTATUSREGISTER, "piaSR" );
    piaSR->setAlignment( int( QLineEdit::AlignHCenter ) );

    PIASTATUSREGISTERLayout->addWidget( piaSR, 0, 0 );

    piaSRRead = new QPushButton( PIASTATUSREGISTER, "piaSRRead" );

    PIASTATUSREGISTERLayout->addWidget( piaSRRead, 3, 1 );
    spacer56 = new QSpacerItem( 20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding );
    PIASTATUSREGISTERLayout->addItem( spacer56, 2, 1 );

    layout181->addMultiCellWidget( PIASTATUSREGISTER, 0, 1, 1, 1 );

    PIADATAREGISTER = new QGroupBox( privateLayoutWidget_8, "PIADATAREGISTER" );
    PIADATAREGISTER->setColumnLayout(0, Qt::Vertical );
    PIADATAREGISTER->layout()->setSpacing( 6 );
    PIADATAREGISTER->layout()->setMargin( 11 );
    PIADATAREGISTERLayout = new QGridLayout( PIADATAREGISTER->layout() );
    PIADATAREGISTERLayout->setAlignment( Qt::AlignTop );

    TextLabel5 = new QLabel( PIADATAREGISTER, "TextLabel5" );

    PIADATAREGISTERLayout->addWidget( TextLabel5, 2, 0 );

    TextLabel4 = new QLabel( PIADATAREGISTER, "TextLabel4" );

    PIADATAREGISTERLayout->addWidget( TextLabel4, 3, 0 );

    piaEnableOperationBit = new QCheckBox( PIADATAREGISTER, "piaEnableOperationBit" );

    PIADATAREGISTERLayout->addMultiCellWidget( piaEnableOperationBit, 1, 1, 0, 2 );

    piaDuration = new QLineEdit( PIADATAREGISTER, "piaDuration" );
    piaDuration->setEnabled( TRUE );

    PIADATAREGISTERLayout->addMultiCellWidget( piaDuration, 2, 2, 1, 2 );

    piaSleepTime = new QLineEdit( PIADATAREGISTER, "piaSleepTime" );
    piaSleepTime->setEnabled( TRUE );

    PIADATAREGISTERLayout->addMultiCellWidget( piaSleepTime, 3, 3, 1, 2 );

    piaDataRegister = new QLineEdit( PIADATAREGISTER, "piaDataRegister" );
    piaDataRegister->setAlignment( int( QLineEdit::AlignHCenter ) );

    PIADATAREGISTERLayout->addMultiCellWidget( piaDataRegister, 0, 0, 0, 1 );

    piaDataRegisterRead = new QPushButton( PIADATAREGISTER, "piaDataRegisterRead" );

    PIADATAREGISTERLayout->addWidget( piaDataRegisterRead, 4, 1 );

    piaDataRegisterWrite = new QPushButton( PIADATAREGISTER, "piaDataRegisterWrite" );

    PIADATAREGISTERLayout->addWidget( piaDataRegisterWrite, 4, 2 );

    layout181->addWidget( PIADATAREGISTER, 2, 2 );

    PIAGENERALCONTROLREGISTER = new QGroupBox( privateLayoutWidget_8, "PIAGENERALCONTROLREGISTER" );
    PIAGENERALCONTROLREGISTER->setColumnLayout(0, Qt::Vertical );
    PIAGENERALCONTROLREGISTER->layout()->setSpacing( 6 );
    PIAGENERALCONTROLREGISTER->layout()->setMargin( 11 );
    PIAGENERALCONTROLREGISTERLayout = new QGridLayout( PIAGENERALCONTROLREGISTER->layout() );
    PIAGENERALCONTROLREGISTERLayout->setAlignment( Qt::AlignTop );

    piaGCRWrite = new QPushButton( PIAGENERALCONTROLREGISTER, "piaGCRWrite" );

    PIAGENERALCONTROLREGISTERLayout->addMultiCellWidget( piaGCRWrite, 0, 0, 1, 2 );

    piaGCR = new QLineEdit( PIAGENERALCONTROLREGISTER, "piaGCR" );
    piaGCR->setAlignment( int( QLineEdit::AlignHCenter ) );

    PIAGENERALCONTROLREGISTERLayout->addWidget( piaGCR, 0, 0 );

    Layout72 = new QGridLayout( 0, 1, 1, 0, 6, "Layout72"); 

    piaWidth100 = new QRadioButton( PIAGENERALCONTROLREGISTER, "piaWidth100" );

    Layout72->addWidget( piaWidth100, 2, 2 );

    piaWidth500 = new QRadioButton( PIAGENERALCONTROLREGISTER, "piaWidth500" );

    Layout72->addWidget( piaWidth500, 1, 2 );

    piaEnp = new QCheckBox( PIAGENERALCONTROLREGISTER, "piaEnp" );

    Layout72->addMultiCellWidget( piaEnp, 3, 3, 0, 2 );

    piaStrb = new QCheckBox( PIAGENERALCONTROLREGISTER, "piaStrb" );

    Layout72->addMultiCellWidget( piaStrb, 4, 4, 0, 2 );

    piaStrinp = new QCheckBox( PIAGENERALCONTROLREGISTER, "piaStrinp" );

    Layout72->addMultiCellWidget( piaStrinp, 7, 7, 0, 2 );

    piaEnintA = new QCheckBox( PIAGENERALCONTROLREGISTER, "piaEnintA" );

    Layout72->addMultiCellWidget( piaEnintA, 6, 6, 0, 2 );

    piaWidth200 = new QRadioButton( PIAGENERALCONTROLREGISTER, "piaWidth200" );

    Layout72->addWidget( piaWidth200, 2, 1 );

    TextLabel25_2_2 = new QLabel( PIAGENERALCONTROLREGISTER, "TextLabel25_2_2" );

    Layout72->addMultiCellWidget( TextLabel25_2_2, 0, 0, 0, 1 );

    piaWidth1000 = new QRadioButton( PIAGENERALCONTROLREGISTER, "piaWidth1000" );
    piaWidth1000->setChecked( FALSE );

    Layout72->addWidget( piaWidth1000, 1, 1 );
    Spacer13_3_2 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout72->addItem( Spacer13_3_2, 1, 0 );

    piaStroutp = new QCheckBox( PIAGENERALCONTROLREGISTER, "piaStroutp" );

    Layout72->addMultiCellWidget( piaStroutp, 5, 5, 0, 2 );

    PIAGENERALCONTROLREGISTERLayout->addMultiCellLayout( Layout72, 1, 1, 0, 2 );

    piaGCRRead = new QPushButton( PIAGENERALCONTROLREGISTER, "piaGCRRead" );

    PIAGENERALCONTROLREGISTERLayout->addMultiCellWidget( piaGCRRead, 2, 2, 0, 1 );

    piaGCRWriteBit = new QPushButton( PIAGENERALCONTROLREGISTER, "piaGCRWriteBit" );

    PIAGENERALCONTROLREGISTERLayout->addWidget( piaGCRWriteBit, 2, 2 );

    layout181->addMultiCellWidget( PIAGENERALCONTROLREGISTER, 1, 2, 0, 0 );

    PIACOMMAND = new QGroupBox( privateLayoutWidget_8, "PIACOMMAND" );
    PIACOMMAND->setColumnLayout(0, Qt::Vertical );
    PIACOMMAND->layout()->setSpacing( 6 );
    PIACOMMAND->layout()->setMargin( 11 );
    PIACOMMANDLayout = new QGridLayout( PIACOMMAND->layout() );
    PIACOMMANDLayout->setAlignment( Qt::AlignTop );

    piaChannelReset = new QPushButton( PIACOMMAND, "piaChannelReset" );

    PIACOMMANDLayout->addWidget( piaChannelReset, 0, 1 );

    piaEnable = new QCheckBox( PIACOMMAND, "piaEnable" );

    PIACOMMANDLayout->addWidget( piaEnable, 0, 0 );

    layout181->addWidget( PIACOMMAND, 0, 0 );

    PIADATADIRECTIONPORT = new QGroupBox( privateLayoutWidget_8, "PIADATADIRECTIONPORT" );
    PIADATADIRECTIONPORT->setColumnLayout(0, Qt::Vertical );
    PIADATADIRECTIONPORT->layout()->setSpacing( 6 );
    PIADATADIRECTIONPORT->layout()->setMargin( 11 );
    PIADATADIRECTIONPORTLayout = new QGridLayout( PIADATADIRECTIONPORT->layout() );
    PIADATADIRECTIONPORTLayout->setAlignment( Qt::AlignTop );

    piaDDRPWrite = new QPushButton( PIADATADIRECTIONPORT, "piaDDRPWrite" );

    PIADATADIRECTIONPORTLayout->addMultiCellWidget( piaDDRPWrite, 0, 0, 3, 4 );

    TextLabel3 = new QLabel( PIADATADIRECTIONPORT, "TextLabel3" );

    PIADATADIRECTIONPORTLayout->addWidget( TextLabel3, 1, 0 );

    TextLabel2_4 = new QLabel( PIADATADIRECTIONPORT, "TextLabel2_4" );

    PIADATADIRECTIONPORTLayout->addWidget( TextLabel2_4, 1, 1 );

    TextLabel2_4_2 = new QLabel( PIADATADIRECTIONPORT, "TextLabel2_4_2" );

    PIADATADIRECTIONPORTLayout->addWidget( TextLabel2_4_2, 1, 4 );

    TextLabel3_2 = new QLabel( PIADATADIRECTIONPORT, "TextLabel3_2" );

    PIADATADIRECTIONPORTLayout->addMultiCellWidget( TextLabel3_2, 1, 1, 2, 3 );

    piaDDRP = new QLineEdit( PIADATADIRECTIONPORT, "piaDDRP" );
    piaDDRP->setAlignment( int( QLineEdit::AlignHCenter ) );

    PIADATADIRECTIONPORTLayout->addMultiCellWidget( piaDDRP, 0, 0, 0, 2 );

    layout49 = new QHBoxLayout( 0, 0, 6, "layout49"); 

    Layout70 = new QGridLayout( 0, 1, 1, 0, 6, "Layout70"); 

    piaDDRBit1O = new QCheckBox( PIADATADIRECTIONPORT, "piaDDRBit1O" );

    Layout70->addWidget( piaDDRBit1O, 1, 2 );

    piaDDRBit2O = new QCheckBox( PIADATADIRECTIONPORT, "piaDDRBit2O" );

    Layout70->addWidget( piaDDRBit2O, 2, 2 );

    piaDDRBit1I = new QCheckBox( PIADATADIRECTIONPORT, "piaDDRBit1I" );
    piaDDRBit1I->setChecked( TRUE );

    Layout70->addWidget( piaDDRBit1I, 1, 0 );

    piaDDRBit0I = new QCheckBox( PIADATADIRECTIONPORT, "piaDDRBit0I" );
    piaDDRBit0I->setChecked( TRUE );

    Layout70->addWidget( piaDDRBit0I, 0, 0 );

    TextLabel1_2 = new QLabel( PIADATADIRECTIONPORT, "TextLabel1_2" );

    Layout70->addWidget( TextLabel1_2, 0, 1 );

    TextLabel1_2_3 = new QLabel( PIADATADIRECTIONPORT, "TextLabel1_2_3" );

    Layout70->addWidget( TextLabel1_2_3, 2, 1 );

    TextLabel1_2_3_2 = new QLabel( PIADATADIRECTIONPORT, "TextLabel1_2_3_2" );

    Layout70->addWidget( TextLabel1_2_3_2, 3, 1 );

    piaDDRBit2I = new QCheckBox( PIADATADIRECTIONPORT, "piaDDRBit2I" );
    piaDDRBit2I->setChecked( TRUE );

    Layout70->addWidget( piaDDRBit2I, 2, 0 );

    TextLabel1_2_2 = new QLabel( PIADATADIRECTIONPORT, "TextLabel1_2_2" );

    Layout70->addWidget( TextLabel1_2_2, 1, 1 );

    piaDDRBit3I = new QCheckBox( PIADATADIRECTIONPORT, "piaDDRBit3I" );
    piaDDRBit3I->setChecked( TRUE );

    Layout70->addWidget( piaDDRBit3I, 3, 0 );

    piaDDRBit3O = new QCheckBox( PIADATADIRECTIONPORT, "piaDDRBit3O" );

    Layout70->addWidget( piaDDRBit3O, 3, 2 );

    piaDDRBit0O = new QCheckBox( PIADATADIRECTIONPORT, "piaDDRBit0O" );

    Layout70->addWidget( piaDDRBit0O, 0, 2 );
    layout49->addLayout( Layout70 );

    Layout71 = new QGridLayout( 0, 1, 1, 0, 6, "Layout71"); 

    piaDDRBit7O = new QCheckBox( PIADATADIRECTIONPORT, "piaDDRBit7O" );

    Layout71->addWidget( piaDDRBit7O, 3, 2 );

    piaDDRBit6O = new QCheckBox( PIADATADIRECTIONPORT, "piaDDRBit6O" );

    Layout71->addWidget( piaDDRBit6O, 2, 2 );

    piaDDRBit4I = new QCheckBox( PIADATADIRECTIONPORT, "piaDDRBit4I" );
    piaDDRBit4I->setChecked( TRUE );

    Layout71->addWidget( piaDDRBit4I, 0, 0 );

    piaDDRBit6I = new QCheckBox( PIADATADIRECTIONPORT, "piaDDRBit6I" );
    piaDDRBit6I->setChecked( TRUE );

    Layout71->addWidget( piaDDRBit6I, 2, 0 );

    piaDDRBit4O = new QCheckBox( PIADATADIRECTIONPORT, "piaDDRBit4O" );

    Layout71->addWidget( piaDDRBit4O, 0, 2 );

    piaDDRBit7I = new QCheckBox( PIADATADIRECTIONPORT, "piaDDRBit7I" );
    piaDDRBit7I->setChecked( TRUE );

    Layout71->addWidget( piaDDRBit7I, 3, 0 );

    TextLabel1_2_4 = new QLabel( PIADATADIRECTIONPORT, "TextLabel1_2_4" );

    Layout71->addWidget( TextLabel1_2_4, 0, 1 );

    TextLabel1_2_2_2 = new QLabel( PIADATADIRECTIONPORT, "TextLabel1_2_2_2" );

    Layout71->addWidget( TextLabel1_2_2_2, 1, 1 );

    piaDDRBit5O = new QCheckBox( PIADATADIRECTIONPORT, "piaDDRBit5O" );

    Layout71->addWidget( piaDDRBit5O, 1, 2 );

    TextLabel1_2_3_3 = new QLabel( PIADATADIRECTIONPORT, "TextLabel1_2_3_3" );

    Layout71->addWidget( TextLabel1_2_3_3, 2, 1 );

    piaDDRBit5I = new QCheckBox( PIADATADIRECTIONPORT, "piaDDRBit5I" );
    piaDDRBit5I->setChecked( TRUE );

    Layout71->addWidget( piaDDRBit5I, 1, 0 );

    TextLabel1_2_3_2_2 = new QLabel( PIADATADIRECTIONPORT, "TextLabel1_2_3_2_2" );

    Layout71->addWidget( TextLabel1_2_3_2_2, 3, 1 );
    layout49->addLayout( Layout71 );

    PIADATADIRECTIONPORTLayout->addMultiCellLayout( layout49, 2, 2, 0, 4 );

    piaDDRPRead = new QPushButton( PIADATADIRECTIONPORT, "piaDDRPRead" );

    PIADATADIRECTIONPORTLayout->addMultiCellWidget( piaDDRPRead, 4, 4, 2, 3 );

    piaDDRPWriteBit = new QPushButton( PIADATADIRECTIONPORT, "piaDDRPWriteBit" );

    PIADATADIRECTIONPORTLayout->addWidget( piaDDRPWriteBit, 4, 4 );
    spacer57 = new QSpacerItem( 20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
    PIADATADIRECTIONPORTLayout->addMultiCell( spacer57, 3, 3, 3, 4 );

    layout181->addMultiCellWidget( PIADATADIRECTIONPORT, 0, 1, 2, 2 );

    layout183->addLayout( layout181, 1, 0 );
    FEC->insertTab( tab_5, QString::fromLatin1("") );

    tab_6 = new QWidget( FEC, "tab_6" );
    tabLayout = new QHBoxLayout( tab_6, 11, 6, "tabLayout"); 

    layout196 = new QHBoxLayout( 0, 0, 6, "layout196"); 

    layout195 = new QVBoxLayout( 0, 0, 6, "layout195"); 

    TRACKERDEVICES = new QGroupBox( tab_6, "TRACKERDEVICES" );
    TRACKERDEVICES->setColumnLayout(0, Qt::Vertical );
    TRACKERDEVICES->layout()->setSpacing( 6 );
    TRACKERDEVICES->layout()->setMargin( 11 );
    TRACKERDEVICESLayout = new QGridLayout( TRACKERDEVICES->layout() );
    TRACKERDEVICESLayout->setAlignment( Qt::AlignTop );

    trackerDeleteDevice = new QPushButton( TRACKERDEVICES, "trackerDeleteDevice" );
    trackerDeleteDevice->setEnabled( TRUE );

    TRACKERDEVICESLayout->addWidget( trackerDeleteDevice, 2, 0 );

    layout47 = new QVBoxLayout( 0, 0, 6, "layout47"); 

    trackerListHybridDetected = new QListBox( TRACKERDEVICES, "trackerListHybridDetected" );
    trackerListHybridDetected->setVScrollBarMode( QListBox::AlwaysOn );
    trackerListHybridDetected->setSelectionMode( QListBox::Extended );
    layout47->addWidget( trackerListHybridDetected );

    trackerListDeviceDetected = new QListBox( TRACKERDEVICES, "trackerListDeviceDetected" );
    trackerListDeviceDetected->setVScrollBarMode( QListBox::AlwaysOn );
    trackerListDeviceDetected->setSelectionMode( QListBox::Extended );
    layout47->addWidget( trackerListDeviceDetected );

    TRACKERDEVICESLayout->addLayout( layout47, 0, 0 );

    trackerAutoDectection = new QPushButton( TRACKERDEVICES, "trackerAutoDectection" );

    TRACKERDEVICESLayout->addWidget( trackerAutoDectection, 1, 0 );
    layout195->addWidget( TRACKERDEVICES );

    TRACKERLOOP = new QGroupBox( tab_6, "TRACKERLOOP" );
    TRACKERLOOP->setColumnLayout(0, Qt::Vertical );
    TRACKERLOOP->layout()->setSpacing( 6 );
    TRACKERLOOP->layout()->setMargin( 11 );
    TRACKERLOOPLayout = new QGridLayout( TRACKERLOOP->layout() );
    TRACKERLOOPLayout->setAlignment( Qt::AlignTop );

    Layout50_2 = new QVBoxLayout( 0, 0, 6, "Layout50_2"); 

    Layout45_2 = new QGridLayout( 0, 1, 1, 0, 6, "Layout45_2"); 

    TextLabel8 = new QLabel( TRACKERLOOP, "TextLabel8" );

    Layout45_2->addWidget( TextLabel8, 0, 2 );

    trackerDecimal = new QCheckBox( TRACKERLOOP, "trackerDecimal" );
    trackerDecimal->setEnabled( FALSE );

    Layout45_2->addMultiCellWidget( trackerDecimal, 1, 1, 0, 1 );

    trackerErrorNumber = new QLineEdit( TRACKERLOOP, "trackerErrorNumber" );
    trackerErrorNumber->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout45_2->addWidget( trackerErrorNumber, 1, 3 );

    trackerLoopNumber = new QLineEdit( TRACKERLOOP, "trackerLoopNumber" );
    trackerLoopNumber->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout45_2->addWidget( trackerLoopNumber, 0, 3 );

    trackerAllDevices = new QCheckBox( TRACKERLOOP, "trackerAllDevices" );

    Layout45_2->addMultiCellWidget( trackerAllDevices, 2, 2, 2, 3 );

    TextLabel8_3 = new QLabel( TRACKERLOOP, "TextLabel8_3" );

    Layout45_2->addWidget( TextLabel8_3, 1, 2 );

    trackerInfiniteLoop = new QCheckBox( TRACKERLOOP, "trackerInfiniteLoop" );
    trackerInfiniteLoop->setEnabled( FALSE );

    Layout45_2->addWidget( trackerInfiniteLoop, 0, 0 );

    trackerDoComparison = new QCheckBox( TRACKERLOOP, "trackerDoComparison" );
    trackerDoComparison->setEnabled( TRUE );
    trackerDoComparison->setChecked( TRUE );

    Layout45_2->addMultiCellWidget( trackerDoComparison, 2, 2, 0, 1 );
    Spacer71 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout45_2->addItem( Spacer71, 0, 1 );
    Layout50_2->addLayout( Layout45_2 );

    Layout49 = new QHBoxLayout( 0, 0, 6, "Layout49"); 

    trackerDefaultValues = new QPushButton( TRACKERLOOP, "trackerDefaultValues" );
    Layout49->addWidget( trackerDefaultValues );

    trackerAllRead = new QPushButton( TRACKERLOOP, "trackerAllRead" );
    Layout49->addWidget( trackerAllRead );

    trackerAllWrite = new QPushButton( TRACKERLOOP, "trackerAllWrite" );
    Layout49->addWidget( trackerAllWrite );
    Layout50_2->addLayout( Layout49 );

    TRACKERLOOPLayout->addLayout( Layout50_2, 0, 0 );
    layout195->addWidget( TRACKERLOOP );

    DeviceDriver_2 = new QGroupBox( tab_6, "DeviceDriver_2" );
    DeviceDriver_2->setFrameShape( QGroupBox::Box );
    DeviceDriver_2->setFrameShadow( QGroupBox::Sunken );
    DeviceDriver_2->setColumnLayout(0, Qt::Vertical );
    DeviceDriver_2->layout()->setSpacing( 6 );
    DeviceDriver_2->layout()->setMargin( 11 );
    DeviceDriver_2Layout = new QGridLayout( DeviceDriver_2->layout() );
    DeviceDriver_2Layout->setAlignment( Qt::AlignTop );

    Layout63_2 = new QGridLayout( 0, 1, 1, 0, 6, "Layout63_2"); 

    trackerFecWarningDevice = new QLineEdit( DeviceDriver_2, "trackerFecWarningDevice" );
    trackerFecWarningDevice->setAlignment( int( QLineEdit::AlignHCenter ) );
    trackerFecWarningDevice->setReadOnly( TRUE );

    Layout63_2->addWidget( trackerFecWarningDevice, 4, 1 );

    trackerFecNbPlxReset = new QLineEdit( DeviceDriver_2, "trackerFecNbPlxReset" );
    trackerFecNbPlxReset->setAlignment( int( QLineEdit::AlignHCenter ) );
    trackerFecNbPlxReset->setReadOnly( TRUE );

    Layout63_2->addWidget( trackerFecNbPlxReset, 5, 1 );

    trackerFecBadTransaction = new QLineEdit( DeviceDriver_2, "trackerFecBadTransaction" );
    trackerFecBadTransaction->setAlignment( int( QLineEdit::AlignHCenter ) );
    trackerFecBadTransaction->setReadOnly( TRUE );

    Layout63_2->addWidget( trackerFecBadTransaction, 3, 1 );

    TextLabel16_2 = new QLabel( DeviceDriver_2, "TextLabel16_2" );

    Layout63_2->addWidget( TextLabel16_2, 6, 0 );

    TextLabel12_2 = new QLabel( DeviceDriver_2, "TextLabel12_2" );

    Layout63_2->addWidget( TextLabel12_2, 2, 0 );

    trackerFecDDStatus = new QLineEdit( DeviceDriver_2, "trackerFecDDStatus" );
    trackerFecDDStatus->setAlignment( int( QLineEdit::AlignHCenter ) );
    trackerFecDDStatus->setReadOnly( TRUE );

    Layout63_2->addWidget( trackerFecDDStatus, 0, 1 );

    TextLabel15_2 = new QLabel( DeviceDriver_2, "TextLabel15_2" );

    Layout63_2->addWidget( TextLabel15_2, 5, 0 );

    TextLabel13_2 = new QLabel( DeviceDriver_2, "TextLabel13_2" );

    Layout63_2->addWidget( TextLabel13_2, 3, 0 );

    TextLabel11_2 = new QLabel( DeviceDriver_2, "TextLabel11_2" );

    Layout63_2->addWidget( TextLabel11_2, 1, 0 );

    TextLabel14_2 = new QLabel( DeviceDriver_2, "TextLabel14_2" );

    Layout63_2->addWidget( TextLabel14_2, 4, 0 );

    trackerFecLongFrame = new QLineEdit( DeviceDriver_2, "trackerFecLongFrame" );
    trackerFecLongFrame->setAlignment( int( QLineEdit::AlignHCenter ) );
    trackerFecLongFrame->setReadOnly( TRUE );

    Layout63_2->addWidget( trackerFecLongFrame, 1, 1 );

    TextLabel10_2 = new QLabel( DeviceDriver_2, "TextLabel10_2" );

    Layout63_2->addWidget( TextLabel10_2, 0, 0 );

    trackerFecNbFecReset = new QLineEdit( DeviceDriver_2, "trackerFecNbFecReset" );
    trackerFecNbFecReset->setAlignment( int( QLineEdit::AlignHCenter ) );
    trackerFecNbFecReset->setReadOnly( TRUE );

    Layout63_2->addWidget( trackerFecNbFecReset, 6, 1 );

    trackerFecShortFrame = new QLineEdit( DeviceDriver_2, "trackerFecShortFrame" );
    trackerFecShortFrame->setAlignment( int( QLineEdit::AlignHCenter ) );
    trackerFecShortFrame->setReadOnly( TRUE );

    Layout63_2->addWidget( trackerFecShortFrame, 2, 1 );

    DeviceDriver_2Layout->addLayout( Layout63_2, 1, 0 );

    Layout66_2 = new QHBoxLayout( 0, 0, 6, "Layout66_2"); 

    trackerFecEnableErrorCounter = new QCheckBox( DeviceDriver_2, "trackerFecEnableErrorCounter" );
    trackerFecEnableErrorCounter->setChecked( FALSE );
    Layout66_2->addWidget( trackerFecEnableErrorCounter );

    trackerFecResetCounter = new QPushButton( DeviceDriver_2, "trackerFecResetCounter" );
    Layout66_2->addWidget( trackerFecResetCounter );

    DeviceDriver_2Layout->addLayout( Layout66_2, 0, 0 );

    trackerFecReadDeviceDriver = new QPushButton( DeviceDriver_2, "trackerFecReadDeviceDriver" );

    DeviceDriver_2Layout->addWidget( trackerFecReadDeviceDriver, 2, 0 );
    layout195->addWidget( DeviceDriver_2 );
    layout196->addLayout( layout195 );

    layout191 = new QVBoxLayout( 0, 0, 6, "layout191"); 

    TRACKERPIADATAREGISTER = new QGroupBox( tab_6, "TRACKERPIADATAREGISTER" );
    TRACKERPIADATAREGISTER->setColumnLayout(0, Qt::Vertical );
    TRACKERPIADATAREGISTER->layout()->setSpacing( 6 );
    TRACKERPIADATAREGISTER->layout()->setMargin( 11 );
    TRACKERPIADATAREGISTERLayout = new QGridLayout( TRACKERPIADATAREGISTER->layout() );
    TRACKERPIADATAREGISTERLayout->setAlignment( Qt::AlignTop );

    TextLabel5_2 = new QLabel( TRACKERPIADATAREGISTER, "TextLabel5_2" );

    TRACKERPIADATAREGISTERLayout->addMultiCellWidget( TextLabel5_2, 2, 2, 0, 1 );

    TextLabel4_2 = new QLabel( TRACKERPIADATAREGISTER, "TextLabel4_2" );

    TRACKERPIADATAREGISTERLayout->addMultiCellWidget( TextLabel4_2, 3, 3, 0, 1 );

    trackerPiaEnableOperationBit = new QCheckBox( TRACKERPIADATAREGISTER, "trackerPiaEnableOperationBit" );
    trackerPiaEnableOperationBit->setEnabled( FALSE );
    trackerPiaEnableOperationBit->setChecked( TRUE );

    TRACKERPIADATAREGISTERLayout->addMultiCellWidget( trackerPiaEnableOperationBit, 1, 1, 0, 3 );

    TextLabel26_2_3 = new QLabel( TRACKERPIADATAREGISTER, "TextLabel26_2_3" );

    TRACKERPIADATAREGISTERLayout->addWidget( TextLabel26_2_3, 4, 3 );

    TextLabel2_3_2_3_3 = new QLabel( TRACKERPIADATAREGISTER, "TextLabel2_3_2_3_3" );
    TextLabel2_3_2_3_3->setAlignment( int( QLabel::AlignCenter ) );

    TRACKERPIADATAREGISTERLayout->addWidget( TextLabel2_3_2_3_3, 4, 0 );

    TextLabel2_5_2_3_3 = new QLabel( TRACKERPIADATAREGISTER, "TextLabel2_5_2_3_3" );
    TextLabel2_5_2_3_3->setAlignment( int( QLabel::AlignCenter ) );

    TRACKERPIADATAREGISTERLayout->addMultiCellWidget( TextLabel2_5_2_3_3, 4, 4, 1, 2 );

    trackerPiaDuration = new QLineEdit( TRACKERPIADATAREGISTER, "trackerPiaDuration" );
    trackerPiaDuration->setEnabled( TRUE );

    TRACKERPIADATAREGISTERLayout->addMultiCellWidget( trackerPiaDuration, 2, 2, 2, 3 );

    trackerPiaSleepTime = new QLineEdit( TRACKERPIADATAREGISTER, "trackerPiaSleepTime" );
    trackerPiaSleepTime->setEnabled( TRUE );

    TRACKERPIADATAREGISTERLayout->addMultiCellWidget( trackerPiaSleepTime, 3, 3, 2, 3 );

    Layout71_2 = new QHBoxLayout( 0, 0, 6, "Layout71_2"); 

    trackerSelectAllPiaB = new QPushButton( TRACKERPIADATAREGISTER, "trackerSelectAllPiaB" );
    Layout71_2->addWidget( trackerSelectAllPiaB );

    trackerPiaDataRegisterWrite = new QPushButton( TRACKERPIADATAREGISTER, "trackerPiaDataRegisterWrite" );
    Layout71_2->addWidget( trackerPiaDataRegisterWrite );

    TRACKERPIADATAREGISTERLayout->addMultiCellLayout( Layout71_2, 8, 8, 0, 3 );

    Layout250 = new QHBoxLayout( 0, 0, 6, "Layout250"); 

    trackerPiaResetAdd = new QPushButton( TRACKERPIADATAREGISTER, "trackerPiaResetAdd" );
    Layout250->addWidget( trackerPiaResetAdd );

    trackerClearListPiaReset = new QPushButton( TRACKERPIADATAREGISTER, "trackerClearListPiaReset" );
    Layout250->addWidget( trackerClearListPiaReset );

    trackerPiaResetDelete = new QPushButton( TRACKERPIADATAREGISTER, "trackerPiaResetDelete" );
    Layout250->addWidget( trackerPiaResetDelete );

    TRACKERPIADATAREGISTERLayout->addMultiCellLayout( Layout250, 6, 6, 0, 3 );

    trackerListPiaReset = new QListBox( TRACKERPIADATAREGISTER, "trackerListPiaReset" );
    trackerListPiaReset->setVScrollBarMode( QListBox::AlwaysOn );
    trackerListPiaReset->setSelectionMode( QListBox::Extended );

    TRACKERPIADATAREGISTERLayout->addMultiCellWidget( trackerListPiaReset, 7, 7, 0, 3 );

    trackerPiaDataRegister = new QLineEdit( TRACKERPIADATAREGISTER, "trackerPiaDataRegister" );
    trackerPiaDataRegister->setAlignment( int( QLineEdit::AlignHCenter ) );

    TRACKERPIADATAREGISTERLayout->addMultiCellWidget( trackerPiaDataRegister, 0, 0, 0, 2 );

    trackerPiaResetAddAllCCU = new QPushButton( TRACKERPIADATAREGISTER, "trackerPiaResetAddAllCCU" );

    TRACKERPIADATAREGISTERLayout->addWidget( trackerPiaResetAddAllCCU, 0, 3 );

    trackerPiaFecSlots = new QComboBox( FALSE, TRACKERPIADATAREGISTER, "trackerPiaFecSlots" );

    TRACKERPIADATAREGISTERLayout->addWidget( trackerPiaFecSlots, 5, 0 );

    trackerPiaCcuAddresses = new QComboBox( FALSE, TRACKERPIADATAREGISTER, "trackerPiaCcuAddresses" );

    TRACKERPIADATAREGISTERLayout->addMultiCellWidget( trackerPiaCcuAddresses, 5, 5, 1, 2 );

    trackerPiaChannels = new QComboBox( FALSE, TRACKERPIADATAREGISTER, "trackerPiaChannels" );

    TRACKERPIADATAREGISTERLayout->addWidget( trackerPiaChannels, 5, 3 );
    layout191->addWidget( TRACKERPIADATAREGISTER );

    GroupBox175 = new QGroupBox( tab_6, "GroupBox175" );
    GroupBox175->setColumnLayout(0, Qt::Vertical );
    GroupBox175->layout()->setSpacing( 6 );
    GroupBox175->layout()->setMargin( 11 );
    GroupBox175Layout = new QGridLayout( GroupBox175->layout() );
    GroupBox175Layout->setAlignment( Qt::AlignTop );

    TextLabel6_14 = new QLabel( GroupBox175, "TextLabel6_14" );

    GroupBox175Layout->addWidget( TextLabel6_14, 0, 0 );

    apvMuxResistorT = new QLineEdit( GroupBox175, "apvMuxResistorT" );
    apvMuxResistorT->setAlignment( int( QLineEdit::AlignHCenter ) );

    GroupBox175Layout->addWidget( apvMuxResistorT, 0, 1 );

    Layout42 = new QHBoxLayout( 0, 0, 6, "Layout42"); 

    trackerApvMuxRead = new QPushButton( GroupBox175, "trackerApvMuxRead" );
    Layout42->addWidget( trackerApvMuxRead );

    trackerApvMuxWrite = new QPushButton( GroupBox175, "trackerApvMuxWrite" );
    Layout42->addWidget( trackerApvMuxWrite );

    GroupBox175Layout->addLayout( Layout42, 1, 1 );
    layout191->addWidget( GroupBox175 );

    GroupBox139 = new QGroupBox( tab_6, "GroupBox139" );
    GroupBox139->setColumnLayout(0, Qt::Vertical );
    GroupBox139->layout()->setSpacing( 6 );
    GroupBox139->layout()->setMargin( 11 );
    GroupBox139Layout = new QGridLayout( GroupBox139->layout() );
    GroupBox139Layout->setAlignment( Qt::AlignTop );

    Layout54 = new QGridLayout( 0, 1, 1, 0, 6, "Layout54"); 

    apvErrorT = new QLineEdit( GroupBox139, "apvErrorT" );
    apvErrorT->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout54->addWidget( apvErrorT, 8, 3 );

    TextLabel6_5_3 = new QLabel( GroupBox139, "TextLabel6_5_3" );

    Layout54->addWidget( TextLabel6_5_3, 8, 2 );

    TextLabel6_4_2 = new QLabel( GroupBox139, "TextLabel6_4_2" );

    Layout54->addWidget( TextLabel6_4_2, 5, 2 );

    TextLabel6_12 = new QLabel( GroupBox139, "TextLabel6_12" );

    Layout54->addWidget( TextLabel6_12, 7, 2 );

    apvIshaT = new QLineEdit( GroupBox139, "apvIshaT" );
    apvIshaT->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout54->addWidget( apvIshaT, 3, 1 );

    apvVfpT = new QLineEdit( GroupBox139, "apvVfpT" );
    apvVfpT->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout54->addWidget( apvVfpT, 6, 1 );

    apvModeT = new QLineEdit( GroupBox139, "apvModeT" );
    apvModeT->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout54->addWidget( apvModeT, 0, 1 );

    TextLabel6_3 = new QLabel( GroupBox139, "TextLabel6_3" );

    Layout54->addWidget( TextLabel6_3, 2, 2 );

    apvLatencyT = new QLineEdit( GroupBox139, "apvLatencyT" );
    apvLatencyT->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout54->addWidget( apvLatencyT, 0, 3 );

    apvIssfT = new QLineEdit( GroupBox139, "apvIssfT" );
    apvIssfT->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout54->addWidget( apvIssfT, 3, 3 );

    apvVfsT = new QLineEdit( GroupBox139, "apvVfsT" );
    apvVfsT->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout54->addWidget( apvVfsT, 6, 3 );

    TextLabel6_4_3 = new QLabel( GroupBox139, "TextLabel6_4_3" );

    Layout54->addWidget( TextLabel6_4_3, 8, 0 );

    TextLabel6_9_2 = new QLabel( GroupBox139, "TextLabel6_9_2" );

    Layout54->addWidget( TextLabel6_9_2, 7, 0 );

    TextLabel6_11 = new QLabel( GroupBox139, "TextLabel6_11" );

    Layout54->addWidget( TextLabel6_11, 5, 0 );

    apvMuxGainT = new QLineEdit( GroupBox139, "apvMuxGainT" );
    apvMuxGainT->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout54->addWidget( apvMuxGainT, 1, 1 );

    TextLabel6_2 = new QLabel( GroupBox139, "TextLabel6_2" );

    Layout54->addWidget( TextLabel6_2, 0, 2 );

    apvIpspT = new QLineEdit( GroupBox139, "apvIpspT" );
    apvIpspT->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout54->addWidget( apvIpspT, 4, 1 );

    apvVpspT = new QLineEdit( GroupBox139, "apvVpspT" );
    apvVpspT->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout54->addWidget( apvVpspT, 7, 1 );

    TextLabel6_5_2 = new QLabel( GroupBox139, "TextLabel6_5_2" );

    Layout54->addWidget( TextLabel6_5_2, 6, 0 );

    TextLabel6_7 = new QLabel( GroupBox139, "TextLabel6_7" );

    Layout54->addWidget( TextLabel6_7, 3, 0 );

    TextLabel6_6 = new QLabel( GroupBox139, "TextLabel6_6" );

    Layout54->addWidget( TextLabel6_6, 1, 2 );

    apvIpreT = new QLineEdit( GroupBox139, "apvIpreT" );
    apvIpreT->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout54->addWidget( apvIpreT, 1, 3 );

    apvImuxinT = new QLineEdit( GroupBox139, "apvImuxinT" );
    apvImuxinT->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout54->addWidget( apvImuxinT, 4, 3 );

    apvCdrvT = new QLineEdit( GroupBox139, "apvCdrvT" );
    apvCdrvT->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout54->addWidget( apvCdrvT, 7, 3 );

    TextLabel6_7_2 = new QLabel( GroupBox139, "TextLabel6_7_2" );

    Layout54->addWidget( TextLabel6_7_2, 6, 2 );

    TextLabel6_10 = new QLabel( GroupBox139, "TextLabel6_10" );

    Layout54->addWidget( TextLabel6_10, 4, 2 );

    apvIpcascT = new QLineEdit( GroupBox139, "apvIpcascT" );
    apvIpcascT->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout54->addWidget( apvIpcascT, 2, 1 );

    TextLabel6_9 = new QLabel( GroupBox139, "TextLabel6_9" );

    Layout54->addWidget( TextLabel6_9, 4, 0 );

    apvIcalT = new QLineEdit( GroupBox139, "apvIcalT" );
    apvIcalT->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout54->addWidget( apvIcalT, 5, 1 );

    TextLabel6_8 = new QLabel( GroupBox139, "TextLabel6_8" );

    Layout54->addWidget( TextLabel6_8, 3, 2 );

    apvCselT = new QLineEdit( GroupBox139, "apvCselT" );
    apvCselT->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout54->addWidget( apvCselT, 8, 1 );

    TextLabel6_5 = new QLabel( GroupBox139, "TextLabel6_5" );

    Layout54->addWidget( TextLabel6_5, 2, 0 );

    apvIpsfT = new QLineEdit( GroupBox139, "apvIpsfT" );
    apvIpsfT->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout54->addWidget( apvIpsfT, 2, 3 );

    apvIspareT = new QLineEdit( GroupBox139, "apvIspareT" );
    apvIspareT->setAlignment( int( QLineEdit::AlignHCenter ) );

    Layout54->addWidget( apvIspareT, 5, 3 );

    TextLabel6_4 = new QLabel( GroupBox139, "TextLabel6_4" );

    Layout54->addWidget( TextLabel6_4, 1, 0 );

    TextLabel6 = new QLabel( GroupBox139, "TextLabel6" );

    Layout54->addWidget( TextLabel6, 0, 0 );

    GroupBox139Layout->addLayout( Layout54, 0, 0 );

    Layout34_3 = new QHBoxLayout( 0, 0, 6, "Layout34_3"); 

    trackerApvRead = new QPushButton( GroupBox139, "trackerApvRead" );
    Layout34_3->addWidget( trackerApvRead );

    trackerApvWrite = new QPushButton( GroupBox139, "trackerApvWrite" );
    Layout34_3->addWidget( trackerApvWrite );

    GroupBox139Layout->addLayout( Layout34_3, 1, 0 );
    layout191->addWidget( GroupBox139 );
    layout196->addLayout( layout191 );

    layout193 = new QVBoxLayout( 0, 0, 6, "layout193"); 

    GroupBox175_2 = new QGroupBox( tab_6, "GroupBox175_2" );
    GroupBox175_2->setColumnLayout(0, Qt::Vertical );
    GroupBox175_2->layout()->setSpacing( 6 );
    GroupBox175_2->layout()->setMargin( 11 );
    GroupBox175_2Layout = new QGridLayout( GroupBox175_2->layout() );
    GroupBox175_2Layout->setAlignment( Qt::AlignTop );

    TextLabel6_4_3_3 = new QLabel( GroupBox175_2, "TextLabel6_4_3_3" );

    GroupBox175_2Layout->addWidget( TextLabel6_4_3_3, 0, 0 );

    TextLabel6_5_3_3 = new QLabel( GroupBox175_2, "TextLabel6_5_3_3" );

    GroupBox175_2Layout->addWidget( TextLabel6_5_3_3, 0, 2 );

    pllDelayCoarseT = new QLineEdit( GroupBox175_2, "pllDelayCoarseT" );
    pllDelayCoarseT->setAlignment( int( QLineEdit::AlignHCenter ) );

    GroupBox175_2Layout->addWidget( pllDelayCoarseT, 0, 3 );

    pllDelayFineT = new QLineEdit( GroupBox175_2, "pllDelayFineT" );
    pllDelayFineT->setAlignment( int( QLineEdit::AlignHCenter ) );

    GroupBox175_2Layout->addWidget( pllDelayFineT, 0, 1 );

    Layout41 = new QHBoxLayout( 0, 0, 6, "Layout41"); 

    trackerPllRead = new QPushButton( GroupBox175_2, "trackerPllRead" );
    Layout41->addWidget( trackerPllRead );

    trackerPllWrite = new QPushButton( GroupBox175_2, "trackerPllWrite" );
    Layout41->addWidget( trackerPllWrite );

    GroupBox175_2Layout->addMultiCellLayout( Layout41, 1, 1, 1, 3 );
    layout193->addWidget( GroupBox175_2 );

    GroupBox175_2_2 = new QGroupBox( tab_6, "GroupBox175_2_2" );
    GroupBox175_2_2->setColumnLayout(0, Qt::Vertical );
    GroupBox175_2_2->layout()->setSpacing( 6 );
    GroupBox175_2_2->layout()->setMargin( 11 );
    GroupBox175_2_2Layout = new QGridLayout( GroupBox175_2_2->layout() );
    GroupBox175_2_2Layout->setAlignment( Qt::AlignTop );

    Layout40 = new QHBoxLayout( 0, 0, 6, "Layout40"); 

    trackerLaserdriverRead = new QPushButton( GroupBox175_2_2, "trackerLaserdriverRead" );
    Layout40->addWidget( trackerLaserdriverRead );

    trackerLaserdriverWrite = new QPushButton( GroupBox175_2_2, "trackerLaserdriverWrite" );
    Layout40->addWidget( trackerLaserdriverWrite );

    GroupBox175_2_2Layout->addLayout( Layout40, 2, 0 );

    layout50 = new QGridLayout( 0, 1, 1, 0, 6, "layout50"); 

    laserdriverGain1T = new QLineEdit( GroupBox175_2_2, "laserdriverGain1T" );
    laserdriverGain1T->setAlignment( int( QLineEdit::AlignHCenter ) );

    layout50->addWidget( laserdriverGain1T, 0, 1 );

    TextLabel6_5_5 = new QLabel( GroupBox175_2_2, "TextLabel6_5_5" );

    layout50->addWidget( TextLabel6_5_5, 2, 0 );

    laserdriverGain3T = new QLineEdit( GroupBox175_2_2, "laserdriverGain3T" );
    laserdriverGain3T->setAlignment( int( QLineEdit::AlignHCenter ) );

    layout50->addWidget( laserdriverGain3T, 2, 1 );

    TextLabel6_6_3 = new QLabel( GroupBox175_2_2, "TextLabel6_6_3" );

    layout50->addWidget( TextLabel6_6_3, 1, 2 );

    TextLabel6_4_5 = new QLabel( GroupBox175_2_2, "TextLabel6_4_5" );

    layout50->addWidget( TextLabel6_4_5, 1, 0 );

    TextLabel6_15 = new QLabel( GroupBox175_2_2, "TextLabel6_15" );

    layout50->addWidget( TextLabel6_15, 0, 0 );

    laserdriverBias1T = new QLineEdit( GroupBox175_2_2, "laserdriverBias1T" );
    laserdriverBias1T->setAlignment( int( QLineEdit::AlignHCenter ) );

    layout50->addWidget( laserdriverBias1T, 0, 3 );

    TextLabel6_2_3 = new QLabel( GroupBox175_2_2, "TextLabel6_2_3" );

    layout50->addWidget( TextLabel6_2_3, 0, 2 );

    TextLabel6_3_3 = new QLabel( GroupBox175_2_2, "TextLabel6_3_3" );

    layout50->addWidget( TextLabel6_3_3, 2, 2 );

    laserdriverBias2T = new QLineEdit( GroupBox175_2_2, "laserdriverBias2T" );
    laserdriverBias2T->setAlignment( int( QLineEdit::AlignHCenter ) );

    layout50->addWidget( laserdriverBias2T, 1, 3 );

    laserdriverBias3T = new QLineEdit( GroupBox175_2_2, "laserdriverBias3T" );
    laserdriverBias3T->setAlignment( int( QLineEdit::AlignHCenter ) );

    layout50->addWidget( laserdriverBias3T, 2, 3 );

    laserdriverGain2T = new QLineEdit( GroupBox175_2_2, "laserdriverGain2T" );
    laserdriverGain2T->setAlignment( int( QLineEdit::AlignHCenter ) );

    layout50->addWidget( laserdriverGain2T, 1, 1 );

    GroupBox175_2_2Layout->addLayout( layout50, 1, 0 );

    TextLabel1_4 = new QLabel( GroupBox175_2_2, "TextLabel1_4" );
    TextLabel1_4->setPaletteForegroundColor( QColor( 255, 0, 0 ) );

    GroupBox175_2_2Layout->addWidget( TextLabel1_4, 0, 0 );
    layout193->addWidget( GroupBox175_2_2 );

    TRACKERDCU = new QGroupBox( tab_6, "TRACKERDCU" );
    TRACKERDCU->setColumnLayout(0, Qt::Vertical );
    TRACKERDCU->layout()->setSpacing( 6 );
    TRACKERDCU->layout()->setMargin( 11 );
    TRACKERDCULayout = new QGridLayout( TRACKERDCU->layout() );
    TRACKERDCULayout->setAlignment( Qt::AlignTop );

    TextLabel6_7_4 = new QLabel( TRACKERDCU, "TextLabel6_7_4" );

    TRACKERDCULayout->addWidget( TextLabel6_7_4, 4, 0 );

    TextLabel6_2_4 = new QLabel( TRACKERDCU, "TextLabel6_2_4" );

    TRACKERDCULayout->addWidget( TextLabel6_2_4, 1, 3 );

    dcuChannel4T = new QLineEdit( TRACKERDCU, "dcuChannel4T" );
    dcuChannel4T->setAlignment( int( QLineEdit::AlignHCenter ) );

    TRACKERDCULayout->addWidget( dcuChannel4T, 2, 4 );

    dcuChannel6Text = new QLabel( TRACKERDCU, "dcuChannel6Text" );

    TRACKERDCULayout->addWidget( dcuChannel6Text, 3, 3 );

    TextLabel6_5_6 = new QLabel( TRACKERDCU, "TextLabel6_5_6" );

    TRACKERDCULayout->addWidget( TextLabel6_5_6, 3, 0 );

    TextLabel6_4_6 = new QLabel( TRACKERDCU, "TextLabel6_4_6" );

    TRACKERDCULayout->addWidget( TextLabel6_4_6, 2, 0 );

    TextLabel6_8_3 = new QLabel( TRACKERDCU, "TextLabel6_8_3" );

    TRACKERDCULayout->addWidget( TextLabel6_8_3, 4, 3 );

    TextLabel6_17 = new QLabel( TRACKERDCU, "TextLabel6_17" );

    TRACKERDCULayout->addWidget( TextLabel6_17, 1, 0 );

    dcuChannel6T = new QLineEdit( TRACKERDCU, "dcuChannel6T" );
    dcuChannel6T->setAlignment( int( QLineEdit::AlignHCenter ) );

    TRACKERDCULayout->addWidget( dcuChannel6T, 3, 4 );

    TextLabel6_6_4 = new QLabel( TRACKERDCU, "TextLabel6_6_4" );

    TRACKERDCULayout->addWidget( TextLabel6_6_4, 2, 3 );

    dcuChannel7T = new QLineEdit( TRACKERDCU, "dcuChannel7T" );
    dcuChannel7T->setAlignment( int( QLineEdit::AlignHCenter ) );

    TRACKERDCULayout->addMultiCellWidget( dcuChannel7T, 4, 4, 1, 2 );

    dcuChannel5T = new QLineEdit( TRACKERDCU, "dcuChannel5T" );
    dcuChannel5T->setAlignment( int( QLineEdit::AlignHCenter ) );

    TRACKERDCULayout->addMultiCellWidget( dcuChannel5T, 3, 3, 1, 2 );

    dcuChannel2T = new QLineEdit( TRACKERDCU, "dcuChannel2T" );
    dcuChannel2T->setAlignment( int( QLineEdit::AlignHCenter ) );

    TRACKERDCULayout->addWidget( dcuChannel2T, 1, 4 );

    TextLabel6_16 = new QLabel( TRACKERDCU, "TextLabel6_16" );

    TRACKERDCULayout->addMultiCellWidget( TextLabel6_16, 0, 0, 0, 1 );

    dcuHardIdT = new QLineEdit( TRACKERDCU, "dcuHardIdT" );
    dcuHardIdT->setAlignment( int( QLineEdit::AlignHCenter ) );

    TRACKERDCULayout->addMultiCellWidget( dcuHardIdT, 0, 0, 2, 4 );

    dcuChannel8T = new QLineEdit( TRACKERDCU, "dcuChannel8T" );
    dcuChannel8T->setAlignment( int( QLineEdit::AlignHCenter ) );

    TRACKERDCULayout->addWidget( dcuChannel8T, 4, 4 );

    trackerDcuRead = new QPushButton( TRACKERDCU, "trackerDcuRead" );
    trackerDcuRead->setDefault( FALSE );

    TRACKERDCULayout->addMultiCellWidget( trackerDcuRead, 5, 5, 2, 4 );

    dcuChannel3T = new QLineEdit( TRACKERDCU, "dcuChannel3T" );
    dcuChannel3T->setAlignment( int( QLineEdit::AlignHCenter ) );

    TRACKERDCULayout->addMultiCellWidget( dcuChannel3T, 2, 2, 1, 2 );

    dcuChannel1T = new QLineEdit( TRACKERDCU, "dcuChannel1T" );
    dcuChannel1T->setAlignment( int( QLineEdit::AlignHCenter ) );

    TRACKERDCULayout->addMultiCellWidget( dcuChannel1T, 1, 1, 1, 2 );
    layout193->addWidget( TRACKERDCU );

    GroupBox181 = new QGroupBox( tab_6, "GroupBox181" );
    GroupBox181->setColumnLayout(0, Qt::Vertical );
    GroupBox181->layout()->setSpacing( 6 );
    GroupBox181->layout()->setMargin( 11 );
    GroupBox181Layout = new QGridLayout( GroupBox181->layout() );
    GroupBox181Layout->setAlignment( Qt::AlignTop );

    trackerUseFile = new QRadioButton( GroupBox181, "trackerUseFile" );
    trackerUseFile->setEnabled( TRUE );
    trackerUseFile->setChecked( TRUE );

    GroupBox181Layout->addWidget( trackerUseFile, 0, 0 );

    trackerFileName = new QLineEdit( GroupBox181, "trackerFileName" );
    trackerFileName->setEnabled( TRUE );

    GroupBox181Layout->addMultiCellWidget( trackerFileName, 0, 0, 1, 2 );

    trackerUseDatabase = new QRadioButton( GroupBox181, "trackerUseDatabase" );
    trackerUseDatabase->setEnabled( FALSE );

    GroupBox181Layout->addMultiCellWidget( trackerUseDatabase, 1, 1, 0, 1 );

    trackerDatabaseVersion = new QListBox( GroupBox181, "trackerDatabaseVersion" );
    trackerDatabaseVersion->setEnabled( FALSE );
    trackerDatabaseVersion->setVScrollBarMode( QListBox::AlwaysOn );
    trackerDatabaseVersion->setSelectionMode( QListBox::Extended );

    GroupBox181Layout->addMultiCellWidget( trackerDatabaseVersion, 1, 2, 2, 2 );
    Spacer16 = new QSpacerItem( 20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
    GroupBox181Layout->addMultiCell( Spacer16, 2, 2, 0, 1 );

    Layout55 = new QHBoxLayout( 0, 0, 6, "Layout55"); 

    trackerFindFile = new QPushButton( GroupBox181, "trackerFindFile" );
    trackerFindFile->setEnabled( TRUE );
    Layout55->addWidget( trackerFindFile );

    trackerXMLDownload = new QPushButton( GroupBox181, "trackerXMLDownload" );
    trackerXMLDownload->setEnabled( TRUE );
    Layout55->addWidget( trackerXMLDownload );

    trackerXMLUpload = new QPushButton( GroupBox181, "trackerXMLUpload" );
    trackerXMLUpload->setEnabled( TRUE );
    Layout55->addWidget( trackerXMLUpload );

    GroupBox181Layout->addMultiCellLayout( Layout55, 4, 4, 0, 2 );

    Layout47 = new QGridLayout( 0, 1, 1, 0, 6, "Layout47"); 

    trackerStructureName = new QLineEdit( GroupBox181, "trackerStructureName" );
    trackerStructureName->setEnabled( FALSE );

    Layout47->addWidget( trackerStructureName, 0, 1 );

    trackerPartitionName = new QLineEdit( GroupBox181, "trackerPartitionName" );
    trackerPartitionName->setEnabled( FALSE );

    Layout47->addWidget( trackerPartitionName, 1, 1 );

    textLabelStructureName = new QLabel( GroupBox181, "textLabelStructureName" );
    textLabelStructureName->setEnabled( FALSE );

    Layout47->addWidget( textLabelStructureName, 0, 0 );

    textLabelPartitionName = new QLabel( GroupBox181, "textLabelPartitionName" );
    textLabelPartitionName->setEnabled( FALSE );

    Layout47->addWidget( textLabelPartitionName, 1, 0 );

    GroupBox181Layout->addMultiCellLayout( Layout47, 3, 3, 0, 2 );
    layout193->addWidget( GroupBox181 );
    layout196->addLayout( layout193 );
    tabLayout->addLayout( layout196 );
    FEC->insertTab( tab_6, QString::fromLatin1("") );

    tab_7 = new QWidget( FEC, "tab_7" );

    TextLabel2_6 = new QLabel( tab_7, "TextLabel2_6" );
    TextLabel2_6->setGeometry( QRect( 12, 21, 58, 32 ) );
    TextLabel2_6->setAlignment( int( QLabel::AlignCenter ) );

    fecSlotsCcuRedundancy = new QComboBox( FALSE, tab_7, "fecSlotsCcuRedundancy" );
    fecSlotsCcuRedundancy->setGeometry( QRect( 76, 23, 92, 27 ) );

    CCURedundancyClearButton = new QPushButton( tab_7, "CCURedundancyClearButton" );
    CCURedundancyClearButton->setGeometry( QRect( 260, 21, 80, 32 ) );

    TextLabel2_5_3 = new QLabel( tab_7, "TextLabel2_5_3" );
    TextLabel2_5_3->setGeometry( QRect( 455, 21, 79, 32 ) );
    TextLabel2_5_3->setAlignment( int( QLabel::AlignCenter ) );

    ccuRedundancyIRQEnableButton = new QCheckBox( tab_7, "ccuRedundancyIRQEnableButton" );
    ccuRedundancyIRQEnableButton->setGeometry( QRect( 846, 26, 94, 21 ) );

    ccuRedundancyAddresses = new QComboBox( FALSE, tab_7, "ccuRedundancyAddresses" );
    ccuRedundancyAddresses->setGeometry( QRect( 540, 23, 92, 27 ) );

    ccuRedundancyReadDDStatusButton = new QPushButton( tab_7, "ccuRedundancyReadDDStatusButton" );
    ccuRedundancyReadDDStatusButton->setGeometry( QRect( 760, 20, 81, 32 ) );
    ccuRedundancyReadDDStatusButton->setMinimumSize( QSize( 0, 0 ) );
    ccuRedundancyReadDDStatusButton->setMaximumSize( QSize( 32767, 32767 ) );

    CCURedundancyFecInputGroup = new QButtonGroup( tab_7, "CCURedundancyFecInputGroup" );
    CCURedundancyFecInputGroup->setGeometry( QRect( 800, 70, 110, 114 ) );
    CCURedundancyFecInputGroup->setMinimumSize( QSize( 110, 114 ) );
    CCURedundancyFecInputGroup->setMaximumSize( QSize( 110, 114 ) );
    CCURedundancyFecInputGroup->setPaletteBackgroundColor( QColor( 255, 255, 230 ) );
    CCURedundancyFecInputGroup->setColumnLayout(0, Qt::Vertical );
    CCURedundancyFecInputGroup->layout()->setSpacing( 6 );
    CCURedundancyFecInputGroup->layout()->setMargin( 11 );
    CCURedundancyFecInputGroupLayout = new QVBoxLayout( CCURedundancyFecInputGroup->layout() );
    CCURedundancyFecInputGroupLayout->setAlignment( Qt::AlignTop );

    textLabel4_3 = new QLabel( CCURedundancyFecInputGroup, "textLabel4_3" );
    CCURedundancyFecInputGroupLayout->addWidget( textLabel4_3 );

    CCURedundancyFecInputAButton = new QRadioButton( CCURedundancyFecInputGroup, "CCURedundancyFecInputAButton" );
    CCURedundancyFecInputAButton->setChecked( TRUE );
    CCURedundancyFecInputGroupLayout->addWidget( CCURedundancyFecInputAButton );

    CCURedundancyFecInputBButton = new QRadioButton( CCURedundancyFecInputGroup, "CCURedundancyFecInputBButton" );
    CCURedundancyFecInputGroupLayout->addWidget( CCURedundancyFecInputBButton );

    ccuRedundancyScanForCcus = new QPushButton( tab_7, "ccuRedundancyScanForCcus" );
    ccuRedundancyScanForCcus->setGeometry( QRect( 346, 21, 103, 32 ) );

    CCURedundancyCacheButton = new QPushButton( tab_7, "CCURedundancyCacheButton" );
    CCURedundancyCacheButton->setGeometry( QRect( 174, 21, 80, 32 ) );

    CCURedundancyTestRingB = new QPushButton( tab_7, "CCURedundancyTestRingB" );
    CCURedundancyTestRingB->setGeometry( QRect( 650, 20, 85, 32 ) );
    CCURedundancyTestRingB->setCursor( QCursor( 0 ) );

    CCURedundancyFecOutputGroup = new QButtonGroup( tab_7, "CCURedundancyFecOutputGroup" );
    CCURedundancyFecOutputGroup->setGeometry( QRect( 40, 70, 110, 114 ) );
    CCURedundancyFecOutputGroup->setMinimumSize( QSize( 110, 114 ) );
    CCURedundancyFecOutputGroup->setMaximumSize( QSize( 110, 114 ) );
    CCURedundancyFecOutputGroup->setPaletteBackgroundColor( QColor( 255, 255, 230 ) );
    CCURedundancyFecOutputGroup->setColumnLayout(0, Qt::Vertical );
    CCURedundancyFecOutputGroup->layout()->setSpacing( 6 );
    CCURedundancyFecOutputGroup->layout()->setMargin( 11 );
    CCURedundancyFecOutputGroupLayout = new QVBoxLayout( CCURedundancyFecOutputGroup->layout() );
    CCURedundancyFecOutputGroupLayout->setAlignment( Qt::AlignTop );

    textLabel4_2_2_2 = new QLabel( CCURedundancyFecOutputGroup, "textLabel4_2_2_2" );
    CCURedundancyFecOutputGroupLayout->addWidget( textLabel4_2_2_2 );

    CCURedundancyFecOutputAButton = new QRadioButton( CCURedundancyFecOutputGroup, "CCURedundancyFecOutputAButton" );
    CCURedundancyFecOutputAButton->setChecked( TRUE );
    CCURedundancyFecOutputGroupLayout->addWidget( CCURedundancyFecOutputAButton );

    CCURedundancyFecOutputBButton = new QRadioButton( CCURedundancyFecOutputGroup, "CCURedundancyFecOutputBButton" );
    CCURedundancyFecOutputGroupLayout->addWidget( CCURedundancyFecOutputBButton );

    CCURedundancyAddCCU = new QGroupBox( tab_7, "CCURedundancyAddCCU" );
    CCURedundancyAddCCU->setGeometry( QRect( 780, 650, 170, 172 ) );
    CCURedundancyAddCCU->setColumnLayout(0, Qt::Vertical );
    CCURedundancyAddCCU->layout()->setSpacing( 6 );
    CCURedundancyAddCCU->layout()->setMargin( 11 );
    CCURedundancyAddCCULayout = new QVBoxLayout( CCURedundancyAddCCU->layout() );
    CCURedundancyAddCCULayout->setAlignment( Qt::AlignTop );

    layout145 = new QHBoxLayout( 0, 0, 6, "layout145"); 

    textLabel5 = new QLabel( CCURedundancyAddCCU, "textLabel5" );
    layout145->addWidget( textLabel5 );

    CCURedundancyCCUHandCCUAddress = new QLineEdit( CCURedundancyAddCCU, "CCURedundancyCCUHandCCUAddress" );
    layout145->addWidget( CCURedundancyCCUHandCCUAddress );
    CCURedundancyAddCCULayout->addLayout( layout145 );

    CCURedundancyCCUHandAddCCUButton = new QPushButton( CCURedundancyAddCCU, "CCURedundancyCCUHandAddCCUButton" );
    CCURedundancyAddCCULayout->addWidget( CCURedundancyCCUHandAddCCUButton );

    CCURedundancySave = new QPushButton( CCURedundancyAddCCU, "CCURedundancySave" );
    CCURedundancyAddCCULayout->addWidget( CCURedundancySave );

    CCURedundancyLoad = new QPushButton( CCURedundancyAddCCU, "CCURedundancyLoad" );
    CCURedundancyAddCCULayout->addWidget( CCURedundancyLoad );

    buttonGroup1 = new QButtonGroup( tab_7, "buttonGroup1" );
    buttonGroup1->setGeometry( QRect( 4, 650, 770, 170 ) );
    buttonGroup1->setColumnLayout(0, Qt::Vertical );
    buttonGroup1->layout()->setSpacing( 6 );
    buttonGroup1->layout()->setMargin( 11 );
    buttonGroup1Layout = new QGridLayout( buttonGroup1->layout() );
    buttonGroup1Layout->setAlignment( Qt::AlignTop );

    ccuRedundancyErrorText = new QListBox( buttonGroup1, "ccuRedundancyErrorText" );
    ccuRedundancyErrorText->setAcceptDrops( FALSE );
    ccuRedundancyErrorText->setFrameShadow( QListBox::Sunken );
    ccuRedundancyErrorText->setVScrollBarMode( QListBox::Auto );
    ccuRedundancyErrorText->setHScrollBarMode( QListBox::Auto );
    ccuRedundancyErrorText->setDragAutoScroll( TRUE );
    ccuRedundancyErrorText->setSelectionMode( QListBox::NoSelection );

    buttonGroup1Layout->addMultiCellWidget( ccuRedundancyErrorText, 0, 1, 0, 0 );

    ccuRedundancyClearErrorMessages = new QPushButton( buttonGroup1, "ccuRedundancyClearErrorMessages" );

    buttonGroup1Layout->addWidget( ccuRedundancyClearErrorMessages, 1, 1 );
    spacer63 = new QSpacerItem( 20, 61, QSizePolicy::Minimum, QSizePolicy::Expanding );
    buttonGroup1Layout->addItem( spacer63, 0, 1 );

    ccuOrderLabel = new QLabel( buttonGroup1, "ccuOrderLabel" );

    buttonGroup1Layout->addMultiCellWidget( ccuOrderLabel, 2, 2, 0, 1 );
    FEC->insertTab( tab_7, QString::fromLatin1("") );
    languageChange();
    resize( QSize(1211, 878).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // signals and slots connections
    connect( about, SIGNAL( pressed() ), this, SLOT( helpAbout() ) );
    connect( about, SIGNAL( pressed() ), about, SLOT( setFocus() ) );
    connect( ccuAddresses, SIGNAL( activated(int) ), this, SLOT( ccuSelected() ) );
    connect( ccuAddresses, SIGNAL( activated(int) ), ccuAddresses, SLOT( setFocus() ) );
    connect( ccuAllEnable, SIGNAL( pressed() ), this, SLOT( ccuEnableAllChannels() ) );
    connect( ccuAllEnable, SIGNAL( pressed() ), ccuAllEnable, SLOT( setFocus() ) );
    connect( ccuAllRead, SIGNAL( pressed() ), this, SLOT( ccuReadAll() ) );
    connect( ccuAllRead, SIGNAL( pressed() ), ccuAllRead, SLOT( setFocus() ) );
    connect( ccuCRARead, SIGNAL( pressed() ), this, SLOT( ccuReadCRA() ) );
    connect( ccuCRARead, SIGNAL( pressed() ), ccuCRARead, SLOT( setFocus() ) );
    connect( ccuCRAWrite, SIGNAL( pressed() ), this, SLOT( ccuWriteCRA() ) );
    connect( ccuCRAWrite, SIGNAL( pressed() ), ccuCRAWrite, SLOT( setFocus() ) );
    connect( ccuCRAWriteBit, SIGNAL( pressed() ), this, SLOT( ccuWriteCRABit() ) );
    connect( ccuCRAWriteBit, SIGNAL( pressed() ), ccuCRAWriteBit, SLOT( setFocus() ) );
    connect( ccuCRBRead, SIGNAL( pressed() ), this, SLOT( ccuReadCRB() ) );
    connect( ccuCRBRead, SIGNAL( pressed() ), ccuCRBRead, SLOT( setFocus() ) );
    connect( ccuCRBWrite, SIGNAL( pressed() ), this, SLOT( ccuWriteCRB() ) );
    connect( ccuCRBWrite, SIGNAL( pressed() ), ccuCRBWrite, SLOT( setFocus() ) );
    connect( ccuCRBWriteBit, SIGNAL( pressed() ), this, SLOT( ccuWriteCRBBit() ) );
    connect( ccuCRBWriteBit, SIGNAL( pressed() ), ccuCRBWriteBit, SLOT( setFocus() ) );
    connect( ccuCRCRead, SIGNAL( pressed() ), this, SLOT( ccuReadCRC() ) );
    connect( ccuCRCRead, SIGNAL( pressed() ), ccuCRCRead, SLOT( setFocus() ) );
    connect( ccuCRCWrite, SIGNAL( pressed() ), this, SLOT( ccuWriteCRC() ) );
    connect( ccuCRCWrite, SIGNAL( pressed() ), ccuCRCWrite, SLOT( setFocus() ) );
    connect( ccuCRCWriteBit, SIGNAL( pressed() ), this, SLOT( ccuWriteCRCBit() ) );
    connect( ccuCRCWriteBit, SIGNAL( pressed() ), ccuCRCWriteBit, SLOT( setFocus() ) );
    connect( ccuCRDRead, SIGNAL( pressed() ), this, SLOT( ccuReadCRD() ) );
    connect( ccuCRDRead, SIGNAL( pressed() ), ccuCRDRead, SLOT( setFocus() ) );
    connect( ccuCRDWrite, SIGNAL( pressed() ), this, SLOT( ccuWriteCRD() ) );
    connect( ccuCRDWrite, SIGNAL( pressed() ), ccuCRDWrite, SLOT( setFocus() ) );
    connect( ccuCRERead, SIGNAL( pressed() ), this, SLOT( ccuReadCRE() ) );
    connect( ccuCRERead, SIGNAL( pressed() ), ccuCRERead, SLOT( setFocus() ) );
    connect( ccuCREWrite, SIGNAL( pressed() ), this, SLOT( ccuWriteCRE() ) );
    connect( ccuCREWrite, SIGNAL( pressed() ), ccuCREWrite, SLOT( setFocus() ) );
    connect( ccuCREWriteBit, SIGNAL( pressed() ), this, SLOT( ccuWriteCREBit() ) );
    connect( ccuCREWriteBit, SIGNAL( pressed() ), ccuCREWriteBit, SLOT( setFocus() ) );
    connect( ccuFecSlots, SIGNAL( activated(int) ), ccuFecSlots, SLOT( setFocus() ) );
    connect( ccuFecSlots, SIGNAL( activated(int) ), this, SLOT( fecSlotsCcuModify() ) );
    connect( CCURedundancyCacheButton, SIGNAL( clicked() ), this, SLOT( ccuRedundancyCache() ) );
    connect( CCURedundancyCCUHandAddCCUButton, SIGNAL( pressed() ), this, SLOT( ccuRedundancyAddCcuHand() ) );
    connect( CCURedundancyCCUHandCCUAddress, SIGNAL( returnPressed() ), this, SLOT( ccuRedundancyAddCcuHand() ) );
    connect( CCURedundancyCCUHandCCUAddress, SIGNAL( returnPressed() ), this, SLOT( ccuRedundancyAddCcuHand() ) );
    connect( CCURedundancyClearButton, SIGNAL( pressed() ), this, SLOT( ccuRedundancyClearCCU() ) );
    connect( ccuRedundancyClearErrorMessages, SIGNAL( pressed() ), ccuRedundancyErrorText, SLOT( clear() ) );
    connect( ccuRedundancyClearErrorMessages, SIGNAL( pressed() ), ccuRedundancyClearErrorMessages, SLOT( toggle() ) );
    connect( CCURedundancyFecInputAButton, SIGNAL( clicked() ), this, SLOT( fecRedundancy() ) );
    connect( CCURedundancyFecInputBButton, SIGNAL( clicked() ), this, SLOT( fecRedundancy() ) );
    connect( CCURedundancyFecOutputAButton, SIGNAL( clicked() ), this, SLOT( fecRedundancy() ) );
    connect( CCURedundancyFecOutputBButton, SIGNAL( clicked() ), this, SLOT( fecRedundancy() ) );
    connect( CCURedundancyLoad, SIGNAL( clicked() ), this, SLOT( ccuRedundancyLoadCCU() ) );
    connect( ccuRedundancyReadDDStatusButton, SIGNAL( pressed() ), this, SLOT( fecReadDDStatus() ) );
    connect( ccuRedundancyReadDDStatusButton, SIGNAL( pressed() ), ccuRedundancyReadDDStatusButton, SLOT( setFocus() ) );
    connect( CCURedundancySave, SIGNAL( clicked() ), this, SLOT( ccuRedundanceSaveCCU() ) );
    connect( ccuRedundancyScanForCcus, SIGNAL( pressed() ), this, SLOT( scanForCcus() ) );
    connect( CCURedundancyTestRingB, SIGNAL( pressed() ), this, SLOT( ccuRedundancyTestRingB() ) );
    connect( ccuScanForCcus, SIGNAL( pressed() ), this, SLOT( scanForCcus() ) );
    connect( ccuScanForCcus, SIGNAL( pressed() ), ccuScanForCcus, SLOT( setFocus() ) );
    connect( ccuSRARead, SIGNAL( pressed() ), this, SLOT( ccuReadSRA() ) );
    connect( ccuSRARead, SIGNAL( pressed() ), ccuSRARead, SLOT( setFocus() ) );
    connect( ccuSRERead, SIGNAL( pressed() ), this, SLOT( ccuReadSRE() ) );
    connect( ccuSRERead, SIGNAL( pressed() ), ccuSRERead, SLOT( setFocus() ) );
    connect( ccuSRRead, SIGNAL( pressed() ), this, SLOT( ccuReadSR() ) );
    connect( ccuSRRead, SIGNAL( pressed() ), ccuSRRead, SLOT( setFocus() ) );
    connect( fecClearErrors, SIGNAL( pressed() ), fecClearErrors, SLOT( setFocus() ) );
    connect( fecClearFifoRecButton, SIGNAL( pressed() ), this, SLOT( fecEmptyFifoRec() ) );
    connect( fecClearFifoRetButton, SIGNAL( pressed() ), this, SLOT( fecEmptyFifoRet() ) );
    connect( fecClearFifoTraButton, SIGNAL( pressed() ), this, SLOT( fecEmptyFifoTra() ) );
    connect( fecCR0Bis, SIGNAL( returnPressed() ), this, SLOT( fecWriteCR0_() ) );
    connect( fecCR0InputLine, SIGNAL( returnPressed() ), this, SLOT( fecWriteCR0() ) );
    connect( fecCR0InputLine, SIGNAL( textChanged(const QString&) ), fecCR0InputLine, SLOT( setFocus() ) );
    connect( fecCR0Read, SIGNAL( pressed() ), this, SLOT( fecReadCR0() ) );
    connect( fecCR0Read, SIGNAL( pressed() ), fecCR0Read, SLOT( setFocus() ) );
    connect( fecCR0WriteBit, SIGNAL( pressed() ), this, SLOT( fecWriteCR0Bit() ) );
    connect( fecCR0WriteBit, SIGNAL( pressed() ), fecCR0WriteBit, SLOT( setFocus() ) );
    connect( fecCR0writeCR0, SIGNAL( pressed() ), this, SLOT( fecWriteCR0() ) );
    connect( fecCR0writeCR0, SIGNAL( pressed() ), fecCR0writeCR0, SLOT( setFocus() ) );
    connect( fecCR1InputLine, SIGNAL( returnPressed() ), this, SLOT( fecWriteCR1() ) );
    connect( fecCR1InputLine, SIGNAL( textChanged(const QString&) ), fecCR1InputLine, SLOT( setFocus() ) );
    connect( fecCR1Read, SIGNAL( pressed() ), this, SLOT( fecReadCR1() ) );
    connect( fecCR1Read, SIGNAL( pressed() ), fecCR1Read, SLOT( setFocus() ) );
    connect( fecCR1WriteBit, SIGNAL( pressed() ), this, SLOT( fecWriteCR1Bit() ) );
    connect( fecCR1WriteBit, SIGNAL( pressed() ), fecCR1WriteBit, SLOT( setFocus() ) );
    connect( fecCR1WriteCR1, SIGNAL( pressed() ), this, SLOT( fecWriteCR1() ) );
    connect( fecCR1WriteCR1, SIGNAL( pressed() ), fecCR1WriteCR1, SLOT( setFocus() ) );
    connect( fecDisableIRQ, SIGNAL( pressed() ), this, SLOT( ddIRQEnableDisable() ) );
    connect( fecDisableReceive, SIGNAL( pressed() ), this, SLOT( fecDisableReceiveFec() ) );
    connect( fecEnableErrorCounter, SIGNAL( pressed() ), this, SLOT( fecEnableCounters() ) );
    connect( fecEnableErrorCounter, SIGNAL( pressed() ), fecEnableErrorCounter, SLOT( setFocus() ) );
    connect( fecReadAll, SIGNAL( pressed() ), this, SLOT( fecReadAllRegisters() ) );
    connect( fecReadAll, SIGNAL( pressed() ), fecReadAll, SLOT( setFocus() ) );
    connect( fecReadCR0Bis, SIGNAL( pressed() ), this, SLOT( fecReadCR0() ) );
    connect( fecReadCR0Bis, SIGNAL( pressed() ), fecReadCR0Bis, SLOT( setFocus() ) );
    connect( fecReadDeviceDriver, SIGNAL( pressed() ), this, SLOT( fecReadDDStatus() ) );
    connect( fecReadDeviceDriver, SIGNAL( pressed() ), fecReadDeviceDriver, SLOT( setFocus() ) );
    connect( fecReadFifoRecWordButton, SIGNAL( pressed() ), this, SLOT( fecReadFifoRec() ) );
    connect( fecReadFifoRetWordButton, SIGNAL( pressed() ), this, SLOT( fecReadFifoRet() ) );
    connect( fecReadFifoTraWordButton, SIGNAL( released() ), this, SLOT( fecReadFifoTra() ) );
    connect( fecReadSR0Bis, SIGNAL( pressed() ), this, SLOT( fecReadSR0() ) );
    connect( fecReadSR0Bis, SIGNAL( pressed() ), fecReadSR0Bis, SLOT( setFocus() ) );
    connect( fecReadSR1Bis, SIGNAL( pressed() ), this, SLOT( fecReadSR1() ) );
    connect( fecReadSR1Bis, SIGNAL( pressed() ), fecReadSR1Bis, SLOT( setFocus() ) );
    connect( fecRelease, SIGNAL( pressed() ), this, SLOT( fecReleaseFec() ) );
    connect( fecRelease, SIGNAL( pressed() ), fecRelease, SLOT( setFocus() ) );
    connect( fecResetCounter, SIGNAL( pressed() ), this, SLOT( fecResetErrorCounter() ) );
    connect( fecResetCounter, SIGNAL( pressed() ), fecResetCounter, SLOT( setFocus() ) );
    connect( fecResetFec, SIGNAL( pressed() ), this, SLOT( fecSoftReset() ) );
    connect( fecResetFec, SIGNAL( pressed() ), fecResetFec, SLOT( setFocus() ) );
    connect( fecResetPlx, SIGNAL( pressed() ), this, SLOT( fecPlxReset() ) );
    connect( fecResetPlx, SIGNAL( pressed() ), fecResetPlx, SLOT( setFocus() ) );
    connect( fecScanForFECs, SIGNAL( pressed() ), this, SLOT( scanForFecs() ) );
    connect( fecScanForFECs, SIGNAL( pressed() ), fecScanForFECs, SLOT( setFocus() ) );
    connect( fecSlots, SIGNAL( activated(int) ), fecSlots, SLOT( setFocus() ) );
    connect( fecSlots, SIGNAL( activated(int) ), this, SLOT( fecSlotsModify() ) );
    connect( fecSlotsCcuRedundancy, SIGNAL( activated(int) ), this, SLOT( fecSlotsRedundancyModify() ) );
    connect( fecSR0Read, SIGNAL( pressed() ), this, SLOT( fecReadSR0() ) );
    connect( fecSR0Read, SIGNAL( pressed() ), fecSR0Read, SLOT( setFocus() ) );
    connect( fecSR1Read, SIGNAL( pressed() ), this, SLOT( fecReadSR1() ) );
    connect( fecSR1Read, SIGNAL( pressed() ), fecSR1Read, SLOT( setFocus() ) );
    connect( fecWriteCR0Bis, SIGNAL( pressed() ), this, SLOT( fecWriteCR0_() ) );
    connect( fecWriteCR0Bis, SIGNAL( pressed() ), fecWriteCR0Bis, SLOT( setFocus() ) );
    connect( fecWriteFifoRecWordButton, SIGNAL( pressed() ), this, SLOT( fecWriteFifoRec() ) );
    connect( fecWriteFifoRetWordButton, SIGNAL( pressed() ), this, SLOT( fecWriteFifoRet() ) );
    connect( fecWriteFifoTraWordButton, SIGNAL( pressed() ), this, SLOT( fecWriteFifoTra() ) );
    connect( i2cAllRead, SIGNAL( pressed() ), this, SLOT( i2cReadAll() ) );
    connect( i2cAllRead, SIGNAL( pressed() ), i2cAllRead, SLOT( setFocus() ) );
    connect( i2cAndOperation, SIGNAL( pressed() ), this, SLOT( i2cSetMaskAnd() ) );
    connect( i2cAndOperation, SIGNAL( pressed() ), i2cAndOperation, SLOT( setFocus() ) );
    connect( i2cAutoDectection, SIGNAL( pressed() ), this, SLOT( scanForI2CDevices() ) );
    connect( i2cAutoDectection, SIGNAL( pressed() ), i2cAutoDectection, SLOT( setFocus() ) );
    connect( i2cCcuAddresses, SIGNAL( activated(int) ), this, SLOT( ccuSelected() ) );
    connect( i2cCcuAddresses, SIGNAL( activated(int) ), i2cCcuAddresses, SLOT( setFocus() ) );
    connect( i2cChannelReset, SIGNAL( pressed() ), this, SLOT( i2cResetChannel() ) );
    connect( i2cChannelReset, SIGNAL( pressed() ), i2cChannelReset, SLOT( setFocus() ) );
    connect( i2cChannels, SIGNAL( activated(int) ), this, SLOT( i2cIsChannelEnable() ) );
    connect( i2cChannels, SIGNAL( activated(int) ), i2cChannels, SLOT( setFocus() ) );
    connect( i2cCRARead, SIGNAL( pressed() ), this, SLOT( i2cReadCRA() ) );
    connect( i2cCRARead, SIGNAL( pressed() ), i2cCRARead, SLOT( setFocus() ) );
    connect( i2cCRAWrite, SIGNAL( pressed() ), this, SLOT( i2cWriteCRA() ) );
    connect( i2cCRAWrite, SIGNAL( pressed() ), i2cCRAWrite, SLOT( setFocus() ) );
    connect( i2cCRAWriteBit, SIGNAL( pressed() ), this, SLOT( i2cWriteCRABit() ) );
    connect( i2cCRAWriteBit, SIGNAL( pressed() ), i2cCRAWriteBit, SLOT( setFocus() ) );
    connect( i2cEnable, SIGNAL( toggled(bool) ), this, SLOT( i2cEnableChannel() ) );
    connect( i2cEnable, SIGNAL( pressed() ), i2cEnable, SLOT( setFocus() ) );
    connect( i2cExtendedMode, SIGNAL( pressed() ), this, SLOT( i2cSetModeExtended() ) );
    connect( i2cExtendedMode, SIGNAL( pressed() ), i2cExtendedMode, SLOT( setFocus() ) );
    connect( i2cFecSlots, SIGNAL( activated(int) ), i2cFecSlots, SLOT( setFocus() ) );
    connect( i2cFecSlots, SIGNAL( activated(int) ), this, SLOT( fecSlotsI2cModify() ) );
    connect( i2cNormalMode, SIGNAL( pressed() ), this, SLOT( i2cSetModeNormal() ) );
    connect( i2cNormalMode, SIGNAL( pressed() ), i2cNormalMode, SLOT( setFocus() ) );
    connect( i2cOrOperation, SIGNAL( pressed() ), this, SLOT( i2cSetMaskOr() ) );
    connect( i2cOrOperation, SIGNAL( pressed() ), i2cOrOperation, SLOT( setFocus() ) );
    connect( i2cRalMode, SIGNAL( pressed() ), this, SLOT( i2cSetModeRal() ) );
    connect( i2cRalMode, SIGNAL( pressed() ), i2cRalMode, SLOT( setFocus() ) );
    connect( i2cReadB, SIGNAL( pressed() ), this, SLOT( i2cReadI2c() ) );
    connect( i2cReadB, SIGNAL( pressed() ), i2cReadB, SLOT( setFocus() ) );
    connect( i2cReadModifyWriteB, SIGNAL( pressed() ), this, SLOT( i2cReadModifyWrite() ) );
    connect( i2cSpeed100, SIGNAL( pressed() ), this, SLOT( i2cSetSpeed100() ) );
    connect( i2cSpeed1000, SIGNAL( pressed() ), this, SLOT( i2cSetSpeed1000() ) );
    connect( i2cSpeed200, SIGNAL( pressed() ), this, SLOT( i2cSetSpeed200() ) );
    connect( i2cSpeed400, SIGNAL( pressed() ), this, SLOT( i2cSetSpeed400() ) );
    connect( i2cSRARead, SIGNAL( pressed() ), this, SLOT( i2cReadSRA() ) );
    connect( i2cSRARead, SIGNAL( pressed() ), i2cSRARead, SLOT( setFocus() ) );
    connect( i2cSRRead, SIGNAL( pressed() ), this, SLOT( i2cReadSR() ) );
    connect( i2cSRRead, SIGNAL( pressed() ), i2cSRRead, SLOT( setFocus() ) );
    connect( i2cWriteB, SIGNAL( pressed() ), this, SLOT( i2cWriteI2c() ) );
    connect( i2cWriteB, SIGNAL( pressed() ), i2cWriteB, SLOT( setFocus() ) );
    connect( i2cXorOperation, SIGNAL( pressed() ), this, SLOT( i2cSetMaskXor() ) );
    connect( i2cXorOperation, SIGNAL( pressed() ), i2cXorOperation, SLOT( setFocus() ) );
    connect( memoryAllRead, SIGNAL( pressed() ), this, SLOT( memoryReadAll() ) );
    connect( memoryAllRead, SIGNAL( pressed() ), memoryAllRead, SLOT( setFocus() ) );
    connect( memoryAndOperation, SIGNAL( pressed() ), this, SLOT( memorySetMaskAnd() ) );
    connect( memoryAndOperation, SIGNAL( pressed() ), memoryAndOperation, SLOT( setFocus() ) );
    connect( memoryCcuAddresses, SIGNAL( activated(int) ), this, SLOT( ccuSelected() ) );
    connect( memoryCcuAddresses, SIGNAL( activated(int) ), memoryCcuAddresses, SLOT( setFocus() ) );
    connect( memoryChannelReset, SIGNAL( pressed() ), this, SLOT( memoryResetChannel() ) );
    connect( memoryChannelReset, SIGNAL( pressed() ), memoryChannelReset, SLOT( setFocus() ) );
    connect( memoryCRARead, SIGNAL( pressed() ), this, SLOT( memoryReadCRA() ) );
    connect( memoryCRARead, SIGNAL( pressed() ), memoryCRARead, SLOT( setFocus() ) );
    connect( memoryCRAWrite, SIGNAL( pressed() ), this, SLOT( memoryWriteCRA() ) );
    connect( memoryCRAWrite, SIGNAL( pressed() ), memoryCRAWrite, SLOT( setFocus() ) );
    connect( memoryCRAWriteBit, SIGNAL( pressed() ), this, SLOT( memoryWriteCRABit() ) );
    connect( memoryCRAWriteBit, SIGNAL( pressed() ), memoryCRAWriteBit, SLOT( setFocus() ) );
    connect( memoryEnable, SIGNAL( toggled(bool) ), this, SLOT( memoryEnableChannel() ) );
    connect( memoryEnable, SIGNAL( pressed() ), memoryEnable, SLOT( setFocus() ) );
    connect( memoryFecSlots, SIGNAL( activated(int) ), memoryFecSlots, SLOT( setFocus() ) );
    connect( memoryFecSlots, SIGNAL( activated(int) ), this, SLOT( fecSlotsMemoryModify() ) );
    connect( memoryMaskRegRead, SIGNAL( pressed() ), this, SLOT( memoryReadMaskReg() ) );
    connect( memoryMaskRegRead, SIGNAL( pressed() ), memoryMaskRegRead, SLOT( setFocus() ) );
    connect( memoryOrOperation, SIGNAL( pressed() ), this, SLOT( memorySetMaskOr() ) );
    connect( memoryOrOperation, SIGNAL( pressed() ), memoryOrOperation, SLOT( setFocus() ) );
    connect( memoryReadBMB, SIGNAL( pressed() ), this, SLOT( memoryReadMB() ) );
    connect( memoryReadBMB, SIGNAL( pressed() ), memoryReadBMB, SLOT( setFocus() ) );
    connect( memoryReadBSB, SIGNAL( pressed() ), this, SLOT( memoryReadSB() ) );
    connect( memoryReadBSB, SIGNAL( pressed() ), memoryReadBSB, SLOT( setFocus() ) );
    connect( memoryReadModifyWriteB, SIGNAL( pressed() ), this, SLOT( memoryReadModifyWrite() ) );
    connect( memoryReadModifyWriteB, SIGNAL( pressed() ), memoryReadModifyWriteB, SLOT( setFocus() ) );
    connect( memorySpeed1, SIGNAL( pressed() ), this, SLOT( memorySetSpeed1() ) );
    connect( memorySpeed1, SIGNAL( pressed() ), memorySpeed1, SLOT( setFocus() ) );
    connect( memorySpeed10, SIGNAL( pressed() ), this, SLOT( memorySetSpeed10() ) );
    connect( memorySpeed10, SIGNAL( pressed() ), memorySpeed10, SLOT( setFocus() ) );
    connect( memorySpeed20, SIGNAL( pressed() ), this, SLOT( memorySetSpeed20() ) );
    connect( memorySpeed20, SIGNAL( pressed() ), memorySpeed20, SLOT( setFocus() ) );
    connect( memorySpeed4, SIGNAL( pressed() ), this, SLOT( memorySetSpeed4() ) );
    connect( memorySpeed4, SIGNAL( pressed() ), memorySpeed4, SLOT( setFocus() ) );
    connect( memorySRRead, SIGNAL( pressed() ), this, SLOT( memoryReadSR() ) );
    connect( memorySRRead, SIGNAL( pressed() ), memorySRRead, SLOT( setFocus() ) );
    connect( memoryWin1Enable, SIGNAL( pressed() ), memoryWin1Enable, SLOT( setFocus() ) );
    connect( memoryWin2Enable, SIGNAL( pressed() ), memoryWin2Enable, SLOT( setFocus() ) );
    connect( memoryWinRead, SIGNAL( pressed() ), this, SLOT( memoryReadWindowRegisters() ) );
    connect( memoryWinRead, SIGNAL( pressed() ), memoryWinRead, SLOT( setFocus() ) );
    connect( memoryWinWrite, SIGNAL( pressed() ), this, SLOT( memoryWriteWindowRegisters() ) );
    connect( memoryWinWrite, SIGNAL( pressed() ), memoryWinWrite, SLOT( setFocus() ) );
    connect( memoryWriteBMB, SIGNAL( pressed() ), this, SLOT( memoryWriteMB() ) );
    connect( memoryWriteBMB, SIGNAL( pressed() ), memoryWriteBMB, SLOT( setFocus() ) );
    connect( memoryWriteBMBCompare, SIGNAL( pressed() ), this, SLOT( memoryWriteCompareMB() ) );
    connect( memoryWriteBMBCompare, SIGNAL( pressed() ), memoryWriteBMBCompare, SLOT( setFocus() ) );
    connect( memoryWriteBSB, SIGNAL( pressed() ), this, SLOT( memoryWriteSB() ) );
    connect( memoryWriteBSB, SIGNAL( pressed() ), memoryWriteBSB, SLOT( setFocus() ) );
    connect( memoryXorOperation, SIGNAL( pressed() ), this, SLOT( memorySetMaskXor() ) );
    connect( memoryXorOperation, SIGNAL( pressed() ), memoryXorOperation, SLOT( setFocus() ) );
    connect( piaAllRead, SIGNAL( pressed() ), this, SLOT( piaReadAll() ) );
    connect( piaAllRead, SIGNAL( pressed() ), piaAllRead, SLOT( setFocus() ) );
    connect( piaCcuAddresses, SIGNAL( activated(int) ), this, SLOT( ccuSelected() ) );
    connect( piaChannelReset, SIGNAL( pressed() ), this, SLOT( piaResetChannel() ) );
    connect( piaChannelReset, SIGNAL( pressed() ), piaChannelReset, SLOT( setFocus() ) );
    connect( piaChannels, SIGNAL( activated(int) ), this, SLOT( piaIsChannelEnable() ) );
    connect( piaChannels, SIGNAL( activated(int) ), piaChannels, SLOT( setFocus() ) );
    connect( piaDataRegisterRead, SIGNAL( pressed() ), this, SLOT( piaDataRead() ) );
    connect( piaDataRegisterRead, SIGNAL( pressed() ), piaDataRegisterRead, SLOT( setFocus() ) );
    connect( piaDataRegisterWrite, SIGNAL( pressed() ), this, SLOT( piaDataWrite() ) );
    connect( piaDataRegisterWrite, SIGNAL( pressed() ), piaDataRegisterWrite, SLOT( setFocus() ) );
    connect( piaDDRBit0I, SIGNAL( pressed() ), piaDDRBit0O, SLOT( toggle() ) );
    connect( piaDDRBit0O, SIGNAL( pressed() ), piaDDRBit0I, SLOT( toggle() ) );
    connect( piaDDRBit1I, SIGNAL( pressed() ), piaDDRBit1O, SLOT( toggle() ) );
    connect( piaDDRBit1O, SIGNAL( pressed() ), piaDDRBit1I, SLOT( toggle() ) );
    connect( piaDDRBit2I, SIGNAL( pressed() ), piaDDRBit2O, SLOT( toggle() ) );
    connect( piaDDRBit2O, SIGNAL( pressed() ), piaDDRBit2I, SLOT( toggle() ) );
    connect( piaDDRBit3I, SIGNAL( pressed() ), piaDDRBit3O, SLOT( toggle() ) );
    connect( piaDDRBit3O, SIGNAL( pressed() ), piaDDRBit3I, SLOT( toggle() ) );
    connect( piaDDRBit4I, SIGNAL( pressed() ), piaDDRBit4O, SLOT( toggle() ) );
    connect( piaDDRBit4O, SIGNAL( pressed() ), piaDDRBit4I, SLOT( toggle() ) );
    connect( piaDDRBit5I, SIGNAL( pressed() ), piaDDRBit5O, SLOT( toggle() ) );
    connect( piaDDRBit5O, SIGNAL( pressed() ), piaDDRBit5I, SLOT( toggle() ) );
    connect( piaDDRBit6I, SIGNAL( pressed() ), piaDDRBit6O, SLOT( toggle() ) );
    connect( piaDDRBit6O, SIGNAL( pressed() ), piaDDRBit6I, SLOT( toggle() ) );
    connect( piaDDRBit7I, SIGNAL( pressed() ), piaDDRBit7O, SLOT( toggle() ) );
    connect( piaDDRBit7O, SIGNAL( pressed() ), piaDDRBit7I, SLOT( toggle() ) );
    connect( piaDDRPRead, SIGNAL( pressed() ), this, SLOT( piaReadDDRP() ) );
    connect( piaDDRPRead, SIGNAL( pressed() ), piaDDRPRead, SLOT( setFocus() ) );
    connect( piaDDRPWrite, SIGNAL( pressed() ), this, SLOT( piaWriteDDRP() ) );
    connect( piaDDRPWrite, SIGNAL( pressed() ), piaDDRPWrite, SLOT( setFocus() ) );
    connect( piaDDRPWriteBit, SIGNAL( pressed() ), this, SLOT( piaWriteDDRPBit() ) );
    connect( piaDDRPWriteBit, SIGNAL( pressed() ), piaDDRPWriteBit, SLOT( setFocus() ) );
    connect( piaEnable, SIGNAL( toggled(bool) ), this, SLOT( piaEnableChannel() ) );
    connect( piaEnable, SIGNAL( pressed() ), piaEnable, SLOT( setFocus() ) );
    connect( piaEnableOperationBit, SIGNAL( pressed() ), piaDuration, SLOT( selectAll() ) );
    connect( piaEnableOperationBit, SIGNAL( pressed() ), piaEnableOperationBit, SLOT( setFocus() ) );
    connect( piaEnableOperationBit, SIGNAL( pressed() ), piaDuration, SLOT( setFocus() ) );
    connect( piaFecSlots, SIGNAL( activated(int) ), this, SLOT( fecSlotsPiaModify() ) );
    connect( piaGCRRead, SIGNAL( pressed() ), this, SLOT( piaReadGCR() ) );
    connect( piaGCRRead, SIGNAL( pressed() ), piaGCRRead, SLOT( setFocus() ) );
    connect( piaGCRWrite, SIGNAL( pressed() ), this, SLOT( piaWriteGCR() ) );
    connect( piaGCRWrite, SIGNAL( pressed() ), piaGCRWrite, SLOT( setFocus() ) );
    connect( piaGCRWriteBit, SIGNAL( pressed() ), this, SLOT( piaWriteGCRBit() ) );
    connect( piaGCRWriteBit, SIGNAL( pressed() ), piaGCRWriteBit, SLOT( setFocus() ) );
    connect( piaSRRead, SIGNAL( pressed() ), this, SLOT( piaReadSR() ) );
    connect( piaSRRead, SIGNAL( pressed() ), piaSRRead, SLOT( setFocus() ) );
    connect( quitB, SIGNAL( pressed() ), this, SLOT( quitTout() ) );
    connect( quitB, SIGNAL( pressed() ), quitB, SLOT( setFocus() ) );
    connect( scanAll, SIGNAL( pressed() ), this, SLOT( scanFecsCcusDevices() ) );
    connect( scanAll, SIGNAL( pressed() ), scanAll, SLOT( setFocus() ) );
    connect( StartThreadButton, SIGNAL( clicked() ), this, SLOT( startThread() ) );
    connect( trackerAllDevices, SIGNAL( pressed() ), trackerAllDevices, SLOT( setFocus() ) );
    connect( trackerAllDevices, SIGNAL( pressed() ), this, SLOT( trackerSetAllDevicesSelected() ) );
    connect( trackerAllRead, SIGNAL( pressed() ), trackerAllRead, SLOT( setFocus() ) );
    connect( trackerAllRead, SIGNAL( pressed() ), this, SLOT( trackerReadAll() ) );
    connect( trackerAllWrite, SIGNAL( pressed() ), trackerAllWrite, SLOT( setFocus() ) );
    connect( trackerAllWrite, SIGNAL( pressed() ), this, SLOT( trackerWriteAll() ) );
    connect( trackerApvMuxRead, SIGNAL( pressed() ), trackerApvMuxRead, SLOT( setFocus() ) );
    connect( trackerApvMuxRead, SIGNAL( pressed() ), this, SLOT( trackerReadAll() ) );
    connect( trackerApvMuxWrite, SIGNAL( pressed() ), trackerApvMuxWrite, SLOT( setFocus() ) );
    connect( trackerApvMuxWrite, SIGNAL( pressed() ), this, SLOT( trackerWriteAll() ) );
    connect( trackerApvRead, SIGNAL( pressed() ), trackerApvRead, SLOT( setFocus() ) );
    connect( trackerApvRead, SIGNAL( pressed() ), this, SLOT( trackerReadAll() ) );
    connect( trackerApvWrite, SIGNAL( pressed() ), trackerApvWrite, SLOT( setFocus() ) );
    connect( trackerApvWrite, SIGNAL( pressed() ), this, SLOT( trackerWriteAll() ) );
    connect( trackerAutoDectection, SIGNAL( pressed() ), this, SLOT( scanForTrackerDevices() ) );
    connect( trackerAutoDectection, SIGNAL( pressed() ), trackerAutoDectection, SLOT( setFocus() ) );
    connect( trackerClearListPiaReset, SIGNAL( pressed() ), trackerListPiaReset, SLOT( clear() ) );
    connect( trackerClearListPiaReset, SIGNAL( pressed() ), trackerClearListPiaReset, SLOT( setFocus() ) );
    connect( trackerDatabaseVersion, SIGNAL( doubleClicked(QListBoxItem*) ), this, SLOT( trackerSelectPartition() ) );
    connect( trackerDcuRead, SIGNAL( pressed() ), trackerDcuRead, SLOT( setFocus() ) );
    connect( trackerDcuRead, SIGNAL( pressed() ), this, SLOT( trackerReadAll() ) );
    connect( trackerDefaultValues, SIGNAL( pressed() ), trackerDefaultValues, SLOT( setFocus() ) );
    connect( trackerDefaultValues, SIGNAL( pressed() ), this, SLOT( trackerSetDefaultValues() ) );
    connect( trackerDeleteDevice, SIGNAL( pressed() ), this, SLOT( trackerDeleteDeviceList() ) );
    connect( trackerFecEnableErrorCounter, SIGNAL( pressed() ), this, SLOT( trackerFecEnableCounters() ) );
    connect( trackerFecReadDeviceDriver, SIGNAL( pressed() ), this, SLOT( fecReadDDStatus() ) );
    connect( trackerFecReadDeviceDriver, SIGNAL( pressed() ), trackerFecReadDeviceDriver, SLOT( setFocus() ) );
    connect( trackerFecResetCounter, SIGNAL( pressed() ), this, SLOT( fecResetErrorCounter() ) );
    connect( trackerFecResetCounter, SIGNAL( pressed() ), trackerFecResetCounter, SLOT( setFocus() ) );
    connect( trackerFindFile, SIGNAL( pressed() ), trackerFindFile, SLOT( setFocus() ) );
    connect( trackerFindFile, SIGNAL( pressed() ), this, SLOT( trackerFindXMLFile() ) );
    connect( trackerLaserdriverRead, SIGNAL( pressed() ), trackerLaserdriverRead, SLOT( setFocus() ) );
    connect( trackerLaserdriverRead, SIGNAL( pressed() ), this, SLOT( trackerReadAll() ) );
    connect( trackerLaserdriverWrite, SIGNAL( pressed() ), trackerLaserdriverWrite, SLOT( setFocus() ) );
    connect( trackerLaserdriverWrite, SIGNAL( pressed() ), this, SLOT( trackerWriteAll() ) );
    connect( trackerListDeviceDetected, SIGNAL( selectionChanged() ), this, SLOT( trackerListDeviceDetectedChanged() ) );
    connect( trackerPiaDataRegisterWrite, SIGNAL( pressed() ), trackerPiaDataRegisterWrite, SLOT( setFocus() ) );
    connect( trackerPiaDataRegisterWrite, SIGNAL( pressed() ), this, SLOT( trackerWritePiaSelected() ) );
    connect( trackerPiaResetAdd, SIGNAL( pressed() ), trackerPiaResetAdd, SLOT( setFocus() ) );
    connect( trackerPiaResetAdd, SIGNAL( pressed() ), this, SLOT( trackerAddPiaKey() ) );
    connect( trackerPiaResetAddAllCCU, SIGNAL( pressed() ), trackerPiaResetAddAllCCU, SLOT( setFocus() ) );
    connect( trackerPiaResetAddAllCCU, SIGNAL( pressed() ), this, SLOT( trackerPiaResetAddAllCCUKey() ) );
    connect( trackerPiaResetDelete, SIGNAL( pressed() ), trackerPiaResetDelete, SLOT( setFocus() ) );
    connect( trackerPiaResetDelete, SIGNAL( pressed() ), this, SLOT( trackerDeletePiaKey() ) );
    connect( trackerPllRead, SIGNAL( pressed() ), trackerPllRead, SLOT( setFocus() ) );
    connect( trackerPllRead, SIGNAL( pressed() ), this, SLOT( trackerReadAll() ) );
    connect( trackerPllWrite, SIGNAL( pressed() ), trackerPllWrite, SLOT( setFocus() ) );
    connect( trackerPllWrite, SIGNAL( pressed() ), this, SLOT( trackerWriteAll() ) );
    connect( trackerSelectAllPiaB, SIGNAL( pressed() ), trackerSelectAllPiaB, SLOT( setFocus() ) );
    connect( trackerSelectAllPiaB, SIGNAL( pressed() ), this, SLOT( trackerSetAllPiaSelected() ) );
    connect( trackerUseDatabase, SIGNAL( clicked() ), this, SLOT( trackerDatabaseToggle() ) );
    connect( trackerUseFile, SIGNAL( clicked() ), this, SLOT( trackerFileToggle() ) );
    connect( trackerXMLDownload, SIGNAL( pressed() ), trackerXMLDownload, SLOT( setFocus() ) );
    connect( trackerXMLDownload, SIGNAL( pressed() ), this, SLOT( trackerLoadXML() ) );
    connect( trackerXMLUpload, SIGNAL( pressed() ), trackerXMLUpload, SLOT( setFocus() ) );
    connect( trackerXMLUpload, SIGNAL( pressed() ), this, SLOT( trackerSaveXML() ) );
    connect( fecResetFSM, SIGNAL( pressed() ), this, SLOT( fecResetFSM_pressed() ) );
    connect( vmeCrateReset, SIGNAL( pressed() ), this, SLOT( vmeCrateReset_pressed() ) );

    // tab order
    setTabOrder( FEC, fecScanForFECs );
    setTabOrder( fecScanForFECs, fecSlots );
    setTabOrder( fecSlots, fecReadAll );
    setTabOrder( fecReadAll, fecCR0Read );
    setTabOrder( fecCR0Read, fecCR0InputLine );
    setTabOrder( fecCR0InputLine, fecCR0writeCR0 );
    setTabOrder( fecCR0writeCR0, fecCR0enableFEC );
    setTabOrder( fecCR0enableFEC, fecCR0Send );
    setTabOrder( fecCR0Send, fecCR0InternalClock );
    setTabOrder( fecCR0InternalClock, fecCR0SelSerOut );
    setTabOrder( fecCR0SelSerOut, fecCR0SelSerIn );
    setTabOrder( fecCR0SelSerIn, fecCR0ResetTTCRx );
    setTabOrder( fecCR0ResetTTCRx, fecCR0WriteBit );
    setTabOrder( fecCR0WriteBit, fecCR1Read );
    setTabOrder( fecCR1Read, fecCR1InputLine );
    setTabOrder( fecCR1InputLine, fecCR1WriteCR1 );
    setTabOrder( fecCR1WriteCR1, fecCR1ClearIrq );
    setTabOrder( fecCR1ClearIrq, fecCR1ClearErrors );
    setTabOrder( fecCR1ClearErrors, fecCR1Release );
    setTabOrder( fecCR1Release, fecCR1WriteBit );
    setTabOrder( fecCR1WriteBit, fecDDStatus );
    setTabOrder( fecDDStatus, fecSR0Read );
    setTabOrder( fecSR0Read, fecSR0InputLine );
    setTabOrder( fecSR0InputLine, fecSR0TraRunning );
    setTabOrder( fecSR0TraRunning, fecSR0RecRunning );
    setTabOrder( fecSR0RecRunning, fecSR0RecFull );
    setTabOrder( fecSR0RecFull, fecSR0RecEmpty );
    setTabOrder( fecSR0RecEmpty, fecSR0RetFull );
    setTabOrder( fecSR0RetFull, fecSR0RetEmpty );
    setTabOrder( fecSR0RetEmpty, fecSR0TraFull );
    setTabOrder( fecSR0TraFull, fecSR0LinkInit );
    setTabOrder( fecSR0LinkInit, fecSR0PendingIrq );
    setTabOrder( fecSR0PendingIrq, fecSR0DataToFec );
    setTabOrder( fecSR0DataToFec, fecSR0TTCRx );
    setTabOrder( fecSR0TTCRx, fecSR1Read );
    setTabOrder( fecSR1Read, fecSR1InputLine );
    setTabOrder( fecSR1InputLine, fecSR1IllData );
    setTabOrder( fecSR1IllData, fecCR1IllSeq );
    setTabOrder( fecCR1IllSeq, fecSR1CRCError );
    setTabOrder( fecSR1CRCError, fecSR1DataCopied );
    setTabOrder( fecSR1DataCopied, fecSR1AddrSeen );
    setTabOrder( fecSR1AddrSeen, fecSR1Error );
}

/*
 *  Destroys the object and frees any allocated resources
 */
FecDialog::~FecDialog()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void FecDialog::languageChange()
{
    setCaption( tr( "GUIDebugger" ) );
    QWhatsThis::add( this, QString::null );
    Command->setTitle( tr( "Commands" ) );
    fecResetPlx->setText( tr( "PLX Reset" ) );
    vmeCrateReset->setText( tr( "VME Crate Reset" ) );
    fecResetFec->setText( tr( "FEC Reset" ) );
    fecResetFSM->setText( tr( "Reset FSM" ) );
    fecDisableReceive->setText( tr( "Disable Receive" ) );
    fecRelease->setText( tr( "FEC Release" ) );
    fecClearErrors->setText( tr( "Clear Errors / IRQ" ) );
    scanAll->setText( tr( "Scan FECs and CCUs" ) );
    fecDisableIRQ->setText( tr( "Disable IRQ" ) );
    TextLabel23->setText( tr( "CR0" ) );
    fecReadCR0Bis->setText( tr( "R" ) );
    fecWriteCR0Bis->setText( tr( "W" ) );
    fecCR0Bis->setText( tr( "0x0000" ) );
    fecReadSR0Bis->setText( tr( "Read SR0" ) );
    fecSR1Bis->setText( tr( "0x0000" ) );
    fecReadSR1Bis->setText( tr( "Read SR1" ) );
    fecSR0Bis->setText( tr( "0x0000" ) );
    fecReadFifoRetWordButton->setText( tr( "R" ) );
    QWhatsThis::add( fecReadFifoRetWordButton, tr( "read a word in FIFO return" ) );
    fecClearFifoTraButton->setText( tr( "C" ) );
    fecWriteFifoRetWordButton->setText( tr( "W" ) );
    fecClearFifoRetButton->setText( tr( "C" ) );
    TextLabel23_2->setText( tr( "Rec" ) );
    fecWriteFifoTraWordButton->setText( tr( "W" ) );
    fecReadFifoRecWordButton->setText( tr( "R" ) );
    QToolTip::add( fecReadFifoRecWordButton, tr( "Read a word in FIFO receive" ) );
    fecReadFifoTraWordButton->setText( tr( "R" ) );
    QToolTip::add( fecReadFifoTraWordButton, tr( "read a word in FIFO transmit" ) );
    fecFifoRecWord->setText( tr( "0x00000000" ) );
    TextLabel23_2_2->setText( tr( "Tra" ) );
    fecFifoRetWord->setText( tr( "0x00000000" ) );
    fecClearFifoRecButton->setText( tr( "C" ) );
    QWhatsThis::add( fecClearFifoRecButton, tr( "clear the FIFO receive" ) );
    fecFifoTraWord->setText( tr( "0x00000000" ) );
    TextLabel23_2_2_2->setText( tr( "Ret" ) );
    fecWriteFifoRecWordButton->setText( tr( "W" ) );
    QToolTip::add( fecWriteFifoRecWordButton, tr( "only optical FEC" ) );
    groupBox175->setTitle( tr( "Thread" ) );
    textLabel2->setText( tr( "SR0" ) );
    fecSR0Thread->setText( tr( "0x0000" ) );
    StartThreadButton->setText( tr( "Start Thread" ) );
    textLabel3->setText( tr( "Link Initialise" ) );
    about->setText( tr( "About" ) );
    quitB->setText( tr( "Quit" ) );
    fecScanForFECs->setText( tr( "Scan for FECs" ) );
    QWhatsThis::add( fecScanForFECs, tr( "Scan all PCI slot for device driver" ) );
    TextLabel2->setText( tr( "FEC slots" ) );
    QWhatsThis::add( fecSlots, tr( "FEC device driver slot" ) );
    fecReadAll->setText( tr( "Read all" ) );
    DeviceDriver->setTitle( tr( "Device driver" ) );
    fecEnableErrorCounter->setText( tr( "Enable error count" ) );
    fecResetCounter->setText( tr( "Reset Error Counters" ) );
    fecIRQEnableButton->setText( tr( "IRQ Enable" ) );
    fecWarningDevice->setText( tr( "0" ) );
    fecNbPlxReset->setText( tr( "0" ) );
    fecBadTransaction->setText( tr( "0" ) );
    TextLabel16->setText( tr( "Number of FEC reset" ) );
    TextLabel12->setText( tr( "Too short frame errors" ) );
    fecDDStatus->setText( tr( "0x0000" ) );
    TextLabel15->setText( tr( "Number of PLX reset" ) );
    TextLabel13->setText( tr( "Bad transaction errors" ) );
    TextLabel11->setText( tr( "Too long frame errors" ) );
    TextLabel14->setText( tr( "Warning emitted by a device" ) );
    fecLongFrame->setText( tr( "0" ) );
    TextLabel10->setText( tr( "Device driver status" ) );
    fecNbFecReset->setText( tr( "0" ) );
    fecShortFrame->setText( tr( "0" ) );
    fecReadDeviceDriver->setText( tr( "Read" ) );
    GroupBox2_3_2->setTitle( tr( "SR1" ) );
    fecSR1InputLine->setText( tr( "0x0000" ) );
    fecSR1IllData->setText( tr( "Ill. Data" ) );
    fecCR1IllSeq->setText( tr( "Ill. Seq." ) );
    fecSR1CRCError->setText( tr( "CRC Error" ) );
    fecSR1DataCopied->setText( tr( "Data Copied" ) );
    fecSR1AddrSeen->setText( tr( "Addr. Seen" ) );
    fecSR1Error->setText( tr( "Error" ) );
    fecSR1Timeout->setText( tr( "Timeout" ) );
    fecSR1ClockError->setText( tr( "Clock error" ) );
    fecSR1Read->setText( tr( "Read" ) );
    fecGroupCR1->setTitle( tr( "CR1" ) );
    fecCR1ClearErrors->setText( tr( "Clear Errors Indicators" ) );
    fecCR1InputLine->setText( tr( "0x0000" ) );
    fecCR1Release->setText( tr( "FEC release" ) );
    fecCR1ClearIrq->setText( tr( "Clear IRQ" ) );
    fecCR1WriteCR1->setText( tr( "Write" ) );
    fecCR1Read->setText( tr( "Read" ) );
    fecCR1WriteBit->setText( tr( "Write" ) );
    fecGroupCR0->setTitle( tr( "CR0" ) );
    fecCR0InputLine->setText( tr( "0x0000" ) );
    fecCR0writeCR0->setText( tr( "Write" ) );
    fecCR0InvertPolarity->setText( tr( "Invert Polarity" ) );
    fecCR0ResetTTCRx->setText( tr( "Reset TTCRx" ) );
    fecCR0SelSerIn->setText( tr( "Sel. Ser. Input" ) );
    fecCR0InternalClock->setText( tr( "Internal Clock" ) );
    fecCR0enableFEC->setText( tr( "Enable FEC" ) );
    fecCR0ResetFSM->setText( tr( "Reload FSM" ) );
    fecCR0Send->setText( tr( "Send" ) );
    fecCR0DisableRec->setText( tr( "Disable Rec." ) );
    fecCR0SelSerOut->setText( tr( "Sel. Ser. Output" ) );
    fecCR0ResetB->setText( tr( "Reset B" ) );
    fecCR0ResetOut->setText( tr( "Reset Out" ) );
    fecCR0Read->setText( tr( "Read" ) );
    fecCR0WriteBit->setText( tr( "Write" ) );
    GroupBox2_3->setTitle( tr( "SR0" ) );
    fecSR0Read->setText( tr( "Read" ) );
    fecSR0InputLine->setText( tr( "0x0000" ) );
    fecSR0TraRunning->setText( tr( "Tra. Running" ) );
    fecSR0TraFull->setText( tr( "Tra. Full" ) );
    fecSR0RecFull->setText( tr( "Rec. Full" ) );
    fecSR0RetFull->setText( tr( "Ret. Full" ) );
    fecSR0LinkInit->setText( tr( "Link Init" ) );
    fecSR0PendingIrq->setText( tr( "Pending IRQ" ) );
    fecSR0RetEmpty->setText( tr( "Ret. Empty" ) );
    QWhatsThis::add( fecSR0RetEmpty, tr( "FIFO return empty" ) );
    fecSR0RecEmpty->setText( tr( "Rec. Empty" ) );
    fecSR0TraEmpty->setText( tr( "Tra. Empty" ) );
    fecSR0RecRunning->setText( tr( "Rec. running" ) );
    fecSR0TTCRx->setText( tr( "TTCRx" ) );
    fecSR0DataToFec->setText( tr( "Data to FEC" ) );
    FEC->changeTab( tab, tr( "FEC" ) );
    TextLabel2_3->setText( tr( "FEC slots" ) );
    QWhatsThis::add( ccuFecSlots, tr( "FEC device driver slot" ) );
    ccuScanForCcus->setText( tr( "Scan for CCUs" ) );
    QWhatsThis::add( ccuScanForCcus, tr( "Scan all PCI slot for device driver" ) );
    TextLabel2_5->setText( tr( "CCU Address" ) );
    QWhatsThis::add( ccuAddresses, tr( "FEC device driver slot" ) );
    ccuIsACcu25->setText( tr( "Is A CCU 25 ?" ) );
    ccuAllRead->setText( tr( "Read all" ) );
    CCUCRB->setTitle( tr( "CRB" ) );
    ccuCRB->setText( tr( "Not Implemented" ) );
    ccuCRBWrite->setText( tr( "Write" ) );
    ccuCRBRead->setText( tr( "Read" ) );
    ccuCRBWriteBit->setText( tr( "Write" ) );
    ccuEnableAlarm1->setText( tr( "Enable Alarm 1 IRQ" ) );
    ccuEnableAlarm2->setText( tr( "Enable Alarm 2 IRQ" ) );
    ccuEnableAlarm3->setText( tr( "Enable Alarm 3 IRQ" ) );
    ccuEnableAlarm4->setText( tr( "Enable Alarm 4 IRQ" ) );
    ccuNoRety->setText( tr( "No retry" ) );
    ccuRetryOnce->setText( tr( "Retry Once" ) );
    ccuRetryTwice->setText( tr( "Retry twice" ) );
    ccuRetryFourth->setText( tr( "Retry Fourth" ) );
    CCUSR->setTitle( tr( "CCU Status Registers" ) );
    TextLabel17->setText( tr( "SRB" ) );
    QWhatsThis::add( TextLabel17, tr( "Last transaction number" ) );
    TextLabel18->setText( tr( "Parity Error counter" ) );
    TextLabel17_3->setText( tr( "SRD" ) );
    QWhatsThis::add( TextLabel17_3, tr( "Input port" ) );
    TextLabel20->setText( tr( "Last ring message" ) );
    TextLabel17_5->setText( tr( "SRF" ) );
    QWhatsThis::add( TextLabel17_5, tr( "Parity error counter" ) );
    TextLabel24->setText( tr( "SRC" ) );
    ccuSRCInputA->setText( tr( "Input Port A" ) );
    ccuSRCInputB->setText( tr( "Input Port B" ) );
    TextLabel22->setText( tr( "Last transaction number" ) );
    ccuSRB->setText( tr( "0x0000" ) );
    QWhatsThis::add( ccuSRB, tr( "Last transaction number" ) );
    ccuSRD->setText( tr( "0x0000" ) );
    QWhatsThis::add( ccuSRD, tr( "Input port" ) );
    ccuSRF->setText( tr( "0x0000" ) );
    QWhatsThis::add( ccuSRF, tr( "Last transaction number" ) );
    ccuSRRead->setText( tr( "Read" ) );
    CCUSRE->setTitle( tr( "SRE: I2C Channel busy" ) );
    ccuSRERead->setText( tr( "Read" ) );
    ccuSREI2C16->setText( tr( "I2C - 16" ) );
    ccuSREI2C3->setText( tr( "I2C - 3" ) );
    ccuSREI2C6->setText( tr( "I2C - 6" ) );
    ccuSREI2C7->setText( tr( "I2C - 7" ) );
    ccuSREI2C11->setText( tr( "I2C - 11" ) );
    ccuSREI2C1->setText( tr( "I2C - 1" ) );
    ccuSREI2C4->setText( tr( "I2C - 4" ) );
    ccuSREI2C13->setText( tr( "I2C - 13" ) );
    ccuSREI2C15->setText( tr( "I2C - 15" ) );
    ccuSREI2C10->setText( tr( "I2C - 10" ) );
    ccuSREI2C12->setText( tr( "I2C - 12" ) );
    ccuSREI2C2->setText( tr( "I2C - 2" ) );
    ccuSREI2C5->setText( tr( "I2C - 5" ) );
    ccuSREI2C9->setText( tr( "I2C - 9" ) );
    ccuSREI2C8->setText( tr( "I2C - 8" ) );
    ccuSREI2C14->setText( tr( "I2C - 14" ) );
    ccuSRE->setText( tr( "0x0000" ) );
    CCUCRC->setTitle( tr( "CRC" ) );
    ccuCRC->setText( tr( "0x0000" ) );
    ccuCRCRead->setText( tr( "Read" ) );
    ccuCRCWriteBit->setText( tr( "Write" ) );
    QToolTip::add( ccuCRCWriteBit, tr( "redundancy ring" ) );
    ccuCRCWrite->setText( tr( "Write" ) );
    QToolTip::add( ccuCRCWrite, tr( "Redundancy ring" ) );
    ccuSelectInputB->setText( tr( "Select input B" ) );
    ccuSelectOuputB->setText( tr( "Select output B" ) );
    CCUCRD->setTitle( tr( "CRD: broadcast class" ) );
    ccuCRDWrite->setText( tr( "Write" ) );
    ccuCRD->setText( tr( "0x0000" ) );
    ccuCRDRead->setText( tr( "Read" ) );
    CCUCRA->setTitle( tr( "CRA" ) );
    ccuCRAWrite->setText( tr( "Write" ) );
    ccuExternalReset->setText( tr( "External reset" ) );
    ccuClearError->setText( tr( "Clear Error" ) );
    ccuCRA->setText( tr( "0x0000" ) );
    ccuCRARead->setText( tr( "Read" ) );
    ccuCRAWriteBit->setText( tr( "Write" ) );
    ccuResetAllChannels->setText( tr( "Reset all channels" ) );
    CCUSRA->setTitle( tr( "SRA" ) );
    ccuSRA->setText( tr( "0x0000" ) );
    ccuSRARead->setText( tr( "Read" ) );
    ccuSRAGlobalError->setText( tr( "Global error" ) );
    ccuSRAIllegalSequence->setText( tr( "Illegal sequence" ) );
    ccuSRAAlarmInputActive->setText( tr( "Alarm input active" ) );
    ccuSRACRCError->setText( tr( "CRC Error" ) );
    ccuSRACCUParityError->setText( tr( "CCU Parity Error" ) );
    ccuSRAChannelParityError->setText( tr( "Channel parity error" ) );
    ccuSRAInternalError->setText( tr( "Internal Error" ) );
    ccuSRAInvalidCommand->setText( tr( "Invalid command" ) );
    CCUCRE->setTitle( tr( "CRE: channels enabled" ) );
    ccuCREMemory->setText( tr( "Memory" ) );
    ccuCREI2C5->setText( tr( "I2C - 20" ) );
    ccuCREPIA1->setText( tr( "PIA - 1" ) );
    ccuCREPIA2->setText( tr( "PIA - 2" ) );
    ccuCREI2C11->setText( tr( "I2C - 26" ) );
    ccuCREI2C9->setText( tr( "I2C - 24" ) );
    ccuCREI2C14->setText( tr( "I2C - 29" ) );
    ccuCRERead->setText( tr( "Read" ) );
    ccuCREWrite->setText( tr( "Write" ) );
    ccuCREI2C15->setText( tr( "I2C - 31" ) );
    ccuCREJTAG->setText( tr( "Jtag" ) );
    ccuCRE->setText( tr( "0x000000" ) );
    ccuAllEnable->setText( tr( "Enable all" ) );
    ccuCREI2C10->setText( tr( "I2C - 25" ) );
    ccuCREWriteBit->setText( tr( "Write" ) );
    ccuCREI2C3->setText( tr( "I2C - 18" ) );
    ccuCREI2C12->setText( tr( "I2C - 27" ) );
    ccuCREI2C6->setText( tr( "I2C - 21" ) );
    ccuCREI2C8->setText( tr( "I2C - 23" ) );
    ccuCREI2C7->setText( tr( "I2C - 22" ) );
    ccuCREPIA3->setText( tr( "PIA - 3" ) );
    ccuCREI2C13->setText( tr( "I2C - 28" ) );
    ccuCRETrigger->setText( tr( "Trigger" ) );
    ccuCREI2C2->setText( tr( "I2C - 17" ) );
    ccuCREI2C4->setText( tr( "I2C - 19" ) );
    ccuCREI2C1->setText( tr( "I2C - 16" ) );
    ccuCREI2C16->setText( tr( "I2C - 32" ) );
    TextLabel1_3_2->setText( tr( "CRC is forseen for the redundancy ring." ) );
    FEC->changeTab( tab_2, tr( "CCU" ) );
    TextLabel2_3_2->setText( tr( "FEC slots" ) );
    QWhatsThis::add( i2cFecSlots, tr( "FEC device driver slot" ) );
    TextLabel2_5_2->setText( tr( "CCU Address" ) );
    QWhatsThis::add( i2cCcuAddresses, tr( "FEC device driver slot" ) );
    i2cCcuIsACcu25->setText( tr( "Is A CCU 25 ?" ) );
    TextLabel26->setText( tr( "I2C channels" ) );
    i2cAllRead->setText( tr( "Read all" ) );
    CCUCRA_2_3->setTitle( tr( "I2C devices" ) );
    i2cAutoDectection->setText( tr( "Auto dectection for any i2c registers" ) );
    CCUCRA_3->setTitle( tr( "SRA" ) );
    i2cSRA->setText( tr( "0x0000" ) );
    i2cSRARead->setText( tr( "Read" ) );
    i2cLastTrSuccessfull->setText( tr( "Last transaction successfull" ) );
    i2cLow->setText( tr( "I2C low" ) );
    i2cInvalidCommand->setText( tr( "Invalid command" ) );
    i2cLastOperationNotAck->setText( tr( "Last operation was not acknowledged" ) );
    i2cGlobalError->setText( tr( "Global error" ) );
    CCUSR_2->setTitle( tr( "I2C Channel Status Registers" ) );
    TextLabel17_2->setText( tr( "SRB" ) );
    QWhatsThis::add( TextLabel17_2, tr( "Last transaction number" ) );
    TextLabel17_3_2->setText( tr( "SRC" ) );
    QWhatsThis::add( TextLabel17_3_2, tr( "Input port" ) );
    TextLabel17_5_2->setText( tr( "SRD" ) );
    QWhatsThis::add( TextLabel17_5_2, tr( "Parity error counter" ) );
    i2cSRC->setText( tr( "0x0000" ) );
    QWhatsThis::add( i2cSRC, tr( "Input port" ) );
    i2cSRD->setText( tr( "0x0000" ) );
    QWhatsThis::add( i2cSRD, tr( "Last transaction number" ) );
    i2cSRRead->setText( tr( "Read" ) );
    i2cSRB->setText( tr( "0x0000" ) );
    QWhatsThis::add( i2cSRB, tr( "Last transaction number" ) );
    TextLabel18_2->setText( tr( "Last command send to this i2c channel" ) );
    TextLabel20_2->setText( tr( "Transaction nb. for incorrect transaction" ) );
    TextLabel22_2->setText( tr( "Transaction nb. for last correct operation" ) );
    CCUCRA_2->setTitle( tr( "CRA" ) );
    i2cCRAWrite->setText( tr( "Write" ) );
    i2cCRA->setText( tr( "0x0000" ) );
    i2cCRARead->setText( tr( "Read" ) );
    i2cCRAWriteBit->setText( tr( "Write" ) );
    i2cSpeed400->setText( tr( "400 KHz" ) );
    i2cSpeed200->setText( tr( "200 KHz" ) );
    TextLabel25->setText( tr( "I2C Speed" ) );
    i2cSpeed100->setText( tr( "100 KHz" ) );
    i2cSpeed1000->setText( tr( "1MHz" ) );
    i2cForceAcknowledge->setText( tr( "Force acknowledge" ) );
    i2cEnableBroadCast->setText( tr( "Enable Broadcast" ) );
    GroupBox85->setTitle( tr( "I2C Commands" ) );
    i2cChannelReset->setText( tr( "Reset Channel" ) );
    i2cEnable->setText( tr( "Enable Channel" ) );
    GroupBox63->setTitle( tr( "Read / Write Access" ) );
    i2cRalMode->setText( tr( "Ral Mode" ) );
    i2cExtendedMode->setText( tr( "Extended Mode" ) );
    i2cNormalMode->setText( tr( "Normal Mode" ) );
    i2cReadModifyWriteB->setText( tr( "RMW" ) );
    i2cReadB->setText( tr( "Read" ) );
    i2cWriteB->setText( tr( "Write" ) );
    TextLabel27_3_2->setText( tr( "Shift" ) );
    i2cShiftAccess->setText( tr( "0x0000" ) );
    TextLabel2_2->setText( tr( "Mask Register" ) );
    i2cLogMaskRegister->setText( tr( "0x0000" ) );
    TextLabel27_3->setText( tr( "Offset" ) );
    TextLabel27_2->setText( tr( "Data" ) );
    i2cDataAccess->setText( tr( "0x0000" ) );
    TextLabel27->setText( tr( "I2C Address" ) );
    i2cOffsetAccess->setText( tr( "0x0000" ) );
    i2cAddressAccess->setText( tr( "0x0000" ) );
    i2cAndOperation->setText( tr( "AND" ) );
    i2cOrOperation->setText( tr( "OR" ) );
    i2cXorOperation->setText( tr( "XOR" ) );
    TextLabel1_3->setText( tr( "The read/write method with a mask and a logical operation is not implemented" ) );
    FEC->changeTab( tab_3, tr( "I2C Channel" ) );
    TextLabel2_3_2_2->setText( tr( "FEC slots" ) );
    QWhatsThis::add( memoryFecSlots, tr( "FEC device driver slot" ) );
    TextLabel2_5_2_2->setText( tr( "CCU Address" ) );
    QWhatsThis::add( memoryCcuAddresses, tr( "FEC device driver slot" ) );
    memoryCcuIsACcu25->setText( tr( "Is A CCU 25 ?" ) );
    TextLabel1->setText( tr( "Memory Channel 0x40" ) );
    memoryAllRead->setText( tr( "Read all" ) );
    GroupBox57->setTitle( tr( "Multiple Byte Access" ) );
    TextLabel27_3_2_3_2_2->setText( tr( "Size (< 65536)" ) );
    TextLabel27_2_3_2_3->setText( tr( "Bandwith in Mb/s" ) );
    TextLabel27_5_2_3->setText( tr( "Time in seconds" ) );
    memorySize->setText( tr( "65535" ) );
    memoryBandwith->setText( tr( "0.0 Mb/s" ) );
    memoryTime->setText( tr( "0 s" ) );
    memoryFillSameData->setText( tr( "Fill size with data value" ) );
    memoryDisplayOnList->setText( tr( "Display in list" ) );
    memoryReadBMB->setText( tr( "Read" ) );
    memoryWriteBMB->setText( tr( "Write" ) );
    memoryWriteBMBCompare->setText( tr( "Write && Compare" ) );
    CCUCRA_3_2->setTitle( tr( "Status Register" ) );
    memoryInvalidCommand->setText( tr( "Invalid Command" ) );
    memoryInvalidAddress->setText( tr( "Invalid Address" ) );
    memoryGlobalError->setText( tr( "Global Error" ) );
    memorySR->setText( tr( "0x0000" ) );
    memorySRRead->setText( tr( "Read" ) );
    GroupBox56->setTitle( tr( "Read Modify Write" ) );
    TextLabel27_3_2_3_2->setText( tr( "Mask register" ) );
    memoryAndOperation->setText( tr( "AND" ) );
    memoryOrOperation->setText( tr( "OR" ) );
    memoryXorOperation->setText( tr( "XOR" ) );
    memoryMaskRegRead->setText( tr( "Read" ) );
    memoryReadModifyWriteB->setText( tr( "RMW" ) );
    memoryMask->setText( tr( "0x00" ) );
    CCUCRA_2_4->setTitle( tr( "CRA" ) );
    memoryCRARead->setText( tr( "Read" ) );
    memoryCRAWriteBit->setText( tr( "Write" ) );
    memorySpeed10->setText( tr( "10 MHz" ) );
    memorySpeed4->setText( tr( "4 MHz" ) );
    TextLabel25_2->setText( tr( "Memory Speed" ) );
    memorySpeed1->setText( tr( "1 MHz" ) );
    memorySpeed20->setText( tr( "20 MHz" ) );
    memoryWin2Enable->setText( tr( "Enable Window 2" ) );
    memoryWin1Enable->setText( tr( "Enable Window 1" ) );
    memoryCRA->setText( tr( "0x0000" ) );
    memoryCRAWrite->setText( tr( "Write" ) );
    GroupBox63_3_2->setTitle( tr( "Single Byte Access" ) );
    memoryReadBSB->setText( tr( "Read" ) );
    memoryWriteBSB->setText( tr( "Write" ) );
    TextLabel27_5_2->setText( tr( "Address low (AL)" ) );
    memoryData->setText( tr( "0x00" ) );
    TextLabel27_2_3_2->setText( tr( "Address High (AH)" ) );
    memoryAH->setText( tr( "0x00" ) );
    memoryAL->setText( tr( "0x00" ) );
    TextLabel27_3_4_2->setText( tr( "Data" ) );
    GroupBox63_3->setTitle( tr( "Memory Bus Window Registers" ) );
    memoryWinRead->setText( tr( "Read" ) );
    memoryWinWrite->setText( tr( "Write" ) );
    memoryWin1H->setText( tr( "0x0000" ) );
    TextLabel27_3_2_3->setText( tr( "Window 2 High" ) );
    memoryWin2L->setText( tr( "0x0000" ) );
    TextLabel27_5->setText( tr( "Window 1 Low" ) );
    TextLabel27_3_4->setText( tr( "Window 2 Low" ) );
    TextLabel27_2_3->setText( tr( "Window 1 High" ) );
    memoryWin2H->setText( tr( "0x0000" ) );
    memoryWin1L->setText( tr( "0x0000" ) );
    GroupBox85_2->setTitle( tr( "Memory Commands" ) );
    memoryChannelReset->setText( tr( "Reset Channel" ) );
    memoryEnable->setText( tr( "Enable Channel" ) );
    TextLabel1_3_3->setText( tr( "You need a specific fimrware upgrade to use multiple byte accesses" ) );
    TextLabel1_3_4->setText( tr( "Multiple byte access need to run at 20Mhz" ) );
    FEC->changeTab( tab_4, tr( "Memory Channel" ) );
    TextLabel2_3_2_3->setText( tr( "FEC slots" ) );
    QWhatsThis::add( piaFecSlots, tr( "FEC device driver slot" ) );
    TextLabel2_5_2_3->setText( tr( "CCU Address" ) );
    QWhatsThis::add( piaCcuAddresses, tr( "FEC device driver slot" ) );
    piaCcuIsACcu25->setText( tr( "Is A CCU 25 ?" ) );
    TextLabel26_2->setText( tr( "PIA Channels" ) );
    piaAllRead->setText( tr( "Read all" ) );
    PIASTATUSREGISTER->setTitle( tr( "Status Register" ) );
    piaInt->setText( tr( "Interrupt Generated by strobe on port" ) );
    piaInvcom->setText( tr( "Invalid command received" ) );
    piaGlobalError->setText( tr( "Global Error" ) );
    piaSR->setText( tr( "0x0000" ) );
    piaSRRead->setText( tr( "Read" ) );
    PIADATAREGISTER->setTitle( tr( "Data register" ) );
    TextLabel5->setText( trUtf8( "\x50\x49\x41\x20\x64\x75\x72\x61\x74\x69\x6f\x6e\x20\x28\xc2\xb5\x73\x29" ) );
    TextLabel4->setText( trUtf8( "\x53\x6c\x65\x65\x70\x20\x74\x69\x6d\x65\x20\x28\xc2\xb5\x73\x29" ) );
    piaEnableOperationBit->setText( tr( "Enable operation bit per bit" ) );
    piaDuration->setText( tr( "10" ) );
    piaSleepTime->setText( tr( "10000" ) );
    piaDataRegister->setText( tr( "0x00" ) );
    piaDataRegisterRead->setText( tr( "Read" ) );
    piaDataRegisterWrite->setText( tr( "Write" ) );
    PIAGENERALCONTROLREGISTER->setTitle( tr( "General Control Register" ) );
    piaGCRWrite->setText( tr( "Write" ) );
    piaGCR->setText( tr( "0x0000" ) );
    piaWidth100->setText( tr( "100ns" ) );
    piaWidth500->setText( tr( "500ns" ) );
    piaEnp->setText( tr( "Enable operation of port" ) );
    piaStrb->setText( tr( "Strobed operation" ) );
    piaStrinp->setText( tr( "Polarity of input strobe on port" ) );
    piaEnintA->setText( tr( "Enable generation of interrupt to FEC" ) );
    piaWidth200->setText( tr( "200ns" ) );
    TextLabel25_2_2->setText( tr( "Width of the strobe signals" ) );
    piaWidth1000->setText( tr( "1000ns" ) );
    piaStroutp->setText( tr( "Polarity of output strobe on part A" ) );
    piaGCRRead->setText( tr( "Read" ) );
    piaGCRWriteBit->setText( tr( "Write" ) );
    PIACOMMAND->setTitle( tr( "PIA Command" ) );
    piaChannelReset->setText( tr( "Reset Channel" ) );
    piaEnable->setText( tr( "Enable Channel" ) );
    PIADATADIRECTIONPORT->setTitle( tr( "Data Direction Register Port" ) );
    piaDDRPWrite->setText( tr( "Write" ) );
    TextLabel3->setText( tr( "Input" ) );
    TextLabel2_4->setText( tr( "Output" ) );
    TextLabel2_4_2->setText( tr( "Output" ) );
    TextLabel3_2->setText( tr( "Input" ) );
    piaDDRP->setText( tr( "0x00" ) );
    piaDDRBit1O->setText( QString::null );
    QWhatsThis::add( piaDDRBit1O, tr( "Set Output" ) );
    piaDDRBit2O->setText( QString::null );
    QWhatsThis::add( piaDDRBit2O, tr( "Set output" ) );
    piaDDRBit1I->setText( QString::null );
    QWhatsThis::add( piaDDRBit1I, tr( "Set Intput" ) );
    piaDDRBit0I->setText( QString::null );
    QWhatsThis::add( piaDDRBit0I, tr( "Set Input" ) );
    TextLabel1_2->setText( tr( "Bit 0" ) );
    TextLabel1_2_3->setText( tr( "Bit 2" ) );
    TextLabel1_2_3_2->setText( tr( "Bit 3" ) );
    piaDDRBit2I->setText( QString::null );
    QWhatsThis::add( piaDDRBit2I, tr( "Set Intput" ) );
    TextLabel1_2_2->setText( tr( "Bit 1" ) );
    piaDDRBit3I->setText( QString::null );
    QWhatsThis::add( piaDDRBit3I, tr( "Set Input" ) );
    piaDDRBit3O->setText( QString::null );
    QWhatsThis::add( piaDDRBit3O, tr( "Set Output" ) );
    piaDDRBit0O->setText( QString::null );
    QWhatsThis::add( piaDDRBit0O, tr( "Set output" ) );
    piaDDRBit7O->setText( QString::null );
    QWhatsThis::add( piaDDRBit7O, tr( "Set Output" ) );
    piaDDRBit6O->setText( QString::null );
    QWhatsThis::add( piaDDRBit6O, tr( "Set Output" ) );
    piaDDRBit4I->setText( QString::null );
    QWhatsThis::add( piaDDRBit4I, tr( "Set Input" ) );
    piaDDRBit6I->setText( QString::null );
    QWhatsThis::add( piaDDRBit6I, tr( "Set Input" ) );
    piaDDRBit4O->setText( QString::null );
    QWhatsThis::add( piaDDRBit4O, tr( "Set Output" ) );
    piaDDRBit7I->setText( QString::null );
    QWhatsThis::add( piaDDRBit7I, tr( "Set Input" ) );
    TextLabel1_2_4->setText( tr( "Bit 4" ) );
    TextLabel1_2_2_2->setText( tr( "Bit 5" ) );
    piaDDRBit5O->setText( QString::null );
    QWhatsThis::add( piaDDRBit5O, tr( "Set Output" ) );
    TextLabel1_2_3_3->setText( tr( "Bit 6" ) );
    piaDDRBit5I->setText( QString::null );
    QWhatsThis::add( piaDDRBit5I, tr( "Set Input" ) );
    TextLabel1_2_3_2_2->setText( tr( "Bit 7" ) );
    piaDDRPRead->setText( tr( "Read" ) );
    piaDDRPWriteBit->setText( tr( "Write" ) );
    FEC->changeTab( tab_5, tr( "PIA Channels" ) );
    TRACKERDEVICES->setTitle( tr( "Tracker devices" ) );
    trackerDeleteDevice->setText( tr( "Delete selected devices" ) );
    trackerAutoDectection->setText( tr( "Auto dectection for Tracker devices" ) );
    QWhatsThis::add( trackerAutoDectection, tr( "Detect for all FECs/CCUs APV/PLL/MUX/Laserdriver/DCU" ) );
    TRACKERLOOP->setTitle( tr( "Loop && Command" ) );
    TextLabel8->setText( tr( "Loop" ) );
    trackerDecimal->setText( tr( "Value in decimal" ) );
    trackerErrorNumber->setText( tr( "0" ) );
    trackerLoopNumber->setText( tr( "1" ) );
    trackerAllDevices->setText( tr( "All devices" ) );
    TextLabel8_3->setText( tr( "Error" ) );
    trackerInfiniteLoop->setText( tr( "Infinite Loop" ) );
    trackerDoComparison->setText( tr( "Do the comparison" ) );
    trackerDefaultValues->setText( tr( "Default Values" ) );
    trackerAllRead->setText( tr( "Upload" ) );
    trackerAllWrite->setText( tr( "Download" ) );
    DeviceDriver_2->setTitle( tr( "Device driver" ) );
    trackerFecWarningDevice->setText( tr( "0" ) );
    trackerFecNbPlxReset->setText( tr( "0" ) );
    trackerFecBadTransaction->setText( tr( "0" ) );
    TextLabel16_2->setText( tr( "Number of FEC reset" ) );
    TextLabel12_2->setText( tr( "Too short frame errors" ) );
    trackerFecDDStatus->setText( tr( "0x0000" ) );
    TextLabel15_2->setText( tr( "Number of PLX reset" ) );
    TextLabel13_2->setText( tr( "Bad transaction errors" ) );
    TextLabel11_2->setText( tr( "Too long frame errors" ) );
    TextLabel14_2->setText( tr( "Warning emitted by a device" ) );
    trackerFecLongFrame->setText( tr( "0" ) );
    TextLabel10_2->setText( tr( "Device driver status" ) );
    trackerFecNbFecReset->setText( tr( "0" ) );
    trackerFecShortFrame->setText( tr( "0" ) );
    trackerFecEnableErrorCounter->setText( tr( "Enable error count" ) );
    trackerFecResetCounter->setText( tr( "Reset Error Counters" ) );
    trackerFecReadDeviceDriver->setText( tr( "Read" ) );
    TRACKERPIADATAREGISTER->setTitle( tr( "PIA Reset settings" ) );
    TextLabel5_2->setText( trUtf8( "\x50\x49\x41\x20\x64\x75\x72\x61\x74\x69\x6f\x6e\x20\x28\xc2\xb5\x73\x29" ) );
    TextLabel4_2->setText( trUtf8( "\x53\x6c\x65\x65\x70\x20\x74\x69\x6d\x65\x20\x28\xc2\xb5\x73\x29" ) );
    trackerPiaEnableOperationBit->setText( tr( "Enable operation bit per bit" ) );
    TextLabel26_2_3->setText( tr( "PIA Channels" ) );
    TextLabel2_3_2_3_3->setText( tr( "FEC slots" ) );
    TextLabel2_5_2_3_3->setText( tr( "CCU Address" ) );
    trackerPiaDuration->setText( tr( "10" ) );
    trackerPiaSleepTime->setText( tr( "10000" ) );
    trackerSelectAllPiaB->setText( tr( "Select All PIA reset" ) );
    trackerPiaDataRegisterWrite->setText( tr( "Write" ) );
    trackerPiaResetAdd->setText( tr( "Add" ) );
    trackerClearListPiaReset->setText( tr( "Clear" ) );
    trackerPiaResetDelete->setText( tr( "Delete" ) );
    trackerPiaDataRegister->setText( tr( "0xFF" ) );
    trackerPiaResetAddAllCCU->setText( tr( "Add all CCU" ) );
    QWhatsThis::add( trackerPiaFecSlots, tr( "FEC device driver slot" ) );
    QWhatsThis::add( trackerPiaCcuAddresses, tr( "FEC device driver slot" ) );
    GroupBox175->setTitle( tr( "APV Mux" ) );
    TextLabel6_14->setText( tr( "Resistor" ) );
    apvMuxResistorT->setText( tr( "0xFF" ) );
    trackerApvMuxRead->setText( tr( "Read" ) );
    trackerApvMuxWrite->setText( tr( "Write" ) );
    GroupBox139->setTitle( tr( "APV" ) );
    apvErrorT->setText( tr( "0x00" ) );
    TextLabel6_5_3->setText( tr( "Error" ) );
    TextLabel6_4_2->setText( tr( "ISPARE" ) );
    TextLabel6_12->setText( tr( "CDRV" ) );
    apvIshaT->setText( tr( "0x32" ) );
    apvVfpT->setText( tr( "0x43" ) );
    apvModeT->setText( tr( "0x2B" ) );
    TextLabel6_3->setText( tr( "IPSF" ) );
    apvLatencyT->setText( tr( "0x64" ) );
    apvIssfT->setText( tr( "0x32" ) );
    apvVfsT->setText( tr( "0x43" ) );
    TextLabel6_4_3->setText( tr( "Csel" ) );
    TextLabel6_9_2->setText( tr( "VPSP" ) );
    TextLabel6_11->setText( tr( "ICAL" ) );
    apvMuxGainT->setText( tr( "0x04" ) );
    TextLabel6_2->setText( tr( "Latency" ) );
    apvIpspT->setText( tr( "0x50" ) );
    apvVpspT->setText( tr( "0x14" ) );
    TextLabel6_5_2->setText( tr( "VFP" ) );
    TextLabel6_7->setText( tr( "ISHA" ) );
    TextLabel6_6->setText( tr( "IPRE" ) );
    apvIpreT->setText( tr( "0x73" ) );
    apvImuxinT->setText( tr( "0x32" ) );
    apvCdrvT->setText( tr( "0xFB" ) );
    TextLabel6_7_2->setText( tr( "VFS" ) );
    TextLabel6_10->setText( tr( "IMUXIN" ) );
    apvIpcascT->setText( tr( "0x3C" ) );
    TextLabel6_9->setText( tr( "IPSP" ) );
    apvIcalT->setText( tr( "0x50" ) );
    TextLabel6_8->setText( tr( "ISSF" ) );
    apvCselT->setText( tr( "0xFE" ) );
    TextLabel6_5->setText( tr( "IPCASC" ) );
    apvIpsfT->setText( tr( "0x32" ) );
    apvIspareT->setText( tr( "0x00" ) );
    TextLabel6_4->setText( tr( "Mux Gain" ) );
    TextLabel6->setText( tr( "APV Mode" ) );
    trackerApvRead->setText( tr( "Read" ) );
    trackerApvWrite->setText( tr( "Write" ) );
    GroupBox175_2->setTitle( tr( "PLL" ) );
    TextLabel6_4_3_3->setText( tr( "Delay fine" ) );
    TextLabel6_5_3_3->setText( tr( "Delay coarse" ) );
    pllDelayCoarseT->setText( tr( "0x01" ) );
    pllDelayFineT->setText( tr( "0x06" ) );
    trackerPllRead->setText( tr( "Read" ) );
    trackerPllWrite->setText( tr( "Write" ) );
    GroupBox175_2_2->setTitle( tr( "Laserdriver (4.2)" ) );
    trackerLaserdriverRead->setText( tr( "Read" ) );
    trackerLaserdriverWrite->setText( tr( "Write" ) );
    laserdriverGain1T->setText( tr( "0x2" ) );
    TextLabel6_5_5->setText( tr( "Gain 3" ) );
    laserdriverGain3T->setText( tr( "0x2" ) );
    TextLabel6_6_3->setText( tr( "Bias 2" ) );
    TextLabel6_4_5->setText( tr( "Gain 2" ) );
    TextLabel6_15->setText( tr( "Gain 1" ) );
    laserdriverBias1T->setText( tr( "0x12" ) );
    TextLabel6_2_3->setText( tr( "Bias 1" ) );
    TextLabel6_3_3->setText( tr( "Bias 3" ) );
    laserdriverBias2T->setText( tr( "0x12" ) );
    laserdriverBias3T->setText( tr( "0x12" ) );
    laserdriverGain2T->setText( tr( "0x2" ) );
    TextLabel1_4->setText( tr( "<font size=\"-1\">DOH is automatically configured with predefined values</font>" ) );
    TRACKERDCU->setTitle( tr( "DCU" ) );
    TextLabel6_7_4->setText( tr( "Channel 7" ) );
    TextLabel6_2_4->setText( tr( "Channel 2" ) );
    dcuChannel4T->setText( tr( "0" ) );
    dcuChannel6Text->setText( tr( "Channel 6" ) );
    TextLabel6_5_6->setText( tr( "Channel 5" ) );
    TextLabel6_4_6->setText( tr( "Channel 3" ) );
    TextLabel6_8_3->setText( tr( "Channel 8" ) );
    TextLabel6_17->setText( tr( "Channel 1" ) );
    dcuChannel6T->setText( tr( "0" ) );
    TextLabel6_6_4->setText( tr( "Channel 4" ) );
    dcuChannel7T->setText( tr( "0" ) );
    dcuChannel5T->setText( tr( "0" ) );
    dcuChannel2T->setText( tr( "0" ) );
    TextLabel6_16->setText( tr( "DCU Hardware ID" ) );
    dcuHardIdT->setText( tr( "0x000000" ) );
    dcuChannel8T->setText( tr( "0" ) );
    trackerDcuRead->setText( tr( "Read" ) );
    dcuChannel3T->setText( tr( "0" ) );
    dcuChannel1T->setText( tr( "0" ) );
    GroupBox181->setTitle( tr( "XML" ) );
    trackerUseFile->setText( tr( "File" ) );
    trackerUseDatabase->setText( tr( "Database" ) );
    trackerFindFile->setText( tr( "Find" ) );
    trackerXMLDownload->setText( tr( "Load" ) );
    trackerXMLUpload->setText( tr( "Save" ) );
    trackerStructureName->setText( tr( "StructureName" ) );
    trackerPartitionName->setText( tr( "PartitionName" ) );
    textLabelStructureName->setText( tr( "Structure Name" ) );
    textLabelPartitionName->setText( tr( "Partition Name" ) );
    FEC->changeTab( tab_6, tr( "Tracker Module" ) );
    TextLabel2_6->setText( tr( "FEC slots" ) );
    QWhatsThis::add( fecSlotsCcuRedundancy, tr( "FEC device driver slot" ) );
    CCURedundancyClearButton->setText( tr( "Clear" ) );
    TextLabel2_5_3->setText( tr( "CCU Address" ) );
    ccuRedundancyIRQEnableButton->setText( tr( "IRQ Enable" ) );
    QWhatsThis::add( ccuRedundancyAddresses, tr( "FEC device driver slot" ) );
    ccuRedundancyReadDDStatusButton->setText( tr( "IRQ Status" ) );
    CCURedundancyFecInputGroup->setTitle( tr( "FEC Input" ) );
    textLabel4_3->setText( tr( "Input" ) );
    CCURedundancyFecInputAButton->setText( tr( "A" ) );
    CCURedundancyFecInputBButton->setText( tr( "B" ) );
    ccuRedundancyScanForCcus->setText( tr( "Scan for CCUs" ) );
    QWhatsThis::add( ccuRedundancyScanForCcus, tr( "Scan all PCI slot for device driver" ) );
    CCURedundancyCacheButton->setText( tr( "Start" ) );
    CCURedundancyTestRingB->setText( tr( "Test Ring B" ) );
    QWhatsThis::add( CCURedundancyTestRingB, tr( "Scan all PCI slot for device driver" ) );
    CCURedundancyFecOutputGroup->setTitle( tr( "FEC Output" ) );
    textLabel4_2_2_2->setText( tr( "Output" ) );
    CCURedundancyFecOutputAButton->setText( tr( "A" ) );
    CCURedundancyFecOutputBButton->setText( tr( "B" ) );
    CCURedundancyAddCCU->setTitle( tr( "CCU by hand" ) );
    textLabel5->setText( tr( "CCU Address" ) );
    CCURedundancyCCUHandCCUAddress->setText( tr( "0x" ) );
    CCURedundancyCCUHandAddCCUButton->setText( tr( "Add CCU" ) );
    CCURedundancySave->setText( tr( "Save" ) );
    CCURedundancyLoad->setText( tr( "Load" ) );
    buttonGroup1->setTitle( tr( "Status / Error Messages" ) );
    QWhatsThis::add( ccuRedundancyErrorText, tr( "Error Message for the redundancy" ) );
    ccuRedundancyClearErrorMessages->setText( tr( "Clear" ) );
    ccuOrderLabel->setText( QString::null );
    QWhatsThis::add( ccuOrderLabel, tr( "CCU order after a reset and a scan ring" ) );
    FEC->changeTab( tab_7, tr( "CCU Redundancy" ) );
}

void FecDialog::scanForFecs()
{
    qWarning( "FecDialog::scanForFecs(): Not implemented yet" );
}

void FecDialog::fecReadCR0()
{
    qWarning( "FecDialog::fecReadCR0(): Not implemented yet" );
}

void FecDialog::fecWriteCR0()
{
    qWarning( "FecDialog::fecWriteCR0(): Not implemented yet" );
}

void FecDialog::fecWriteCR0Bit()
{
    qWarning( "FecDialog::fecWriteCR0Bit(): Not implemented yet" );
}

void FecDialog::fecWriteCR1()
{
    qWarning( "FecDialog::fecWriteCR1(): Not implemented yet" );
}

void FecDialog::fecReadCR1()
{
    qWarning( "FecDialog::fecReadCR1(): Not implemented yet" );
}

void FecDialog::fecWriteCR1Bit()
{
    qWarning( "FecDialog::fecWriteCR1Bit(): Not implemented yet" );
}

void FecDialog::fecReadSR0()
{
    qWarning( "FecDialog::fecReadSR0(): Not implemented yet" );
}

void FecDialog::fecReadSR1()
{
    qWarning( "FecDialog::fecReadSR1(): Not implemented yet" );
}

void FecDialog::fecReadDDStatus()
{
    qWarning( "FecDialog::fecReadDDStatus(): Not implemented yet" );
}

void FecDialog::fecSoftReset()
{
    qWarning( "FecDialog::fecSoftReset(): Not implemented yet" );
}

void FecDialog::scanForCcus()
{
    qWarning( "FecDialog::scanForCcus(): Not implemented yet" );
}

void FecDialog::fecPlxReset()
{
    qWarning( "FecDialog::fecPlxReset(): Not implemented yet" );
}

void FecDialog::ccuReadCRA()
{
    qWarning( "FecDialog::ccuReadCRA(): Not implemented yet" );
}

void FecDialog::fecResetErrorCounter()
{
    qWarning( "FecDialog::fecResetErrorCounter(): Not implemented yet" );
}

void FecDialog::fecReadAllRegisters()
{
    qWarning( "FecDialog::fecReadAllRegisters(): Not implemented yet" );
}

void FecDialog::fecReleaseFec()
{
    qWarning( "FecDialog::fecReleaseFec(): Not implemented yet" );
}

void FecDialog::ccuWriteCRABit()
{
    qWarning( "FecDialog::ccuWriteCRABit(): Not implemented yet" );
}

void FecDialog::ccuWriteCRA()
{
    qWarning( "FecDialog::ccuWriteCRA(): Not implemented yet" );
}

void FecDialog::ccuReadCRB()
{
    qWarning( "FecDialog::ccuReadCRB(): Not implemented yet" );
}

void FecDialog::ccuReadCRC()
{
    qWarning( "FecDialog::ccuReadCRC(): Not implemented yet" );
}

void FecDialog::ccuReadCRD()
{
    qWarning( "FecDialog::ccuReadCRD(): Not implemented yet" );
}

void FecDialog::ccuReadCRE()
{
    qWarning( "FecDialog::ccuReadCRE(): Not implemented yet" );
}

void FecDialog::ccuReadSR()
{
    qWarning( "FecDialog::ccuReadSR(): Not implemented yet" );
}

void FecDialog::ccuReadSRA()
{
    qWarning( "FecDialog::ccuReadSRA(): Not implemented yet" );
}

void FecDialog::ccuWriteCRB()
{
    qWarning( "FecDialog::ccuWriteCRB(): Not implemented yet" );
}

void FecDialog::ccuWriteCRBBit()
{
    qWarning( "FecDialog::ccuWriteCRBBit(): Not implemented yet" );
}

void FecDialog::ccuWriteCRC()
{
    qWarning( "FecDialog::ccuWriteCRC(): Not implemented yet" );
}

void FecDialog::ccuWriteCRCBit()
{
    qWarning( "FecDialog::ccuWriteCRCBit(): Not implemented yet" );
}

void FecDialog::ccuWriteCRD()
{
    qWarning( "FecDialog::ccuWriteCRD(): Not implemented yet" );
}

void FecDialog::ccuWriteCRE()
{
    qWarning( "FecDialog::ccuWriteCRE(): Not implemented yet" );
}

void FecDialog::ccuWriteCREBit()
{
    qWarning( "FecDialog::ccuWriteCREBit(): Not implemented yet" );
}

void FecDialog::ccuReadSRE()
{
    qWarning( "FecDialog::ccuReadSRE(): Not implemented yet" );
}

void FecDialog::ccuSelected()
{
    qWarning( "FecDialog::ccuSelected(): Not implemented yet" );
}

void FecDialog::ccuReadAll()
{
    qWarning( "FecDialog::ccuReadAll(): Not implemented yet" );
}

void FecDialog::i2cEnableChannel()
{
    qWarning( "FecDialog::i2cEnableChannel(): Not implemented yet" );
}

void FecDialog::i2cReadAll()
{
    qWarning( "FecDialog::i2cReadAll(): Not implemented yet" );
}

void FecDialog::i2cReadCRA()
{
    qWarning( "FecDialog::i2cReadCRA(): Not implemented yet" );
}

void FecDialog::i2cReadSR()
{
    qWarning( "FecDialog::i2cReadSR(): Not implemented yet" );
}

void FecDialog::i2cReadSRA()
{
    qWarning( "FecDialog::i2cReadSRA(): Not implemented yet" );
}

void FecDialog::i2cWriteCRA()
{
    qWarning( "FecDialog::i2cWriteCRA(): Not implemented yet" );
}

void FecDialog::i2cWriteCRABit()
{
    qWarning( "FecDialog::i2cWriteCRABit(): Not implemented yet" );
}

void FecDialog::scanFecsCcusDevices()
{
    qWarning( "FecDialog::scanFecsCcusDevices(): Not implemented yet" );
}

void FecDialog::scanForI2CDevices()
{
    qWarning( "FecDialog::scanForI2CDevices(): Not implemented yet" );
}

void FecDialog::i2cReadModifyWrite()
{
    qWarning( "FecDialog::i2cReadModifyWrite(): Not implemented yet" );
}

void FecDialog::i2cReadI2c()
{
    qWarning( "FecDialog::i2cReadI2c(): Not implemented yet" );
}

void FecDialog::i2cWriteI2c()
{
    qWarning( "FecDialog::i2cWriteI2c(): Not implemented yet" );
}

void FecDialog::i2cSetSpeed100()
{
    qWarning( "FecDialog::i2cSetSpeed100(): Not implemented yet" );
}

void FecDialog::i2cSetSpeed200()
{
    qWarning( "FecDialog::i2cSetSpeed200(): Not implemented yet" );
}

void FecDialog::i2cSetSpeed400()
{
    qWarning( "FecDialog::i2cSetSpeed400(): Not implemented yet" );
}

void FecDialog::i2cSetSpeed1000()
{
    qWarning( "FecDialog::i2cSetSpeed1000(): Not implemented yet" );
}

void FecDialog::i2cSetMaskAnd()
{
    qWarning( "FecDialog::i2cSetMaskAnd(): Not implemented yet" );
}

void FecDialog::i2cSetMaskOr()
{
    qWarning( "FecDialog::i2cSetMaskOr(): Not implemented yet" );
}

void FecDialog::i2cSetMaskXor()
{
    qWarning( "FecDialog::i2cSetMaskXor(): Not implemented yet" );
}

void FecDialog::i2cIsChannelEnable()
{
    qWarning( "FecDialog::i2cIsChannelEnable(): Not implemented yet" );
}

void FecDialog::i2cResetChannel()
{
    qWarning( "FecDialog::i2cResetChannel(): Not implemented yet" );
}

void FecDialog::ccuEnableAllChannels()
{
    qWarning( "FecDialog::ccuEnableAllChannels(): Not implemented yet" );
}

void FecDialog::i2cSetModeExtended()
{
    qWarning( "FecDialog::i2cSetModeExtended(): Not implemented yet" );
}

void FecDialog::i2cSetModeNormal()
{
    qWarning( "FecDialog::i2cSetModeNormal(): Not implemented yet" );
}

void FecDialog::i2cSetModeRal()
{
    qWarning( "FecDialog::i2cSetModeRal(): Not implemented yet" );
}

void FecDialog::memoryEnableChannel()
{
    qWarning( "FecDialog::memoryEnableChannel(): Not implemented yet" );
}

void FecDialog::memoryReadCRA()
{
    qWarning( "FecDialog::memoryReadCRA(): Not implemented yet" );
}

void FecDialog::memoryWriteCRA()
{
    qWarning( "FecDialog::memoryWriteCRA(): Not implemented yet" );
}

void FecDialog::memoryWriteCRABit()
{
    qWarning( "FecDialog::memoryWriteCRABit(): Not implemented yet" );
}

void FecDialog::memorySetSpeed1()
{
    qWarning( "FecDialog::memorySetSpeed1(): Not implemented yet" );
}

void FecDialog::memorySetSpeed4()
{
    qWarning( "FecDialog::memorySetSpeed4(): Not implemented yet" );
}

void FecDialog::memorySetSpeed10()
{
    qWarning( "FecDialog::memorySetSpeed10(): Not implemented yet" );
}

void FecDialog::memorySetSpeed20()
{
    qWarning( "FecDialog::memorySetSpeed20(): Not implemented yet" );
}

void FecDialog::memoryReadSR()
{
    qWarning( "FecDialog::memoryReadSR(): Not implemented yet" );
}

void FecDialog::memoryReadAll()
{
    qWarning( "FecDialog::memoryReadAll(): Not implemented yet" );
}

void FecDialog::memoryResetChannel()
{
    qWarning( "FecDialog::memoryResetChannel(): Not implemented yet" );
}

void FecDialog::memoryReadWindowRegisters()
{
    qWarning( "FecDialog::memoryReadWindowRegisters(): Not implemented yet" );
}

void FecDialog::memoryWriteWindowRegisters()
{
    qWarning( "FecDialog::memoryWriteWindowRegisters(): Not implemented yet" );
}

void FecDialog::memoryReadModifyWrite()
{
    qWarning( "FecDialog::memoryReadModifyWrite(): Not implemented yet" );
}

void FecDialog::memoryReadSB()
{
    qWarning( "FecDialog::memoryReadSB(): Not implemented yet" );
}

void FecDialog::memoryWriteSB()
{
    qWarning( "FecDialog::memoryWriteSB(): Not implemented yet" );
}

void FecDialog::memoryReadMB()
{
    qWarning( "FecDialog::memoryReadMB(): Not implemented yet" );
}

void FecDialog::memoryWriteMB()
{
    qWarning( "FecDialog::memoryWriteMB(): Not implemented yet" );
}

void FecDialog::memoryReadMaskReg()
{
    qWarning( "FecDialog::memoryReadMaskReg(): Not implemented yet" );
}

void FecDialog::memorySetMaskAnd()
{
    qWarning( "FecDialog::memorySetMaskAnd(): Not implemented yet" );
}

void FecDialog::memorySetMaskOr()
{
    qWarning( "FecDialog::memorySetMaskOr(): Not implemented yet" );
}

void FecDialog::memorySetMaskXor()
{
    qWarning( "FecDialog::memorySetMaskXor(): Not implemented yet" );
}

void FecDialog::piaDataRead()
{
    qWarning( "FecDialog::piaDataRead(): Not implemented yet" );
}

void FecDialog::piaDataWrite()
{
    qWarning( "FecDialog::piaDataWrite(): Not implemented yet" );
}

void FecDialog::piaEnableChannel()
{
    qWarning( "FecDialog::piaEnableChannel(): Not implemented yet" );
}

void FecDialog::piaIsChannelEnable()
{
    qWarning( "FecDialog::piaIsChannelEnable(): Not implemented yet" );
}

void FecDialog::piaReadAll()
{
    qWarning( "FecDialog::piaReadAll(): Not implemented yet" );
}

void FecDialog::piaReadDDRP()
{
    qWarning( "FecDialog::piaReadDDRP(): Not implemented yet" );
}

void FecDialog::piaReadGCR()
{
    qWarning( "FecDialog::piaReadGCR(): Not implemented yet" );
}

void FecDialog::piaReadSR()
{
    qWarning( "FecDialog::piaReadSR(): Not implemented yet" );
}

void FecDialog::piaResetChannel()
{
    qWarning( "FecDialog::piaResetChannel(): Not implemented yet" );
}

void FecDialog::piaWriteDDRP()
{
    qWarning( "FecDialog::piaWriteDDRP(): Not implemented yet" );
}

void FecDialog::piaWriteDDRPBit()
{
    qWarning( "FecDialog::piaWriteDDRPBit(): Not implemented yet" );
}

void FecDialog::piaWriteGCR()
{
    qWarning( "FecDialog::piaWriteGCR(): Not implemented yet" );
}

void FecDialog::piaWriteGCRBit()
{
    qWarning( "FecDialog::piaWriteGCRBit(): Not implemented yet" );
}

void FecDialog::memoryWriteCompareMB()
{
    qWarning( "FecDialog::memoryWriteCompareMB(): Not implemented yet" );
}

void FecDialog::scanForTrackerDevices()
{
    qWarning( "FecDialog::scanForTrackerDevices(): Not implemented yet" );
}

void FecDialog::fecEnableCounters()
{
    qWarning( "FecDialog::fecEnableCounters(): Not implemented yet" );
}

void FecDialog::trackerFecEnableCounters()
{
    qWarning( "FecDialog::trackerFecEnableCounters(): Not implemented yet" );
}

void FecDialog::trackerAddPiaKey()
{
    qWarning( "FecDialog::trackerAddPiaKey(): Not implemented yet" );
}

void FecDialog::trackerDeletePiaKey()
{
    qWarning( "FecDialog::trackerDeletePiaKey(): Not implemented yet" );
}

void FecDialog::trackerReadAll()
{
    qWarning( "FecDialog::trackerReadAll(): Not implemented yet" );
}

void FecDialog::trackerWriteAll()
{
    qWarning( "FecDialog::trackerWriteAll(): Not implemented yet" );
}

void FecDialog::trackerSetAllDevicesSelected()
{
    qWarning( "FecDialog::trackerSetAllDevicesSelected(): Not implemented yet" );
}

void FecDialog::trackerFindXMLFile()
{
    qWarning( "FecDialog::trackerFindXMLFile(): Not implemented yet" );
}

void FecDialog::helpAbout()
{
    qWarning( "FecDialog::helpAbout(): Not implemented yet" );
}

void FecDialog::trackerLoadXML()
{
    qWarning( "FecDialog::trackerLoadXML(): Not implemented yet" );
}

void FecDialog::trackerWritePiaSelected()
{
    qWarning( "FecDialog::trackerWritePiaSelected(): Not implemented yet" );
}

void FecDialog::trackerSetAllPiaSelected()
{
    qWarning( "FecDialog::trackerSetAllPiaSelected(): Not implemented yet" );
}

void FecDialog::trackerListDeviceDetectedChanged()
{
    qWarning( "FecDialog::trackerListDeviceDetectedChanged(): Not implemented yet" );
}

void FecDialog::trackerSetDefaultValues()
{
    qWarning( "FecDialog::trackerSetDefaultValues(): Not implemented yet" );
}

void FecDialog::trackerSaveXML()
{
    qWarning( "FecDialog::trackerSaveXML(): Not implemented yet" );
}

void FecDialog::quitTout()
{
    qWarning( "FecDialog::quitTout(): Not implemented yet" );
}

void FecDialog::trackerFileToggle()
{
    qWarning( "FecDialog::trackerFileToggle(): Not implemented yet" );
}

void FecDialog::trackerDeleteDeviceList()
{
    qWarning( "FecDialog::trackerDeleteDeviceList(): Not implemented yet" );
}

void FecDialog::trackerSelectPartition()
{
    qWarning( "FecDialog::trackerSelectPartition(): Not implemented yet" );
}

void FecDialog::trackerDatabaseToggle()
{
    qWarning( "FecDialog::trackerDatabaseToggle(): Not implemented yet" );
}

void FecDialog::trackerPiaResetAddAllCCUKey()
{
    qWarning( "FecDialog::trackerPiaResetAddAllCCUKey(): Not implemented yet" );
}

void FecDialog::ccuRedundancyCache()
{
    qWarning( "FecDialog::ccuRedundancyCache(): Not implemented yet" );
}

void FecDialog::startThread()
{
    qWarning( "FecDialog::startThread(): Not implemented yet" );
}

void FecDialog::fecRedundancy()
{
    qWarning( "FecDialog::fecRedundancy(): Not implemented yet" );
}

void FecDialog::ccuRedundancyAddCcuHand()
{
    qWarning( "FecDialog::ccuRedundancyAddCcuHand(): Not implemented yet" );
}

void FecDialog::fecEmptyFifoRet()
{
    qWarning( "FecDialog::fecEmptyFifoRet(): Not implemented yet" );
}

void FecDialog::fecEmptyFifoTra()
{
    qWarning( "FecDialog::fecEmptyFifoTra(): Not implemented yet" );
}

void FecDialog::fecReadFifoRec()
{
    qWarning( "FecDialog::fecReadFifoRec(): Not implemented yet" );
}

void FecDialog::fecReadFifoRet()
{
    qWarning( "FecDialog::fecReadFifoRet(): Not implemented yet" );
}

void FecDialog::fecReadFifoTra()
{
    qWarning( "FecDialog::fecReadFifoTra(): Not implemented yet" );
}

void FecDialog::fecWriteFifoRec()
{
    qWarning( "FecDialog::fecWriteFifoRec(): Not implemented yet" );
}

void FecDialog::fecWriteFifoRet()
{
    qWarning( "FecDialog::fecWriteFifoRet(): Not implemented yet" );
}

void FecDialog::fecWriteFifoTra()
{
    qWarning( "FecDialog::fecWriteFifoTra(): Not implemented yet" );
}

void FecDialog::fecEmptyFifoRec()
{
    qWarning( "FecDialog::fecEmptyFifoRec(): Not implemented yet" );
}

void FecDialog::fecDisableReceiveFec()
{
    qWarning( "FecDialog::fecDisableReceiveFec(): Not implemented yet" );
}

void FecDialog::ddIRQEnableDisable()
{
    qWarning( "FecDialog::ddIRQEnableDisable(): Not implemented yet" );
}

void FecDialog::ccuRedundancyClearCCU()
{
    qWarning( "FecDialog::ccuRedundancyClearCCU(): Not implemented yet" );
}

void FecDialog::fecWriteCR0_()
{
    qWarning( "FecDialog::fecWriteCR0_(): Not implemented yet" );
}

void FecDialog::ccuRedundancyTestRingB()
{
    qWarning( "FecDialog::ccuRedundancyTestRingB(): Not implemented yet" );
}

void FecDialog::fecSlotsCcuModify()
{
    qWarning( "FecDialog::fecSlotsCcuModify(): Not implemented yet" );
}

void FecDialog::fecSlotsI2cModify()
{
    qWarning( "FecDialog::fecSlotsI2cModify(): Not implemented yet" );
}

void FecDialog::fecSlotsMemoryModify()
{
    qWarning( "FecDialog::fecSlotsMemoryModify(): Not implemented yet" );
}

void FecDialog::fecSlotsPiaModify()
{
    qWarning( "FecDialog::fecSlotsPiaModify(): Not implemented yet" );
}

void FecDialog::fecSlotsRedundancyModify()
{
    qWarning( "FecDialog::fecSlotsRedundancyModify(): Not implemented yet" );
}

void FecDialog::fecSlotsModify()
{
    qWarning( "FecDialog::fecSlotsModify(): Not implemented yet" );
}

void FecDialog::ccuRedundanceSaveCCU()
{
    qWarning( "FecDialog::ccuRedundanceSaveCCU(): Not implemented yet" );
}

void FecDialog::ccuRedundancyLoadCCU()
{
    qWarning( "FecDialog::ccuRedundancyLoadCCU(): Not implemented yet" );
}

void FecDialog::fecResetFSM_pressed()
{
    qWarning( "FecDialog::fecResetFSM_pressed(): Not implemented yet" );
}

void FecDialog::vmeCrateReset_pressed()
{
    qWarning( "FecDialog::vmeCrateReset_pressed(): Not implemented yet" );
}

