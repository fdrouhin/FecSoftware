/****************************************************************************
** GroupCcuRedundancy meta object code from reading C++ file 'GroupCcuRedundancy.h'
**
** Created by Qt MOC ($Id: qt/moc_yacc.cpp   3.3.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "../include/GroupCcuRedundancy.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *GroupCcuRedundancy::className() const
{
    return "GroupCcuRedundancy";
}

QMetaObject *GroupCcuRedundancy::metaObj = 0;
static QMetaObjectCleanUp cleanUp_GroupCcuRedundancy( "GroupCcuRedundancy", &GroupCcuRedundancy::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString GroupCcuRedundancy::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "GroupCcuRedundancy", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString GroupCcuRedundancy::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "GroupCcuRedundancy", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* GroupCcuRedundancy::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QGroupBox::staticMetaObject();
    static const QUMethod slot_0 = {"redundancyChangeInput", 0, 0 };
    static const QUMethod slot_1 = {"redundancyChangeOutput", 0, 0 };
    static const QMetaData slot_tbl[] = {
	{ "redundancyChangeInput()", &slot_0, QMetaData::Public },
	{ "redundancyChangeOutput()", &slot_1, QMetaData::Public }
    };
    metaObj = QMetaObject::new_metaobject(
	"GroupCcuRedundancy", parentObject,
	slot_tbl, 2,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_GroupCcuRedundancy.setMetaObject( metaObj );
    return metaObj;
}

void* GroupCcuRedundancy::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "GroupCcuRedundancy" ) )
	return this;
    return QGroupBox::qt_cast( clname );
}

bool GroupCcuRedundancy::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: redundancyChangeInput(); break;
    case 1: redundancyChangeOutput(); break;
    default:
	return QGroupBox::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool GroupCcuRedundancy::qt_emit( int _id, QUObject* _o )
{
    return QGroupBox::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool GroupCcuRedundancy::qt_property( int id, int f, QVariant* v)
{
    return QGroupBox::qt_property( id, f, v);
}

bool GroupCcuRedundancy::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
