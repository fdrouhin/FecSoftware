/****************************************************************************
** FecDialog meta object code from reading C++ file 'FecDialog.h'
**
** Created by Qt MOC ($Id: qt/moc_yacc.cpp   3.3.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "../.ui/FecDialog.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 26)
#error "This file was generated using the moc from 3.3.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *FecDialog::className() const
{
    return "FecDialog";
}

QMetaObject *FecDialog::metaObj = 0;
static QMetaObjectCleanUp cleanUp_FecDialog( "FecDialog", &FecDialog::staticMetaObject );

#ifndef QT_NO_TRANSLATION
QString FecDialog::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "FecDialog", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString FecDialog::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "FecDialog", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* FecDialog::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QDialog::staticMetaObject();
    static const QUMethod slot_0 = {"scanForFecs", 0, 0 };
    static const QUMethod slot_1 = {"fecReadCR0", 0, 0 };
    static const QUMethod slot_2 = {"fecWriteCR0", 0, 0 };
    static const QUMethod slot_3 = {"fecWriteCR0Bit", 0, 0 };
    static const QUMethod slot_4 = {"fecWriteCR1", 0, 0 };
    static const QUMethod slot_5 = {"fecReadCR1", 0, 0 };
    static const QUMethod slot_6 = {"fecWriteCR1Bit", 0, 0 };
    static const QUMethod slot_7 = {"fecReadSR0", 0, 0 };
    static const QUMethod slot_8 = {"fecReadSR1", 0, 0 };
    static const QUMethod slot_9 = {"fecReadDDStatus", 0, 0 };
    static const QUMethod slot_10 = {"fecSoftReset", 0, 0 };
    static const QUMethod slot_11 = {"scanForCcus", 0, 0 };
    static const QUMethod slot_12 = {"fecPlxReset", 0, 0 };
    static const QUMethod slot_13 = {"ccuReadCRA", 0, 0 };
    static const QUMethod slot_14 = {"fecResetErrorCounter", 0, 0 };
    static const QUMethod slot_15 = {"fecReadAllRegisters", 0, 0 };
    static const QUMethod slot_16 = {"fecReleaseFec", 0, 0 };
    static const QUMethod slot_17 = {"ccuWriteCRABit", 0, 0 };
    static const QUMethod slot_18 = {"ccuWriteCRA", 0, 0 };
    static const QUMethod slot_19 = {"ccuReadCRB", 0, 0 };
    static const QUMethod slot_20 = {"ccuReadCRC", 0, 0 };
    static const QUMethod slot_21 = {"ccuReadCRD", 0, 0 };
    static const QUMethod slot_22 = {"ccuReadCRE", 0, 0 };
    static const QUMethod slot_23 = {"ccuReadSR", 0, 0 };
    static const QUMethod slot_24 = {"ccuReadSRA", 0, 0 };
    static const QUMethod slot_25 = {"ccuWriteCRB", 0, 0 };
    static const QUMethod slot_26 = {"ccuWriteCRBBit", 0, 0 };
    static const QUMethod slot_27 = {"ccuWriteCRC", 0, 0 };
    static const QUMethod slot_28 = {"ccuWriteCRCBit", 0, 0 };
    static const QUMethod slot_29 = {"ccuWriteCRD", 0, 0 };
    static const QUMethod slot_30 = {"ccuWriteCRE", 0, 0 };
    static const QUMethod slot_31 = {"ccuWriteCREBit", 0, 0 };
    static const QUMethod slot_32 = {"ccuReadSRE", 0, 0 };
    static const QUMethod slot_33 = {"ccuSelected", 0, 0 };
    static const QUMethod slot_34 = {"ccuReadAll", 0, 0 };
    static const QUMethod slot_35 = {"i2cEnableChannel", 0, 0 };
    static const QUMethod slot_36 = {"i2cReadAll", 0, 0 };
    static const QUMethod slot_37 = {"i2cReadCRA", 0, 0 };
    static const QUMethod slot_38 = {"i2cReadSR", 0, 0 };
    static const QUMethod slot_39 = {"i2cReadSRA", 0, 0 };
    static const QUMethod slot_40 = {"i2cWriteCRA", 0, 0 };
    static const QUMethod slot_41 = {"i2cWriteCRABit", 0, 0 };
    static const QUMethod slot_42 = {"scanFecsCcusDevices", 0, 0 };
    static const QUMethod slot_43 = {"scanForI2CDevices", 0, 0 };
    static const QUMethod slot_44 = {"i2cReadModifyWrite", 0, 0 };
    static const QUMethod slot_45 = {"i2cReadI2c", 0, 0 };
    static const QUMethod slot_46 = {"i2cWriteI2c", 0, 0 };
    static const QUMethod slot_47 = {"i2cSetSpeed100", 0, 0 };
    static const QUMethod slot_48 = {"i2cSetSpeed200", 0, 0 };
    static const QUMethod slot_49 = {"i2cSetSpeed400", 0, 0 };
    static const QUMethod slot_50 = {"i2cSetSpeed1000", 0, 0 };
    static const QUMethod slot_51 = {"i2cSetMaskAnd", 0, 0 };
    static const QUMethod slot_52 = {"i2cSetMaskOr", 0, 0 };
    static const QUMethod slot_53 = {"i2cSetMaskXor", 0, 0 };
    static const QUMethod slot_54 = {"i2cIsChannelEnable", 0, 0 };
    static const QUMethod slot_55 = {"i2cResetChannel", 0, 0 };
    static const QUMethod slot_56 = {"ccuEnableAllChannels", 0, 0 };
    static const QUMethod slot_57 = {"i2cSetModeExtended", 0, 0 };
    static const QUMethod slot_58 = {"i2cSetModeNormal", 0, 0 };
    static const QUMethod slot_59 = {"i2cSetModeRal", 0, 0 };
    static const QUMethod slot_60 = {"memoryEnableChannel", 0, 0 };
    static const QUMethod slot_61 = {"memoryReadCRA", 0, 0 };
    static const QUMethod slot_62 = {"memoryWriteCRA", 0, 0 };
    static const QUMethod slot_63 = {"memoryWriteCRABit", 0, 0 };
    static const QUMethod slot_64 = {"memorySetSpeed1", 0, 0 };
    static const QUMethod slot_65 = {"memorySetSpeed4", 0, 0 };
    static const QUMethod slot_66 = {"memorySetSpeed10", 0, 0 };
    static const QUMethod slot_67 = {"memorySetSpeed20", 0, 0 };
    static const QUMethod slot_68 = {"memoryReadSR", 0, 0 };
    static const QUMethod slot_69 = {"memoryReadAll", 0, 0 };
    static const QUMethod slot_70 = {"memoryResetChannel", 0, 0 };
    static const QUMethod slot_71 = {"memoryReadWindowRegisters", 0, 0 };
    static const QUMethod slot_72 = {"memoryWriteWindowRegisters", 0, 0 };
    static const QUMethod slot_73 = {"memoryReadModifyWrite", 0, 0 };
    static const QUMethod slot_74 = {"memoryReadSB", 0, 0 };
    static const QUMethod slot_75 = {"memoryWriteSB", 0, 0 };
    static const QUMethod slot_76 = {"memoryReadMB", 0, 0 };
    static const QUMethod slot_77 = {"memoryWriteMB", 0, 0 };
    static const QUMethod slot_78 = {"memoryReadMaskReg", 0, 0 };
    static const QUMethod slot_79 = {"memorySetMaskAnd", 0, 0 };
    static const QUMethod slot_80 = {"memorySetMaskOr", 0, 0 };
    static const QUMethod slot_81 = {"memorySetMaskXor", 0, 0 };
    static const QUMethod slot_82 = {"piaDataRead", 0, 0 };
    static const QUMethod slot_83 = {"piaDataWrite", 0, 0 };
    static const QUMethod slot_84 = {"piaEnableChannel", 0, 0 };
    static const QUMethod slot_85 = {"piaIsChannelEnable", 0, 0 };
    static const QUMethod slot_86 = {"piaReadAll", 0, 0 };
    static const QUMethod slot_87 = {"piaReadDDRP", 0, 0 };
    static const QUMethod slot_88 = {"piaReadGCR", 0, 0 };
    static const QUMethod slot_89 = {"piaReadSR", 0, 0 };
    static const QUMethod slot_90 = {"piaResetChannel", 0, 0 };
    static const QUMethod slot_91 = {"piaWriteDDRP", 0, 0 };
    static const QUMethod slot_92 = {"piaWriteDDRPBit", 0, 0 };
    static const QUMethod slot_93 = {"piaWriteGCR", 0, 0 };
    static const QUMethod slot_94 = {"piaWriteGCRBit", 0, 0 };
    static const QUMethod slot_95 = {"memoryWriteCompareMB", 0, 0 };
    static const QUMethod slot_96 = {"scanForTrackerDevices", 0, 0 };
    static const QUMethod slot_97 = {"fecEnableCounters", 0, 0 };
    static const QUMethod slot_98 = {"trackerFecEnableCounters", 0, 0 };
    static const QUMethod slot_99 = {"trackerAddPiaKey", 0, 0 };
    static const QUMethod slot_100 = {"trackerDeletePiaKey", 0, 0 };
    static const QUMethod slot_101 = {"trackerReadAll", 0, 0 };
    static const QUMethod slot_102 = {"trackerWriteAll", 0, 0 };
    static const QUMethod slot_103 = {"trackerSetAllDevicesSelected", 0, 0 };
    static const QUMethod slot_104 = {"trackerFindXMLFile", 0, 0 };
    static const QUMethod slot_105 = {"helpAbout", 0, 0 };
    static const QUMethod slot_106 = {"trackerLoadXML", 0, 0 };
    static const QUMethod slot_107 = {"trackerWritePiaSelected", 0, 0 };
    static const QUMethod slot_108 = {"trackerSetAllPiaSelected", 0, 0 };
    static const QUMethod slot_109 = {"trackerListDeviceDetectedChanged", 0, 0 };
    static const QUMethod slot_110 = {"trackerSetDefaultValues", 0, 0 };
    static const QUMethod slot_111 = {"trackerSaveXML", 0, 0 };
    static const QUMethod slot_112 = {"quitTout", 0, 0 };
    static const QUMethod slot_113 = {"trackerFileToggle", 0, 0 };
    static const QUMethod slot_114 = {"trackerDeleteDeviceList", 0, 0 };
    static const QUMethod slot_115 = {"trackerSelectPartition", 0, 0 };
    static const QUMethod slot_116 = {"trackerDatabaseToggle", 0, 0 };
    static const QUMethod slot_117 = {"trackerPiaResetAddAllCCUKey", 0, 0 };
    static const QUMethod slot_118 = {"ccuRedundancyCache", 0, 0 };
    static const QUMethod slot_119 = {"startThread", 0, 0 };
    static const QUMethod slot_120 = {"fecRedundancy", 0, 0 };
    static const QUMethod slot_121 = {"ccuRedundancyAddCcuHand", 0, 0 };
    static const QUMethod slot_122 = {"fecEmptyFifoRet", 0, 0 };
    static const QUMethod slot_123 = {"fecEmptyFifoTra", 0, 0 };
    static const QUMethod slot_124 = {"fecReadFifoRec", 0, 0 };
    static const QUMethod slot_125 = {"fecReadFifoRet", 0, 0 };
    static const QUMethod slot_126 = {"fecReadFifoTra", 0, 0 };
    static const QUMethod slot_127 = {"fecWriteFifoRec", 0, 0 };
    static const QUMethod slot_128 = {"fecWriteFifoRet", 0, 0 };
    static const QUMethod slot_129 = {"fecWriteFifoTra", 0, 0 };
    static const QUMethod slot_130 = {"fecEmptyFifoRec", 0, 0 };
    static const QUMethod slot_131 = {"fecDisableReceiveFec", 0, 0 };
    static const QUMethod slot_132 = {"ddIRQEnableDisable", 0, 0 };
    static const QUMethod slot_133 = {"ccuRedundancyClearCCU", 0, 0 };
    static const QUMethod slot_134 = {"fecWriteCR0_", 0, 0 };
    static const QUMethod slot_135 = {"ccuRedundancyTestRingB", 0, 0 };
    static const QUMethod slot_136 = {"fecSlotsCcuModify", 0, 0 };
    static const QUMethod slot_137 = {"fecSlotsI2cModify", 0, 0 };
    static const QUMethod slot_138 = {"fecSlotsMemoryModify", 0, 0 };
    static const QUMethod slot_139 = {"fecSlotsPiaModify", 0, 0 };
    static const QUMethod slot_140 = {"fecSlotsRedundancyModify", 0, 0 };
    static const QUMethod slot_141 = {"fecSlotsModify", 0, 0 };
    static const QUMethod slot_142 = {"ccuRedundanceSaveCCU", 0, 0 };
    static const QUMethod slot_143 = {"ccuRedundancyLoadCCU", 0, 0 };
    static const QUMethod slot_144 = {"fecResetFSM_pressed", 0, 0 };
    static const QUMethod slot_145 = {"vmeCrateReset_pressed", 0, 0 };
    static const QUMethod slot_146 = {"languageChange", 0, 0 };
    static const QMetaData slot_tbl[] = {
	{ "scanForFecs()", &slot_0, QMetaData::Public },
	{ "fecReadCR0()", &slot_1, QMetaData::Public },
	{ "fecWriteCR0()", &slot_2, QMetaData::Public },
	{ "fecWriteCR0Bit()", &slot_3, QMetaData::Public },
	{ "fecWriteCR1()", &slot_4, QMetaData::Public },
	{ "fecReadCR1()", &slot_5, QMetaData::Public },
	{ "fecWriteCR1Bit()", &slot_6, QMetaData::Public },
	{ "fecReadSR0()", &slot_7, QMetaData::Public },
	{ "fecReadSR1()", &slot_8, QMetaData::Public },
	{ "fecReadDDStatus()", &slot_9, QMetaData::Public },
	{ "fecSoftReset()", &slot_10, QMetaData::Public },
	{ "scanForCcus()", &slot_11, QMetaData::Public },
	{ "fecPlxReset()", &slot_12, QMetaData::Public },
	{ "ccuReadCRA()", &slot_13, QMetaData::Public },
	{ "fecResetErrorCounter()", &slot_14, QMetaData::Public },
	{ "fecReadAllRegisters()", &slot_15, QMetaData::Public },
	{ "fecReleaseFec()", &slot_16, QMetaData::Public },
	{ "ccuWriteCRABit()", &slot_17, QMetaData::Public },
	{ "ccuWriteCRA()", &slot_18, QMetaData::Public },
	{ "ccuReadCRB()", &slot_19, QMetaData::Public },
	{ "ccuReadCRC()", &slot_20, QMetaData::Public },
	{ "ccuReadCRD()", &slot_21, QMetaData::Public },
	{ "ccuReadCRE()", &slot_22, QMetaData::Public },
	{ "ccuReadSR()", &slot_23, QMetaData::Public },
	{ "ccuReadSRA()", &slot_24, QMetaData::Public },
	{ "ccuWriteCRB()", &slot_25, QMetaData::Public },
	{ "ccuWriteCRBBit()", &slot_26, QMetaData::Public },
	{ "ccuWriteCRC()", &slot_27, QMetaData::Public },
	{ "ccuWriteCRCBit()", &slot_28, QMetaData::Public },
	{ "ccuWriteCRD()", &slot_29, QMetaData::Public },
	{ "ccuWriteCRE()", &slot_30, QMetaData::Public },
	{ "ccuWriteCREBit()", &slot_31, QMetaData::Public },
	{ "ccuReadSRE()", &slot_32, QMetaData::Public },
	{ "ccuSelected()", &slot_33, QMetaData::Public },
	{ "ccuReadAll()", &slot_34, QMetaData::Public },
	{ "i2cEnableChannel()", &slot_35, QMetaData::Public },
	{ "i2cReadAll()", &slot_36, QMetaData::Public },
	{ "i2cReadCRA()", &slot_37, QMetaData::Public },
	{ "i2cReadSR()", &slot_38, QMetaData::Public },
	{ "i2cReadSRA()", &slot_39, QMetaData::Public },
	{ "i2cWriteCRA()", &slot_40, QMetaData::Public },
	{ "i2cWriteCRABit()", &slot_41, QMetaData::Public },
	{ "scanFecsCcusDevices()", &slot_42, QMetaData::Public },
	{ "scanForI2CDevices()", &slot_43, QMetaData::Public },
	{ "i2cReadModifyWrite()", &slot_44, QMetaData::Public },
	{ "i2cReadI2c()", &slot_45, QMetaData::Public },
	{ "i2cWriteI2c()", &slot_46, QMetaData::Public },
	{ "i2cSetSpeed100()", &slot_47, QMetaData::Public },
	{ "i2cSetSpeed200()", &slot_48, QMetaData::Public },
	{ "i2cSetSpeed400()", &slot_49, QMetaData::Public },
	{ "i2cSetSpeed1000()", &slot_50, QMetaData::Public },
	{ "i2cSetMaskAnd()", &slot_51, QMetaData::Public },
	{ "i2cSetMaskOr()", &slot_52, QMetaData::Public },
	{ "i2cSetMaskXor()", &slot_53, QMetaData::Public },
	{ "i2cIsChannelEnable()", &slot_54, QMetaData::Public },
	{ "i2cResetChannel()", &slot_55, QMetaData::Public },
	{ "ccuEnableAllChannels()", &slot_56, QMetaData::Public },
	{ "i2cSetModeExtended()", &slot_57, QMetaData::Public },
	{ "i2cSetModeNormal()", &slot_58, QMetaData::Public },
	{ "i2cSetModeRal()", &slot_59, QMetaData::Public },
	{ "memoryEnableChannel()", &slot_60, QMetaData::Public },
	{ "memoryReadCRA()", &slot_61, QMetaData::Public },
	{ "memoryWriteCRA()", &slot_62, QMetaData::Public },
	{ "memoryWriteCRABit()", &slot_63, QMetaData::Public },
	{ "memorySetSpeed1()", &slot_64, QMetaData::Public },
	{ "memorySetSpeed4()", &slot_65, QMetaData::Public },
	{ "memorySetSpeed10()", &slot_66, QMetaData::Public },
	{ "memorySetSpeed20()", &slot_67, QMetaData::Public },
	{ "memoryReadSR()", &slot_68, QMetaData::Public },
	{ "memoryReadAll()", &slot_69, QMetaData::Public },
	{ "memoryResetChannel()", &slot_70, QMetaData::Public },
	{ "memoryReadWindowRegisters()", &slot_71, QMetaData::Public },
	{ "memoryWriteWindowRegisters()", &slot_72, QMetaData::Public },
	{ "memoryReadModifyWrite()", &slot_73, QMetaData::Public },
	{ "memoryReadSB()", &slot_74, QMetaData::Public },
	{ "memoryWriteSB()", &slot_75, QMetaData::Public },
	{ "memoryReadMB()", &slot_76, QMetaData::Public },
	{ "memoryWriteMB()", &slot_77, QMetaData::Public },
	{ "memoryReadMaskReg()", &slot_78, QMetaData::Public },
	{ "memorySetMaskAnd()", &slot_79, QMetaData::Public },
	{ "memorySetMaskOr()", &slot_80, QMetaData::Public },
	{ "memorySetMaskXor()", &slot_81, QMetaData::Public },
	{ "piaDataRead()", &slot_82, QMetaData::Public },
	{ "piaDataWrite()", &slot_83, QMetaData::Public },
	{ "piaEnableChannel()", &slot_84, QMetaData::Public },
	{ "piaIsChannelEnable()", &slot_85, QMetaData::Public },
	{ "piaReadAll()", &slot_86, QMetaData::Public },
	{ "piaReadDDRP()", &slot_87, QMetaData::Public },
	{ "piaReadGCR()", &slot_88, QMetaData::Public },
	{ "piaReadSR()", &slot_89, QMetaData::Public },
	{ "piaResetChannel()", &slot_90, QMetaData::Public },
	{ "piaWriteDDRP()", &slot_91, QMetaData::Public },
	{ "piaWriteDDRPBit()", &slot_92, QMetaData::Public },
	{ "piaWriteGCR()", &slot_93, QMetaData::Public },
	{ "piaWriteGCRBit()", &slot_94, QMetaData::Public },
	{ "memoryWriteCompareMB()", &slot_95, QMetaData::Public },
	{ "scanForTrackerDevices()", &slot_96, QMetaData::Public },
	{ "fecEnableCounters()", &slot_97, QMetaData::Public },
	{ "trackerFecEnableCounters()", &slot_98, QMetaData::Public },
	{ "trackerAddPiaKey()", &slot_99, QMetaData::Public },
	{ "trackerDeletePiaKey()", &slot_100, QMetaData::Public },
	{ "trackerReadAll()", &slot_101, QMetaData::Public },
	{ "trackerWriteAll()", &slot_102, QMetaData::Public },
	{ "trackerSetAllDevicesSelected()", &slot_103, QMetaData::Public },
	{ "trackerFindXMLFile()", &slot_104, QMetaData::Public },
	{ "helpAbout()", &slot_105, QMetaData::Public },
	{ "trackerLoadXML()", &slot_106, QMetaData::Public },
	{ "trackerWritePiaSelected()", &slot_107, QMetaData::Public },
	{ "trackerSetAllPiaSelected()", &slot_108, QMetaData::Public },
	{ "trackerListDeviceDetectedChanged()", &slot_109, QMetaData::Public },
	{ "trackerSetDefaultValues()", &slot_110, QMetaData::Public },
	{ "trackerSaveXML()", &slot_111, QMetaData::Public },
	{ "quitTout()", &slot_112, QMetaData::Public },
	{ "trackerFileToggle()", &slot_113, QMetaData::Public },
	{ "trackerDeleteDeviceList()", &slot_114, QMetaData::Public },
	{ "trackerSelectPartition()", &slot_115, QMetaData::Public },
	{ "trackerDatabaseToggle()", &slot_116, QMetaData::Public },
	{ "trackerPiaResetAddAllCCUKey()", &slot_117, QMetaData::Public },
	{ "ccuRedundancyCache()", &slot_118, QMetaData::Public },
	{ "startThread()", &slot_119, QMetaData::Public },
	{ "fecRedundancy()", &slot_120, QMetaData::Public },
	{ "ccuRedundancyAddCcuHand()", &slot_121, QMetaData::Public },
	{ "fecEmptyFifoRet()", &slot_122, QMetaData::Public },
	{ "fecEmptyFifoTra()", &slot_123, QMetaData::Public },
	{ "fecReadFifoRec()", &slot_124, QMetaData::Public },
	{ "fecReadFifoRet()", &slot_125, QMetaData::Public },
	{ "fecReadFifoTra()", &slot_126, QMetaData::Public },
	{ "fecWriteFifoRec()", &slot_127, QMetaData::Public },
	{ "fecWriteFifoRet()", &slot_128, QMetaData::Public },
	{ "fecWriteFifoTra()", &slot_129, QMetaData::Public },
	{ "fecEmptyFifoRec()", &slot_130, QMetaData::Public },
	{ "fecDisableReceiveFec()", &slot_131, QMetaData::Public },
	{ "ddIRQEnableDisable()", &slot_132, QMetaData::Public },
	{ "ccuRedundancyClearCCU()", &slot_133, QMetaData::Public },
	{ "fecWriteCR0_()", &slot_134, QMetaData::Public },
	{ "ccuRedundancyTestRingB()", &slot_135, QMetaData::Public },
	{ "fecSlotsCcuModify()", &slot_136, QMetaData::Public },
	{ "fecSlotsI2cModify()", &slot_137, QMetaData::Public },
	{ "fecSlotsMemoryModify()", &slot_138, QMetaData::Public },
	{ "fecSlotsPiaModify()", &slot_139, QMetaData::Public },
	{ "fecSlotsRedundancyModify()", &slot_140, QMetaData::Public },
	{ "fecSlotsModify()", &slot_141, QMetaData::Public },
	{ "ccuRedundanceSaveCCU()", &slot_142, QMetaData::Public },
	{ "ccuRedundancyLoadCCU()", &slot_143, QMetaData::Public },
	{ "fecResetFSM_pressed()", &slot_144, QMetaData::Public },
	{ "vmeCrateReset_pressed()", &slot_145, QMetaData::Public },
	{ "languageChange()", &slot_146, QMetaData::Protected }
    };
    metaObj = QMetaObject::new_metaobject(
	"FecDialog", parentObject,
	slot_tbl, 147,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_FecDialog.setMetaObject( metaObj );
    return metaObj;
}

void* FecDialog::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "FecDialog" ) )
	return this;
    return QDialog::qt_cast( clname );
}

bool FecDialog::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: scanForFecs(); break;
    case 1: fecReadCR0(); break;
    case 2: fecWriteCR0(); break;
    case 3: fecWriteCR0Bit(); break;
    case 4: fecWriteCR1(); break;
    case 5: fecReadCR1(); break;
    case 6: fecWriteCR1Bit(); break;
    case 7: fecReadSR0(); break;
    case 8: fecReadSR1(); break;
    case 9: fecReadDDStatus(); break;
    case 10: fecSoftReset(); break;
    case 11: scanForCcus(); break;
    case 12: fecPlxReset(); break;
    case 13: ccuReadCRA(); break;
    case 14: fecResetErrorCounter(); break;
    case 15: fecReadAllRegisters(); break;
    case 16: fecReleaseFec(); break;
    case 17: ccuWriteCRABit(); break;
    case 18: ccuWriteCRA(); break;
    case 19: ccuReadCRB(); break;
    case 20: ccuReadCRC(); break;
    case 21: ccuReadCRD(); break;
    case 22: ccuReadCRE(); break;
    case 23: ccuReadSR(); break;
    case 24: ccuReadSRA(); break;
    case 25: ccuWriteCRB(); break;
    case 26: ccuWriteCRBBit(); break;
    case 27: ccuWriteCRC(); break;
    case 28: ccuWriteCRCBit(); break;
    case 29: ccuWriteCRD(); break;
    case 30: ccuWriteCRE(); break;
    case 31: ccuWriteCREBit(); break;
    case 32: ccuReadSRE(); break;
    case 33: ccuSelected(); break;
    case 34: ccuReadAll(); break;
    case 35: i2cEnableChannel(); break;
    case 36: i2cReadAll(); break;
    case 37: i2cReadCRA(); break;
    case 38: i2cReadSR(); break;
    case 39: i2cReadSRA(); break;
    case 40: i2cWriteCRA(); break;
    case 41: i2cWriteCRABit(); break;
    case 42: scanFecsCcusDevices(); break;
    case 43: scanForI2CDevices(); break;
    case 44: i2cReadModifyWrite(); break;
    case 45: i2cReadI2c(); break;
    case 46: i2cWriteI2c(); break;
    case 47: i2cSetSpeed100(); break;
    case 48: i2cSetSpeed200(); break;
    case 49: i2cSetSpeed400(); break;
    case 50: i2cSetSpeed1000(); break;
    case 51: i2cSetMaskAnd(); break;
    case 52: i2cSetMaskOr(); break;
    case 53: i2cSetMaskXor(); break;
    case 54: i2cIsChannelEnable(); break;
    case 55: i2cResetChannel(); break;
    case 56: ccuEnableAllChannels(); break;
    case 57: i2cSetModeExtended(); break;
    case 58: i2cSetModeNormal(); break;
    case 59: i2cSetModeRal(); break;
    case 60: memoryEnableChannel(); break;
    case 61: memoryReadCRA(); break;
    case 62: memoryWriteCRA(); break;
    case 63: memoryWriteCRABit(); break;
    case 64: memorySetSpeed1(); break;
    case 65: memorySetSpeed4(); break;
    case 66: memorySetSpeed10(); break;
    case 67: memorySetSpeed20(); break;
    case 68: memoryReadSR(); break;
    case 69: memoryReadAll(); break;
    case 70: memoryResetChannel(); break;
    case 71: memoryReadWindowRegisters(); break;
    case 72: memoryWriteWindowRegisters(); break;
    case 73: memoryReadModifyWrite(); break;
    case 74: memoryReadSB(); break;
    case 75: memoryWriteSB(); break;
    case 76: memoryReadMB(); break;
    case 77: memoryWriteMB(); break;
    case 78: memoryReadMaskReg(); break;
    case 79: memorySetMaskAnd(); break;
    case 80: memorySetMaskOr(); break;
    case 81: memorySetMaskXor(); break;
    case 82: piaDataRead(); break;
    case 83: piaDataWrite(); break;
    case 84: piaEnableChannel(); break;
    case 85: piaIsChannelEnable(); break;
    case 86: piaReadAll(); break;
    case 87: piaReadDDRP(); break;
    case 88: piaReadGCR(); break;
    case 89: piaReadSR(); break;
    case 90: piaResetChannel(); break;
    case 91: piaWriteDDRP(); break;
    case 92: piaWriteDDRPBit(); break;
    case 93: piaWriteGCR(); break;
    case 94: piaWriteGCRBit(); break;
    case 95: memoryWriteCompareMB(); break;
    case 96: scanForTrackerDevices(); break;
    case 97: fecEnableCounters(); break;
    case 98: trackerFecEnableCounters(); break;
    case 99: trackerAddPiaKey(); break;
    case 100: trackerDeletePiaKey(); break;
    case 101: trackerReadAll(); break;
    case 102: trackerWriteAll(); break;
    case 103: trackerSetAllDevicesSelected(); break;
    case 104: trackerFindXMLFile(); break;
    case 105: helpAbout(); break;
    case 106: trackerLoadXML(); break;
    case 107: trackerWritePiaSelected(); break;
    case 108: trackerSetAllPiaSelected(); break;
    case 109: trackerListDeviceDetectedChanged(); break;
    case 110: trackerSetDefaultValues(); break;
    case 111: trackerSaveXML(); break;
    case 112: quitTout(); break;
    case 113: trackerFileToggle(); break;
    case 114: trackerDeleteDeviceList(); break;
    case 115: trackerSelectPartition(); break;
    case 116: trackerDatabaseToggle(); break;
    case 117: trackerPiaResetAddAllCCUKey(); break;
    case 118: ccuRedundancyCache(); break;
    case 119: startThread(); break;
    case 120: fecRedundancy(); break;
    case 121: ccuRedundancyAddCcuHand(); break;
    case 122: fecEmptyFifoRet(); break;
    case 123: fecEmptyFifoTra(); break;
    case 124: fecReadFifoRec(); break;
    case 125: fecReadFifoRet(); break;
    case 126: fecReadFifoTra(); break;
    case 127: fecWriteFifoRec(); break;
    case 128: fecWriteFifoRet(); break;
    case 129: fecWriteFifoTra(); break;
    case 130: fecEmptyFifoRec(); break;
    case 131: fecDisableReceiveFec(); break;
    case 132: ddIRQEnableDisable(); break;
    case 133: ccuRedundancyClearCCU(); break;
    case 134: fecWriteCR0_(); break;
    case 135: ccuRedundancyTestRingB(); break;
    case 136: fecSlotsCcuModify(); break;
    case 137: fecSlotsI2cModify(); break;
    case 138: fecSlotsMemoryModify(); break;
    case 139: fecSlotsPiaModify(); break;
    case 140: fecSlotsRedundancyModify(); break;
    case 141: fecSlotsModify(); break;
    case 142: ccuRedundanceSaveCCU(); break;
    case 143: ccuRedundancyLoadCCU(); break;
    case 144: fecResetFSM_pressed(); break;
    case 145: vmeCrateReset_pressed(); break;
    case 146: languageChange(); break;
    default:
	return QDialog::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool FecDialog::qt_emit( int _id, QUObject* _o )
{
    return QDialog::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool FecDialog::qt_property( int id, int f, QVariant* v)
{
    return QDialog::qt_property( id, f, v);
}

bool FecDialog::qt_static_property( QObject* , int , int , QVariant* ){ return FALSE; }
#endif // QT_NO_PROPERTIES
