




			
			
			/*******************************************************
			COMPLETE SCANRING FOR NEW CCU's - probe devices via READ method
			*******************************************************/
			case DBG_NCCU_SR_READ:

				/* print header */
				fprintf(stdout,"\n\nScanning ring for NEW CCU's - Devices are probed via READ method\n");
				fprintf(stdout,"scanned CCU's range is [0x%x .. 0x%x]\n", MIN_CCU, MAX_CCU);
				fprintf(stdout,"scanned i2c channels range is [0x%x .. 0x%x]\n", MIN_I2C_CH25, MAX_I2C_CH25);
				fprintf(stdout,"scanned i2c addresses (per channel) range is [0x%x .. 0x%x]\n", MIN_I2C_ADR, MAX_I2C_ADR);


				/* now, scan the ring */
				for (ccu_counter = MIN_CCU; ccu_counter <= MAX_CCU ; ccu_counter++)
				{
					write_stack[0] = ccu_counter;
					//fprintf(stdout,"Probing ccu 0x%x \r", ccu_counter);
					for (lcl_j=1 ; lcl_j<(sizeof(i2c01_talk_ccu25)/sizeof(__u16)); lcl_j++)
					{
						write_stack[lcl_j] = i2c01_talk_ccu25[lcl_j];
					}

					lcl_err = glue_fec_write_frame(fd, write_stack);
					if (lcl_err == DD_RETURN_OK)
					{
						fprintf(stdout,"\nCCU number 0x%x detected.\n",ccu_counter);
						//fprintf(stdout,"Reading back CCU25 CTRL-A\n");

						write_stack[0] = ccu_counter;
						for (lcl_j=1 ; lcl_j<(sizeof(i2c02_talk_ccu25)/sizeof(__u16)); lcl_j++)
						{
							write_stack[lcl_j] = i2c02_talk_ccu25[lcl_j];
						}
						lcl_err = glue_fec_write_frame(fd, write_stack);
						if (lcl_err == DD_RETURN_OK)
						{
							if (isLongFrame(write_stack[DD_MSG_LEN_OFFSET]))
							{
								read_stack[0] = write_stack[DD_TRANS_NUMBER_OFFSET+1];
							}
							else read_stack[0] = write_stack[DD_TRANS_NUMBER_OFFSET];

							lcl_err = glue_fec_read_frame(fd, read_stack);
							if (lcl_err != DD_RETURN_OK)
							{
								glue_fecdriver_print_error_message(lcl_err);
							}
							//else fprintf(stdout,"CTRLA really read back.\n");

						}
						else glue_fecdriver_print_error_message(lcl_err);


						//fprintf(stdout,"Initializing CCU25 CTRL-E\n");
						write_stack[0] = ccu_counter;
						for (lcl_j=1 ; lcl_j<(sizeof(i2c03_talk_ccu25)/sizeof(__u16)); lcl_j++)
						{
							write_stack[lcl_j] = i2c03_talk_ccu25[lcl_j];
						}
						lcl_err = glue_fec_write_frame(fd, write_stack);
						if (lcl_err != DD_RETURN_OK)
						{
							glue_fecdriver_print_error_message(lcl_err);
						}


						//fprintf(stdout,"Reading back CCU25 CTRL-E\n");
						write_stack[0] = ccu_counter;
						for (lcl_j=1 ; lcl_j<(sizeof(i2c04_talk_ccu25)/sizeof(__u16)); lcl_j++)
						{
							write_stack[lcl_j] = i2c04_talk_ccu25[lcl_j];
						}
						lcl_err = glue_fec_write_frame(fd, write_stack);
						if (lcl_err == DD_RETURN_OK)
						{
							if (isLongFrame(write_stack[DD_MSG_LEN_OFFSET]))
							{
								read_stack[0] = write_stack[DD_TRANS_NUMBER_OFFSET+1];
							}
							else read_stack[0] = write_stack[DD_TRANS_NUMBER_OFFSET];

							lcl_err = glue_fec_read_frame(fd, read_stack);
							if (lcl_err != DD_RETURN_OK)
							{
								glue_fecdriver_print_error_message(lcl_err);
							}
							//else fprintf(stdout,"CTRL E readback OK.\n");


						}
						else glue_fecdriver_print_error_message(lcl_err);


						//now, i2c channels are initialised and switched on ; let's try to probe them
						for (channel_counter = MIN_I2C_CH25; channel_counter <= MAX_I2C_CH25 ; channel_counter++)
						{
								
							//fprintf(stdout,"\tNow probing for i2c devices attached to channel 0x%x of CCU 0x%x ; be patient ...\n", channel_counter, ccu_counter);
							//fprintf(stdout,"\tInitializing I2C channel CRA\n");
							write_stack[0] = ccu_counter;
							for (lcl_j=1 ; lcl_j<(sizeof(i2c05_talk_ccu25)/sizeof(__u16)); lcl_j++)
							{
								write_stack[lcl_j] = i2c05_talk_ccu25[lcl_j];
							}
							write_stack[3] = channel_counter;
							lcl_err = glue_fec_write_frame(fd, write_stack);
							if (lcl_err != DD_RETURN_OK)
							{
								glue_fecdriver_print_error_message(lcl_err);
							}

							//fprintf(stdout,"\tReading back I2C channel CRA\n");
							write_stack[0] = ccu_counter;
							for (lcl_j=1 ; lcl_j<(sizeof(i2c06_talk_ccu25)/sizeof(__u16)); lcl_j++)
							{
								write_stack[lcl_j] = i2c06_talk_ccu25[lcl_j];
							}
							write_stack[3] = channel_counter;
							lcl_err = glue_fec_write_frame(fd, write_stack);
							if (lcl_err == DD_RETURN_OK)
							{
								if (isLongFrame(write_stack[DD_MSG_LEN_OFFSET]))
								{
									read_stack[0] = write_stack[DD_TRANS_NUMBER_OFFSET+1];
								}
								else read_stack[0] = write_stack[DD_TRANS_NUMBER_OFFSET];

								lcl_err = glue_fec_read_frame(fd, read_stack);
								if (lcl_err != DD_RETURN_OK)
								{
									glue_fecdriver_print_error_message(lcl_err);
								}
								//else fprintf(stdout,"I2C CTRL A readback OK.\n");
							}
							else glue_fecdriver_print_error_message(lcl_err);


							// Now, scan for devices on this channel.
							for (i2c_counter = MIN_I2C_ADR; i2c_counter <= MAX_I2C_ADR ; i2c_counter++)
							{
								//fprintf(stdout,"\t\tprobing i2c address 0x%x on channel 0x%x of CCU 0x%x ; be patient ...\r", i2c_counter, channel_counter, ccu_counter);
								//Build stack for Normal Mode testing
								write_stack[0] = ccu_counter;
								write_stack[1] = 0x0000;
								write_stack[2] = 0x0004;
								write_stack[3] = channel_counter;
								write_stack[4] = DD_FAKE_TNUM_PROVOKE_ACK;
								write_stack[5] = 0x0001;
								write_stack[6] = (i2c_counter | DD_FRAME_EOF_BITMASK);
								lcl_err = glue_fec_write_frame(fd, write_stack);
								if (lcl_err == DD_RETURN_OK)
								{
									if (isLongFrame(write_stack[DD_MSG_LEN_OFFSET]))
									{
										read_stack[0] = write_stack[DD_TRANS_NUMBER_OFFSET+1];
									}
									else read_stack[0] = write_stack[DD_TRANS_NUMBER_OFFSET];

									lcl_err = glue_fec_read_frame(fd, read_stack);
									if (lcl_err == DD_RETURN_OK)
									{
										if (read_stack[read_stack[2]+2] == 0x8004)
										{
											fprintf(stdout,"\tI2C device detected at address 0x%x on channel 0x%x of CCU 0x%x        \n",i2c_counter, channel_counter, ccu_counter);
										}
									}
								}
	                                               	}
						}

					}
				}
				fprintf(stdout,"END of Scan.\n");
			break;








			/*******************************************************
			QUICK SCANRING FOR NEW CCU's (25)
			*******************************************************/
			case DBG_NCCU_QS:
				fprintf(stdout,"Scanning ring for NEW CCU's\n");
				fprintf(stdout,"\tscanned CCU's range is [0x%x .. 0x%x]\n", MIN_CCU, MAX_CCU);
				for (ccu_counter = MIN_CCU; ccu_counter <= MAX_CCU ; ccu_counter++)
				{
					write_stack[0] = ccu_counter;
					fprintf(stdout,"Probing ccu 0x%x \r", ccu_counter);
					for (lcl_j=1 ; lcl_j<(sizeof(i2c01_talk_ccu25)/sizeof(__u16)); lcl_j++)
					{
						write_stack[lcl_j] = i2c01_talk_ccu25[lcl_j];
					}

					lcl_err = glue_fec_write_frame(fd, write_stack);
					if (lcl_err == DD_RETURN_OK) fprintf(stdout,"\nCCU number 0x%x detected.\n",ccu_counter);
				}
    			break;
