/*
   FileName : 		DEBUGGER.C

   Content : 		Device Driver Debugger / Low level accesses

   Used in : 		Debugger only

   Programmer : 	Laurent GROSS

   Version : 		Unified-6.0

   Date of last modification : 17/05/2005

   Support : 		mail to : fec-support@ires.in2p3.fr
*/

/*
This file is part of Fec Software project.

Fec Software is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Fec Software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Fec Software; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Copyright 2002 - 2005, Laurent GROSS - IReS/IN2P3
*/

/*
standard includes
*/
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <errno.h>
#include <linux/ioctl.h>
#include <unistd.h>




#include <string>
#include <time.h>
#include <fstream>
#include <boost/thread.hpp>
//#include <boost/chrono/thread_clock.hpp>
#include "uhal/uhal.hpp"




/*Driver-specific includes
*/
#include "glue.h"
#include "debugger.hh"
#include "driver_conf.h"
#include "dderrors.h"
#
/*
some defines...
*/
/* min-max CCUs addresses, for old and new CCUS */
#define MIN_CCU		0x0001
#define MAX_CCU		0x007f

/* min-max I2C channels, for old CCUS */
#define MIN_I2C_CH	0x0001
#define MAX_I2C_CH	0x0006

/* min-max I2C channels, for new CCUS */
#define MIN_I2C_CH25	0x0010
#define MAX_I2C_CH25	0x001F

/* min-max I2C addresses, for old and new CCUS */
#define MIN_I2C_ADR	0x0000
#define MAX_I2C_ADR	0x007F


#define CONTROL0    0x00000000
#define CONTROL1    0x00000004
#define STATUS0     0x00000008
#define STATUS1     0x0000000c
#define SOURCE      0x00000010
#define VERSION     0x00000010
#define TRA_FIFO    0x00000020
#define RET_FIFO    0x00000024
#define REC_FIFO    0x00000028


#define RINGNUMBER 0

/*!
Internal functions ; names are explicit.
*/
void dbg_decode_control0(DD_FEC_REGISTER_DATA param_control0)
{
	if (DD_FEC_ENABLE_FEC & param_control0) fprintf(stdout,"\t FEC enabled\n");
	if (DD_FEC_SEND & param_control0) fprintf(stdout,"\t Send data to ring\n");
//	if (DD_FEC_RESEND & param_control0) fprintf(stdout,"\t FEC resend option enabled\n");
	if (DD_FEC_SEL_SER_OUT & param_control0) fprintf(stdout,"\t FEC select serial out selected\n");
	if (DD_FEC_SEL_SER_IN & param_control0) fprintf(stdout,"\t FEC select serial in selected\n");
//	if (DD_FEC_SEL_IRQ_MODE & param_control0) fprintf(stdout,"\t FEC select IRQ mode selected\n");
	if (DD_FEC_RESET_TTCRX & param_control0) fprintf(stdout,"\t FEC TTCRX reset flag setted\n");
	if (DD_FEC_RESET_OUT & param_control0) fprintf(stdout,"\t FEC soft reset flag setted\n");
	if (DD_FEC_CTRL0_INIT_TTCRX & param_control0) fprintf(stdout,"\t FEC init TTCRX flag setted\n");
}




/*!
Internal functions ; names are explicit.
*/
void dbg_decode_control1(DD_FEC_REGISTER_DATA param_control1)
{
	if (DD_FEC_CLEAR_IRQ & param_control1) fprintf(stdout,"\t Clear IRQ's\n");
	if (DD_FEC_CLEAR_ERRORS & param_control1) fprintf(stdout,"\t Clear errors\n");
}




/*!
Internal functions ; names are explicit.
*/
void dbg_decode_status0(DD_FEC_REGISTER_DATA param_status0)
{
	if (DD_FEC_TRA_RUN & param_status0) fprintf(stdout,"\t fifo transmit running\n");
	if (DD_FEC_REC_RUN & param_status0) fprintf(stdout,"\t fifo receive running\n");
//	if (DD_FEC_REC_HALF_F & param_status0) fprintf(stdout,"\t fifo receive half full\n");
	if (DD_FEC_REC_FULL & param_status0) fprintf(stdout,"\t fifo receive full\n");
	if (DD_FEC_REC_EMPTY & param_status0) fprintf(stdout,"\t fifo receive empty\n");
//	if (DD_FEC_RET_HALF_F & param_status0) fprintf(stdout,"\t fifo return half full\n");
	if (DD_FEC_RET_FULL & param_status0) fprintf(stdout,"\t fifo return full\n");
	if (DD_FEC_RET_EMPTY & param_status0) fprintf(stdout,"\t fifo return empty\n");
//	if (DD_FEC_TRA_HALF_F & param_status0) fprintf(stdout,"\t fifo transmit half full\n");
	if (DD_FEC_TRA_FULL & param_status0) fprintf(stdout,"\t fifo transmit full\n");
	if (DD_FEC_TRA_EMPTY & param_status0) fprintf(stdout,"\t fifo transmit empty\n");
	if (DD_FEC_LINK_INITIALIZED & param_status0) fprintf(stdout,"\t Link initialized\n");
	if (DD_FEC_PENDING_IRQ & param_status0) fprintf(stdout,"\t pending IRQ\n");
	if (DD_FEC_DATA_TO_FEC & param_status0) fprintf(stdout,"\t data to fec\n");
}




/*!
Internal functions ; names are explicit.
*/
void dbg_decode_status1(DD_FEC_REGISTER_DATA param_status1)
{
	if (DD_FEC_ILL_DATA & param_status1) fprintf(stdout,"\t Illegal data received\n");
	if (DD_FEC_ILL_SEQ & param_status1) fprintf(stdout,"\t Illegal sequence received\n");
	if (DD_FEC_CRC_ERROR & param_status1) fprintf(stdout,"\t CRC error\n");
	if (DD_FEC_DATA_COPIED & param_status1) fprintf(stdout,"\t Data copied\n");
	if (DD_FEC_ADDR_SEEN & param_status1) fprintf(stdout,"\t address seen\n");
	if (DD_FEC_ERROR & param_status1) fprintf(stdout,"\t Error\n");
}








//Local use only ; this function is not exported in glue.h
DD_TYPE_ERROR dd_cat_from_std_to_32(DD_FEC_STD_WORD *fstd_buffer, DD_FEC_FIFO_DATA_32 *f32_buffer, int fstd_length, int *f32_length)
{
//#define DEBUG_dd_cat_from_std_to_32

int fstd_index, f32_index;
DD_FEC_FIFO_DATA_32 one_32_word, full_32_word;
DD_FEC_STD_WORD one_std_word;


	#ifdef DEBUG_dd_cat_from_std_to_32
		printf("Processing dd_cat_from_std_to_32\n");
	#endif
	fstd_index=0;
	f32_index=0;
	do
	{
		full_32_word = 0x0;
		if (fstd_index < fstd_length)
		{
			one_32_word = 0x0;
			one_std_word = fstd_buffer[fstd_index];
			#ifdef DEBUG_dd_cat_from_std_to_32
				printf("Processing word 1 of fstd_buffer. Word is 0x%x\n",one_std_word);
			#endif
			fstd_index++;
			one_std_word = one_std_word & 0xFF;
			one_32_word = (DD_FEC_FIFO_DATA_32)one_std_word;
			one_32_word = one_32_word << 24;
			full_32_word = full_32_word | one_32_word;
			#ifdef DEBUG_dd_cat_from_std_to_32
				printf("Full word is now 0x%x\n",full_32_word);
			#endif
		}

		if (fstd_index < fstd_length)
		{
			one_32_word = 0x0;
			one_std_word = fstd_buffer[fstd_index];
			#ifdef DEBUG_dd_cat_from_std_to_32
				printf("Processing word 2 of fstd_buffer. Word is 0x%x\n",one_std_word);
			#endif
			fstd_index++;
			one_std_word = one_std_word & 0xFF;
			one_32_word = (DD_FEC_FIFO_DATA_32)one_std_word;
			one_32_word = one_32_word << 16;
			full_32_word = full_32_word | one_32_word;
			#ifdef DEBUG_dd_cat_from_std_to_32
				printf("Full word is now 0x%x\n",full_32_word);
			#endif
		}


		if (fstd_index < fstd_length)
		{
			one_32_word = 0x0;
			one_std_word = fstd_buffer[fstd_index];
			#ifdef DEBUG_dd_cat_from_std_to_32
				printf("Processing word 3 of fstd_buffer. Word is 0x%x\n",one_std_word);
			#endif
			fstd_index++;
			one_std_word = one_std_word & 0xFF;
			one_32_word = (DD_FEC_FIFO_DATA_32)one_std_word;
			one_32_word = one_32_word << 8;
			full_32_word = full_32_word | one_32_word;
			#ifdef DEBUG_dd_cat_from_std_to_32
				printf("Full word is now 0x%x\n",full_32_word);
			#endif
		}

		if (fstd_index < fstd_length)
		{
			one_32_word = 0x0;
			one_std_word = fstd_buffer[fstd_index];
			#ifdef DEBUG_dd_cat_from_std_to_32
				printf("Processing word 4 of fstd_buffer. Word is 0x%x\n",one_std_word);
			#endif
			fstd_index++;
			one_std_word = one_std_word & 0xFF;
			one_32_word = (DD_FEC_FIFO_DATA_32)one_std_word;
			full_32_word = full_32_word | one_32_word;
			#ifdef DEBUG_dd_cat_from_std_to_32
				printf("Full word is now 0x%x\n",full_32_word);
			#endif
		}
		f32_buffer[f32_index] = full_32_word;
		f32_index++;
	} while (fstd_index < fstd_length);
	(*f32_length)=f32_index;
	#ifdef DEBUG_dd_cat_from_std_to_32
		printf("The 32 bits frame is 0x%x words long\n",(*f32_length));
	#endif
return DD_RETURN_OK;
}
















DD_TYPE_ERROR dd_uncat_from_32_to_std(DD_FEC_STD_WORD *fstd_buffer, DD_FEC_FIFO_DATA_32 *f32_buffer, int *fstd_length, int f32_length)
{
//#define DEBUG_dd_uncat_from_32_to_std
int fstd_index, f32_index;
DD_FEC_FIFO_DATA_32 one_32_word;
DD_FEC_STD_WORD one_std_word;

	#ifdef DEBUG_dd_uncat_from_32_to_std
		printf("Processing dd_uncat_from_32_to_std\n");
	#endif
	fstd_index=0;
	f32_index=0;
	do
	{
		#ifdef DEBUG_dd_uncat_from_32_to_std
			printf("In loop\n");
		#endif

		if (f32_index < f32_length)
		{
			one_32_word =f32_buffer[f32_index];
			one_32_word = (one_32_word >> 24) & 0xFF;
			#ifdef DEBUG_dd_uncat_from_32_to_std
				printf("Rotated (24) 32 bits word is : 0x%x\n", one_32_word);
			#endif
			one_std_word = (DD_FEC_STD_WORD)one_32_word;
			fstd_buffer[fstd_index] = one_std_word;
			#ifdef DEBUG_dd_uncat_from_32_to_std
				printf("I have bufferized : 0x%x\n", one_std_word);
			#endif
			fstd_index++;


			one_32_word =f32_buffer[f32_index];
			one_32_word = (one_32_word >> 16) & 0xFF;
			#ifdef DEBUG_dd_uncat_from_32_to_std
				printf("Rotated (16) 32 bits word is : 0x%x\n", one_32_word);
			#endif
			one_std_word = (DD_FEC_STD_WORD)one_32_word;
			fstd_buffer[fstd_index] = one_std_word;
			#ifdef DEBUG_dd_uncat_from_32_to_std
				printf("I have bufferized : 0x%x\n", one_std_word);
			#endif
			fstd_index++;


			one_32_word =f32_buffer[f32_index];
			one_32_word = (one_32_word >> 8) & 0xFF;
			#ifdef DEBUG_dd_uncat_from_32_to_std
				printf("Rotated (8) 32 bits word is : 0x%x\n", one_32_word);
			#endif
			one_std_word = (DD_FEC_STD_WORD)one_32_word;
			fstd_buffer[fstd_index] = one_std_word;
			#ifdef DEBUG_dd_uncat_from_32_to_std
				printf("I have bufferized : 0x%x\n", one_std_word);
			#endif
			fstd_index++;


			one_32_word =f32_buffer[f32_index];
			one_32_word = one_32_word & 0xFF;
			#ifdef DEBUG_dd_uncat_from_32_to_std
				printf("Rotated (0) 32 bits word is : 0x%x\n", one_32_word);
			#endif
			one_std_word = (DD_FEC_STD_WORD)one_32_word;
			fstd_buffer[fstd_index] = one_std_word;
			#ifdef DEBUG_dd_uncat_from_32_to_std
				printf("I have bufferized : 0x%x\n", one_std_word);
			#endif
			fstd_index++;
		}
		f32_index++;
	} while (f32_index < f32_length);

	if (isLongFrame(fstd_buffer[DD_MSG_LEN_OFFSET]))
	{
		(*fstd_length) = ((((fstd_buffer[DD_MSG_LEN_OFFSET] & DD_UPPER_BITMASK_FOR_LONGFRAME)<<8) + fstd_buffer[DD_MSG_LEN_OFFSET+1]) + DD_FRAME_HEAD_LENGTH_IF_LONGFRAME);
	}
	else
	{
		(*fstd_length) = (fstd_buffer[DD_MSG_LEN_OFFSET] + DD_FRAME_HEAD_LENGTH_IF_SHORTFRAME);
	}

	#ifdef DEBUG_dd_uncat_from_32_to_std
		printf("Final standard frame length is : 0x%x\n", (*fstd_length));
	#endif

	/* Add statusk bits to the last data word */
	fstd_buffer[(*fstd_length)-1] = fstd_buffer[(*fstd_length)-1] | (fstd_buffer[(*fstd_length)]<<8);
	#ifdef DEBUG_dd_uncat_from_32_to_std
		printf("Status byte of reconstructed frame will be : 0x%x\n", (fstd_buffer[(*fstd_length)]<<8));
	#endif

return DD_RETURN_OK;
}






int glue_utca_fec_get_firmware_version(uhal::HwInterface * hw, DD_FEC_REGISTER_DATA * lcl_firmware_version)
{

	uhal::ValWord< uint32_t > reg = hw->getNode("user_fw_id.user_fw_ver_nb").read();
	hw->dispatch();
	*lcl_firmware_version = (DD_FEC_REGISTER_DATA)reg.value();
return DD_RETURN_OK;
}



int glue_utca_fec_display_user_wb_reg1_id(uhal::HwInterface * hw)
{
#define DEBUG_READ_WB_REGISTERS
	std::string mystring="";
	uhal::ValWord< uint32_t > user_wb_reg1_id_char1 = hw->getNode("user_ascii_code_01to04.user_ascii_code_01").read();
	uhal::ValWord< uint32_t > user_wb_reg1_id_char2 = hw->getNode("user_ascii_code_01to04.user_ascii_code_02").read();
	uhal::ValWord< uint32_t > user_wb_reg1_id_char3 = hw->getNode("user_ascii_code_01to04.user_ascii_code_03").read();
	uhal::ValWord< uint32_t > user_wb_reg1_id_char4 = hw->getNode("user_ascii_code_01to04.user_ascii_code_04").read();

	uhal::ValWord< uint32_t > user_wb_reg1_id_char5 = hw->getNode("user_ascii_code_05to08.user_ascii_code_05").read();
	uhal::ValWord< uint32_t > user_wb_reg1_id_char6 = hw->getNode("user_ascii_code_05to08.user_ascii_code_06").read();
	uhal::ValWord< uint32_t > user_wb_reg1_id_char7 = hw->getNode("user_ascii_code_05to08.user_ascii_code_07").read();
	uhal::ValWord< uint32_t > user_wb_reg1_id_char8 = hw->getNode("user_ascii_code_05to08.user_ascii_code_08").read();

	hw->dispatch();
	#ifdef DEBUG_READ_WB_REGISTERS
		std::cout << "Request for user_wb_reg1_id value sent" << std::endl;
	#endif

	mystring += (char)user_wb_reg1_id_char1;
	mystring += (char)user_wb_reg1_id_char2;
	mystring += (char)user_wb_reg1_id_char3;
	mystring += (char)user_wb_reg1_id_char4;
	mystring += " -- ";
	mystring += (char)user_wb_reg1_id_char5;
	mystring += (char)user_wb_reg1_id_char6;
	mystring += (char)user_wb_reg1_id_char7;
	mystring += (char)user_wb_reg1_id_char8;
	std::cout << "Board string identifier from user_wb_reg1_id is : " << mystring << std::endl << std::endl;

return DD_RETURN_OK;
}




int glue_utca_fec_reset_board(uhal::HwInterface * hw)
{
//#define DEBUG_RESET_BOARD

	//init reset




	//init shift clock
	hw->getNode("fec_tx_clk_shift").write (1); // 0:0� / 1:180�
	hw->dispatch();
	#ifdef DEBUG_RESET_BOARD
		std::cout << "mFEC Tx clock shift setted" << std::endl;
	#endif

	hw->getNode("fec_rx_clk_shift").write (1); //0:0� / 1:180�
	hw->dispatch();
	#ifdef DEBUG_RESET_BOARD
		std::cout << "mFEC Rx clock shift setted" << std::endl;
	#endif





	hw->getNode("aclr_n_0").write (0); //reset FSM ctrl
	hw->dispatch();
	hw->getNode("mfec_CR0_0").write (0); //Set 0 to CR0
	hw->dispatch();


	hw->getNode("aclr_n_1").write (0); //reset FSM ctrl
	hw->dispatch();
	hw->getNode("mfec_CR0_1").write (0); //Set 0 to CR0
	hw->dispatch();


	hw->getNode("aclr_n_2").write (0); //reset FSM ctrl
	hw->dispatch();
	hw->getNode("mfec_CR0_2").write (0); //Set 0 to CR0
	hw->dispatch();


	hw->getNode("aclr_n_3").write (0); //reset FSM ctrl
	hw->dispatch();
	hw->getNode("mfec_CR0_3").write (0); //Set 0 to CR0
	hw->dispatch();


	


	#ifdef DEBUG_RESET_BOARD
		std::cout << "FSM resetted" << std::endl;
	#endif

	usleep(250000);


	//reset released
	hw->getNode("aclr_n_0").write (1); //reset FSM ctrl
	hw->dispatch();
	hw->getNode("mfec_CR0_0").write (1); //Set 0 to CR0
	hw->dispatch();


	hw->getNode("aclr_n_1").write (1); //reset FSM ctrl
	hw->dispatch();
	hw->getNode("mfec_CR0_1").write (1); //Set 0 to CR0
	hw->dispatch();


	hw->getNode("aclr_n_2").write (1); //reset FSM ctrl
	hw->dispatch();
	hw->getNode("mfec_CR0_2").write (1); //Set 0 to CR0
	hw->dispatch();


	hw->getNode("aclr_n_3").write (1); //reset FSM ctrl
	hw->dispatch();
	hw->getNode("mfec_CR0_3").write (1); //Set 0 to CR0
	hw->dispatch();
	#ifdef DEBUG_RESET_BOARD
		std::cout << "FSM reset released" << std::endl;
	#endif


	usleep(250000);

return DD_RETURN_OK;
}





int glue_utca_fec_partial_reset_board(uhal::HwInterface * hw)
{
return DD_RETURN_OK;
}





int glue_utca_fec_read_mfec_value(uhal::HwInterface * hw, uint32_t READ_OFFSET_ADDRESS, uint32_t * resultVal)
{
int returnValue;
//Define compilation options here

#define DEBUG_READ_REGISTER
//#define USE_MFEC_READ_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH
//#define MFEC_READ_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH	10

	numberOfReadAccessToFirmware++;



	//Transcode old defines to new nodes matching content of XML file
	std::string fecNodeName;
	if (READ_OFFSET_ADDRESS==CONTROL0) {fecNodeName = "mfec_CR0";}
	else if (READ_OFFSET_ADDRESS==CONTROL1) {fecNodeName = "mfec_CR1";}		
	else if (READ_OFFSET_ADDRESS==STATUS0) {fecNodeName = "mfec_SR0";}		
	else if (READ_OFFSET_ADDRESS==STATUS1) {fecNodeName = "mfec_SR1";}				
	else if (READ_OFFSET_ADDRESS==REC_FIFO) {fecNodeName = "ipb_user_rec_fifo";}		


	if (RINGNUMBER==0) fecNodeName = fecNodeName + "_0";
	if (RINGNUMBER==1) fecNodeName = fecNodeName + "_1";
	if (RINGNUMBER==2) fecNodeName = fecNodeName + "_2";
	if (RINGNUMBER==3) fecNodeName = fecNodeName + "_3";

	if (READ_OFFSET_ADDRESS==VERSION) {fecNodeName = "VERSION";}


	//Header if trace mode on
	#ifdef DEBUG_READ_REGISTER
		std::cout << std::endl << std::endl << "READ OPERATION REQUESTED" << std::endl;
	#endif

	//Set default return value
	returnValue = DD_RETURN_OK;
/*
	//Set first three parameters to prepare read operation
	hw->getNode("mFEC_ctrl.mfec_address").write (READ_OFFSET_ADDRESS);
	#ifdef DEBUG_READ_REGISTER
		std::cout << "Offset for READ operation given to mfec_address is : " << std::hex << READ_OFFSET_ADDRESS << std::dec << std::endl;
	#endif

	//hw->getNode("mFEC_ctrl.mfec_write_data").write (0);
	//#ifdef DEBUG_WRITE_REGISTER
	//	std::cout << "Value " << valToWrite << " written in mfec_write_data node" << std::endl;
	//#endif

	hw->getNode("mFEC_ctrl.AccessMode").write (0); //0 : read / 1 : write
	#ifdef DEBUG_READ_REGISTER
		std::cout << "Access mode set to 0 (read) via AccessMode node" << std::endl;
	#endif

	hw->dispatch();
	#ifdef USE_MFEC_READ_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH
		usleep (MFEC_READ_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH);
	#endif


	//Set f/w mode to access requested
	#ifdef DEBUG_READ_REGISTER
		std::cout << "Now taking hand on access manager before sending a read request...waiting..." << std::endl;
	#endif

	//Make request
	#ifdef DEBUG_READ_REGISTER
		std::cout << "\tSetting mFEC_ctrl.AccessRequest to 1 (access requested)" << std::endl;
	#endif
	hw->getNode("mFEC_ctrl.AccessRequest").write (1); //0 : no access request / 1 : access to perform
	hw->dispatch();
	#ifdef USE_MFEC_READ_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH
		usleep (MFEC_READ_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH);
	#endif
	#ifdef DEBUG_READ_REGISTER
		std::cout << "\tmFEC_ctrl.AccessRequest set to 1" << std::endl;
	#endif

	//Wait for Request taken into account and ack'ed
	uhal::ValWord< uint32_t > AccessAcknowledge = 0;
	do
	{
		AccessAcknowledge = hw->getNode("mFEC_ctrl.AccessACK").read();
		hw->dispatch();
		#ifdef USE_MFEC_READ_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH
			usleep (MFEC_READ_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH);
		#endif
		#ifdef DEBUG_READ_REGISTER
			std::cout << "\tIn loop, waiting for access granted flag (node AccessAcknowledge) beeing ONE, current value is " << AccessAcknowledge << std::endl;
		#endif
	} while (AccessAcknowledge == 0);


	#ifdef DEBUG_READ_REGISTER
		std::cout << "End of loop, AccessAcknowledge read back with value " << AccessAcknowledge << std::endl;
	#endif

	//If access granted, perform read operation
	if (AccessAcknowledge==1)
	{
		#ifdef DEBUG_READ_REGISTER
			std::cout << "Access granted flag value is good (=1), now reading data from mfec_read_data node" << std::endl;
		#endif
*/		
		#ifdef DEBUG_READ_REGISTER
			std::cout << "Access granted flag value is good (=1), now reading data from mfec_read_data node" << std::endl;
		#endif
		uhal::ValWord< uint32_t > valToRead = hw->getNode(fecNodeName).read();
		hw->dispatch();
		#ifdef USE_MFEC_READ_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH
			usleep (MFEC_READ_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH);
		#endif
		#ifdef DEBUG_READ_REGISTER
		std::cout << "Value of data read out is : " << std::hex << valToRead << std::dec << std::endl;
		#endif
		*(resultVal) = valToRead;
/*
	}
	else //If access not granted, tell me why...
	{
		if (AccessAcknowledge==2)
		{
			std::cout << "Access granted flag value is NOT good (=2) : Time Out - Access not performed" << std::endl;
			returnValue = -1;
		}
		else 
		{
			std::cout << "Access granted flag value is NOT good : Time Out plus illegal value read back for AccessACK : " << AccessAcknowledge << std::endl;
			returnValue = -2;
		}
	}


	//Now reset access flags for next operation
	#ifdef DEBUG_READ_REGISTER
		std::cout << "Lowering access request to 0 (no access requested) via AccessRequest node..." << std::endl;
	#endif

	hw->getNode("mFEC_ctrl.AccessRequest").write (0); //0 : no access request / 1 : access to perform
	hw->dispatch();
	#ifdef USE_MFEC_READ_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH
		usleep (MFEC_READ_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH);
	#endif
	#ifdef DEBUG_READ_REGISTER
		std::cout << "Access request lowered to 0 (no access requested) via AccessRequest node" << std::endl;
	#endif
	
	do
	{
		AccessAcknowledge = hw->getNode("mFEC_ctrl.AccessACK").read();
		hw->dispatch();
		#ifdef USE_MFEC_READ_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH
			usleep (MFEC_READ_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH);
		#endif
		#ifdef DEBUG_READ_REGISTER
			std::cout << "\tIn loop, waiting for access granted flag (node AccessACK) beeing resetted to ZERO, current value is " << AccessAcknowledge << std::endl;
		#endif
	} while (AccessAcknowledge != 0);

	#ifdef DEBUG_READ_REGISTER
		std::cout << "Access request set back to 0, access granted flag (node AccessACK) back to 0, system free for a new operation" << std::endl;
	#endif
*/

	//Trailer if trace mode on
	#ifdef DEBUG_READ_REGISTER
		std::cout << "READ OPERATION PERFORMED" << std::endl << std::endl;
	#endif

return returnValue;

}









int glue_utca_fec_write_mfec_value(uhal::HwInterface * hw, uint32_t WRITE_OFFSET_ADDRESS, uint32_t valToWrite)
{
int returnValue;
//Define compilation options here

#define DEBUG_WRITE_REGISTER
//#define USE_MFEC_WRITE_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH
//#define MFEC_WRITE_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH	0

	numberOfWriteAccessToFirmware++;

	//Header if trace mode on
	#ifdef DEBUG_WRITE_REGISTER
		std::cout << std::endl << std::endl << "WRITE OPERATION REQUESTED" << std::endl;
	#endif

	//Set default return value
	returnValue = DD_RETURN_OK;


	//Transcode old defines to new nodes matching content of XML file
	std::string fecNodeName;
	if (WRITE_OFFSET_ADDRESS==CONTROL0) {fecNodeName = "mfec_CR0";}
	else if (WRITE_OFFSET_ADDRESS==CONTROL1) {fecNodeName = "mfec_CR1";}		
	else if (WRITE_OFFSET_ADDRESS==TRA_FIFO) {fecNodeName = "ipb_user_tra_fifo";}		

	if (RINGNUMBER==0) fecNodeName = fecNodeName + "_0";
	if (RINGNUMBER==1) fecNodeName = fecNodeName + "_1";
	if (RINGNUMBER==2) fecNodeName = fecNodeName + "_2";
	if (RINGNUMBER==3) fecNodeName = fecNodeName + "_3";


	#ifdef DEBUG_WRITE_REGISTER
		std::cout << "Offset for WRITE operation given to mfec_address is : " << std::hex << WRITE_OFFSET_ADDRESS << std::dec << std::endl;
	#endif

	//Set first three parameters to prepare write operation
	hw->getNode(fecNodeName).write (valToWrite);
	hw->dispatch();

	#ifdef DEBUG_WRITE_REGISTER
		std::cout << "Value 0x" << std::hex << valToWrite << std::dec << " written in node " << fecNodeName << std::endl;
	#endif


/*

	hw->getNode("mFEC_ctrl.AccessMode").write (1); //0 : read / 1 : write
	#ifdef DEBUG_WRITE_REGISTER
		std::cout << "Access mode set to 1 (write) via AccessMode node" << std::endl;
	#endif

	hw->dispatch();
	#ifdef USE_MFEC_WRITE_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH
		usleep (MFEC_WRITE_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH);
	#endif


	//Set f/w mode to access requested
	#ifdef DEBUG_READ_REGISTER
		std::cout << "Now taking hand on access manager before sending a write request...waiting..." << std::endl;
	#endif

	//Make request
	#ifdef DEBUG_READ_REGISTER
		std::cout << "\tSetting mFEC_ctrl.AccessRequest to 1 (access requested)" << std::endl;
	#endif
	hw->getNode("mFEC_ctrl.AccessRequest").write (1); //0 : no access request / 1 : access to perform
	hw->dispatch();
	#ifdef USE_MFEC_WRITE_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH
		usleep (MFEC_WRITE_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH);
	#endif
	#ifdef DEBUG_WRITE_REGISTER
		std::cout << "\tmFEC_ctrl.AccessRequest set to 1" << std::endl;
	#endif

	//Wait for Request taken into account and ack'ed
	uhal::ValWord< uint32_t > AccessAcknowledge = 0;
	do
	{
		AccessAcknowledge = hw->getNode("mFEC_ctrl.AccessACK").read();
		hw->dispatch();
		#ifdef USE_MFEC_WRITE_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH
			usleep (MFEC_WRITE_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH);
		#endif
		#ifdef DEBUG_WRITE_REGISTER
			std::cout << "\tIn loop, waiting for access granted flag (node AccessACK) beeing ONE, current value is " << AccessAcknowledge << std::endl;
		#endif
	} while (AccessAcknowledge == 0);

	#ifdef DEBUG_WRITE_REGISTER
		std::cout << "End of loop, AccessAcknowledge read back with value " << AccessAcknowledge << std::endl;
	#endif

	//If access granted, check write operation status
	if (AccessAcknowledge==1)
	{
		#ifdef DEBUG_WRITE_REGISTER
			std::cout << "Access granted flag value is good (=1) ; data should have been written from mfec_write_data node" << std::endl;
		#endif
	}
	else //If access not granted, tell me why...
	{
		if (AccessAcknowledge==2)
		{
			std::cout << "Access granted flag value is NOT good (=2) : Time Out - Access not performed" << std::endl;
			returnValue = -1;
		}
		else
		{
			std::cout << "Access granted flag value is NOT good : Time Out plus illegal value read back for AccessACK : " << AccessAcknowledge << std::endl;
			returnValue = -2;
		}
	}

	//Now reset access flags for next operation
	#ifdef DEBUG_WRITE_REGISTER
		std::cout << "Lowering access request to 0 (no access requested) via AccessRequest node..." << std::endl;
	#endif
	hw->getNode("mFEC_ctrl.AccessRequest").write (0); //0 : no access request / 1 : access to perform
	hw->dispatch();
	#ifdef USE_MFEC_WRITE_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH
		usleep (MFEC_WRITE_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH);
	#endif
	#ifdef DEBUG_WRITE_REGISTER
	std::cout << "Access request lowered to 0 (no access requested) via AccessRequest node" << std::endl;
	#endif

	do
	{
		AccessAcknowledge = hw->getNode("mFEC_ctrl.AccessACK").read();
		hw->dispatch();
		#ifdef USE_MFEC_WRITE_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH
			usleep (MFEC_WRITE_WAIT_MICROSECONDS_DELAY_AFTER_DISPATCH);
		#endif
		#ifdef DEBUG_WRITE_REGISTER
			std::cout << "\tIn loop, waiting for access granted flag (node AccessACK) beeing resetted to ZERO, current value is " << AccessAcknowledge << std::endl;
		#endif
	} while (AccessAcknowledge != 0);

	#ifdef DEBUG_WRITE_REGISTER
		std::cout << "Access request set back to 0, access granted flag (node AccessACK) back to 0, system free for a new operation" << std::endl;
	#endif

	//Trailer if trace mode on
	#ifdef DEBUG_READ_REGISTER
		std::cout << "WRITE OPERATION PERFORMED" << std::endl << std::endl;
	#endif
*/
return returnValue;
}









int glue_utca_fec_soft_reset(uhal::HwInterface * hw)
{
	uint32_t backControlValue;
	uint32_t valToWrite;
	
	//Read CTRL0 value before FEC reset
	int lcl_err = glue_utca_fec_read_mfec_value(hw, (uint32_t)CONTROL0, &backControlValue);

	//Modify value
	valToWrite = backControlValue | DD_FEC_ENABLE_FEC | DD_FEC_RESET_OUT;
	
	//Write reset-enabled modified value
	lcl_err = glue_utca_fec_write_mfec_value(hw, (uint32_t)CONTROL0, valToWrite);

	//Restore original CTRL0 value
	lcl_err = glue_utca_fec_write_mfec_value(hw, (uint32_t)CONTROL0, backControlValue);
return DD_RETURN_OK;
}




int glue_utca_fec_write_to_fifo_transmit(uhal::HwInterface * hw, uint32_t valToWrite)
{	
	int lcl_err = glue_utca_fec_write_mfec_value(hw, (uint32_t)TRA_FIFO, valToWrite);
return lcl_err;
}


int glue_utca_fec_read_from_fifo_transmit(uhal::HwInterface * hw, uint32_t * valToRead)
{	
	int lcl_err = glue_utca_fec_read_mfec_value(hw, (uint32_t)TRA_FIFO, valToRead);
return lcl_err;
}



int glue_utca_fec_write_to_fifo_return(uhal::HwInterface * hw, uint32_t valToWrite)
{	
	int lcl_err = glue_utca_fec_write_mfec_value(hw, (uint32_t)RET_FIFO, valToWrite);
return lcl_err;
}


int glue_utca_fec_read_from_fifo_return(uhal::HwInterface * hw, uint32_t * valToRead)
{	
	int lcl_err = glue_utca_fec_read_mfec_value(hw, (uint32_t)RET_FIFO, valToRead);
return lcl_err;
}


int glue_utca_fec_write_to_fifo_receive(uhal::HwInterface * hw, uint32_t valToWrite)
{	
	int lcl_err = glue_utca_fec_write_mfec_value(hw, (uint32_t)REC_FIFO, valToWrite);
return lcl_err;
}


int glue_utca_fec_read_from_fifo_receive(uhal::HwInterface * hw, uint32_t * valToRead)
{	
	int lcl_err = glue_utca_fec_read_mfec_value(hw, (uint32_t)REC_FIFO, valToRead);
return lcl_err;
}




int glue_utca_fec_init_ttcrx(uhal::HwInterface * hw)
{
	uint32_t backControlValue;
	uint32_t valToWrite;
	
	//Read CTRL0 value before FEC reset
	int lcl_err = glue_utca_fec_read_mfec_value(hw, (uint32_t)CONTROL0, &backControlValue);

	//Modify value
	valToWrite = backControlValue | DD_FEC_RESET_TTCRX;
	
	//Write reset-enabled modified value
	lcl_err = glue_utca_fec_write_mfec_value(hw, (uint32_t)CONTROL0, valToWrite);

	//Restore original CTRL0 value
	lcl_err = glue_utca_fec_write_mfec_value(hw, (uint32_t)CONTROL0, backControlValue);
return DD_RETURN_OK;
}







int glue_utca_fec_write_msg(uhal::HwInterface * hw, DD_FEC_STD_WORD *param_stack, DD_FEC_STD_WORD *read_stack, DD_FEC_STD_WORD *glb_read_status, DD_FEC_STD_WORD *glb_copied_status)
{

//#define DD_SPY_DEBUG

int lcl_count;
DD_FEC_STD_WORD lcl_buf[DD_USER_MAX_MSG_LENGTH];
DD_FEC_STD_WORD lcl_trans_number_offset;
int lcl_counter;
int lcl_i;
int lcl_trans;
DD_FEC_STD_WORD lcl_trans_number;
int lcl_j;
uint32_t lcl_fec_value;
int lcl_ctrl_loop;
DD_FEC_FIFO_DATA_32 lcl_buf32[DD_MAX_MSG_LENGTH_32];
int lcl_count32;
int lcl_err;

uint32_t lcl_fifo_word;
int lcl_fec_ctrl0_memory;



	#ifdef DD_SPY_DEBUG
		printf("Entering glue_utca_fec_write_msg() method\n");
	#endif

	/*! First, copy 4 first bytes of buffer in order to get the frame length */
	for (int i=0; i< DD_FRAME_HEAD_LENGTH_IF_LONGFRAME; i++)
	{
		lcl_buf[i] = *(param_stack + i);
	}



	/* Now, compute frame length */
	/* If lenght is less than or equal to 127, than just copy frame length */
	/* Else, compute real frame length. */
	if (isLongFrame(lcl_buf[DD_MSG_LEN_OFFSET]))
	{
		lcl_count = ((((lcl_buf[DD_MSG_LEN_OFFSET] & DD_UPPER_BITMASK_FOR_LONGFRAME)<<8) + lcl_buf[DD_MSG_LEN_OFFSET+1]) + DD_FRAME_HEAD_LENGTH_IF_LONGFRAME);
		lcl_trans_number_offset = DD_TRANS_NUMBER_OFFSET+1;
	}
	else
	{
		lcl_count = (lcl_buf[DD_MSG_LEN_OFFSET] + DD_FRAME_HEAD_LENGTH_IF_SHORTFRAME);
		lcl_trans_number_offset = DD_TRANS_NUMBER_OFFSET;
	}
	#ifdef DD_SPY_DEBUG
		printf("I see a frame of total length: 0x%x\n",lcl_count);
	#endif

	/*! get frame length and check consistency */
	if (lcl_count > DD_USER_MAX_MSG_LENGTH) return DD_TOO_LONG_FRAME_LENGTH;
	/*! Rem : no check performed on min length of the frame ; */
	/*! if needed, this check can be coded here. */

	/*! Now, get the whole frame in stack. */
	for (int i=0; i< lcl_count; i++)
	{
		lcl_buf[i] = *(param_stack + i);
	}






	/*! Can we access the transaction number manager ? If no, */
	/*! how many times will we go to sleep before returning an error ? */
	lcl_counter = 0;
	while ( (glb_atomic_flags & DD_TRANS_NUMBER_MNGR_IN_USE) && (lcl_counter < DD_NBR_OF_RETRY_IF_TRANS_NUMBER_MNGR_BUSY) )
	/*! transaction number manager is already used by another write call */
	{
		usleep(1000);
		lcl_counter++;
	}
	/*! exit loop when !(glb_atomic_flags & DD_TRANS_NUMBER_MNGR_IN_USE) OR if counter set*/

	/*! too much tries ; exit */
	if (lcl_counter >= DD_NBR_OF_RETRY_IF_TRANS_NUMBER_MNGR_BUSY) return DD_TIMEOUT_TRANS_NUMBER_MNGR_IN_USE;


	/*! set flag for atomic operation on transaction number manager */
	glb_atomic_flags = glb_atomic_flags | DD_TRANS_NUMBER_MNGR_IN_USE;
	





	/*! search the first free transaction number occuring after the last used one.*/
	/*! First, keep memory of last used transaction number */
	lcl_i = glb_currentTrans;

	/*! fake invalid transaction number for loop control */
	lcl_trans = DD_INVALID_TRANSACTION_NUMBER_EXAMPLE;

	/*! search */
	do
	{
		/*! point to next of last used transaction number */
		lcl_i++;
		/*! if out of range, loop as circular buffer */
		if (lcl_i > DD_MAX_TRANS_NUMBER) { lcl_i = DD_MIN_TRANS_NUMBER;}
		/*! check if slot is free */
		if (glb_fec_write_trans_in_use[lcl_i] == DD_TRANS_NUMBER_IS_FREE) {lcl_trans=(DD_FEC_STD_WORD)lcl_i;}
		/*! if one full loop was performed and no free transaction was found, force exit */
		if (lcl_i == glb_currentTrans) {lcl_trans=(DD_FEC_STD_WORD)lcl_i;}

	} while (lcl_trans == DD_INVALID_TRANSACTION_NUMBER_EXAMPLE);


	/*! if no free transaction number found, return an error */
	/*! if the only free transaction number found is the last used one, return an error too */
	if (lcl_trans == glb_currentTrans)
	{
		glb_atomic_flags = glb_atomic_flags & !DD_TRANS_NUMBER_MNGR_IN_USE;
		return DD_NO_MORE_FREE_TRANSACTION_NUMBER;
	}

	/*! if one free transaction number is found, tag this number as currently used */
	glb_fec_write_trans_in_use[lcl_trans] = DD_TRANS_NUMBER_IS_IN_USE;

	/*! switch to unsigned 16 value */
	lcl_trans_number = lcl_trans;

	/*! keep memory of last used number */
	glb_currentTrans = lcl_trans_number;

	/*! clear flag for end of atomic operation */
	glb_atomic_flags = glb_atomic_flags & !DD_TRANS_NUMBER_MNGR_IN_USE;




	/*! RAZ indicateurs pour cette transaction */
	glb_read[lcl_trans] = DD_READ_DATA_UNSUCCESSFUL;
	glb_copied[lcl_trans] = DD_COPY_DATA_UNSUCCESSFUL;

	for (lcl_j=0 ; lcl_j < DD_USER_MAX_MSG_LENGTH ; lcl_j++)
	{
		glb_back[lcl_trans][lcl_j] = DD_BACK_STACK_INIT_VALUE;
		glb_received[lcl_trans][lcl_j] = DD_RECEIVED_STACK_INIT_VALUE;
	}


	/*! check, according to Fake Transaction number of frame, if F-ACK is awaited. */
	/*! if fake trans number is DD_FAKE_TNUM_PROVOKE_ACK, then F-ACK is requested. */
	/*! else, it is not. */
	if (lcl_buf[lcl_trans_number_offset] == DD_FAKE_TNUM_PROVOKE_ACK)
	{
		glb_fec_trans_must_be_f_acked[lcl_trans] = 1;
	}
	else glb_fec_trans_must_be_f_acked[lcl_trans] = 0;


	/*! set transaction number in frame */
	/*! Rem : frame previously read from user space directly in lcl_buf */
	//lcl_trans = 0x55;
	lcl_buf[lcl_trans_number_offset] = lcl_trans;


	/*! Set EOF flag for last data of fifo transmit */
	//#ifdef DD_SET_AUTO_EOF_BIT
	lcl_buf[lcl_count-1] = (lcl_buf[lcl_count-1] | DD_EOF_BITMASK);
	//#endif



	/* Check if "fifo transmit running" flag  is down before writing in fifo */

	lcl_err = glue_utca_fec_read_mfec_value(hw, (uint32_t)STATUS0, &lcl_fec_value);
	lcl_ctrl_loop=0;
	while ( ((lcl_fec_value & DD_FEC_TRA_RUN)==1) && (lcl_ctrl_loop<DD_LOOP_LIMIT_WHEN_POLLING_FIFOTRA_RUNNING))
	{
		usleep(100);
        	lcl_err = glue_utca_fec_read_mfec_value(hw, (uint32_t)STATUS0, &lcl_fec_value);
        	lcl_ctrl_loop++;
	}
	if (lcl_ctrl_loop >= DD_LOOP_LIMIT_WHEN_POLLING_FIFOTRA_RUNNING)
	{
		std::cout << "PROBLEM BEFORE FILLING FIFOTRA : FIFO TRANSMIT FLAG IS ALWAYS ON." << std::endl;
		return DD_FIFOTRA_RUNNING_FLAG_IS_ALWAYS_ON;
	}


	#ifdef DD_SPY_DEBUG
		printf("I will put in fifo transmit :\n");
	#endif



	/*! convert from 16 bits to 32 bits word */
	dd_cat_from_std_to_32(lcl_buf, lcl_buf32, lcl_count, &lcl_count32);

	/*! copy send buffer to fifo transmit */
	for (lcl_j = 0 ; lcl_j < lcl_count32 ; lcl_j++)
	{
		lcl_fifo_word = lcl_buf32[lcl_j];
		lcl_err = glue_utca_fec_write_to_fifo_transmit(hw, lcl_fifo_word);
		#ifdef DD_SPY_DEBUG
			printf("\t0x%x\n",lcl_buf32[lcl_j]);
		#endif
	}
	#ifdef DD_SPY_DEBUG
		printf("Done, fifo transmit filled\n");
	#endif







	/* Check if "fifo empty" flag is down (it should, we just wrote data in it) */
	//dd_internal_read_from_fec_register(DD_FEC_STAT0_OFFSET, &lcl_fec_value);
	lcl_err = glue_utca_fec_read_mfec_value(hw, (uint32_t)STATUS0, &lcl_fec_value);
	if ((lcl_fec_value & DD_FEC_TRA_EMPTY)==1)
	{
		printf("PROBLEM : FIFOTRA IS EMPTY, AND IT SHOULD BE FILLED WITH THE FRAME TO SEND.\n");
		return DD_FIFOTRA_RUNNING_FLAG_IS_ALWAYS_ON;
		
	}

	/*! Read current CTRL0 value */
	//dd_internal_read_from_fec_register(DD_FEC_CTRL0_OFFSET, &lcl_fec_value);
	lcl_err = glue_utca_fec_read_mfec_value(hw, (uint32_t)CONTROL0, &lcl_fec_value);
	/*! keep memory */
	lcl_fec_ctrl0_memory = lcl_fec_value;

	/*! Make OR */
	lcl_fec_value = (lcl_fec_ctrl0_memory | DD_FEC_ENABLE_FEC );
	/*! send fifotransmit to ring - step 1/3 */
	//dd_internal_write_to_fec_register(DD_FEC_CTRL0_OFFSET, lcl_fec_value);
	lcl_err = glue_utca_fec_write_mfec_value(hw, (uint32_t)CONTROL0, lcl_fec_value);

	/*! Make OR */
	lcl_fec_value = (lcl_fec_ctrl0_memory | DD_FEC_ENABLE_FEC | DD_FEC_SEND);
	/*! send fifotransmit to ring - step 2/3 */
	//dd_internal_write_to_fec_register(DD_FEC_CTRL0_OFFSET, lcl_fec_value);
	lcl_err = glue_utca_fec_write_mfec_value(hw, (uint32_t)CONTROL0, lcl_fec_value);


	/*! Make OR */
	lcl_fec_value = (lcl_fec_ctrl0_memory | DD_FEC_ENABLE_FEC);
	/*! send fifotransmit to ring - step 1/3 */
	//dd_internal_write_to_fec_register(DD_FEC_CTRL0_OFFSET, lcl_fec_value);
	lcl_err = glue_utca_fec_write_mfec_value(hw, (uint32_t)CONTROL0, lcl_fec_value);





//FRAME IS NOW SENT ON RING
//WAIT A LITTLE BIT UNTIL TRANSACRTION IS ACHIEVED,

//sleep(1);
//usleep(1000);


//THEN, IN ANY CASE, READ DATA BACVK FROM FIFO RECEIVE

	int lcl_ncar32;
//	DD_FEC_FIFO_DATA_32 lcl_buf32[DD_MAX_MSG_LENGTH_32];
	DD_FEC_FIFO_DATA_32 lcl_data32;
	DD_FEC_STD_WORD lcl_fl1, lcl_fl2, lcl_frame_length, lcl_frame_32_length;

	int lcl_length;
	DD_FEC_STD_WORD lcl_status=0;
	int lcl_ncar = 0;


	lcl_ncar32 = 0;
	lcl_frame_32_length = 0;
	#ifdef DD_SPY_DEBUG
		printf("Now reading from Fifo Receive : \n");
	#endif
	do
	{
		lcl_err = glue_utca_fec_read_from_fifo_receive(hw, &lcl_data32);
		#ifdef DD_SPY_DEBUG
			printf("\t0x%x : \n",lcl_data32);
		#endif
		//If buffer not full, store data. Else, avoid buffer overflow
		if (lcl_ncar32 < DD_MAX_MSG_LENGTH_32) lcl_buf32[lcl_ncar32] = lcl_data32;


		//length analysis
		if (lcl_ncar32 == 0)
		{

			// How many DATA elements will I have in my standard frame ?
			lcl_fl1 = (DD_FEC_STD_WORD)((lcl_data32>>8) & 0xFF);
			lcl_fl2 = (DD_FEC_STD_WORD)(lcl_data32 & 0xFF);
			#ifdef DD_SPY_DEBUG
				printf("In function : length analysis\n");
				printf("MSB 8 bits are lcl_fl1 = 0x%x\n",lcl_fl1);
				printf("LSB 8 bits are lcl_fl2 = 0x%x\n",lcl_fl2);
			#endif

			if ((lcl_fl1 & 0x0080)==0)
			{
				lcl_frame_length = lcl_fl1 +3;
				#ifdef DD_SPY_DEBUG
					printf("I'm in short frame case\n");
					printf("Partial computed length is : 0x%x\n",lcl_frame_length);
				#endif
			}
			else
			{
				lcl_frame_length = ((((lcl_fl1 & 0x007F) << 8) + lcl_fl2) +4);
				#ifdef DD_SPY_DEBUG
					printf("I'm in long frame case\n");
					printf("Partial computed length is : 0x%x\n",lcl_frame_length);
				#endif
			}
			//increment data frame length of 1 item (+1) for the status byte to add
			//because when splitted in 32 bits words, the last standard word
			//status(8bits) -- data (8 bits) == 1 frame element is seen as 2 elements
			//when read from fifo receive
			lcl_frame_length++;
			#ifdef DD_SPY_DEBUG
				printf("Final computed frame length (in Bytes) is : 0x%x\n",lcl_frame_length);
			#endif

			//now, check how many 32 bits elements we must extract from the
			//fifo receive to fill our standard frame
			lcl_frame_32_length = lcl_frame_length / 4;
			if ( (lcl_frame_length % 4) != 0 ) lcl_frame_32_length++;

			#ifdef DD_SPY_DEBUG
				printf("Final computed frame length (in 32 bits words) is : 0x%x\n", lcl_frame_32_length);
			#endif
		}
		lcl_ncar32++;

	} while (lcl_ncar32 < lcl_frame_32_length);




	//switch to 16 bits representation
	dd_uncat_from_32_to_std(lcl_buf, lcl_buf32, &lcl_length, lcl_ncar32);
	lcl_ncar = lcl_length;
	lcl_status = lcl_buf[lcl_length-1];
	#ifdef DEBUG_dd_read_fifo
		printf("Un-cated frame is :\n");
		for (lcl_j = 0 ; lcl_j < lcl_ncar ; lcl_j++)
		{
			printf("    0x%x\n", lcl_buf[lcl_j]);
		}
		printf("frame length : 0x%x   ;;;   Frame status : 0x%x\n",lcl_ncar, lcl_status);
	#endif
	



	if (lcl_buf[DD_DESTINATION_OFFSET] == DD_FEC_IS_MSG_DESTINATION)
	//i.e. lecture data frame
	//or could be an ACK
	{
		#ifdef DD_SPY_DEBUG
			printf("In dd_read_fifo : Received frame is answer to a read request or a f-ack.\n");
		#endif

		for (lcl_j = 0 ; lcl_j < lcl_ncar ; lcl_j++)
		{
			glb_received[lcl_trans][lcl_j] = lcl_buf[lcl_j];
			(*(read_stack + lcl_j)) = lcl_buf[lcl_j];
		}

		glb_read[lcl_trans] = DD_READ_DATA_SUCCESSFUL;
		*glb_read_status = DD_READ_DATA_SUCCESSFUL;

		#ifdef DD_SPY_DEBUG
			printf("\tRead request is sucessful.\n");
		#endif
	}
	else /*! lecture frame retour ; frame sent to a CCU and coming back modified */
	{
		#ifdef DD_SPY_DEBUG
			printf("In dd_read_fifo : Received frame is a direct acknowledge return.\n");
		#endif
		for (lcl_j = 0 ; lcl_j < lcl_ncar ; lcl_j++)
		{
			glb_back[lcl_trans][lcl_j] = lcl_buf[lcl_j];
			(*(param_stack + lcl_j)) = lcl_buf[lcl_j];
		}
		/*! if address seen and data copied */
		if ((lcl_status & DD_FEC_STATUS_FLAGS) == DD_FEC_ADDR_SEEN_AND_DATA_COPIED)
		{
			glb_copied[lcl_trans] = DD_COPY_DATA_SUCCESSFUL;
			*glb_copied_status = DD_COPY_DATA_SUCCESSFUL;
			#ifdef DD_SPY_DEBUG
				printf("\tDirect acknowledge is sucessful.\n");
			#endif
		}
	}







//IF FIFO RECEIVE NOT EMPTY, READ FRAME AGAIN

lcl_err = glue_utca_fec_read_mfec_value(hw, (uint32_t)STATUS0, &lcl_fec_value);
if ( !(lcl_fec_value & DD_FEC_REC_EMPTY) )
{
	


	int lcl_ncar32;
//	DD_FEC_FIFO_DATA_32 lcl_buf32[DD_MAX_MSG_LENGTH_32];
	DD_FEC_FIFO_DATA_32 lcl_data32;
	DD_FEC_STD_WORD lcl_fl1, lcl_fl2, lcl_frame_length, lcl_frame_32_length;

	int lcl_length;
	DD_FEC_STD_WORD lcl_status=0;
	int lcl_ncar = 0;


	lcl_ncar32 = 0;
	lcl_frame_32_length = 0;
	#ifdef DD_SPY_DEBUG
		printf("Now reading from Fifo Receive : \n");
	#endif
	do
	{
		lcl_err = glue_utca_fec_read_from_fifo_receive(hw, &lcl_data32);
		#ifdef DD_SPY_DEBUG
			printf("\t0x%x : \n",lcl_data32);
		#endif
		//If buffer not full, store data. Else, avoid buffer overflow
		if (lcl_ncar32 < DD_MAX_MSG_LENGTH_32) lcl_buf32[lcl_ncar32] = lcl_data32;


		//length analysis
		if (lcl_ncar32 == 0)
		{

			// How many DATA elements will I have in my standard frame ?
			lcl_fl1 = (DD_FEC_STD_WORD)((lcl_data32>>8) & 0xFF);
			lcl_fl2 = (DD_FEC_STD_WORD)(lcl_data32 & 0xFF);
			#ifdef DD_SPY_DEBUG
				printf("In function : length analysis\n");
				printf("MSB 8 bits are lcl_fl1 = 0x%x\n",lcl_fl1);
				printf("LSB 8 bits are lcl_fl2 = 0x%x\n",lcl_fl2);
			#endif

			if ((lcl_fl1 & 0x0080)==0)
			{
				lcl_frame_length = lcl_fl1 +3;
				#ifdef DD_SPY_DEBUG
					printf("I'm in short frame case\n");
					printf("Partial computed length is : 0x%x\n",lcl_frame_length);
				#endif
			}
			else
			{
				lcl_frame_length = ((((lcl_fl1 & 0x007F) << 8) + lcl_fl2) +4);
				#ifdef DD_SPY_DEBUG
					printf("I'm in long frame case\n");
					printf("Partial computed length is : 0x%x\n",lcl_frame_length);
				#endif
			}
			//increment data frame length of 1 item (+1) for the status byte to add
			//because when splitted in 32 bits words, the last standard word
			//status(8bits) -- data (8 bits) == 1 frame element is seen as 2 elements
			//when read from fifo receive
			lcl_frame_length++;
			#ifdef DD_SPY_DEBUG
				printf("Final computed frame length (in Bytes) is : 0x%x\n",lcl_frame_length);
			#endif

			//now, check how many 32 bits elements we must extract from the
			//fifo receive to fill our standard frame
			lcl_frame_32_length = lcl_frame_length / 4;
			if ( (lcl_frame_length % 4) != 0 ) lcl_frame_32_length++;

			#ifdef DD_SPY_DEBUG
				printf("Final computed frame length (in 32 bits words) is : 0x%x\n", lcl_frame_32_length);
			#endif
		}
		lcl_ncar32++;

	} while (lcl_ncar32 < lcl_frame_32_length);
	



	//switch to 16 bits representation
	dd_uncat_from_32_to_std(lcl_buf, lcl_buf32, &lcl_length, lcl_ncar32);
	lcl_ncar = lcl_length;
	lcl_status = lcl_buf[lcl_length-1];
	#ifdef DEBUG_dd_read_fifo
		printf("Un-cated frame is :\n");
		for (lcl_j = 0 ; lcl_j < lcl_ncar ; lcl_j++)
		{
			printf("    0x%x\n", lcl_buf[lcl_j]);
		}
		printf("frame length : 0x%x   ;;;   Frame status : 0x%x\n",lcl_ncar, lcl_status);
	#endif
	



	if (lcl_buf[DD_DESTINATION_OFFSET] == DD_FEC_IS_MSG_DESTINATION)
	//i.e. lecture data frame
	//or could be an ACK
	{
		#ifdef DD_SPY_DEBUG
			printf("In dd_read_fifo : Received frame is answer to a read request or a f-ack.\n");
		#endif

		for (lcl_j = 0 ; lcl_j < lcl_ncar ; lcl_j++)
		{
			glb_received[lcl_trans][lcl_j] = lcl_buf[lcl_j];
			(*(read_stack + lcl_j)) = lcl_buf[lcl_j];
		}

		glb_read[lcl_trans] = DD_READ_DATA_SUCCESSFUL;
		*glb_read_status = DD_READ_DATA_SUCCESSFUL;

		#ifdef DD_SPY_DEBUG
			printf("\tRead request is sucessful.\n");
		#endif
	}
	else // lecture frame retour ; frame sent to a CCU and coming back modified
	{
		#ifdef DD_SPY_DEBUG
			printf("In dd_read_fifo : Received frame is a direct acknowledge return.\n");
		#endif
		for (lcl_j = 0 ; lcl_j < lcl_ncar ; lcl_j++)
		{
			glb_back[lcl_trans][lcl_j] = lcl_buf[lcl_j];
			(*(param_stack + lcl_j)) = lcl_buf[lcl_j];
		}
		// if address seen and data copied
		if ((lcl_status & DD_FEC_STATUS_FLAGS) == DD_FEC_ADDR_SEEN_AND_DATA_COPIED)
		{
			glb_copied[lcl_trans] = DD_COPY_DATA_SUCCESSFUL;
			*glb_copied_status = DD_COPY_DATA_SUCCESSFUL;
			#ifdef DD_SPY_DEBUG
				printf("\tDirect acknowledge is sucessful.\n");
			#endif
		}
	}
}








	//FREE TRANSACTION NUMBER
	glb_fec_write_trans_in_use[lcl_trans] = DD_TRANS_NUMBER_IS_FREE;


	//Return appropriate transaction status
	if (glb_copied[lcl_trans] == DD_COPY_DATA_SUCCESSFUL)
	{	
		return DD_RETURN_OK;
	}
	else return DD_DATA_CORRUPT_ON_WRITE;

}










































































int glue_utca_fec_write_msg_reworked(uhal::HwInterface * hw, DD_FEC_STD_WORD *param_stack)
{

//#define DD_DEBUG_FEC_WRITE_MSG

int lcl_count;
DD_FEC_STD_WORD lcl_trans_number_offset;
int lcl_i;
int lcl_trans;
DD_FEC_STD_WORD lcl_trans_number;
int lcl_j;
uint32_t lcl_fec_value;
int lcl_ctrl_loop;
DD_FEC_FIFO_DATA_32 lcl_buf32[DD_MAX_MSG_LENGTH_32];
int lcl_count32;
int lcl_err;
int lcl_fec_ctrl0_memory;



	#ifdef DD_DEBUG_FEC_WRITE_MSG
		std::cout << std::endl << std::endl << "Entering glue_utca_fec_write_msg_reworked() method\n" << std::endl;
	#endif


	// Now, compute frame length
	// If lenght is less than or equal to 127, than just copy frame length
	// Else, compute real frame length
	if (isLongFrame(param_stack[DD_MSG_LEN_OFFSET]))
	{
		lcl_count = ((((param_stack[DD_MSG_LEN_OFFSET] & DD_UPPER_BITMASK_FOR_LONGFRAME)<<8) + param_stack[DD_MSG_LEN_OFFSET+1]) + DD_FRAME_HEAD_LENGTH_IF_LONGFRAME);
		lcl_trans_number_offset = DD_TRANS_NUMBER_OFFSET+1;
	}
	else
	{
		lcl_count = (param_stack[DD_MSG_LEN_OFFSET] + DD_FRAME_HEAD_LENGTH_IF_SHORTFRAME);
		lcl_trans_number_offset = DD_TRANS_NUMBER_OFFSET;
	}
	#ifdef DD_DEBUG_FEC_WRITE_MSG
		std::cout << "I see a frame of total length: 0x" << std::hex << lcl_count << std::dec << std::endl;
	#endif






	// search the first free transaction number occuring after the last used one
	// First, keep memory of last used transaction number
	lcl_i = glb_currentTrans;

	// fake invalid transaction number for loop control
	lcl_trans = DD_INVALID_TRANSACTION_NUMBER_EXAMPLE;

	do
	{
		// point to next of last used transaction number
		lcl_i++;
		// if out of range, loop as circular buffer
		if (lcl_i > DD_MAX_TRANS_NUMBER) { lcl_i = DD_MIN_TRANS_NUMBER;}
		// check if slot is free
		//if (glb_fec_write_trans_in_use[lcl_i] == DD_TRANS_NUMBER_IS_FREE) {lcl_trans=(DD_FEC_STD_WORD)lcl_i;}
		lcl_trans=(DD_FEC_STD_WORD)lcl_i;
		// if one full loop was performed and no free transaction was found, force exit
		if (lcl_i == glb_currentTrans) {lcl_trans=(DD_FEC_STD_WORD)lcl_i;}

	} while (lcl_trans == DD_INVALID_TRANSACTION_NUMBER_EXAMPLE);


	// if no free transaction number found, return an error
	// if the only free transaction number found is the last used one, return an error too
	if (lcl_trans == glb_currentTrans)
	{
		return DD_NO_MORE_FREE_TRANSACTION_NUMBER;
	}

	// if one free transaction number is found, tag this number as currently used
	glb_fec_write_trans_in_use[lcl_trans] = DD_TRANS_NUMBER_IS_IN_USE;


	// switch to unsigned 16 value
	lcl_trans_number = lcl_trans;

	// keep memory of last used number
	glb_currentTrans = lcl_trans_number;

	// set transaction number in frame
	param_stack[lcl_trans_number_offset] = lcl_trans;
	//param_stack[lcl_trans_number_offset] = 0x66;





	// Set EOF flag for last data of fifo transmit
	param_stack[lcl_count-1] = (param_stack[lcl_count-1] | DD_EOF_BITMASK);




	// Check if "fifo transmit running" flag  is down before writing in fifo
	lcl_err = glue_utca_fec_read_mfec_value(hw, (uint32_t)STATUS0, &lcl_fec_value);
	lcl_ctrl_loop=0;
	while ( ((lcl_fec_value & DD_FEC_TRA_RUN)==1) && (lcl_ctrl_loop < DD_LOOP_LIMIT_WHEN_POLLING_FIFOTRA_RUNNING))
	{
		usleep(10);
        	lcl_err = glue_utca_fec_read_mfec_value(hw, (uint32_t)STATUS0, &lcl_fec_value);
        	lcl_ctrl_loop++;
	}
	if (lcl_ctrl_loop >= DD_LOOP_LIMIT_WHEN_POLLING_FIFOTRA_RUNNING)
	{
		std::cout << "PROBLEM BEFORE FILLING FIFOTRA : FIFO TRANSMIT FLAG IS ALWAYS ON." << std::endl;
		return DD_FIFOTRA_RUNNING_FLAG_IS_ALWAYS_ON;
	}


	#ifdef DD_DEBUG_FEC_WRITE_MSG
		std::cout << "I will put in fifo transmit :\n" << std::endl;
	#endif



	// convert from 16 bits to 32 bits word
	dd_cat_from_std_to_32(param_stack, lcl_buf32, lcl_count, &lcl_count32);

	// copy send buffer to fifo transmit
	for (lcl_j = 0 ; lcl_j < lcl_count32 ; lcl_j++)
	{
		//lcl_fifo_word = lcl_buf32[lcl_j];
		lcl_err = glue_utca_fec_write_to_fifo_transmit(hw, lcl_buf32[lcl_j]);
		#ifdef DD_DEBUG_FEC_WRITE_MSG
			std::cout << "\t0x" << std::hex << lcl_buf32[lcl_j] << std::dec << std::endl;
		#endif
	}
	#ifdef DD_DEBUG_FEC_WRITE_MSG
		std::cout << "Done, fifo transmit filled\n" << std::endl;
	#endif




	// Read current CTRL0 value
	lcl_err = glue_utca_fec_read_mfec_value(hw, (uint32_t)CONTROL0, &lcl_fec_value);
	// keep memory
	lcl_fec_ctrl0_memory = lcl_fec_value;

	// Make OR
	//lcl_fec_value = (lcl_fec_ctrl0_memory | DD_FEC_ENABLE_FEC );
	// send fifotransmit to ring - step 1/3
	//lcl_err = glue_utca_fec_write_mfec_value(hw, (uint32_t)CONTROL0, lcl_fec_value);

	// Make OR 
	lcl_fec_value = (lcl_fec_ctrl0_memory | DD_FEC_ENABLE_FEC | DD_FEC_SEND);
	// send fifotransmit to ring - step 2/3
	lcl_err = glue_utca_fec_write_mfec_value(hw, (uint32_t)CONTROL0, lcl_fec_value);


	// Make OR
	lcl_fec_value = (lcl_fec_ctrl0_memory | DD_FEC_ENABLE_FEC);
	// send fifotransmit to ring - step 1/3
	lcl_err = glue_utca_fec_write_mfec_value(hw, (uint32_t)CONTROL0, lcl_fec_value);


	//FRAME IS NOW SENT ON RING


	// Return only when "fifo transmit running" flag  is down (end of transmission)
	lcl_err = glue_utca_fec_read_mfec_value(hw, (uint32_t)STATUS0, &lcl_fec_value);
	lcl_ctrl_loop=0;
	while ( ((lcl_fec_value & DD_FEC_TRA_RUN)==1) && (lcl_ctrl_loop < DD_LOOP_LIMIT_WHEN_POLLING_FIFOTRA_RUNNING))
	{
		usleep(10);
        	lcl_err = glue_utca_fec_read_mfec_value(hw, (uint32_t)STATUS0, &lcl_fec_value);
        	lcl_ctrl_loop++;
	}
	if (lcl_ctrl_loop >= DD_LOOP_LIMIT_WHEN_POLLING_FIFOTRA_RUNNING)
	{
		std::cout << "PROBLEM AFTER SENDING FIFOTRA CONTENT ON RING : FIFO TRANSMIT FLAG IS ALWAYS ON." << std::endl;
		return DD_FIFOTRA_RUNNING_FLAG_IS_ALWAYS_ON;
	}

	return DD_RETURN_OK;
}





































int glue_utca_fec_read_msg_reworked(uhal::HwInterface * hw, DD_FEC_STD_WORD *read_stack, int *frameType)
{

//#define DD_DEBUG_FEC_READ_MSG



#ifdef DD_DEBUG_FEC_READ_MSG
	int lcl_j;
#endif
uint32_t lcl_fec_value;
int lcl_ctrl_loop;
DD_FEC_FIFO_DATA_32 lcl_buf32[DD_MAX_MSG_LENGTH_32];
int lcl_err;
int lcl_ncar32;
DD_FEC_FIFO_DATA_32 lcl_data32;
DD_FEC_STD_WORD lcl_fl1, lcl_fl2, lcl_frame_length, lcl_frame_32_length;
int lcl_length;
DD_FEC_STD_WORD lcl_status=0;






	#ifdef DD_DEBUG_FEC_READ_MSG
		std::cout << "Entering glue_utca_fec_read_msg_reworked() method\n" << std::endl;
	#endif


	//Wait until a frame present in fifo receive AND Fifo Receive running is false
	lcl_err = glue_utca_fec_read_mfec_value(hw, (uint32_t)STATUS0, &lcl_fec_value);
	lcl_ctrl_loop=0;
	while (                 (  ((lcl_fec_value & DD_FEC_REC_EMPTY)==1) && (lcl_ctrl_loop < 1000)  ) || ((lcl_fec_value & DD_FEC_REC_RUN) ==1)                        )
	{
		usleep(10);
        	lcl_err = glue_utca_fec_read_mfec_value(hw, (uint32_t)STATUS0, &lcl_fec_value);
        	lcl_ctrl_loop++;
	}
	if (lcl_ctrl_loop >= 1000)
	{
		std::cout << "PROBLEM WHEN TRYING TO READ A FRAME : FIFO RECEIVE IS ALWAYS EMPTY." << std::endl;
		return -1;
	}



	//Now, frame is present into FIFO RECEIVE
	//So read it...
	lcl_ncar32 = 0;
	lcl_frame_32_length = 0;
	#ifdef DD_DEBUG_FEC_READ_MSG
		std::cout << "Now reading from Fifo Receive : \n" << std::endl;
	#endif
	do
	{
		lcl_err = glue_utca_fec_read_from_fifo_receive(hw, &lcl_data32);
		#ifdef DD_DEBUG_FEC_READ_MSG
			std::cout << "\t0x" << std::hex << lcl_data32 << std::dec << std::endl;
		#endif
		//If buffer not full, store data. Else, avoid buffer overflow
		if (lcl_ncar32 < DD_MAX_MSG_LENGTH_32)
		{
			lcl_buf32[lcl_ncar32] = lcl_data32;
		}
		else
		{
			std::cout << "Too many words read from FIFO RECEIVE for one frame (more than DD_MAX_MSG_LENGTH_32) ; aborting." << std::endl;
			return -1;
		}


		//length analysis
		if (lcl_ncar32 == 0)
		{

			// How many DATA elements will I have in my standard frame ?
			lcl_fl1 = (DD_FEC_STD_WORD)((lcl_data32>>8) & 0xFF);
			lcl_fl2 = (DD_FEC_STD_WORD)(lcl_data32 & 0xFF);
			#ifdef DD_DEBUG_FEC_READ_MSG
				std::cout << "In function : length analysis" << std::endl;
				std::cout << "MSB 8 bits are lcl_fl1 = 0x" << std::hex << lcl_fl1 << std::dec << std::endl;
				std::cout << "LSB 8 bits are lcl_fl2 = 0x" << std::hex << lcl_fl2 << std::dec << std::endl;
			#endif

			if ((lcl_fl1 & 0x0080)==0)
			{
				lcl_frame_length = lcl_fl1 + DD_FRAME_HEAD_LENGTH_IF_SHORTFRAME;
				#ifdef DD_DEBUG_FEC_READ_MSG
					std::cout << "I'm in short frame case" << std::endl;
				#endif
			}
			else
			{
				lcl_frame_length = ((((lcl_fl1 & 0x007F) << 8) + lcl_fl2) + DD_FRAME_HEAD_LENGTH_IF_LONGFRAME);
				#ifdef DD_DEBUG_FEC_READ_MSG
					std::cout << "I'm in long frame case" << std::endl;
				#endif
			}
			//increment data frame length of 1 item (+1) for the status byte to add
			//because when splitted in 32 bits words, the last standard word
			//status(8bits) -- data (8 bits) == 1 frame element is seen as 2 elements
			//when read from fifo receive
			lcl_frame_length++;
			#ifdef DD_DEBUG_FEC_READ_MSG
				std::cout << "Final computed frame length (in Bytes) is : 0x" << std::hex << lcl_frame_length << std::dec << std::endl;
			#endif

			//now, check how many 32 bits elements we must extract from the
			//fifo receive to fill our standard frame
			lcl_frame_32_length = lcl_frame_length / 4;
			if ( (lcl_frame_length % 4) != 0 ) lcl_frame_32_length++;

			#ifdef DD_DEBUG_FEC_READ_MSG
				std::cout << "Final computed frame length (in 32 bits words) is : 0x" << std::hex << lcl_frame_32_length << std::dec << std::endl;
			#endif
		}
		lcl_ncar32++;

	} while (lcl_ncar32 < lcl_frame_32_length);




	//switch to 16 bits representation
	dd_uncat_from_32_to_std(read_stack, lcl_buf32, &lcl_length, lcl_ncar32);
	//lcl_ncar = lcl_length;
	lcl_status = read_stack[lcl_length-1];
	#ifdef DD_DEBUG_FEC_READ_MSG
		std::cout << "Un-cat frame is :" << std::endl;
		for (lcl_j = 0 ; lcl_j < lcl_length ; lcl_j++)
		{
			std::cout << "    0x" << std::hex << read_stack[lcl_j] << std::dec << std::endl;
		}
		std::cout << "frame length : 0x" << std::hex << lcl_length <<"   ;;;   Frame status : 0x" << lcl_status << std::dec << std::endl;
	#endif
	



	if (read_stack[DD_DESTINATION_OFFSET] == DD_FEC_IS_MSG_DESTINATION)
	//i.e. lecture data frame
	//or could be an ACK
	{
		#ifdef DD_DEBUG_FEC_READ_MSG
			std::cout << "In dd_read_fifo : Received frame is answer to a read request or a f-ack.\n" << std::endl;
		#endif
		*frameType = ANSWER_FRAME_FROM_READ_REQUEST_OR_FORCED_ACK;
		return DD_RETURN_OK;
	}
	else /*! lecture frame retour ; frame sent to a CCU and coming back modified */
	{
		#ifdef DD_DEBUG_FEC_READ_MSG
			std::cout << "In dd_read_fifo : Received frame is a direct acknowledge return.\n" << std::endl;
		#endif
		*frameType = DIRECT_ACK_FRAME;
		/*! if address seen and data copied */
		if ((lcl_status & DD_FEC_STATUS_FLAGS) == DD_FEC_ADDR_SEEN_AND_DATA_COPIED)
		{
			#ifdef DD_DEBUG_FEC_READ_MSG
				std::cout << "\tDirect acknowledge is sucessful.\n" << std::endl;
			#endif
			return DD_RETURN_OK;
		}
		else
		{
			#ifdef DD_DEBUG_FEC_READ_MSG
				std::cout << "\tBad status for direct acknowledge frame.\n" << std::endl;
			#endif
			return -2;
		}
	}


}






int glue_utca_fec_free_transaction_number_reworked(int transactionNumber)
{
	glb_fec_write_trans_in_use[transactionNumber] = DD_TRANS_NUMBER_IS_FREE;
	return DD_RETURN_OK;
}









































/*!
Main Function
*/

int main(void)
{

glb_atomic_flags = 0;
glb_currentTrans = DD_MIN_TRANS_NUMBER;

for (int i=0; i<DD_TRANS_ARRAY_UPPER_LIMIT; i++)
{
	glb_fec_write_trans_in_use[i] = DD_TRANS_NUMBER_IS_FREE;
}









/* control of the main loop */
int lcl_choix = 999;

/* generic error container */
DD_TYPE_ERROR lcl_err;

/* bunch of containers, possibly covering all the interesting hardware registers */
DD_FEC_REGISTER_DATA lcl_control0;
DD_FEC_REGISTER_DATA lcl_control1;
DD_FEC_REGISTER_DATA lcl_status0;
DD_FEC_REGISTER_DATA lcl_status1;
DD_FEC_REGISTER_DATA fwVersion;

DD_FEC_FIFO_DATA_32 lcl_fifotra_32;

DD_FEC_FIFO_DATA_32 lcl_fiforet_32;

DD_FEC_FIFO_DATA_32 lcl_fiforec_32;










/* init ccu25 CTRL A frame, used for CCU25 detection */
DD_FEC_STD_WORD i2c01_talk_ccu25[DD_USER_MAX_MSG_LENGTH] = {0xFFFF, 0x0000, DD_CCU25_WRITE_CRA_FRAME_LENGTH, DD_CCU_NODE_CONTROLLER_CHANNEL, DD_FAKE_TRANSACTION_NUMBER, DD_CCU25_COMMAND_WRITE_CTRL_A, (DD_CCU25_DATA_WCRA_ON_INIT | DD_FRAME_EOF_BITMASK)};

/* Frame used to READ BACK ccu25 CTRL A */
DD_FEC_STD_WORD i2c02_talk_ccu25[DD_USER_MAX_MSG_LENGTH] = {0xFFFF, 0x0000, DD_CCU25_READ_CRA_FRAME_LENGTH, DD_CCU_NODE_CONTROLLER_CHANNEL, DD_FAKE_TRANSACTION_NUMBER, (DD_CCU25_COMMAND_READ_CTRL_A | DD_FRAME_EOF_BITMASK)};

/* Frame used to initialize ccu25 CTRL E */
DD_FEC_STD_WORD i2c03_talk_ccu25[DD_USER_MAX_MSG_LENGTH] = {0xFFFF, 0x0000, 0x0006, 0x0000, 0x0000, 0x0004, 0x0000, 0x00FF, 0x80FF};

/* Frame used to READ BACK ccu25 CTRL E */
DD_FEC_STD_WORD i2c04_talk_ccu25[DD_USER_MAX_MSG_LENGTH] = {0xFFFF, 0x0000, DD_CCU25_READ_CRE_FRAME_LENGTH, DD_CCU_NODE_CONTROLLER_CHANNEL, DD_FAKE_TRANSACTION_NUMBER, (DD_CCU25_COMMAND_READ_CTRL_E | DD_FRAME_EOF_BITMASK)};

/* Frame used to Write I2C CRA on CCU25 (iniot I2C channels)*/
DD_FEC_STD_WORD i2c05_talk_ccu25[DD_USER_MAX_MSG_LENGTH] = {0xFFFF, 0x0000, 0x0004, 0x0010, DD_FAKE_TRANSACTION_NUMBER, 0x00F0, (0x0040 | DD_FRAME_EOF_BITMASK)};

/* Frame used to Read back the CCU25 I2C CRA */
DD_FEC_STD_WORD i2c06_talk_ccu25[DD_USER_MAX_MSG_LENGTH] = {0xFFFF, 0x0000, 0x0003, 0x0010, DD_FAKE_TRANSACTION_NUMBER, (0x00F1 | DD_FRAME_EOF_BITMASK)};







/* incremental counters used in full scanring function */
DD_FEC_STD_WORD ccu_counter;
DD_FEC_STD_WORD channel_counter;
DD_FEC_STD_WORD i2c_counter;





/* typical write_then_read_answer stacks */
DD_FEC_STD_WORD write_stack[DD_USER_MAX_MSG_LENGTH];
DD_FEC_STD_WORD read_stack[DD_USER_MAX_MSG_LENGTH];





int lcl_fifo_depth=0;

unsigned int lcl_scanme;
int lcl_j;

glb_atomic_flags = 0;

numberOfWriteAccessToFirmware = 0;
numberOfReadAccessToFirmware = 0;



	/***********************************************************************
	open uhal connection
	***********************************************************************/
        uhal::ConnectionManager lConnectionManager ("file:///opt/testing/trackerDAQ-3.2/FecSoftwareV3_0/ThirdParty/UTCAConsoleDebugger/glibconfig/connections.xml");
    	std::cout << "Using board " << "board" << std::endl;
    	uhal::HwInterface lBoard = lConnectionManager.getDevice ( "board" );

std::cout << "\n\n\nBOARD ANOW ACTIVE\n\n\n" << std::endl;

	/***********************************************************************
	Display a board Id ; be sure we are physically connected to the board
	***********************************************************************/

	glue_utca_fec_get_firmware_version(&lBoard, &fwVersion);
	glue_utca_fec_reset_board(&lBoard);
	lcl_err = glue_utca_fec_display_user_wb_reg1_id(&lBoard);
	

	/***********************************************************************
	Initialise mFEC board  -  equivalent to reset mFEC
	***********************************************************************/
	lcl_err = glue_utca_fec_reset_board(&lBoard);
	std::cout << std::endl << "Board resetted, ready to go" << std::endl << std::endl;
	lcl_err = glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL0, (DD_FEC_REGISTER_DATA)1);

	/***********************************************************************
	Get firmware version from board
	***********************************************************************/

	uint32_t firmware_version;
	lcl_err = glue_utca_fec_read_mfec_value(&lBoard, (uint32_t)VERSION, &firmware_version);
	if ( lcl_err != DD_RETURN_OK)
	{
		fprintf(stderr,"Error during glue_utca_fec_read_mfec_value\n");
	}
	else printf("uTCA mFEC firmware version is : 0x%x\n", firmware_version);



	/***********************************************************************
	Force FIFOs depth to 32 ; program will be used with new optical mFECs only
	***********************************************************************/
	//
	lcl_fifo_depth = 32;


	/***********************************************************************
	Test Write/Read mode
	***********************************************************************/
	
	uint32_t valToRead;
	lcl_err = glue_utca_fec_read_mfec_value(&lBoard, (uint32_t)CONTROL0, &valToRead);
	std::cout << "At readout, CONTROL0=" << std::hex << valToRead << std::dec << std::endl;

	std::cout << "Now forcing CONTROL0 to a value of 0x1" << std::endl;
	lcl_err = glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL0, 0x1);
	
	lcl_err = glue_utca_fec_read_mfec_value(&lBoard, (uint32_t)CONTROL0, &valToRead);
	std::cout << "Reading back, CONTROL0=" << std::hex << valToRead << std::dec << std::endl;
	

	/***********************************************************************
	Main Menu
	***********************************************************************/
	while (lcl_choix != DBG_QUIT)
	{
		fprintf(stdout,"\nMENU\n");
		fprintf(stdout,"%d : Quit\n",DBG_QUIT);
		fprintf(stdout,"%d : Write to control 0\n",DBG_WCR0);
		fprintf(stdout,"%d : Read control 0\n",DBG_RCR0);
		fprintf(stdout,"%d : Write control 1\n",DBG_WCR1);
		fprintf(stdout,"%d : Read status 0\n",DBG_RSR0);
		fprintf(stdout,"%d : Read status 1\n",DBG_RSR1);
		fprintf(stdout,"%d : Write to fifotransmit\n",DBG_WFIFOTRA);
		fprintf(stdout,"%d : Read from fifotransmit\n",DBG_RFIFOTRA);
		fprintf(stdout,"%d : Write to fiforeturn\n",DBG_WFIFORET);
		fprintf(stdout,"%d : Read from fiforeturn\n",DBG_RFIFORET);
		fprintf(stdout,"%d : Write to fiforeceive\n",DBG_WFIFOREC);
		fprintf(stdout,"%d : Read from fiforeceive\n",DBG_RFIFOREC);
		fprintf(stdout,"%d : init TTCRX\n",DBG_INITTTCRX);
		fprintf(stdout,"%d : Resets PLX\n",DBG_RESETPLX);
		fprintf(stdout,"%d : Resets FEC\n",DBG_RESETFEC);
		fprintf(stdout,"%d : NEW CCU (i.e. CCU 25) ONLY : Full Scan Ring - READ from devices\n",DBG_NCCU_SR_READ);
		fprintf(stdout,"%d : NEW CCU (i.e. CCU 25) ONLY : Full Scan Ring - READ from devices -- REWORKED\n",DBG_NCCU_SR_READ_REWORKED);
		fprintf(stdout,"%d : NEW CCU ONLY : Quick scan ; detect only CCU's on ring\n",DBG_NCCU_QS);
		fprintf(stdout,"%d : Readout Firmware version number.\n",DBG_READ_FIRMWARE_VERSION);
		fprintf(stdout,"%d : Enable full redundancy for CCU 0xD.\n",DBG_RECONFIGURE_CCU_HEX_D);
		fprintf(stdout,"%d : Send partial reset.\n",DBG_SEND_PARTIAL_RESET);
		fprintf(stdout,"%d : Send one single frame on ring.\n",DBG_SEND_ONE_FRAME);
		fprintf(stdout,"%d : Send one block of two frames on ring.\n",DBG_SEND_TWO_FRAMES_AS_BLOCK);
		fprintf(stdout,"%d : Send sequentially two frames on ring.\n",DBG_SEND_TWO_FRAMES_SEQUENTIAL);



		
		fprintf(stdout,"Your choice ? : ");
		scanf("%d",&lcl_choix);


		switch(lcl_choix)
		{
			uint32_t valToRead;

			case DBG_QUIT:
			break;




			/*******************************************************
			Write control 0
			*******************************************************/
			case DBG_WCR0:
				fprintf(stdout,"Value to write to CONTROL 0 (HEXA) : ");
				scanf("%x", &lcl_scanme);
				lcl_control0 = (DD_FEC_REGISTER_DATA)lcl_scanme;
				lcl_err = glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL0, lcl_control0);
				if (lcl_err != DD_RETURN_OK)
				{
					//glue_fecdriver_print_error_message(lcl_err);
					std::cout << "ERROR!!" << std::endl;
				}
				else fprintf(stdout,"Value wrote in FEC CTRL0 : 0x%x\n",lcl_control0);
			break;


			/*******************************************************
			Read control 0
			*******************************************************/
			case DBG_RCR0:
				lcl_err = glue_utca_fec_read_mfec_value(&lBoard, (uint32_t)CONTROL0, &valToRead);
				lcl_control0 = valToRead;
				if (lcl_err != DD_RETURN_OK)
				{
					//glue_fecdriver_print_error_message(lcl_err);
					std::cout << "ERROR!!" << std::endl;
				}
				else
				{
					fprintf(stdout,"CONTROL 0 current value : 0x%x\n",lcl_control0);
					dbg_decode_control0(lcl_control0);
				}
			break;








			/*******************************************************
			Write control 1
			*******************************************************/
			case DBG_WCR1:
				fprintf(stdout,"Value to write to CONTROL 1 (HEXA) : ");
				scanf("%x", &lcl_scanme);
				lcl_control1 = (DD_FEC_REGISTER_DATA)lcl_scanme;
				lcl_err = glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL1, lcl_control1);
				if (lcl_err != DD_RETURN_OK)
				{
					//glue_fecdriver_print_error_message(lcl_err);
					std::cout << "ERROR!!" << std::endl;
				}
				else fprintf(stdout,"Value wrote in FEC CTRL1 : 0x%x\n",lcl_control1);
			break;



			/*******************************************************
			Read status 0
			*******************************************************/
			case DBG_RSR0:
				lcl_err = glue_utca_fec_read_mfec_value(&lBoard, (uint32_t)STATUS0, &valToRead);
				lcl_status0 = valToRead;
				if (lcl_err != DD_RETURN_OK)
				{
					//glue_fecdriver_print_error_message(lcl_err);
					std::cout << "ERROR!!" << std::endl;
				}
				else
				{
					fprintf(stdout,"STATUS 0 value : 0x%x\n",lcl_status0);
					dbg_decode_status0(lcl_status0);
				}
			break;



			/*******************************************************
			Read status  1
			*******************************************************/
			case DBG_RSR1:
				lcl_err = glue_utca_fec_read_mfec_value(&lBoard, (uint32_t)STATUS1, &valToRead);
				lcl_status1 = valToRead;
				if (lcl_err != DD_RETURN_OK)
				{
					//glue_fecdriver_print_error_message(lcl_err);
					std::cout << "ERROR!!" << std::endl;
				}
				else
				{
					fprintf(stdout,"STATUS 1 value : 0x%x\n",lcl_status1);
					dbg_decode_status1(lcl_status1);
				}
			break;




			/*******************************************************
			Write to fifotransmit
			*******************************************************/
			case DBG_WFIFOTRA:
				fprintf(stdout,"Value to write to FIFO TRANSMIT (HEXA) : ");
				scanf("%x", &lcl_scanme);
				fprintf(stdout,"You have entered the value : 0x%x\n", lcl_scanme);
				
				lcl_fifotra_32 = (DD_FEC_FIFO_DATA_32)lcl_scanme;
				lcl_err = glue_utca_fec_write_to_fifo_transmit(&lBoard, lcl_fifotra_32);
				if (lcl_err != DD_RETURN_OK)
				{
					//glue_fecdriver_print_error_message(lcl_err);
					std::cout << "ERROR!!" << std::endl;
				}
				else
				{
					fprintf(stdout,"Value wrote in FEC Fifo Transmit : 0x%x\n",lcl_fifotra_32);
				}
			break;



			/*******************************************************
			Read from fifotransmit
			*******************************************************/
			case DBG_RFIFOTRA:
				lcl_err = glue_utca_fec_read_from_fifo_transmit(&lBoard, &lcl_fifotra_32);
				if (lcl_err != DD_RETURN_OK)
				{
					//glue_fecdriver_print_error_message(lcl_err);
					std::cout << "ERROR!!" << std::endl;
				}
				else
				{
					fprintf(stdout,"fifo transmit value : 0x%x\n",lcl_fifotra_32);
				}
			break;





			/*******************************************************
			Write to fiforeturn
			*******************************************************/
			case DBG_WFIFORET:
				fprintf(stdout,"Value to write to FIFO RETURN (HEXA) : ");
				scanf("%x", &lcl_scanme);

				lcl_fiforet_32 = (DD_FEC_FIFO_DATA_32)lcl_scanme;
				lcl_err = glue_utca_fec_write_to_fifo_return(&lBoard, lcl_fiforet_32);
				if (lcl_err != DD_RETURN_OK)
				{
					//glue_fecdriver_print_error_message(lcl_err);
					std::cout << "ERROR!!" << std::endl;
				}
				else
				{
					fprintf(stdout,"Value wrote in FEC Fifo Return : 0x%x\n",lcl_fiforet_32);
				}
			break;



			/*******************************************************
			Read from fiforeturn
			*******************************************************/
			case DBG_RFIFORET:
				lcl_err = glue_utca_fec_read_from_fifo_return(&lBoard, &lcl_fiforet_32);
				if (lcl_err != DD_RETURN_OK)
				{
					//glue_fecdriver_print_error_message(lcl_err);
					std::cout << "ERROR!!" << std::endl;
				}
				else
				{
					fprintf(stdout,"fifo return value : 0x%x\n",lcl_fiforet_32);
				}
			break;




			/*******************************************************
			Write to fiforeceive
			*******************************************************/
			case DBG_WFIFOREC:
				fprintf(stdout,"Value to write to FIFO RECEIVE (HEXA) : ");
				scanf("%x", &lcl_scanme);

				lcl_fiforec_32 = (DD_FEC_FIFO_DATA_32)lcl_scanme;
				lcl_err = glue_utca_fec_write_to_fifo_receive(&lBoard, lcl_fiforec_32);
				if (lcl_err != DD_RETURN_OK)
				{
					//glue_fecdriver_print_error_message(lcl_err);
					std::cout << "ERROR!!" << std::endl;
				}
				else
				{
					fprintf(stdout,"Value wrote in FEC Fifo Receive : 0x%x\n",lcl_fiforec_32);
				}
			break;



			/*******************************************************
			Read from fiforeturn
			*******************************************************/
			case DBG_RFIFOREC:
				lcl_err = glue_utca_fec_read_from_fifo_receive(&lBoard, &lcl_fiforec_32);
				if (lcl_err != DD_RETURN_OK)
				{
					//glue_fecdriver_print_error_message(lcl_err);
					std::cout << "ERROR!!" << std::endl;
				}
				else
				{
					fprintf(stdout,"fifo receive value : 0x%x\n",lcl_fiforec_32);
				}
			break;




			/*******************************************************
			Init TTCRX
			*******************************************************/
			case DBG_INITTTCRX:
				lcl_err = glue_utca_fec_init_ttcrx(&lBoard);
				if (lcl_err != DD_RETURN_OK)
				{
					//glue_fecdriver_print_error_message(lcl_err);
					std::cout << "ERROR!!" << std::endl;
				}
				else fprintf(stdout,"TTCRX Initialised\n");
			break;












































			/*******************************************************
			Reset PLX
			*******************************************************/
			case DBG_RESETPLX:
				lcl_err = glue_utca_fec_reset_board(&lBoard);
				if (lcl_err != DD_RETURN_OK)
				{
					//glue_fecdriver_print_error_message(lcl_err);
					std::cout << "ERROR!!" << std::endl;
				}
				else fprintf(stdout,"PLX Resetted\n");
				lcl_err = glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL0, (DD_FEC_REGISTER_DATA)1);

			break;




			/*******************************************************
			Reset FEC
			*******************************************************/
			case DBG_RESETFEC:
				lcl_err = glue_utca_fec_soft_reset(&lBoard);
				if (lcl_err != DD_RETURN_OK)
				{
					//glue_fecdriver_print_error_message(lcl_err);
					std::cout << "ERROR!!" << std::endl;
				}
				else fprintf(stdout,"FEC Soft-Resetted\n");
			break;








			/*******************************************************
			Read firmware version
			*******************************************************/
			case DBG_READ_FIRMWARE_VERSION:
				uint32_t firmware_version;
				lcl_err = glue_utca_fec_read_mfec_value(&lBoard, (uint32_t)VERSION, &firmware_version);
				if ( lcl_err != DD_RETURN_OK)
				{
					fprintf(stderr,"Error during glue_utca_fec_read_mfec_value\n");
				}
				else printf("FEC firmware version is : 0x%x\n", firmware_version);
			break;




			/*******************************************************
			QUICK SCANRING FOR NEW CCU's (25)
			*******************************************************/

			case DBG_NCCU_QS:
				DD_FEC_STD_WORD glb_read_status, glb_copied_status;
				fprintf(stdout,"Scanning ring for NEW CCU's\n");
				fprintf(stdout,"\tscanned CCU's range is [0x%x .. 0x%x]\n", MIN_CCU, MAX_CCU);
				for (ccu_counter = MIN_CCU; ccu_counter <= MAX_CCU ; ccu_counter++)
				//for (ccu_counter = 0x18; ccu_counter <= 0x18 ; ccu_counter++)
				{
					write_stack[0] = ccu_counter;
					fprintf(stdout,"Probing ccu 0x%x \r", ccu_counter);
					for (lcl_j=1 ; lcl_j<((int)(sizeof(i2c01_talk_ccu25)/sizeof(__u16))); lcl_j++)
					{
						write_stack[lcl_j] = i2c01_talk_ccu25[lcl_j];
					}

					//lcl_err = glue_fec_write_frame(fd, write_stack);
					lcl_err = glue_utca_fec_write_msg(&lBoard, write_stack, read_stack, &glb_read_status, &glb_copied_status);
					if (lcl_err == DD_RETURN_OK) fprintf(stdout,"\nCCU number 0x%x detected.\n",ccu_counter);
				}
   			break;







			/*******************************************************
			COMPLETE SCANRING FOR NEW CCU's - probe devices via READ method
			*******************************************************/
			case DBG_NCCU_SR_READ:

				/* print header */
				fprintf(stdout,"\n\nScanning ring for NEW CCU's - Devices are probed via READ method\n");
				fprintf(stdout,"scanned CCU's range is [0x%x .. 0x%x]\n", MIN_CCU, MAX_CCU);
				fprintf(stdout,"scanned i2c channels range is [0x%x .. 0x%x]\n", MIN_I2C_CH25, MAX_I2C_CH25);
				fprintf(stdout,"scanned i2c addresses (per channel) range is [0x%x .. 0x%x]\n", MIN_I2C_ADR, MAX_I2C_ADR);


				/* now, scan the ring */
//XXXXXXXXXXXXXXXXXXXX

				for (ccu_counter = MIN_CCU; ccu_counter <= MAX_CCU ; ccu_counter++)
				//for (ccu_counter = 0x18; ccu_counter <= 0x18 ; ccu_counter++)
				{
					
					
					
					//fprintf(stdout,"Initializing CCU25 CTRL-A\r");
					write_stack[0] = ccu_counter;
					for (lcl_j=1 ; lcl_j<(int)((sizeof(i2c01_talk_ccu25)/sizeof(__u16))); lcl_j++)
					{
						write_stack[lcl_j] = i2c01_talk_ccu25[lcl_j];
					}

					//lcl_err = glue_fec_write_frame(fd, write_stack);
					lcl_err = glue_utca_fec_write_msg(&lBoard, write_stack, read_stack, &glb_read_status, &glb_copied_status);
					if (lcl_err == DD_RETURN_OK)
					{
						fprintf(stdout,"\nCCU number 0x%x detected.\n",ccu_counter);


						//fprintf(stdout,"Reading back CCU25 CTRL-A\n");
						write_stack[0] = ccu_counter;
						for (lcl_j=1 ; lcl_j<(int)((sizeof(i2c02_talk_ccu25)/sizeof(__u16))); lcl_j++)
						{
							write_stack[lcl_j] = i2c02_talk_ccu25[lcl_j];
						}
						//lcl_err = glue_fec_write_frame(fd, write_stack);
						lcl_err = glue_utca_fec_write_msg(&lBoard, write_stack, read_stack, &glb_read_status, &glb_copied_status);
						if (lcl_err == DD_RETURN_OK)
						{
							//fprintf(stdout,"Register read request understood, direct ack received.\n");
							if (glb_read_status != DD_READ_DATA_SUCCESSFUL)
							{
								//fprintf(stdout,"CCU register readback failed.\n");
							}
							//else fprintf(stdout,"CTRLA really read back.\n\n");
						}
						//else fprintf(stdout,"Failure reading direct ack.\n");
							
							






						//fprintf(stdout,"Initializing CCU25 CTRL-E\n");
						write_stack[0] = ccu_counter;
						for (lcl_j=1 ; lcl_j<(int)((sizeof(i2c03_talk_ccu25)/sizeof(__u16))); lcl_j++)
						{
							write_stack[lcl_j] = i2c03_talk_ccu25[lcl_j];
						}
						//lcl_err = glue_fec_write_frame(fd, write_stack);
						lcl_err = glue_utca_fec_write_msg(&lBoard, write_stack, read_stack, &glb_read_status, &glb_copied_status);
						if (lcl_err != DD_RETURN_OK)
						{
							//fprintf(stdout,"Failure reading direct ack.\n");
						}
						//else fprintf(stdout,"\nCCU25 CTRL-E accessed.\n");


						//fprintf(stdout,"Reading back CCU25 CTRL-E\n");
						write_stack[0] = ccu_counter;
						for (lcl_j=1 ; lcl_j<(int)((sizeof(i2c04_talk_ccu25)/sizeof(__u16))); lcl_j++)
						{
							write_stack[lcl_j] = i2c04_talk_ccu25[lcl_j];
						}
						//lcl_err = glue_fec_write_frame(fd, write_stack);
						lcl_err = glue_utca_fec_write_msg(&lBoard, write_stack, read_stack, &glb_read_status, &glb_copied_status);
						if (lcl_err == DD_RETURN_OK)
						{
							//fprintf(stdout,"Register read request understood, direct ack received.\n");
							if (glb_read_status != DD_READ_DATA_SUCCESSFUL)
							{
								//fprintf(stdout,"CCU register readback failed.\n");
							}
							//else fprintf(stdout,"CTRL-E really read back.\n\n");
						}
						//else fprintf(stdout,"Failure reading direct ack.\n");


						//now, i2c channels are initialised and switched on ; let's try to probe them
//XXXXXXXXXXXXXXXXXXXX
						for (channel_counter = MIN_I2C_CH25; channel_counter <= MAX_I2C_CH25 ; channel_counter++)
						//for (channel_counter = 0x1d; channel_counter <= 0x1f ; channel_counter++)
						{
								
							fprintf(stdout,"\tNow probing for i2c devices attached to channel 0x%x of CCU 0x%x ; be patient ...\n", channel_counter, ccu_counter);
							//fprintf(stdout,"\tInitializing I2C channel CRA\n");
							write_stack[0] = ccu_counter;
							for (lcl_j=1 ; lcl_j<(int)((sizeof(i2c05_talk_ccu25)/sizeof(__u16))); lcl_j++)
							{
								write_stack[lcl_j] = i2c05_talk_ccu25[lcl_j];
							}
							write_stack[3] = channel_counter;
							//lcl_err = glue_fec_write_frame(fd, write_stack);
							lcl_err = glue_utca_fec_write_msg(&lBoard, write_stack, read_stack, &glb_read_status, &glb_copied_status);
							if (lcl_err != DD_RETURN_OK)
							{
								//fprintf(stdout,"Failure reading direct ack.\n");
							}
							//else fprintf(stdout,"Register read request understood, direct ack received.\n");

							//fprintf(stdout,"\tReading back I2C channel CRA\n");
							write_stack[0] = ccu_counter;
							for (lcl_j=1 ; lcl_j<(int)((sizeof(i2c06_talk_ccu25)/sizeof(__u16))); lcl_j++)
							{
								write_stack[lcl_j] = i2c06_talk_ccu25[lcl_j];
							}
							write_stack[3] = channel_counter;
							//lcl_err = glue_fec_write_frame(fd, write_stack);
							lcl_err = glue_utca_fec_write_msg(&lBoard, write_stack, read_stack, &glb_read_status, &glb_copied_status);
							if (lcl_err == DD_RETURN_OK)
							{
								//fprintf(stdout,"Register read request understood, direct ack received.\n");
								if (glb_read_status != DD_READ_DATA_SUCCESSFUL)
								{
									//fprintf(stdout,"I2C CTRL A readback FAILED.\n");
								}
								//else fprintf(stdout,"I2C CTRL A readback OK.\n\n");
							}
							//else fprintf(stdout,"Failure reading direct ack.\n");


							// Now, scan for devices on this channel.
//XXXXXXXXXXXXXXXXXXXX
							for (i2c_counter = MIN_I2C_ADR; i2c_counter <= MAX_I2C_ADR ; i2c_counter++)
							//for (i2c_counter = 0x20; i2c_counter <= 0x20 ; i2c_counter++)
							{
								fprintf(stdout,"\t\tprobing i2c address 0x%x on channel 0x%x of CCU 0x%x ; be patient ...\r", i2c_counter, channel_counter, ccu_counter);
								
								//Build stack for Normal Mode testing
								write_stack[0] = ccu_counter;
								write_stack[1] = 0x0000;
								write_stack[2] = 0x0004;
								write_stack[3] = channel_counter;
								write_stack[4] = DD_FAKE_TNUM_PROVOKE_ACK;
								write_stack[5] = 0x0001;
								write_stack[6] = (i2c_counter | DD_FRAME_EOF_BITMASK);
								//lcl_err = glue_fec_write_frame(fd, write_stack);
								lcl_err = glue_utca_fec_write_msg(&lBoard, write_stack, read_stack, &glb_read_status, &glb_copied_status);
								if (lcl_err == DD_RETURN_OK)
								{
									//fprintf(stdout,"Register read request understood, direct ack received.\n");

									if (glb_read_status != DD_READ_DATA_SUCCESSFUL)
									{
										//fprintf(stdout,"\tNo device detected at address 0x%x on channel 0x%x of CCU 0x%x        \n",i2c_counter, channel_counter, ccu_counter);
									}
									else
									{
										if (read_stack[read_stack[2]+2] == 0x8004)
										{
											fprintf(stdout,"\t\t                                                                       \r");
											fprintf(stdout,"\t\tI2C device detected at address 0x%x on channel 0x%x of CCU 0x%x        \n",i2c_counter, channel_counter, ccu_counter);
											//getchar();
										}
									}
									/*
									lcl_err = glue_fec_read_frame(fd, read_stack);
									if (lcl_err == DD_RETURN_OK)
									{
										if (read_stack[read_stack[2]+2] == 0x8004)
										{
											fprintf(stdout,"\tI2C device detected at address 0x%x on channel 0x%x of CCU 0x%x        \n",i2c_counter, channel_counter, ccu_counter);
										}
									}
									*/
									//exit(0);
									
								}
								//else fprintf(stdout,"Failure reading direct ack.\n");
							}
							fprintf(stdout,"\t\t                                                                       \r");
							
						}

					}
				}
			
			break;



























			/*******************************************************
			COMPLETE SCANRING FOR NEW CCU's - REWORKED -- probe devices via READ method
			*******************************************************/
			case DBG_NCCU_SR_READ_REWORKED:
//#define DEBUG_SEE_FRAMES
				int frameType;

				// print header
				std::cout << "\n\nScanning ring for NEW CCU's - Devices are probed via READ method\n" << std::endl;
				std::cout << "scanned CCU's range is [0x" << std::hex << MIN_CCU << " .. 0x" << MAX_CCU  << "]" << std::dec << std::endl;
				std::cout << "scanned i2c channels range is [0x" << std::hex << MIN_I2C_CH25 << " .. 0x" << MAX_I2C_CH25  << "]" << std::dec << std::endl;
				std::cout << "scanned i2c addresses (per channel) range is [0x" << std::hex << MIN_I2C_ADR << " .. 0x" << MAX_I2C_ADR  << "]" << std::dec << std::endl;


				/* now, scan the ring */
//YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY

				for (ccu_counter = MIN_CCU; ccu_counter <= MAX_CCU ; ccu_counter++)
				//for (ccu_counter = 0x18; ccu_counter <= 0x18 ; ccu_counter++)
				{
					
					
					
					//fprintf(stdout,"Initializing CCU25 CTRL-A\r");
					write_stack[0] = ccu_counter;
					for (lcl_j=1 ; lcl_j<(int)((sizeof(i2c01_talk_ccu25)/sizeof(__u16))); lcl_j++)
					{
						write_stack[lcl_j] = i2c01_talk_ccu25[lcl_j];
					}
					lcl_err = glue_utca_fec_write_msg_reworked(&lBoard, write_stack);
					lcl_err = glue_utca_fec_read_msg_reworked(&lBoard, read_stack, &frameType);
					//Caveat : next command is valid only for short frames !!
					glb_fec_write_trans_in_use[read_stack[DD_TRANS_NUMBER_OFFSET]] = DD_TRANS_NUMBER_IS_FREE;
					#ifdef DEBUG_SEE_FRAMES
						std::cout << std::endl << "Direct ACK for CTRL-A initialisation : ";
						for (int g=0; g < read_stack[2]+3; g++)
						{
							std::cout << "Ox" << std::hex << read_stack[g] << std::dec << " ";
						}
						std::cout << std::endl;
					#endif
					if ((lcl_err == DD_RETURN_OK) && (frameType = DIRECT_ACK_FRAME))
					{
						std::cout << std::endl << "CCU number 0x" << std::hex << ccu_counter << std::dec << " detected." << std::endl;


						//fprintf(stdout,"Reading back CCU25 CTRL-A\n");
						write_stack[0] = ccu_counter;
						for (lcl_j=1 ; lcl_j<(int)((sizeof(i2c02_talk_ccu25)/sizeof(__u16))); lcl_j++)
						{
							write_stack[lcl_j] = i2c02_talk_ccu25[lcl_j];
						}
						lcl_err = glue_utca_fec_write_msg_reworked(&lBoard, write_stack);
						lcl_err = glue_utca_fec_read_msg_reworked(&lBoard, read_stack, &frameType);
						#ifdef DEBUG_SEE_FRAMES
							std::cout << std::endl << "Direct ACK for CTRL-A read request : ";
							for (int g=0; g < read_stack[2]+3; g++)
							{
								std::cout << "Ox" << std::hex << read_stack[g] << std::dec << " ";
							}
							std::cout << std::endl;
						#endif
						lcl_err = glue_utca_fec_read_msg_reworked(&lBoard, read_stack, &frameType);
						#ifdef DEBUG_SEE_FRAMES
							std::cout << std::endl << "Answer frame CTRL-A read request : ";
							for (int g=0; g < read_stack[2]+3; g++)
							{
								std::cout << "Ox" << std::hex << read_stack[g] << std::dec << " ";
							}
							std::cout << std::endl;
						#endif
						//Caveat : next command is valid only for short frames !!
						glb_fec_write_trans_in_use[read_stack[DD_TRANS_NUMBER_OFFSET]] = DD_TRANS_NUMBER_IS_FREE;

						//if (lcl_err == DD_RETURN_OK) && (frameType = DIRECT_ACK_FRAME)
						//{
							//fprintf(stdout,"Register read request understood, direct ack received.\n");
							//if (glb_read_status != DD_READ_DATA_SUCCESSFUL)
							//{
								//fprintf(stdout,"CCU register readback failed.\n");
							//}
							//else fprintf(stdout,"CTRLA really read back.\n\n");
						//}
						//else fprintf(stdout,"Failure reading direct ack.\n");
							
							






						//fprintf(stdout,"Initializing CCU25 CTRL-E\n");
						write_stack[0] = ccu_counter;
						for (lcl_j=1 ; lcl_j<(int)((sizeof(i2c03_talk_ccu25)/sizeof(__u16))); lcl_j++)
						{
							write_stack[lcl_j] = i2c03_talk_ccu25[lcl_j];
						}
						lcl_err = glue_utca_fec_write_msg_reworked(&lBoard, write_stack);
						lcl_err = glue_utca_fec_read_msg_reworked(&lBoard, read_stack, &frameType);
						//Caveat : next command is valid only for short frames !!
						glb_fec_write_trans_in_use[read_stack[DD_TRANS_NUMBER_OFFSET]] = DD_TRANS_NUMBER_IS_FREE;
						#ifdef DEBUG_SEE_FRAMES
							std::cout << std::endl << "Direct ACK for CTRL-E initialisation : ";
							for (int g=0; g < read_stack[2]+3; g++)
							{
								std::cout << "Ox" << std::hex << read_stack[g] << std::dec << " ";
							}
							std::cout << std::endl;
						#endif

						//if (lcl_err != DD_RETURN_OK)
						//{
							//fprintf(stdout,"Failure reading direct ack.\n");
						//}
						//else fprintf(stdout,"\nCCU25 CTRL-E accessed.\n");


						//fprintf(stdout,"Reading back CCU25 CTRL-E\n");
						write_stack[0] = ccu_counter;
						for (lcl_j=1 ; lcl_j<(int)((sizeof(i2c04_talk_ccu25)/sizeof(__u16))); lcl_j++)
						{
							write_stack[lcl_j] = i2c04_talk_ccu25[lcl_j];
						}
						lcl_err = glue_utca_fec_write_msg_reworked(&lBoard, write_stack);
						lcl_err = glue_utca_fec_read_msg_reworked(&lBoard, read_stack, &frameType);
						#ifdef DEBUG_SEE_FRAMES
							std::cout << std::endl << "Direct ACK for CTRL-E read request : ";
							for (int g=0; g < read_stack[2]+3; g++)
							{
								std::cout << "Ox" << std::hex << read_stack[g] << std::dec << " ";
							}
							std::cout << std::endl;
						#endif
						lcl_err = glue_utca_fec_read_msg_reworked(&lBoard, read_stack, &frameType);
						#ifdef DEBUG_SEE_FRAMES
							std::cout << std::endl << "Answer frame CTRL-E read request : ";
							for (int g=0; g < read_stack[2]+3; g++)
							{
								std::cout << "Ox" << std::hex << read_stack[g] << std::dec << " ";
							}
							std::cout << std::endl;
						#endif
						//Caveat : next command is valid only for short frames !!
						glb_fec_write_trans_in_use[read_stack[DD_TRANS_NUMBER_OFFSET]] = DD_TRANS_NUMBER_IS_FREE;
						if (lcl_err == DD_RETURN_OK)
						//{
							//fprintf(stdout,"Register read request understood, direct ack received.\n");
							//if (glb_read_status != DD_READ_DATA_SUCCESSFUL)
							//{
								//fprintf(stdout,"CCU register readback failed.\n");
							//}
							//else fprintf(stdout,"CTRL-E really read back.\n\n");
						//}
						//else fprintf(stdout,"Failure reading direct ack.\n");


						//now, i2c channels are initialised and switched on ; let's try to probe them
//YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY
						for (channel_counter = MIN_I2C_CH25; channel_counter <= MAX_I2C_CH25 ; channel_counter++)
						//for (channel_counter = 0x10; channel_counter <= 0x1F ; channel_counter++)
						{
								
							std::cout << "\tNow probing for i2c devices attached to channel 0x" << std::hex << channel_counter << " of CCU 0x" << ccu_counter << std::dec << " ; be patient ..." << std::endl;
							//fprintf(stdout,"\tInitializing I2C channel CRA\n");
							write_stack[0] = ccu_counter;
							for (lcl_j=1 ; lcl_j<(int)((sizeof(i2c05_talk_ccu25)/sizeof(__u16))); lcl_j++)
							{
								write_stack[lcl_j] = i2c05_talk_ccu25[lcl_j];
							}
							write_stack[3] = channel_counter;
							lcl_err = glue_utca_fec_write_msg_reworked(&lBoard, write_stack);
							lcl_err = glue_utca_fec_read_msg_reworked(&lBoard, read_stack, &frameType);
							#ifdef DEBUG_SEE_FRAMES
								std::cout << std::endl << "Direct ACK for I2C channel initialisation : ";
								for (int g=0; g < read_stack[2]+3; g++)
								{
									std::cout << "Ox" << std::hex << read_stack[g] << std::dec << " ";
								}
								std::cout << std::endl;
							#endif

							//Caveat : next command is valid only for short frames !!
							glb_fec_write_trans_in_use[read_stack[DD_TRANS_NUMBER_OFFSET]] = DD_TRANS_NUMBER_IS_FREE;
							//if (lcl_err != DD_RETURN_OK)
							//{
								//fprintf(stdout,"Failure reading direct ack.\n");
							//}
							//else fprintf(stdout,"Register read request understood, direct ack received.\n");

							//fprintf(stdout,"\tReading back I2C channel CRA\n");
							write_stack[0] = ccu_counter;
							for (lcl_j=1 ; lcl_j<(int)((sizeof(i2c06_talk_ccu25)/sizeof(__u16))); lcl_j++)
							{
								write_stack[lcl_j] = i2c06_talk_ccu25[lcl_j];
							}
							write_stack[3] = channel_counter;
							lcl_err = glue_utca_fec_write_msg_reworked(&lBoard, write_stack);
							lcl_err = glue_utca_fec_read_msg_reworked(&lBoard, read_stack, &frameType);
							#ifdef DEBUG_SEE_FRAMES
								std::cout << std::endl << "Direct ACK I2C channel read request : ";
								for (int g=0; g < read_stack[2]+3; g++)
								{
									std::cout << "Ox" << std::hex << read_stack[g] << std::dec << " ";
								}
								std::cout << std::endl;
							#endif
							lcl_err = glue_utca_fec_read_msg_reworked(&lBoard, read_stack, &frameType);
							#ifdef DEBUG_SEE_FRAMES
								std::cout << std::endl << "Answer frame I2C channel read request : ";
								for (int g=0; g < read_stack[2]+3; g++)
								{
									std::cout << "Ox" << std::hex << read_stack[g] << std::dec << " ";
								}
								std::cout << std::endl;
							#endif


							//Caveat : next command is valid only for short frames !!
							glb_fec_write_trans_in_use[read_stack[DD_TRANS_NUMBER_OFFSET]] = DD_TRANS_NUMBER_IS_FREE;
							
							//if (lcl_err == DD_RETURN_OK)
							//{
								//fprintf(stdout,"Register read request understood, direct ack received.\n");
								//if (glb_read_status != DD_READ_DATA_SUCCESSFUL)
								//{
									//fprintf(stdout,"I2C CTRL A readback FAILED.\n");
								//}
								//else fprintf(stdout,"I2C CTRL A readback OK.\n\n");
							//}
							//else fprintf(stdout,"Failure reading direct ack.\n");


							// Now, scan for devices on this channel.
//YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY
							for (i2c_counter = MIN_I2C_ADR; i2c_counter <= MAX_I2C_ADR ; i2c_counter++)
							//for (i2c_counter = 0x00; i2c_counter <= 0x10 ; i2c_counter++)
							{

								std::cout << "\t\tprobing i2c address 0x" << std::hex << i2c_counter << " on channel 0x" << channel_counter << " of CCU 0x" << ccu_counter << std::dec << " ; be patient ...\r";
								
								//Build stack for Normal Mode testing
								write_stack[0] = ccu_counter;
								write_stack[1] = 0x0000;
								write_stack[2] = 0x0004;
								write_stack[3] = channel_counter;
								write_stack[4] = DD_FAKE_TNUM_PROVOKE_ACK;
								write_stack[5] = 0x0001;
								write_stack[6] = (i2c_counter | DD_FRAME_EOF_BITMASK);
							//std::cout << std::endl << "Point 1 : jumping..." << std::endl;


								lcl_err = glue_utca_fec_write_msg_reworked(&lBoard, write_stack);
							//std::cout << std::endl << "Point 1 : jumping..." << std::endl;
							//goto toto;

								//First, read direct ack
								lcl_err = glue_utca_fec_read_msg_reworked(&lBoard, read_stack, &frameType);
								#ifdef DEBUG_SEE_FRAMES
									std::cout << std::endl << "Direct ACK for I2C probing command : ";
									for (int g=0; g < read_stack[2]+3; g++)
									{
										std::cout << "Ox" << std::hex << read_stack[g] << std::dec << " ";
									}
									std::cout << std::endl;
								#endif

								//Then make an extra read frame to read back answer from I2C command
								
								if (lcl_err == DD_RETURN_OK)
								{
									lcl_err = glue_utca_fec_read_msg_reworked(&lBoard, read_stack, &frameType);
									#ifdef DEBUG_SEE_FRAMES
										std::cout << std::endl << "Answer frame I2C channel read request : ";
										for (int g=0; g < read_stack[2]+3; g++)
										{
											std::cout << "Ox" << std::hex << read_stack[g] << std::dec << " ";
										}
										std::cout << std::endl;
									#endif

									//Caveat : next command is valid only for short frames !!
									glb_fec_write_trans_in_use[read_stack[DD_TRANS_NUMBER_OFFSET]] = DD_TRANS_NUMBER_IS_FREE;
									//if (lcl_err == DD_RETURN_OK)
									//{
										//fprintf(stdout,"Register read request understood, direct ack received.\n");
	
										//if (glb_read_status != DD_READ_DATA_SUCCESSFUL)
										//{
											//fprintf(stdout,"\tNo device detected at address 0x%x on channel 0x%x of CCU 0x%x        \n",i2c_counter, channel_counter, ccu_counter);
										//}
										//else
										//{
											if (read_stack[read_stack[2]+2] == 0x8004)
											{
												std::cout << "\t\t                                                                       \r";
												std::cout << "\t\tI2C device detected at address 0x" << std::hex << i2c_counter << " on channel 0x" << channel_counter << " of CCU 0x" << ccu_counter << "            " << std::dec << std::endl;
												//getchar();
											}
										//}
										/*
										lcl_err = glue_fec_read_frame(fd, read_stack);
										if (lcl_err == DD_RETURN_OK)
										{
											if (read_stack[read_stack[2]+2] == 0x8004)
											{
												fprintf(stdout,"\tI2C device detected at address 0x%x on channel 0x%x of CCU 0x%x        \n",i2c_counter, channel_counter, ccu_counter);
											}
										}
										*/
										//exit(0);
										
									//}
									//else fprintf(stdout,"Failure reading direct ack.\n");
								}
							}
							std::cout << "\t\t                                                                       \r";
							//sleep(2);
						}

					}
				}
			
			break;

















































			/*******************************************************
			Reconfigure CCU 0x18
			*******************************************************/
			case DBG_RECONFIGURE_CCU_HEX_D:
				uint32_t fifotraValToWrite;
				uint32_t ctrl0ValMemory;
				uint32_t ctrl0Val;
				uint32_t ctrl1ValToWrite, ctrl0ValToWrite;
				
/*				
				
				ctrl0ValToWrite = 0x18;
				glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL0, ctrl0ValToWrite);
				std::cout << "Step 1 done, value " << ctrl0ValToWrite << " written in ctrl0" << std::endl;
				ctrl0ValToWrite = 0x19;
				glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL0, ctrl0ValToWrite);
				std::cout << "Step 1 done, value " << ctrl0ValToWrite << " written in ctrl0" << std::endl;
*/

/*
				ctrl0ValToWrite = 0x8;
				glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL0, ctrl0ValToWrite);
				std::cout << "Step 2 done, value " << ctrl0ValToWrite << " written in ctrl0" << std::endl;

*/


				//SWITCH CCU CONFIG
				fifotraValToWrite = 0x18000400;  //DEST - SRC - LENGTH - CCU NODE CONTROLLER
				glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)TRA_FIFO, fifotraValToWrite);
				std::cout << "Step 3.1 done, value " << fifotraValToWrite << " written in fifo transmit" << std::endl;

				fifotraValToWrite = 0x33020300; //TRANS NUMBER - COMMAND (0x2 = write to CCU register C) - VALUE (B0=1=select CCU input B ; B1=1=select CCU output B )
				glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)TRA_FIFO, fifotraValToWrite);
				std::cout << "Step 3.2 done, value " << fifotraValToWrite << " written in fifo transmit" << std::endl;



				//READ - MODIFY - WRITE
				glue_utca_fec_read_mfec_value(&lBoard, (uint32_t)CONTROL0, &ctrl0Val);
				ctrl0ValMemory = ctrl0Val;
				
				ctrl0Val = (ctrl0ValMemory | DD_FEC_ENABLE_FEC | DD_FEC_SEND);
				glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL0, ctrl0Val);
				
				ctrl0Val = (ctrl0ValMemory | DD_FEC_ENABLE_FEC );
				glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL0, ctrl0Val);
				std::cout << "Step 4 done, fifo transmit sent on ring" << std::endl;




				
				ctrl0ValToWrite = 0x19; //(Sel serial IN (B4) + SEL SEZRIAL OUT (B3) + ENABLE FEC (B0)
				glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL0, ctrl0ValToWrite);
				std::cout << "Step 5 done, value " << ctrl0ValToWrite << " written in ctrl0" << std::endl;


				ctrl1ValToWrite = 0x7;// (CLEAR IRQ (B0) + CLEAR ERROR BIT (B1) + INJECT TOKEN (B2)
				glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL1, ctrl1ValToWrite);
				std::cout << "Step 6 done, value " << ctrl1ValToWrite << " written in ctrl1" << std::endl;


			break;




			case DBG_SEND_PARTIAL_RESET:
				glue_utca_fec_partial_reset_board(&lBoard);
			break;





			case DBG_SEND_ONE_FRAME:

				DD_FEC_FIFO_DATA_32 myFifoTraWord;
				DD_FEC_FIFO_DATA_32 myFifoRecWord;
				DD_FEC_FIFO_DATA_32 myFifoRecWord1;


				//READ SR0
				lcl_err = glue_utca_fec_read_mfec_value(&lBoard, (uint32_t)STATUS0, &valToRead);
				lcl_status0 = valToRead;
				fprintf(stdout,"STATUS 0 value : 0x%x\n",lcl_status0);
				dbg_decode_status0(lcl_status0);

				//Write first 32 bits word to fifo transmit
				myFifoTraWord = 0x3f000400;
				lcl_err = glue_utca_fec_write_to_fifo_transmit(&lBoard, myFifoTraWord);


				//Write second 32 bits word to fifo transmit
				//myFifoTraWord = 0x0100A000;
				myFifoTraWord = 0x01000F00;
				lcl_err = glue_utca_fec_write_to_fifo_transmit(&lBoard, myFifoTraWord);


				//Toggle send
				lcl_err = glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL0, (DD_FEC_REGISTER_DATA)3);
				//sleep(5);
				lcl_err = glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL0, (DD_FEC_REGISTER_DATA)1);

				//Read first 32 bits word from fifo receive
				//lcl_err = glue_utca_fec_read_from_fifo_receive(&lBoard, &myFifoRecWord);
				//std::cout << "Read from fifo receive : 0x" << std::hex << myFifoRecWord << std::dec << std::endl;
//sleep(5);
				//Read second 32 bits word from fifo receive
				//lcl_err = glue_utca_fec_read_from_fifo_receive(&lBoard, &myFifoRecWord1);
				//std::cout << "Read from fifo receive : 0x" << std::hex << myFifoRecWord1 << std::dec << std::endl;

    			break;










			case DBG_SEND_TWO_FRAMES_AS_BLOCK:

				//DD_FEC_FIFO_DATA_32 myFifoTraWord;
				//DD_FEC_FIFO_DATA_32 myFifoRecWord;


				//READ SR0
				lcl_err = glue_utca_fec_read_mfec_value(&lBoard, (uint32_t)STATUS0, &valToRead);
				lcl_status0 = valToRead;
				fprintf(stdout,"STATUS 0 value : 0x%x\n",lcl_status0);
				dbg_decode_status0(lcl_status0);

				//Write first 32 bits word to fifo transmit
				myFifoTraWord = 0x18000400;
				lcl_err = glue_utca_fec_write_to_fifo_transmit(&lBoard, myFifoTraWord);


				//Write second 32 bits word to fifo transmit
				myFifoTraWord = 0x0100A000;
				lcl_err = glue_utca_fec_write_to_fifo_transmit(&lBoard, myFifoTraWord);


				//Write third 32 bits word to fifo transmit
				myFifoTraWord = 0x62000400;
				lcl_err = glue_utca_fec_write_to_fifo_transmit(&lBoard, myFifoTraWord);


				//Write fourth 32 bits word to fifo transmit
				myFifoTraWord = 0x0100A000;
				lcl_err = glue_utca_fec_write_to_fifo_transmit(&lBoard, myFifoTraWord);


				//Toggle send
				lcl_err = glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL0, (DD_FEC_REGISTER_DATA)3);
				lcl_err = glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL0, (DD_FEC_REGISTER_DATA)1);

				//Read first 32 bits word from fifo receive
				lcl_err = glue_utca_fec_read_from_fifo_receive(&lBoard, &myFifoRecWord);
				std::cout << "Read from fifo receive : 0x" << std::hex << myFifoRecWord << std::dec << std::endl;

				//Read second 32 bits word from fifo receive
				lcl_err = glue_utca_fec_read_from_fifo_receive(&lBoard, &myFifoRecWord);
				std::cout << "Read from fifo receive : 0x" << std::hex << myFifoRecWord << std::dec << std::endl;


				//Read third 32 bits word from fifo receive
				lcl_err = glue_utca_fec_read_from_fifo_receive(&lBoard, &myFifoRecWord);
				std::cout << "Read from fifo receive : 0x" << std::hex << myFifoRecWord << std::dec << std::endl;

				//Read fourth 32 bits word from fifo receive
				lcl_err = glue_utca_fec_read_from_fifo_receive(&lBoard, &myFifoRecWord);
				std::cout << "Read from fifo receive : 0x" << std::hex << myFifoRecWord << std::dec << std::endl;

    			break;




			case DBG_SEND_TWO_FRAMES_SEQUENTIAL:

				//DD_FEC_FIFO_DATA_32 myFifoTraWord;
				//DD_FEC_FIFO_DATA_32 myFifoRecWord;


				//READ SR0
				lcl_err = glue_utca_fec_read_mfec_value(&lBoard, (uint32_t)STATUS0, &valToRead);
				lcl_status0 = valToRead;
				fprintf(stdout,"STATUS 0 value : 0x%x\n",lcl_status0);
				dbg_decode_status0(lcl_status0);

				//Write first 32 bits word to fifo transmit
				myFifoTraWord = 0x62000400;
				lcl_err = glue_utca_fec_write_to_fifo_transmit(&lBoard, myFifoTraWord);


				//Write second 32 bits word to fifo transmit
				myFifoTraWord = 0x0100A000;
				lcl_err = glue_utca_fec_write_to_fifo_transmit(&lBoard, myFifoTraWord);


				//READ SR0
				lcl_err = glue_utca_fec_read_mfec_value(&lBoard, (uint32_t)STATUS0, &valToRead);
				lcl_status0 = valToRead;
				fprintf(stdout,"STATUS 0 value : 0x%x\n",lcl_status0);
				dbg_decode_status0(lcl_status0);



				//Toggle send
				lcl_err = glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL0, (DD_FEC_REGISTER_DATA)3);
				lcl_err = glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL0, (DD_FEC_REGISTER_DATA)1);


				//READ SR0
				lcl_err = glue_utca_fec_read_mfec_value(&lBoard, (uint32_t)STATUS0, &valToRead);
				lcl_status0 = valToRead;
				fprintf(stdout,"STATUS 0 value : 0x%x\n",lcl_status0);
				dbg_decode_status0(lcl_status0);

				//Read first 32 bits word from fifo receive
				lcl_err = glue_utca_fec_read_from_fifo_receive(&lBoard, &myFifoRecWord);
				std::cout << "Read from fifo receive : 0x" << std::hex << myFifoRecWord << std::dec << std::endl;

				//Read second 32 bits word from fifo receive
				lcl_err = glue_utca_fec_read_from_fifo_receive(&lBoard, &myFifoRecWord);
				std::cout << "Read from fifo receive : 0x" << std::hex << myFifoRecWord << std::dec << std::endl;


				//READ SR0
				lcl_err = glue_utca_fec_read_mfec_value(&lBoard, (uint32_t)STATUS0, &valToRead);
				lcl_status0 = valToRead;
				fprintf(stdout,"STATUS 0 value : 0x%x\n",lcl_status0);
				dbg_decode_status0(lcl_status0);

				//Write first 32 bits word to fifo transmit
				myFifoTraWord = 0x18000400;
				lcl_err = glue_utca_fec_write_to_fifo_transmit(&lBoard, myFifoTraWord);


				//Write second 32 bits word to fifo transmit
				myFifoTraWord = 0x0100A000;
				lcl_err = glue_utca_fec_write_to_fifo_transmit(&lBoard, myFifoTraWord);



				//READ SR0
				lcl_err = glue_utca_fec_read_mfec_value(&lBoard, (uint32_t)STATUS0, &valToRead);
				lcl_status0 = valToRead;
				fprintf(stdout,"STATUS 0 value : 0x%x\n",lcl_status0);
				dbg_decode_status0(lcl_status0);

				//Toggle send
				lcl_err = glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL0, (DD_FEC_REGISTER_DATA)3);
				lcl_err = glue_utca_fec_write_mfec_value(&lBoard, (uint32_t)CONTROL0, (DD_FEC_REGISTER_DATA)1);


				//READ SR0
				lcl_err = glue_utca_fec_read_mfec_value(&lBoard, (uint32_t)STATUS0, &valToRead);
				lcl_status0 = valToRead;
				fprintf(stdout,"STATUS 0 value : 0x%x\n",lcl_status0);
				dbg_decode_status0(lcl_status0);

				//Read first 32 bits word from fifo receive
				lcl_err = glue_utca_fec_read_from_fifo_receive(&lBoard, &myFifoRecWord);
				std::cout << "Read from fifo receive : 0x" << std::hex << myFifoRecWord << std::dec << std::endl;

				//Read second 32 bits word from fifo receive
				lcl_err = glue_utca_fec_read_from_fifo_receive(&lBoard, &myFifoRecWord);
				std::cout << "Read from fifo receive : 0x" << std::hex << myFifoRecWord << std::dec << std::endl;

				//READ SR0
				lcl_err = glue_utca_fec_read_mfec_value(&lBoard, (uint32_t)STATUS0, &valToRead);
				lcl_status0 = valToRead;
				fprintf(stdout,"STATUS 0 value : 0x%x\n",lcl_status0);
				dbg_decode_status0(lcl_status0);



    			break;




			/*******************************************************
			Default gate
			*******************************************************/
			default:
				fprintf(stdout,"Error : invalid choice.\n");
			break;



		}







	}

	/***********************************************************************
	Fermeture du driver
	***********************************************************************/
	//if (close(fd)) fprintf(stdout,"Erreur while closing the driver.\n");
std::cout << std::endl;
std::cout << numberOfWriteAccessToFirmware << " WRITE_WORD accesses have been performed by software to firmware via IPBus" << std::endl;
std::cout << numberOfReadAccessToFirmware << " READ_WORD accesses have been performed by software to firmware via IPBus" << std::endl;

return DD_RETURN_OK;
}


