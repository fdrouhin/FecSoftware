#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
};

static const struct modversion_info ____versions[]
__attribute_used__
__attribute__((section("__versions"))) = {
	{ 0xf8e3dbd2, "struct_module" },
	{ 0xec7bc0d, "__mod_timer" },
	{ 0x4827a016, "del_timer" },
	{ 0xc8b57c27, "autoremove_wake_function" },
	{ 0xeae3dfd6, "__const_udelay" },
	{ 0x59d87ef5, "pci_bus_write_config_word" },
	{ 0xdd5a37a7, "_spin_lock_irqsave" },
	{ 0x7d11c268, "jiffies" },
	{ 0xda4008e6, "cond_resched" },
	{ 0xdd132261, "printk" },
	{ 0x8e0caa25, "pci_find_device" },
	{ 0xccd52b12, "__down_failed_interruptible" },
	{ 0xdf15005, "_spin_unlock_irqrestore" },
	{ 0x9eac042a, "__ioremap" },
	{ 0xb11e2cf, "pci_bus_read_config_word" },
	{ 0xd62c833f, "schedule_timeout" },
	{ 0x1000e51, "schedule" },
	{ 0xeb0ece80, "register_chrdev" },
	{ 0x19cacd0, "init_waitqueue_head" },
	{ 0xa5c89579, "init_timer" },
	{ 0xdb09708f, "__wake_up" },
	{ 0x65f4af5f, "prepare_to_wait" },
	{ 0xd96c8608, "interruptible_sleep_on_timeout" },
	{ 0xedc03953, "iounmap" },
	{ 0xc192d491, "unregister_chrdev" },
	{ 0x271f4d2a, "finish_wait" },
	{ 0x1042cbb5, "__up_wakeup" },
	{ 0x21e5679c, "copy_user_generic" },
	{ 0xe914e41e, "strcpy" },
};

static const char __module_depends[]
__attribute_used__
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "F737496D070063B8203DB32");
